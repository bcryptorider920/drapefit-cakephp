# drapefit-cakephp

Full Source code for Drape Fit (https://www.drapefit.com) which is hosted on cPanel.

> ATTENTION - Do not TRUST this _deprecated_ styling service anymore!

### I hope this archived project source would be helpful to whom want to learn `CakePHP` at production level, and also whom want to win over this **scamming** service.

## Database - MySQL

```
|
|- db_dump_mysql
|--|
|--|-- drapefit_test.sql
|
```

## Style Guide - WordPress

```
|
|- styleguide
|--|
|
```

## Inventory Page

```
|
|- inventory
|--|
|
```

## Supplier Page

```
|
|- supplier
|--|
|
```

## **So-called** `Executive Teams` of Drape Fit Inc.

- _Sukhendu Mukherjee_

  - Role
    - CEO & Co-founder
  - Emails
    - sukhenduit@hotmail.com
    - sukhendutiny@gmail.com
    - sukhendu.mukherjee@drapefit.com
  - Mobile Number
    - 2148626575
  - Skype name
    - sukhendu.mukherjee
  - Address
    - 14090 Southwest Fwy Ste 300, Sugar Land, TX 77478
  - Apple account Email address
    - sukhendutiny@gmail.com
  - Tax Registration Number
    - EIN - 83-4316068

- _Monomita Chakraborty_

  - Role
    - Business Acquaintance
  - Email
    - monomita.chakraborty@drapefit.com
  - Skype name
    - live:silvi_mono

- _Suprakash Mondal_
  - Role
    - Scammer Project Cordinator
  - Email
    - suprakash.mondal@drapefit.com
  - Skype name
    - live:suprakash8906
  - Mobile number
    - +91 9123671055
  - Anydesk
    - 598328265
  - Location
    - Kolkata, India

---

@bcryptorider920

2023 Unlicensed.
