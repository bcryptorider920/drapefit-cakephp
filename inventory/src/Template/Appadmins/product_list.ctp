<script>
    function getChanges(value, category = '') {
        if (value) {
            var url = '<?php echo HTTP_ROOT ?>';
            window.location.href = url + "appadmins/product_list/" + value + "/" + category;
    }
    }
</script>

<div class="content-wrapper">
    <section class="content-header">
        <h1> <?php
            $color_arr = $this->Custom->inColor();
            echo!empty($profile) ? $profile : 'Men';
            ?> Products </h1>        
    </section>

    <section class="content" style="min-height: auto !important;">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">

                        <div class="nav-tabs-custom">
                            <label>Profile</label>
                            <select name="" class="form-control" onchange=" return getChanges(this.value)">
                                <option <?php if (@$profile == 'Men') { ?> selected="" <?php } ?> value="Men">Men</option>
                                <option <?php if (@$profile == 'Women') { ?> selected="" <?php } ?> value="Women">Women</option>
                                <option <?php if (@$profile == 'BoyKids') { ?> selected="" <?php } ?> value="BoyKids">Boy Kids</option>
                                <option <?php if (@$profile == 'GirlKids') { ?> selected="" <?php } ?> value="GirlKids">Girl Kids</option>
                            </select>
                            <label>Category</label>
                            <select name="" class="form-control" onchange=" return getChanges('<?php echo!empty($profile) ? $profile : 'Men'; ?>', this.value)">
                                <option <?php if ($category == '') { ?> selected="" <?php } ?> value="">---</option>
                                <?php foreach ($productType as $ptyp_li) { ?>
                                    <option <?php if ($category == $ptyp_li->id) { ?> selected="" <?php } ?> value="<?php echo $ptyp_li->id; ?>"><?php echo $ptyp_li->product_type . '-' . $ptyp_li->name; ?></option>
                                <?php } ?> 
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <?php if (@$profile == 'Men' || @$profile == '') { ?>
        <section class="content">


            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                    <div class="col-sm-6"> </div>
                                    <div  class="col-sm-6">
                                        <?= $this->Form->create('', array('id' => 'search_frm', 'type' => 'GET', "autocomplete" => "off")); ?>
                                        <div class="form-group">
                                            <select class="form-control" name="search_for" required>
                                                <option value="" selected disabled>Select field</option>
                                                <option value="product_name_one" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_one")) ? "selected" : ""; ?> >Product name one</option> 
                                                <option value="product_name_two" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_two")) ? "selected" : ""; ?> >Product name two</option> 
                                                <option value="style_no" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "style_no")) ? "selected" : ""; ?> >Style no</option> 
<!--                                                <option value="prod_id" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "prod_id")) ? "selected" : ""; ?> >Prod id</option> -->
                                               
                                            </select>
                                            <input style="height: 35px; width: 250px;font-weight: bold;" type="text"  name="search_data" autocomplete="off" placeholder="search" value="<?= (!empty($_GET['search_data'])) ? $_GET['search_data'] : ""; ?>" required >
                                            <button type="submit" class="btn btn-sm btn-info">Search</button>
                                            <a href="<?= HTTP_ROOT; ?>appadmins/product_list/Men" class="btn btn-sm btn-primary">See All</a>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                            </div>
                            <table id="exampleXX" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <?php if (@$utype == '1') { ?>
                                            <th>Brand Name</th>
                                        <?php } ?>
                                        <th>Product Name 1</th>
                                        <th>Product Name 2</th>
                                        <th>Product Image</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Purchse Price</th>
                                        <th>Sale Price</th>
                                        <th>Quantity</th>
                                        <th>Style.no</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($menproductdetails as $pdetails): ?>

                                        <tr id="<?php echo $pdetails->id; ?>" class="message_box">
                                            <?php if (@$utype == '1') { ?>
                                                <td><?php echo $this->Custom->brandNamex(@$pdetails->brand_id); ?> </td>
                                            <?php } ?>
        <!-- <td><?php echo $pdetails->user_id ?></td> -->
                                            <td><?php echo $pdetails->product_name_one; ?></td>
                                            <td><?php echo $pdetails->product_name_two; ?></td>
                                            <td>
                                                <?php
                                                //echo  $pdetails->prodcut_id;

                                                /* if (empty($pdetails->prodcut_id)) { */
                                                ?>
                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                <?php /* } else { ?>
                                                  <img src="<?php echo HTTP_ROOT_BASE . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                  <?php } */ ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->picked_size)) {
                                                    $li_size = explode('-', $pdetails->picked_size);
                                                    foreach ($li_size as $sz_l) {
                                                        if (($pdetails->$sz_l == 0) || ($pdetails->$sz_l == 00)) {
                                                            echo $pdetails->$sz_l;
                                                        } else {
                                                            echo!empty($pdetails->$sz_l) ? $pdetails->$sz_l . '&nbsp;&nbsp;' : '';
                                                        }
                                                    }
                                                }
                                                if (!empty($pdetails->primary_size) && ($pdetails->primary_size == 'free_size')) {
                                                    echo "Free Size";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->color)) {
                                                    echo $color_arr[$pdetails->color];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $pdetails->purchase_price; ?></td>
                                            <td><?php echo $pdetails->sale_price; ?></td>
                                            <td><?php echo $this->Custom->productQuantity($pdetails->prod_id); ?></td>
                                            <td><?php echo (empty($pdetails->style_number)) ? $pdetails->dtls : $pdetails->style_number; ?></td>
                                            <td style="text-align: center;">

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-list')), ['action' => 'listProduct', $pdetails->prod_id], ['escape' => false, "data-placement" => "top", "data-hint" => "View all products", 'class' => 'btn btn-primary hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>

                                                <a href="<?= HTTP_ROOT . "appadmins/all_barcode_prints/" . $pdetails->prod_id; ?>" data-placement="top" target="_blank" data-hint="Print barcode" class="btn btn-info  hint--top  hint" style="padding: 0 7px!important;"><i class="fa fa-print "></i></a>



                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), ['action' => '#'], ['escape' => false, "data-placement" => "top", "data-hint" => "Set New Password", 'data-toggle' => 'modal', 'data-target' => '#myModalproduct-' . $pdetails->id, "title" => "View Product Details", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), ['action' => 'add_product', 'Men', $pdetails->id], ['escape' => false, "data-placement" => "top", "data-hint" => "Edit", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-trash')), ['action' => 'productDelete', $pdetails->id, 'InProducts', $profile], ['escape' => false, "data-placement" => "top", "data-hint" => "Product Delete", 'class' => 'btn btn-danger hint--top  hint', 'style' => 'padding: 0 7px!important;', 'confirm' => __('Are you sure you want to delete Admin ?')]); ?>

                                                <?php if ($pdetails->available_status == 1) { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/deactive/' . $pdetails->id . '/InProducts'; ?>"> <?= $this->Form->button('<i class="fa fa-check"></i>', ["data-placement" => "top", "data-hint" => "Active", 'class' => "btn btn-success hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?> </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/active/' . $pdetails->id . '/InProducts'; ?>"><?= $this->Form->button('<i class="fa fa-times"></i>', ["data-placement" => "top", "data-hint" => "Inactive", 'class' => "btn btn-danger hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <div class="modal fade" id="myModalproduct-<?php echo $pdetails->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php echo $pdetails->id; ?>" aria-hidden="true">
                                        <div class="modal-dialog" style='width: 100%;'>
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Product  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your height?  : <?php echo $pdetails->tall_feet . '.' . $pdetails->tall_inch; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Best Fit for Weight ? : <?php echo $pdetails->best_fit_for_weight; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Best Size Fit ?  : 
                                                                <?php
                                                                echo $pdetails->best_size_fit;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Waist size? : <?php echo $pdetails->waist_size . ' :-'; ?>
                                                                <?php
                                                                echo $pdetails->waist_size_run;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Shirt size?  : 
                                                                <?php
                                                                echo $pdetails->shirt_size;
                                                                ?>
                                                                :-
                                                                <?php
                                                                echo $pdetails->shirt_size_run;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Bottom size? : 
                                                                <?php
                                                                echo $pdetails->men_bottom;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Inseam size? : 
                                                                <?php
                                                                echo $pdetails->inseam_size;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Shoe size?  : 
                                                                <?php
                                                                echo $pdetails->shoe_size;
                                                                ?>
                                                                :-
                                                                <?php
                                                                echo $pdetails->shoe_size_run;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Better body Shape ? : 
                                                                <?php
                                                                if ($pdetails->better_body_shape == '2') {
                                                                    echo 'Rectangle';
                                                                }
                                                                if ($pdetails->better_body_shape == '3') {
                                                                    echo 'Triangle';
                                                                }
                                                                if ($pdetails->better_body_shape == '1') {
                                                                    echo 'Trapezoid';
                                                                }
                                                                if ($pdetails->better_body_shape == '4') {
                                                                    echo 'Oval';
                                                                }
                                                                if ($pdetails->better_body_shape == '5') {
                                                                    echo 'Inverted Triangle';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                For Better Skin tone ?  : 
                                                                <?php
                                                                if ($pdetails->skin_tone == '1') {
                                                                    echo 'IndianRed';
                                                                }
                                                                if ($pdetails->skin_tone == '2') {
                                                                    echo 'DarkSalmon';
                                                                }
                                                                if ($pdetails->skin_tone == '3') {
                                                                    echo 'LightSalmon';
                                                                }
                                                                if ($pdetails->skin_tone == '4') {
                                                                    echo 'DarkRed';
                                                                }
                                                                if ($pdetails->skin_tone == '5') {
                                                                    echo 'Extra Wide';
                                                                }
                                                                if ($pdetails->skin_tone == '6') {
                                                                    echo 'Other';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Typically wear to work ? : 
                                                                <?php
                                                                if ($pdetails->work_type == '1') {
                                                                    echo 'Casual';
                                                                }
                                                                if ($pdetails->work_type == '2') {
                                                                    echo 'Business Casual';
                                                                }
                                                                if ($pdetails->work_type == '3') {
                                                                    echo 'Formal';
                                                                }
                                                                /* if ($pdetails->work_type == '4') {
                                                                  echo 'Oval';
                                                                  }
                                                                  if ($pdetails->work_type == '5') {
                                                                  echo 'Inverted Triangle';
                                                                  } */
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Casual shirts to fit ?  : 
                                                                <?php
                                                                if ($pdetails->casual_shirts_type == '4') {
                                                                    echo 'Slim';
                                                                }
                                                                if ($pdetails->casual_shirts_type == '5') {
                                                                    echo 'Regular';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Bottom up shirt to fit ? : 
                                                                <?php
                                                                if ($pdetails->casual_shirts_type == '6') {
                                                                    echo 'Slim';
                                                                }
                                                                if ($pdetails->casual_shirts_type == '7') {
                                                                    echo 'Regular';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Jeans to Fit ?  : 
                                                                <?php
                                                                if ($pdetails->jeans_Fit == '3') {
                                                                    echo 'Straight';
                                                                }
                                                                if ($pdetails->jeans_Fit == '2') {
                                                                    echo 'Slim';
                                                                }
                                                                if ($pdetails->jeans_Fit == '1') {
                                                                    echo 'Skinny';
                                                                }
                                                                if ($pdetails->jeans_Fit == '4') {
                                                                    echo 'Relaxed';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Shorts long ? : 
                                                                <?php
                                                                if ($pdetails->shorts_long == '4') {
                                                                    echo 'Upper Thigh';
                                                                }
                                                                if ($pdetails->shorts_long == '3') {
                                                                    echo 'Lower Thigh';
                                                                }
                                                                if ($pdetails->shorts_long == '2') {
                                                                    echo 'Above Knee';
                                                                }
                                                                if ($pdetails->shorts_long == '1') {
                                                                    echo 'At The Knee';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Color ?  : 
                                                                <?php
                                                                if ($pdetails->color == '1') {
                                                                    echo 'Black';
                                                                }
                                                                if ($pdetails->color == '2') {
                                                                    echo 'Grey';
                                                                }
                                                                if ($pdetails->color == '3') {
                                                                    echo 'White';
                                                                }
                                                                if ($pdetails->color == '4') {
                                                                    echo 'Cream';
                                                                }
                                                                if ($pdetails->color == '5') {
                                                                    echo 'Brown';
                                                                }
                                                                if ($pdetails->color == '6') {
                                                                    echo 'Purple';
                                                                }
                                                                if ($pdetails->color == '7') {
                                                                    echo 'Green';
                                                                }
                                                                if ($pdetails->color == '8') {
                                                                    echo 'Blue';
                                                                }
                                                                if ($pdetails->color == '9') {
                                                                    echo 'Orange';
                                                                }
                                                                if ($pdetails->color == '10') {
                                                                    echo 'Yellow';
                                                                }
                                                                if ($pdetails->color == '11') {
                                                                    echo 'Red';
                                                                }
                                                                if ($pdetails->color == '12') {
                                                                    echo 'Pink';
                                                                }
                                                                ?>
                                                            </div>
                                                            <!--                                                            <div class="col-md-6">
                                                                                                                            Outfit matches ? : 
                                                            <?php
                                                            if ($pdetails->outfit_matches == '1') {
                                                                echo 'Upper Thigh';
                                                            }
                                                            if ($pdetails->outfit_matches == '2') {
                                                                echo 'Lower Thigh';
                                                            }
                                                            if ($pdetails->outfit_matches == '3') {
                                                                echo 'Above Knee';
                                                            }
                                                            if ($pdetails->outfit_matches == '4') {
                                                                echo 'At The Knee';
                                                            }
                                                            ?>
                                                                                                                        </div>-->
                                                            <div class="col-md-6">
                                                                Bottom fit ? : 
                                                                <?php
                                                                if ($pdetails->men_bottom_prefer == '1') {
                                                                    echo 'Tighter Fitting';
                                                                }
                                                                if ($pdetails->men_bottom_prefer == '2') {
                                                                    echo 'More Relaxed';
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Purchase price  : 
                                                                <?php
                                                                echo $pdetails->purchase_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Sale price  : 
                                                                <?php
                                                                echo $pdetails->sale_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Quantity : 
                                                                <?php
                                                                echo $this->Custom->productQuantity($pdetails->prod_id);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Available status  : 
                                                                <?php
                                                                if ($pdetails->available_status == '1') {
                                                                    echo 'Available';
                                                                }
                                                                if ($pdetails->available_status == '2') {
                                                                    echo 'Not Available';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Profuct Image : 
                                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Note : 
                                                                <?php
                                                                echo $pdetails->note;
                                                                ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php if (@$profile == 'Women') { ?>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                    <div class="col-sm-6"> </div>
                                    <div  class="col-sm-6">
                                        <?= $this->Form->create('', array('id' => 'search_frm', 'type' => 'GET', "autocomplete" => "off")); ?>
                                        <div class="form-group">
                                            <select class="form-control" name="search_for" required>
                                                <option value="" selected disabled>Select field</option>
                                                <option value="product_name_one" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_one")) ? "selected" : ""; ?> >Product name one</option> 
                                                <option value="product_name_two" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_two")) ? "selected" : ""; ?> >Product name two</option> 
                                                <option value="style_no" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "style_no")) ? "selected" : ""; ?> >Style no</option> 
<!--                                                <option value="prod_id" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "prod_id")) ? "selected" : ""; ?> >Prod id</option> -->
                                               
                                            </select>
                                            <input style="height: 35px; width: 250px;font-weight: bold;" type="text"  name="search_data" autocomplete="off" placeholder="search" value="<?= (!empty($_GET['search_data'])) ? $_GET['search_data'] : ""; ?>" required >
                                            <button type="submit" class="btn btn-sm btn-info">Search</button>
                                            <a href="<?= HTTP_ROOT; ?>appadmins/product_list/Women" class="btn btn-sm btn-primary">See All</a>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                            </div>
                            <table id="exampleXX" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <?php if ($utype == 1) { ?>
                                            <th>Brand Name</th>
                                        <?php } ?>
                                        <th>Product Name 1</th>
                                        <th>Product Name 2</th>
                                        <th>Product Image</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Purchse Price</th>
                                        <th>Sale Price</th>
                                        <th>Quantity</th>
                                        <th>Style.no</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($womenproductdetails as $pdetails): ?>
                                        <tr id="<?php echo $pdetails->id; ?>" class="message_box">
                                            <?php if ($utype == 1) { ?>
                                                <td><?php echo $this->Custom->brandNamex(@$pdetails->brand_id); ?> </td>
                                            <?php } ?>
                                            <td><?php echo $pdetails->product_name_one; ?></td>
                                            <td><?php echo $pdetails->product_name_two; ?></td>

                                            <td>
                                                <?php
                                                //echo  $pdetails->prodcut_id;

                                                /* if (empty($pdetails->prodcut_id)) { */
                                                ?>
                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                <?php /* } else { ?>
                                                  <img src="<?php echo HTTP_ROOT_BASE . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                  <?php } */ ?>
                                            </td>

                                            <td>
                                                <?php
                                                if (!empty($pdetails->picked_size)) {
                                                    $li_size = explode('-', $pdetails->picked_size);
                                                    foreach ($li_size as $sz_l) {
                                                        if (($pdetails->$sz_l == 0) || ($pdetails->$sz_l == 00)) {
                                                            echo $pdetails->$sz_l;
                                                        } else {
                                                            echo!empty($pdetails->$sz_l) ? $pdetails->$sz_l . '&nbsp;&nbsp;' : '';
                                                        }
                                                    }
                                                }
                                                if (!empty($pdetails->primary_size) && ($pdetails->primary_size == 'free_size')) {
                                                    echo "Free Size";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->color)) {
                                                    echo $color_arr[$pdetails->color];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $pdetails->purchase_price; ?></td>
                                            <td><?php echo $pdetails->sale_price; ?></td>
                                            <td><?php echo $this->Custom->productQuantity($pdetails->prod_id); ?></td>
                                            <td><?php echo (empty($pdetails->style_number)) ? $pdetails->dtls : $pdetails->style_number; ?></td>
                                            <td style="text-align: center;">

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-list')), ['action' => 'listProduct', $pdetails->prod_id], ['escape' => false, "data-placement" => "top", "data-hint" => "View all products", 'class' => 'btn btn-primary hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>

                                                <a href="<?= HTTP_ROOT . "appadmins/all_barcode_prints/" . $pdetails->prod_id; ?>" data-placement="top" target="_blank" data-hint="Print barcode" class="btn btn-info  hint--top  hint" style="padding: 0 7px!important;"><i class="fa fa-print "></i></a>

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), ['action' => '#'], ['escape' => false, "data-placement" => "top", "data-hint" => "Set New Password", 'data-toggle' => 'modal', 'data-target' => '#myModalproductw-' . $pdetails->id, "title" => "View Product Details", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), ['action' => 'add_product', 'Women', $pdetails->id], ['escape' => false, "data-placement" => "top", "data-hint" => "Edit", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-trash')), ['action' => 'delete', $pdetails->id, 'InProducts', $profile], ['escape' => false, "data-placement" => "top", "data-hint" => "Delete", 'class' => 'btn btn-danger hint--top  hint', 'style' => 'padding: 0 7px!important;', 'confirm' => __('Are you sure you want to delete Admin ?')]); ?>
                                                <?php if ($pdetails->available_status == 1) { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/deactive/' . $pdetails->id . '/InProducts'; ?>"> <?= $this->Form->button('<i class="fa fa-check"></i>', ["data-placement" => "top", "data-hint" => "Active", 'class' => "btn btn-success hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?> </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/active/' . $pdetails->id . '/InProducts'; ?>"><?= $this->Form->button('<i class="fa fa-times"></i>', ["data-placement" => "top", "data-hint" => "Inactive", 'class' => "btn btn-danger hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <div class="modal fade" id="myModalproductw-<?php echo $pdetails->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php echo $pdetails->id; ?>" aria-hidden="true">
                                        <div class="modal-dialog" style='width: 100%;'>
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Product  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your height?  : <?php echo $pdetails->tall_feet . '.' . $pdetails->tall_inch; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Best Fit for Weight ? : <?php echo $pdetails->best_fit_for_weight; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Color ?  : <?php
                                                                if (!empty($pdetails->color)) {
                                                                    echo $color_arr[$pdetails->color];
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What's your body type?: 
                                                                <?php
                                                                if ($pdetails->better_body_shape == '1') {
                                                                    echo 'Inverted Triangle';
                                                                }
                                                                if ($pdetails->better_body_shape == '3') {
                                                                    echo 'Triangle';
                                                                }
                                                                if ($pdetails->better_body_shape == '1') {
                                                                    echo 'Rectangle';
                                                                }
                                                                if ($pdetails->better_body_shape == '4') {
                                                                    echo 'Hourglass';
                                                                }
                                                                if ($pdetails->better_body_shape == '5') {
                                                                    echo 'Apple';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php
                                                                echo $pdetails->pants;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                BRA SIZE: 
                                                                <?php
                                                                echo $pdetails->bra;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php
                                                                echo $pdetails->bra_recomend;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                SKIRT SIZE: 
                                                                <?php
                                                                echo $pdetails->skirt;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                JEANS SIZE: 
                                                                <?php
                                                                echo $pdetails->jeans;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                ACTIVE WEAR SIZE: 
                                                                <?php
                                                                echo $pdetails->active_wr;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                JACKET SIZE: 
                                                                <?php
                                                                echo $pdetails->wo_jackect_size;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                BOTTOM SIZE: 
                                                                <?php
                                                                echo $pdetails->wo_bottom;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                DRESS: 
                                                                <?php
                                                                echo $pdetails->dress;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                JEANS SIZE: 
                                                                <?php
                                                                echo $pdetails->dress_recomended;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                SHIRT & BLOUSE: 
                                                                <?php
                                                                echo $pdetails->shirt_blouse;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php
                                                                echo $pdetails->shirt_blouse_recomend;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                TOP SIZE: 
                                                                <?php
                                                                echo $pdetails->pantsr1;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <?php
                                                                echo $pdetails->pantsr2;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your shoe size?: 
                                                                <?php
                                                                echo $pdetails->shoe_size;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Which style of shoes do you prefer?(Select all that apply)
                                                                <?php
                                                                echo $pdetails->shoe_size_run;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Which heel height do you prefer?:
                                                                <?php
                                                                echo $pdetails->womenHeelHightPrefer;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Tell us your skin Tone?
                                                                <?php
                                                                echo $pdetails->shoe_size_run;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Shoulders?:
                                                                <?php
                                                                echo $pdetails->proportion_shoulders;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Legs?
                                                                <?php
                                                                echo $pdetails->proportion_legs;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Arms?:
                                                                <?php
                                                                echo $pdetails->proportion_arms;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Hips?
                                                                <?php
                                                                echo $pdetails->proportion_arms;
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Purchase price  : 
                                                                <?php
                                                                echo $pdetails->purchase_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Sale price  : 
                                                                <?php
                                                                echo $pdetails->sale_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Quantity : 
                                                                <?php
                                                                echo $this->Custom->productQuantity($pdetails->prod_id);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Available status  : 
                                                                <?php
                                                                if ($pdetails->available_status == '1') {
                                                                    echo 'Available';
                                                                }
                                                                if ($pdetails->available_status == '2') {
                                                                    echo 'Not Available';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Profuct Image : 
                                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Note : 
                                                                <?php
                                                                echo $pdetails->note;
                                                                ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php if (@$profile == 'BoyKids') { ?>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="row">
                                    <div class="col-sm-6"> </div>
                                    <div  class="col-sm-6">
                                        <?= $this->Form->create('', array('id' => 'search_frm', 'type' => 'GET', "autocomplete" => "off")); ?>
                                        <div class="form-group">
                                            <select class="form-control" name="search_for" required>
                                                <option value="" selected disabled>Select field</option>
                                                <option value="product_name_one" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_one")) ? "selected" : ""; ?> >Product name one</option> 
                                                <option value="product_name_two" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_two")) ? "selected" : ""; ?> >Product name two</option> 
                                                <option value="style_no" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "style_no")) ? "selected" : ""; ?> >Style no</option> 
                                                <!--<option value="prod_id" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "prod_id")) ? "selected" : ""; ?> >Prod id</option>--> 
                                               
                                            </select>
                                            <input style="height: 35px; width: 250px;font-weight: bold;" type="text"  name="search_data" autocomplete="off" placeholder="search" value="<?= (!empty($_GET['search_data'])) ? $_GET['search_data'] : ""; ?>" required >
                                            <button type="submit" class="btn btn-sm btn-info">Search</button>
                                            <a href="<?= HTTP_ROOT; ?>appadmins/product_list/BoyKids" class="btn btn-sm btn-primary">See All</a>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                            </div>
                            <table id="exampleXX" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <?php if ($utype == 1) { ?>
                                            <th>Brand Name</th>
                                        <?php } ?>
                                        <th>Product Name 1</th>
                                        <th>Product Name 2</th>
                                        <th>Product Image</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Purchase Price</th>
                                        <th>Sale Price</th>
                                        <th>Quantity</th>
                                        <th>Style.no</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($boyskidsproductdetails as $pdetails): ?>
                                        <tr id="<?php echo $pdetails->id; ?>" class="message_box">
                                            <?php if ($utype == 1) { ?>
                                                <td><?php echo $this->Custom->brandNamex(@$pdetails->brand_id); ?> </td>
                                            <?php } ?>
                                            <td><?php echo $pdetails->product_name_one; ?></td>
                                            <td><?php echo $pdetails->product_name_two; ?></td>
                                            <td>
                                                <?php
                                                //echo  $pdetails->prodcut_id;

                                                /* if (empty($pdetails->prodcut_id)) { */
                                                ?>
                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                <?php /* } else { ?>
                                                  <img src="<?php echo HTTP_ROOT_BASE . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                  <?php } */ ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->picked_size)) {
                                                    $li_size = explode('-', $pdetails->picked_size);
                                                    foreach ($li_size as $sz_l) {
                                                        if (($pdetails->$sz_l == 0) || ($pdetails->$sz_l == 00)) {
                                                            echo $pdetails->$sz_l;
                                                        } else {
                                                            echo!empty($pdetails->$sz_l) ? $pdetails->$sz_l . '&nbsp;&nbsp;' : '';
                                                        }
                                                    }
                                                }

                                                if (!empty($pdetails->primary_size) && ($pdetails->primary_size == 'free_size')) {
                                                    echo "Free Size";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->color)) {
                                                    echo $color_arr[$pdetails->color];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $pdetails->purchase_price; ?></td>
                                            <td><?php echo $pdetails->sale_price; ?></td>
                                            <td><?php echo $this->Custom->productQuantity($pdetails->prod_id); ?></td>
                                            <td><?php echo (empty($pdetails->style_number)) ? $pdetails->dtls : $pdetails->style_number; ?></td>
                                            <td style="text-align: center;">

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-list')), ['action' => 'listProduct', $pdetails->prod_id], ['escape' => false, "data-placement" => "top", "data-hint" => "View all products", 'class' => 'btn btn-primary hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>

                                                <a href="<?= HTTP_ROOT . "appadmins/all_barcode_prints/" . $pdetails->prod_id; ?>" data-placement="top" target="_blank" data-hint="Print barcode" class="btn btn-info  hint--top  hint" style="padding: 0 7px!important;"><i class="fa fa-print "></i></a>

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), ['action' => '#'], ['escape' => false, "data-placement" => "top", "data-hint" => "Set New Password", 'data-toggle' => 'modal', 'data-target' => '#myModalproductbk-' . $pdetails->id, "title" => "View Product Details", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), ['action' => 'add_product', 'BoyKids', $pdetails->id], ['escape' => false, "data-placement" => "top", "data-hint" => "Edit", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-trash')), ['action' => 'delete', $pdetails->id, 'InProducts', $profile], ['escape' => false, "data-placement" => "top", "data-hint" => "Delete", 'class' => 'btn btn-danger hint--top  hint', 'style' => 'padding: 0 7px!important;', 'confirm' => __('Are you sure you want to delete Admin ?')]); ?>
                                                <?php if ($pdetails->available_status == 1) { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/deactive/' . $pdetails->id . '/InProducts'; ?>"> <?= $this->Form->button('<i class="fa fa-check"></i>', ["data-placement" => "top", "data-hint" => "Active", 'class' => "btn btn-success hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?> </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/active/' . $pdetails->id . '/InProducts'; ?>"><?= $this->Form->button('<i class="fa fa-times"></i>', ["data-placement" => "top", "data-hint" => "Inactive", 'class' => "btn btn-danger hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <div class="modal fade" id="myModalproductbk-<?php echo $pdetails->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php echo $pdetails->id; ?>" aria-hidden="true">
                                        <div class="modal-dialog" style='width: 100%;'>
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Product  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your height?  : <?php echo $pdetails->tall_feet . '.' . $pdetails->tall_inch; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Best Fit for Weight ? : <?php echo $pdetails->best_fit_for_weight; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                TOPS SIZE?  : <?php echo $pdetails->top_size; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                BOTTOMS SIZE ? : <?php echo $pdetails->bottom_size; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                SHOE SIZE?  : <?php echo $pdetails->shoe_size; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Purchase price  : 
                                                                <?php
                                                                echo $pdetails->purchase_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Sale price  : 
                                                                <?php
                                                                echo $pdetails->sale_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Quantity : 
                                                                <?php
                                                                echo $this->Custom->productQuantity($pdetails->prod_id);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Available status  : 
                                                                <?php
                                                                if ($pdetails->available_status == '1') {
                                                                    echo 'Available';
                                                                }
                                                                if ($pdetails->available_status == '2') {
                                                                    echo 'Not Available';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Product Image : 
                                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Note : 
                                                                <?php
                                                                echo $pdetails->note;
                                                                ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <?php if (@$profile == 'GirlKids') { ?>
        <section class="content">
            <div class="row">
                <div class="col-xs-12">   
                            
                    <div class="box">
                        
                        <div class="box-body">  
                            <div class="row">
                                    <div class="col-sm-6"> </div>
                                    <div  class="col-sm-6">
                                        <?= $this->Form->create('', array('id' => 'search_frm', 'type' => 'GET', "autocomplete" => "off")); ?>
                                        <div class="form-group">
                                            <select class="form-control" name="search_for" required>
                                                <option value="" selected disabled>Select field</option>
                                                <option value="product_name_one" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_one")) ? "selected" : ""; ?> >Product name one</option> 
                                                <option value="product_name_two" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "product_name_two")) ? "selected" : ""; ?> >Product name two</option> 
                                                <option value="style_no" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "style_no")) ? "selected" : ""; ?> >Style no</option> 
                                                <!--<option value="prod_id" <?= (!empty($_GET['search_for']) && ($_GET['search_for'] == "prod_id")) ? "selected" : ""; ?> >Prod id</option>--> 
                                               
                                            </select>
                                            <input style="height: 35px; width: 250px;font-weight: bold;" type="text"  name="search_data" autocomplete="off" placeholder="search" value="<?= (!empty($_GET['search_data'])) ? $_GET['search_data'] : ""; ?>" required >
                                            <button type="submit" class="btn btn-sm btn-info">Search</button>
                                            <a href="<?= HTTP_ROOT; ?>appadmins/product_list/GirlKids" class="btn btn-sm btn-primary">See All</a>
                                        </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                            </div>
                            <table id="exampleXX" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <?php if ($utype == 1) { ?>
                                            <th>Brand Name</th>
                                        <?php } ?>
                                        <th>Product Name 1</th>
                                        <th>Product Name 2</th>
                                        <th>Product Image</th>
                                        <th>Size</th>
                                        <th>Color</th>
                                        <th>Purchase Price</th>
                                        <th>Sale Price</th>
                                        <th>Quantity</th>
                                        <th>Style.no</th>
                                        <th style="text-align: center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($girlkidsproductdetails as $pdetails): ?>
                                        <tr id="<?php echo $pdetails->id; ?>" class="message_box">
                                            <?php if ($utype == 1) { ?>
                                                <td><?php echo $this->Custom->brandNamex(@$pdetails->brand_id); ?> </td>
                                            <?php } ?>
                                            <td><?php echo $pdetails->product_name_one; ?></td>
                                            <td><?php echo $pdetails->product_name_two; ?></td>
                                            <td>
                                                <?php
                                                //echo  $pdetails->prodcut_id;

                                                /* if (empty($pdetails->prodcut_id)) { */
                                                ?>
                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                <?php /* } else { ?>
                                                  <img src="<?php echo HTTP_ROOT_BASE . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                  <?php } */ ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->picked_size)) {
                                                    $li_size = explode('-', $pdetails->picked_size);
                                                    foreach ($li_size as $sz_l) {
                                                        if (($pdetails->$sz_l == 0) || ($pdetails->$sz_l == 00)) {
                                                            echo $pdetails->$sz_l;
                                                        } else {
                                                            echo!empty($pdetails->$sz_l) ? $pdetails->$sz_l . '&nbsp;&nbsp;' : '';
                                                        }
                                                    }
                                                }
                                                if (!empty($pdetails->primary_size) && ($pdetails->primary_size == 'free_size')) {
                                                    echo "Free Size";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($pdetails->color)) {
                                                    echo $color_arr[$pdetails->color];
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo $pdetails->purchase_price; ?></td>
                                            <td><?php echo $pdetails->sale_price; ?></td>
                                            <td><?php echo $this->Custom->productQuantity($pdetails->prod_id); ?></td>
                                            <td><?php echo (empty($pdetails->style_number)) ? $pdetails->dtls : $pdetails->style_number; ?></td>
                                            <td style="text-align: center;">

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-list')), ['action' => 'listProduct', $pdetails->prod_id], ['escape' => false, "data-placement" => "top", "data-hint" => "View all products", 'class' => 'btn btn-primary hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>

                                                <a href="<?= HTTP_ROOT . "appadmins/all_barcode_prints/" . $pdetails->prod_id; ?>" data-placement="top" target="_blank" data-hint="Print barcode" class="btn btn-info  hint--top  hint" style="padding: 0 7px!important;"><i class="fa fa-print "></i></a>

                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-eye')), ['action' => '#'], ['escape' => false, "data-placement" => "top", "data-hint" => "Set New Password", 'data-toggle' => 'modal', 'data-target' => '#myModalproductgk-' . $pdetails->id, "title" => "View Product Details", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>   
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-edit')), ['action' => 'add_product', 'GirlKids', $pdetails->id], ['escape' => false, "data-placement" => "top", "data-hint" => "Edit", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-trash')), ['action' => 'productDelete', $pdetails->id, 'InProducts', $profile], ['escape' => false, "data-placement" => "top", "data-hint" => "Profile Delete", 'class' => 'btn btn-danger hint--top  hint', 'style' => 'padding: 0 7px!important;', 'confirm' => __('Are you sure you want to delete Admin ?')]); ?>

                                                <?php if ($pdetails->available_status == 1) { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/deactive/' . $pdetails->id . '/InProducts'; ?>"> <?= $this->Form->button('<i class="fa fa-check"></i>', ["data-placement" => "top", "data-hint" => "Active", 'class' => "btn btn-success hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?> </a>
                                                <?php } else { ?>
                                                    <a href="<?php echo HTTP_ROOT . 'appadmins/active/' . $pdetails->id . '/InProducts'; ?>"><?= $this->Form->button('<i class="fa fa-times"></i>', ["data-placement" => "top", "data-hint" => "Inactive", 'class' => "btn btn-danger hint--top  hint", 'style' => 'padding: 0 7px!important;']) ?></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <div class="modal fade" id="myModalproductgk-<?php echo $pdetails->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-<?php echo $pdetails->id; ?>" aria-hidden="true">
                                        <div class="modal-dialog" style='width: 100%;'>
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Product  Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your height?  : <?php echo $pdetails->tall_feet . '.' . $pdetails->tall_inch; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Best Fit for Weight ? : <?php echo $pdetails->best_fit_for_weight; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                What is your height?  : <?php echo $pdetails->tall_feet . '.' . $pdetails->tall_inch; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Best Fit for Weight ? : <?php echo $pdetails->best_fit_for_weight; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                TOPS SIZE?  : <?php echo $pdetails->top_size; ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                BOTTOMS SIZE ? : <?php echo $pdetails->bottom_size; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                SHOE SIZE?  : <?php echo $pdetails->shoe_size; ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Purchase price  : 
                                                                <?php
                                                                echo $pdetails->purchase_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Sale price  : 
                                                                <?php
                                                                echo $pdetails->sale_price;
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Quantity : 
                                                                <?php
                                                                echo $this->Custom->productQuantity($pdetails->prod_id);
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Available status  : 
                                                                <?php
                                                                if ($pdetails->available_status == '1') {
                                                                    echo 'Available';
                                                                }
                                                                if ($pdetails->available_status == '2') {
                                                                    echo 'Not Available';
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="col-md-6">
                                                                Profuct Image : 
                                                                <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" style="width: 50px;"/>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                Note : 
                                                                <?php
                                                                echo $pdetails->note;
                                                                ?>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>   
   <?php
                        echo $this->Paginator->counter('Page {{page}} of {{pages}}, Showing {{current}} records out of {{count}} total');
//                        echo $this->Paginator->counter(
//    'Page {{page}} of {{pages}}, showing {{current}} records out of
//     {{count}} total, starting on record {{start}}, ending on {{end}}'
//);
                        echo "<div class='center' style='float:left;width:100%;'><ul class='pagination' style='margin:20px auto;display: inline-block;width: 100%;float: left;'>";
                        echo $this->Paginator->prev('< ' . __('prev'), array('tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'disabled'), null, array('class' => 'prev disabled'));
                        echo $this->Paginator->numbers(array('first' => 3, 'last' => 3, 'separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
                        echo $this->Paginator->next(__('next') . ' >', array('tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'disabled'), null, array('class' => 'next disabled'));
                        echo "</div></ul>";
                        ?>
</div>

<script>
    function myFunction() {
        window.print();
    }
</script>

<script>
    $(function () {
        $(".example").DataTable();
    });
</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var sizeKB = input.files[0].size / 1000;
            //alert(sizeKB);
            if (parseFloat(sizeKB) <= 21) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var img = $('<img />', {
                        src: e.target.result,
                        alt: 'MyAlt',
                        width: '200'
                    });
                    $('#imagePreview').html(img);

                }
                reader.readAsDataURL(input.files[0]);
            } else {
                //alert("Image size");
                $("#image").val('');
                $('#imagePreview').html('');
            }
        }
    }

    $("#image").change(function () {
        readURL(this);

    });

    function getSubCatg(id) {
        $('select[name=rack]').html('<option value="" selected disabled>Fetching sub-categories</option>');

        $.ajax({
            url: '<?php echo HTTP_ROOT ?>appadmins/getSubCatgList',
            type: 'POST',
            data: {id: id},
            success: function (res) {
                $('select[name=rack]').html(res);
            },
            error: function (err) {
                $('select[name=rack]').html('<option value="" selected disabled>No data found</option>');
            },
            dataType: "html"

        });

    }
</script>

<style>
    #example1_paginate{
        display:none;
    }
    .main-footer{
        float: left;
        width: 100%;
    }
    .ellipsis {
      float: left;
      background: #fff;
      padding: 7px;
    }
</style>