<?php

namespace App\Controller;

ob_start();

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\Core\App;
use Cake\Core\Configure;

require_once(ROOT . '/vendor' . DS . 'PaymentTransactions' . DS . 'authorize-credit-card.php');

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class CronjobsController extends AppController {

    public function initialize() {

        parent::initialize();
        $this->loadComponent('Custom');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Mpdf');
        $this->loadModel('Users');
        $this->loadModel('UserDetails');
        $this->loadModel('Payments');
        $this->loadModel('Settings');
        $this->loadmodel('ShippingAddress');
        $this->loadModel('Pages');
        $this->loadModel('Settings');
        $this->loadModel('PaymentGetways');
        $this->loadModel('PersonalizedFix');
        $this->loadModel('rather downplay');
        $this->loadModel('ReferFriends');
        $this->loadModel('ShippingAddress');
        $this->loadModel('SizeChart');
        $this->loadModel('style_quizs');
        $this->loadModel('YourProportions');
        $this->loadModel('PaymentCardDetails');
        $this->loadModel('FitCut');
        $this->loadModel('FlauntArms');
        $this->loadModel('WemenJeansLength');
        $this->loadModel('WomenJeansRise');
        $this->loadModel('WomenJeansStyle');
        $this->loadModel('WomenPrintsAvoid');
        $this->loadModel('WomenTypicalPurchaseCloth');
        $this->loadModel('WomenIncorporateWardrobe');
        $this->loadModel('WomenFabricsAvoid');
        $this->loadModel('WomenColorAvoid');
        $this->loadModel('WomenPrice');
        $this->loadModel('WomenStyle');
        $this->loadModel('WomenInformation');
        $this->loadModel('WomenRatherDownplay');
        $this->loadModel('MensBrands');
        $this->loadModel('MenFit');
        $this->loadModel('MenStats');
        $this->loadModel('MenStyle');
        $this->loadModel('MenStyleSphereSelections');
        $this->loadModel('TypicallyWearMen');
        $this->loadModel('LetsPlanYourFirstFix');
        $this->loadModel('KidsDetails');
        $this->loadModel('KidsPersonality');
        $this->loadModel('KidsPrimary');
        $this->loadModel('KidsSizeFit');
        $this->loadModel('KidClothingType');
        $this->loadModel('FabricsOrEmbellishments');
        $this->loadModel('KidStyles');
        $this->loadModel('KidsPricingShoping');
        $this->loadModel('KidPurchaseClothing');
        $this->loadModel('DeliverDate');
        $this->loadModel('Products');
        $this->loadModel('CustomerProductReview');
        $this->loadModel('UserUsesPromocode');
        $this->loadModel('Promocode');
        $this->loadModel('ChatMessages');
        $this->loadModel('EmailPreferences');
        $this->loadModel('Wallets');
        $this->loadModel('HelpDesks');
        $this->loadModel('Giftcard');
        $this->loadModel('UserMailTemplateGiftcode');
        $this->loadModel('UserUsesGiftcode');
        $this->loadModel('Notifications');
        $this->loadModel('MenAccessories');
        $this->loadModel('CustomDesine');
        $this->loadModel('WomenShoePrefer');
        $this->loadModel('WomenHeelHightPrefer');
        $this->loadModel('WemenStyleSphereSelections');
        $this->loadModel('UserAppliedCodeOrderReview');
        $this->loadModel('Giftcard');
        $this->viewBuilder()->layout('ajax');
    }

    public function beforeFilter(Event $event) {
        $this->Auth->allow(['checkBydebasish', 'giftMailDelivary', 'authorizeCreditCard', 'autoMentions', 'boxUpdate', 'autocheckoutmail', 'notCompleteProfile', 'notPaidOnce', 'addressNotComplete', 'autocheckoutmail', 'onlyMailCheck', 'kidAutocheckoutmail', 'kidProfileNotComplete']);
    }

    public function checkBydebasish() {
        $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
        $this->Custom->sendEmail('devadash143@gmail.com', $fromMail, 'RERER', 'AUTO FUNCITON');

        exit;
    }

    public function giftMailDelivary() {
        $getDetails = $this->Giftcard->find('all')->where(['type' => 1, 'delivery_date' => date('Y-m-d'), 'mail_status' => 'NULL', 'is_active' => 0]);
        foreach ($getDetails as $details) {
            $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'GIFTCARD_EMAIL'])->first();
            $fromName = $details->from_name;
            $fromMail = $details->from_email;
            $to = $details->to_email;
            $name = $details->to_name;
            $code = $details->giftcode;
            $price = number_format($details->price, 2);
            $expiry_date = $details->expire_date;
            $msg = $details->msg;
            $subject = $emailMessage->display;
            $sitename = SITE_NAME;
            $message = $this->Custom->giftCardEmail($emailMessage->value, $to, $name, $fromName, $fromMail, $price, $code, $expiry_date, $msg, $sitename);
            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
            $this->Custom->sendEmail($to, $from, $subject, $message);
            $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
            $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'GIFTCARD_EMAIL_SUPPORT'])->first();
            $messageSupport = $this->Custom->giftCardEmailSupport($emailMessage->value, $to, $name, $fromName, $fromMail, $code, $price, $expiry_date, $msg, $sitename);
            $subjectSupport = $emailMessageSupport->display;
            $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);
            $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupport, $messageSupport);
            $this->Giftcard->updateAll(array('mail_status' => '1'), array('id' => $details->id));
            $this->checkBydebasish();
        }

        exit;
    }

    public function notCompleteProfile() {
        $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
        $all_clients = $this->Users->find('all')->contain(['UserDetails'])->where(['Users.type' => 2, 'UserDetails.is_progressbar < ' => 100]);
        foreach ($all_clients as $client) {
            if (($client->usrdet == null) || ($client->usrdet->is_progressbar < 100) /* && !empty($client->address) */) {
                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();
                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                $to = $client->email;
                $from = $fromMail->value;
                $subject = $emailMessage->display;
                $sitename = SITE_NAME;
                $mail_msg = "Complete your profile";
                $message = $this->Custom->userProfileComplete($emailMessage->value, $client->name, $mail_msg, $sitename);
                $this->Custom->sendEmail($to, $from, $subject, $message);
                $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();
                $subjectSupport = $emailMessageSupport->display;
                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
                $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);
                echo "Mail send successfully <br>";
            }
        }

        exit;
    }

    public function notPaidOnce() {
        $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
        $this->Users->hasOne('paym', ['className' => 'Payments', 'foreignKey' => 'user_id']);
        $this->Users->hasMany('card_detl', ['className' => 'PaymentCardDetails', 'foreignKey' => 'user_id']);
        $all_clients = $this->Users->find('all')->contain(['usrdet', 'paym', 'card_detl'])->where(['Users.type' => 2]);
        foreach ($all_clients as $client) {

            // if (($client->usrdet != null) &&  /*(($client->usrdet->is_progressbar == 100) && ($client->address != null)) &&*/ ($client->paym == null)) {
            // pj($client);
            // $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();
            // $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_NOT_PAID_ONCE'])->first();
            // $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
            // $to = $client->email;
            // $from = $fromMail->value;
            // $subject = $emailMessage->display;
            // $sitename = SITE_NAME;
            // $mail_msg = "Please complete your payment";
            // $message = $this->Custom->notPaidOnce($emailMessage->value, $client->name, $mail_msg, $sitename);
            // $this->Custom->sendEmail($to, $from, $subject, $message);
            // $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
            // $subjectSupport = $emailMessageSupport->display;
            // $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
            // $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);
            // //onlytesting cuse
            // $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupport, $messageSupport);
            // echo "Mail send successfully <br>";
            // }

            if ($client->card_detl == null) {

                // pj($client);

                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();

                $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();

                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                $to = $client->email;

                $from = $fromMail->value;

                $subject = $emailMessage->display;





                $sitename = SITE_NAME;

                $mail_msg = "Please complete your payment";

                $message = $this->Custom->notPaidOnce($emailMessage->value, $client->name, $mail_msg, $sitename);



                $this->Custom->sendEmail($to, $from, $subject, $message);



                $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $client->name, $mail_msg, $sitename);

                $subjectSupport = $emailMessageSupport->display;

                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                //onlytesting cuse
                //$this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupport, $messageSupport);



                echo "card not added Mail send successfully <br>";
            }
        }

        exit;
    }

    public function addressNotComplete() {

        $this->Users->hasMany('addressess', ['className' => 'ShippingAddress', 'foreignKey' => 'user_id']);

        $all_clients = $this->Users->find('all')->contain(['addressess'])->where(['Users.type' => 2]);

        //pj($all_clients);exit;

        foreach ($all_clients as $client) {

            if ($client->addressess == null) {

                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();

                $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();

                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                $to = $client->email;

                $from = $fromMail->value;

                $subject = $emailMessage->display;



                $sitename = SITE_NAME;

                $mail_msg = "Please complete your shipping address";

                $message = $this->Custom->addressNotComplate($emailMessage->value, $client->name, $mail_msg, $sitename);



                $this->Custom->sendEmail($to, $from, $subject, $message);

                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;



                $messageSupport = $this->Custom->addressNotComplate($emailMessageSupport->value, $client->name, $mail_msg, $sitename);

                $subjectSupport = $emailMessageSupport->display;

                $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                //onlytesting cuse
                // $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupport, $messageSupport);



                echo "Mail send successfully <br>";
            }
        }

        exit;
    }

//they are not update any cart details

    public function autocheckoutmail() {

        // echo date('Y-m-d H:i:s', strtotime('-8 days'));
        //  echo date('Y-m-d H:i:s');

        $notpaid_users = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.created_dt <=' => date('Y-m-d H:i:s', strtotime('-8 days'))]);

        // echo date('Y-m-d');
        // echo date('Y-m-d H:i:s', strtotime('-8 days'));
        //pj($notpaid_users);exit;

        $prData = '';
        $kid_id = 0;
        foreach ($notpaid_users as $notcheckout) {
            $paymentId = $notcheckout->id;
            $user_id = $notcheckout->user_id;
            $kid_id = $notcheckout->kid_id;
            $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
            $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
            if ($kid_id != 0) {
                $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
                $prData = $this->Products->find('all')->where(['kid_id' => $kid_id, 'keep_status' => 0, 'kid_id !=' => 0, 'is_complete' => 0, 'checkedout' => 'N', 'payment_id' => $paymentId]);
                $kid = $kid_id;
                $profileType = 3;
            } else {
                $kid = 0;
                $prData = $this->Products->find('all')->where(['user_id' => $user_id, 'keep_status' => 0, 'is_complete' => 0, 'kid_id =' => '0', 'checkedout' => 'N', 'payment_id' => $paymentId]);
                $profileType = $getUsersDetails->gender;
            }

            //pj($prData);exit;

            $style_pick_total = 0;

            $discount_amt = $this->Custom->styleFitFee();

            foreach ($prData as $pd) {

                $style_pick_total += (double) $pd->sell_price;
            }
            $percentage = 25;
            $discount = ($percentage / 100) * $style_pick_total;
            $subTotal = $style_pick_total - $discount;
            $stylist_picks_subtotal = number_format($style_pick_total, 2);
            $keep_all_discount = number_format((!empty($discount) ? $discount : 0), 2);
            $style_fit_fee = number_format($discount_amt, 2);
            $amount = $subTotal - 20;
            //echo $amount; exit;
            $payment_data = $this->PaymentCardDetails->find('all')->where(['PaymentCardDetails.user_id' => $user_id, 'PaymentCardDetails.use_card' => 1])->first();



            if ($payment_data == null) {
                $getErrorMeg = 'Please Add a valid card.';
                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                $to = $getEmail->email;

                $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                $from = $fromMail->value;
                $sitename = SITE_NAME;
                if ($kid_id != 0) {
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
                    $subject = $emailMessage->display;
                    $kidsdetails = $this->KidsDetails->find()->where(['id' => $kid_id])->first();
                    if ($kidsdetails->kids_first_name == '') {
                        $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                    } else {
                        $kidsname = $kidsdetails->kids_first_name;
                    }

                    $email_message = $this->Custom->paymentFaildKid($emailMessage->value, $name, $kidsname, $getErrorMeg, $sitename);
                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                    $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
                } else {
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                    $subject = $emailMessage->display;
                    $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                    $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
                }
                exit;
            }
            $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.default_set' => 1, 'ShippingAddress.user_id' => $user_id])->first();
            $paymentG = $this->PaymentGetways->newEntity();
            $table1['user_id'] = $user_id;
            $table1['kid_id'] = $kid;
            $table1['emp_id'] = 0;
            $table1['status'] = 0;
            $table1['price'] = $subTotal;
            $table1['profile_type'] = $profileType;
            $table1['payment_type'] = 2;
            $table1['created_dt'] = date('Y-m-d H:i:s');
            $paymentG = $this->PaymentGetways->patchEntity($paymentG, $table1);
            $lastPymentg = $this->PaymentGetways->save($paymentG);
            //pj($lastPymentg); exit;

            $arr_user_info = [
                'card_number' => $payment_data->card_number,
                'exp_date' => $payment_data->card_expire,
                'card_code' => "" . $payment_data->cvv,
                'product' => 'Check out order',
                'first_name' => $billingAddress->full_name,
                'last_name' => $billingAddress->full_name,
                'address' => $billingAddress->address,
                'city' => $billingAddress->city,
                'state' => $billingAddress->state,
                'zip' => $billingAddress->zipcode,
                'country' => 'USA',
                'email' => $getUsersDetails->email,
                'amount' => $amount,
                'invice' => @$lastPymentg->id,
                'refId' => 32,
                'companyName' => 'Drapefit',
            ];

            $message = $this->authorizeCreditCard($arr_user_info);



            //pj($message); 
            //exit;

            if (@$message['status'] == '1') {

                $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId'], 'auto_checkout' => 1], ['id' => $lastPymentg->id]);

                $this->PaymentGetways->updateAll(['work_status' => 2], ['id' => $paymentId]);
                $productData = '';
                $i = 1;
                foreach ($prData as $dataMail) {
                    $priceMail = $dataMail->sell_price;
                    $productData .= "<tr>
                        <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               # " . $i . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                              <img src='" . HTTP_ROOT . PRODUCT_IMAGES . $dataMail->product_image . "' width='85'/>
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->product_name_one . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->product_name_two . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               keep
                            </td> 
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->size . "
                            </td>                                            
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               $" . number_format($priceMail, 2) . "
                            </td>
                        </tr>";
                    $this->Products->updateAll(['checkedout' => 'Y', 'keep_status' => '3', 'is_complete' => '1'], ['id' => $dataMail->id]);

                    $i++;
                }

                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                $to = $getEmail->email;
                $from = $fromMail->value;
                $sitename = SITE_NAME;
                if ($kid_id != 0) {
                    $kname = $this->KidsDetails->find()->where(['id' => $kid_id])->first()->kids_first_name;
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ORDER_PAYMENT_KID'])->first();
                    $subject = $emailMessage->display;
                    $email_message = $this->Custom->order($emailMessage->value, $kname, $sitename, $productData, $stylist_picks_subtotal, $amount, $style_fit_fee, $keep_all_discount);

                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $KidsDetails = $this->KidsDetails->newEntity();
                    $k['id'] = $kid_id;
                    $k['is_redirect'] = 5;
                    $KidsDetails = $this->KidsDetails->patchEntity($KidsDetails, $k);
                    $KidsDetails = $this->KidsDetails->save($KidsDetails);

                    $this->KidsDetails->updateAll(['is_redirect' => 5], ['id' => $kid_id]);
                    $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => $kid_id]);
                    // $this->Custom->sendEmail('devadash143@gmail.com', $from, 'dfdfdfd' . $kid_id, $email_message.'-'.$KidsDetails);
                } else {
                    $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ORDER_PAYMENT'])->first();
                    $subject = $emailMessage->display;
                    $email_message = $this->Custom->order($emailMessage->value, $name, $sitename, $productData, $stylist_picks_subtotal, $amount, $style_fit_fee, $keep_all_discount);
                    //$this->Custom->sendEmail('devadash143@gmail.com', $from, 'dfdfdfd' . $kid_id, $email_message);
                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $this->Users->updateAll(['is_redirect' => 5], ['id' => $user_id]);
                    $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => 0]);
                }

                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
            } else {
                $getErrorMeg = $this->Custom->getAllMeg($message['ErrorCode']);
                $this->Flash->error(__($message['ErrorCode']));
                $getErrorMeg .= '<br>Please Add a valid card';
                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                $to = $getEmail->email;
                $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                $from = $fromMail->value;
                $sitename = SITE_NAME;
                if ($kid_id != 0) {
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
                    $subject = $emailMessage->display;
                    $kidsdetails = $this->KidsDetails->find()->where(['id' => $kid_id])->first();
                    if ($kidsdetails->kids_first_name == '') {
                        $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                    } else {
                        $kidsname = $kidsdetails->kids_first_name;
                    }
                    $email_message = $this->Custom->paymentFaildKid($emailMessage->value, $name, $kidsname, $getErrorMeg, $sitename);
                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                    $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
                } else {
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                    $subject = $emailMessage->display;
                    $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
                    $this->Custom->sendEmail($to, $from, $subject, $email_message);
                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                    $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
                }
                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
            }
        }



        // for between 8days who have not checkout for box

        $checkout_between = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'mail_status' => 1, 'PaymentGetways.created_dt >=' => date('Y-m-d H:i:s', strtotime('-200 days'))]);

        foreach ($checkout_between as $chkBET) {
            $paymentId = $chkBET->id;
            $user_id = $chkBET->user_id;
            $kid_id = $chkBET->kid_id;
            $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
            $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
            $getErrorMeg = 'Please checkout your box';
            $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
            $to = $getEmail->email;
            if ($kid_id != 0) {
                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'CHECKOUTBETWEENDAYS_KIDS'])->first();
                $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
                $name = $getKidsDetails->kids_first_name;
                $kid = $kid_id;
                $profileType = 3;
            } else {
                $kid = 0;
                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'CHECKOUTBETWEENDAYS'])->first();
                $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                $profileType = $getUsersDetails->gender;
            }
            $from = $fromMail->value;
            $sitename = SITE_NAME;
            $email = $to;
            $subject = $emailMessage->display;
            $email_message = $this->Custom->checkoutBetweenDays($emailMessage->value, $name, $email, $sitename);
            $this->Custom->sendEmail($to, $from, $subject, $email_message);
            $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
            $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
        }
        exit;
    }

    public function autoMentions() {
        $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['try_new_items_with_scheduled_fixes' => 1]);
        foreach ($getingData as $data) {
            $planId = $data->id;
            if ($data->how_often_would_you_lik_fixes == 1) {
                date('Y-m-d :H:i:s', strtotime($data->applay_dt . ' + 1 months'));
                $oneMonth = date('Y-m-d :H:i:s', strtotime($data->applay_dt . '+ 1 months'));
                //echo '<br> 1m' . $data->id . ' ' . $data->user_id . ' ' . $oneMonth;
                $dateInTwoWeeks = date('Y-m-d :H:i:s');
                if ($twoWeeks <= $dateInTwoWeeks) {
                    $this->boxUpdate($planId);
                }
            }

            if ($data->how_often_would_you_lik_fixes == 2) {
                $tw0Month = strtotime($data->applay_dt . '+ 2 months');
                $tw0Month = date('Y-m-d :H:i:s', strtotime($data->applay_dt . ' + 2 months'));
                //echo '<br>2m ' . $data->id . ' ' . $data->user_id . ' ' . $twoeMonth;
                $dateInTwoWeeks = date('Y-m-d :H:i:s');
                if ($oneMonth <= $dateInTwoWeeks) {

                    $this->boxUpdate($planId);
                }
            }

            if ($data->how_often_would_you_lik_fixes == 3) {
                $threeMonth = date('Y-m-d :H:i:s', strtotime($data->applay_dt . ' + 3 months'));
                // echo '<br>3m ' . $data->id . ' ' . $data->user_id . ' ' . $threeMonth;
                $dateInTwoWeeks = date('Y-m-d :H:i:s');
                if ($twoMonth <= $dateInTwoWeeks) {
                    $notCompleteCurrentFit = $this->PaymentGetways->find('all')->where(['PaymentGetways.user_id' => $data->user_id, 'PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.created_dt <=' => date('Y-m-d H:i:s', strtotime('-8 days'))])->first();
                    if ($notCompleteCurrentFit->id == '') {

                        $this->boxUpdate($planId);
                    }
                }
            }
        }

        exit;
    }

    public function boxUpdate($planId) {
        if ($planId) {
            //echo $planId;
            $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['id' => $planId])->first();
            $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('addressess', ['className' => 'ShippingAddress', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('card_detl', ['className' => 'PaymentCardDetails', 'foreignKey' => 'user_id']);
            $getUsreDetails = $this->Users->find('all')->contain(['addressess', 'card_detl', 'usrdet'])->where(['Users.id' => $getingData->user_id])->first();
            
                if (($getingData->user_id != '') && ($getingData->kid_id != '')) {
                    $profile_type = '3';
                } else {
                    $getGender = $this->UserDetails->find('all')->where(['user_id' => $getingData->user_id])->first();
                    $getEmail = $this->Users->find('all')->where(['id' => $getingData->user_id])->first()->email;
                    $getUsreDetails = $this->Users->find('all')->where(['id' => $getingData->user_id])->first();
                    if ($getGender->gender == 1) {
                        $profile_type = '1';
                    } else {
                        $profile_type = '2';
                    }
                }

                $cardDetailsCount = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->count();

                if ($cardDetailsCount <= 0) {

                    /*

                      if(($getUsreDetails->usrdet == null) || ($getUsreDetails->usrdet->is_progressbar < 100) ){

                      //profile incomplete mail



                      $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();



                      $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                      $to = $getUsreDetails->email;

                      $from = $fromMail->value;

                      $subject = $emailMessage->display;



                      $sitename = SITE_NAME;

                      $mail_msg = "Complete your profile";

                      $message = $this->Custom->userProfileComplete($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);

                      $this->Custom->sendEmail($to,$from, $subject, $message);

                      //mail($to, $from, $subject, $message);



                      $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_PROFILE_NOT_COMPLETE'])->first();

                      $subjectSupport = $emailMessageSupport->display;

                      $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                      $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                      $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                      }

                      if($getUsreDetails->addressess == null){

                      //address incomplete mail



                      $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();

                      $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_ADDRESS_NOT_COMPLATE'])->first();

                      $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                      $to = $getUsreDetails->email;

                      $from = $fromMail->value;

                      $subject = $emailMessage->display;



                      $sitename = SITE_NAME;

                      $mail_msg = "Please complete your shipping address";

                      $message = $this->Custom->addressNotComplate($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                      $this->Custom->sendEmail($to, $from, $subject, $message);

                      $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;



                      $messageSupport = $this->Custom->addressNotComplate($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                      $subjectSupport = $emailMessageSupport->display;

                      $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                      }

                      if($getUsreDetails->card_detl == null){

                      //card not added mail



                      $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();

                      $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_NOT_PAID_ONCE'])->first();

                      $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                      $to = $getUsreDetails->email;

                      $from = $fromMail->value;

                      $subject = $emailMessage->display;



                      $sitename = SITE_NAME;

                      $mail_msg = "Please complete your payment";

                      $message = $this->Custom->notPaidOnce($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                      $this->Custom->sendEmail($to, $from, $subject, $message);



                      $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                      $subjectSupport = $emailMessageSupport->display;

                      $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                      $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                      }

                     */



                    // $getErrorMeg1 ='Please Add a valid card.';
                    //     $fromMail1 = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                    //     $to1 = $getUsreDetails->email;
                    //     $name1 = $getUsreDetails->usrdet->first_name . ' ' . $getUsreDetails->usrdet->last_name;exit;
                    //     $from1 = $fromMail1->value;
                    //     $sitename1 = SITE_NAME;
                    //     $emailMessage1 = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                    //     $subject1 = $emailMessage1->display;
                    //     $email_message1 = $this->Custom->paymentFaild($emailMessage1->value, $name1, $getErrorMeg1, $sitename1);
                    //     $this->Custom->sendEmail($to1, $from1, $subject1, $email_message1);
                    //     $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                    //     $this->Custom->sendEmail($toSupport, $from1, $subject1, $email_message1);

                    if (!empty($getingData->kid_id)) {
                        $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                        $name = $getUsreDetails->name;
                        $email = $getUsreDetails->email;
                        $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                        $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                        $subject = $emailTemplatesSubcritionsPmt->display;
                        $sitename = "Drafit.com";

                        if ($kidsdetails->kids_first_name == '') {
                            $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                        } else {
                            $kidsname = $kidsdetails->kids_first_name;
                        }
                        $email_message_no_add = $this->Custom->paymentFailedSucritpionsKids($emailTemplatesSubcritionsPmt->value, $name, $kidsname, $sitename);
                        $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                        $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                        $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();

                        $subjectSupportPayment = $emailtemplatesAdminPayment->display;

                        $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);

                        $subjectSupport = $emailMessageSupport->display;

                        $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);
                    } else {

                        $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                        $name = $getUsreDetails->name;
                        $email = $getUsreDetails->email;
                        $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                        $subject = $emailTemplatesSubcritionsPmt->display;
                        $sitename = "Drafit.com";
                        $email_message_no_add = $this->Custom->paymentFailedSucritpions($emailTemplatesSubcritionsPmt->value, $name, $sitename);
                        $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                        $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                        $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                        $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                       
                        $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);

                        $subjectSupport = $emailMessageSupport->display;
                        $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);


                        //onlytesting cuse
                        //  $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupportPayment, $messageSupportPayment);
                    }
                } else {

                    $cardDetails = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->first();
                    $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $profile_type, 'PaymentGetways.kid_id' => $getingData->kid_id, 'user_id' => $getingData->user_id])->count();
                    $newEntity = $this->PaymentGetways->newEntity();
                    $data['user_id'] = $getingData->user_id;
                    $data['kid_id'] = $getingData->kid_id;
                    $data['price'] = 20;
                    $data['profile_type'] = $profile_type;
                    $data['payment_type'] = 1;
                    $data['status'] = 0;
                    $data['payment_card_details_id'] = $cardDetails->id;
                    $data['created_dt'] = date('Y-m-d H:i:s');
                    $data['count'] = $paymentCount + 1;
                    $newEntity = $this->PaymentGetways->patchEntity($newEntity, $data);
                    $PaymentIdlast = $this->PaymentGetways->save($newEntity);
                    $paymentId = $PaymentIdlast->id;
                    $userData = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $getingData->user_id])->first();
                    $exitdata = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id])->first();
                    $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id, 'is_billing' => 1])->first();
                    $full_address = $billingAddress->address . ((!empty($billingAddress->address_line_2)) ? '<br>' . $billingAddress->address_line_2 : '') . '<br>' . $billingAddress->city . ', ' . $billingAddress->state . '<br>' . $billingAddress->country . ' ' . $billingAddress->zipcode;
                    $usr_name = $billingAddress->full_name;
                    $arr_user_info = [
                        'card_number' => $cardDetails->card_number,
                        'exp_date' => $cardDetails->card_expire,
                        'card_code' => $cardDetails->cvv,
                        'product' => 'Test Plugin',
                        'first_name' => $billingAddress->full_name,
                        'last_name' => $billingAddress->full_name,
                        'address' => $billingAddress->address,
                        'city' => $billingAddress->city,
                        'state' => $billingAddress->state,
                        'zip' => $billingAddress->zipcode,
                        'country' => $billingAddress->country,
                        'email' => $getUsreDetails->email,
                        'amount' => 20,
                        'invice' => $paymentId,
                        'refId' => $paymentId,
                        'companyName' => 'Drapefit',
                    ];
                    $message = $this->authorizeCreditCard($arr_user_info);
                    if (@$message['error'] == 'error') {

                        /*

                          if(($getUsreDetails->usrdet == null) || ($getUsreDetails->usrdet->is_progressbar < 100) ){

                          //profile incomplete mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();



                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Complete your profile";

                          $message = $this->Custom->userProfileComplete($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $this->Custom->sendEmail($to,$from, $subject, $message);

                          //mail($to, $from, $subject, $message);



                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_PROFILE_NOT_COMPLETE'])->first();

                          $subjectSupport = $emailMessageSupport->display;

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                          $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                          if($getUsreDetails->addressess == null){

                          //address incomplete mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();

                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_ADDRESS_NOT_COMPLATE'])->first();

                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Please complete your shipping address";

                          $message = $this->Custom->addressNotComplate($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                          $this->Custom->sendEmail($to, $from, $subject, $message);

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;



                          $messageSupport = $this->Custom->addressNotComplate($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $subjectSupport = $emailMessageSupport->display;

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                          if($getUsreDetails->card_detl == null){

                          //card not added mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();

                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_NOT_PAID_ONCE'])->first();

                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Please complete your payment";

                          $message = $this->Custom->notPaidOnce($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                          $this->Custom->sendEmail($to, $from, $subject, $message);



                          $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $subjectSupport = $emailMessageSupport->display;

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                         */





                        //     $getErrorMeg1 ='Please Add a valid card.';
                        // $fromMail1 = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                        // $to1 = $getUsreDetails->email;
                        // $name1 = $getUsreDetails->usrdet->first_name . ' ' . $getUsreDetails->usrdet->last_name;
                        // $from1 = $fromMail1->value;
                        // $sitename1 = SITE_NAME;
                        // $emailMessage1 = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                        // $subject1 = $emailMessage1->display;
                        // $email_message1 = $this->Custom->paymentFaild($emailMessage1->value, $name1, $getErrorMeg1, $sitename1);
                        // $this->Custom->sendEmail($to1, $from1, $subject1, $email_message1);
                        // $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                        // $this->Custom->sendEmail($toSupport, $from1, $subject1, $email_message1);



                        if (!empty($getingData->kid_id)) {
                            $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                            if ($kidsdetails->kids_first_name == '') {
                                $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                            } else {
                                $kidsname = $kidsdetails->kids_first_name;
                            }
                            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                            $name = $getUsreDetails->name;
                            $email = $getUsreDetails->email;
                            $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subject = $emailTemplatesSubcritionsPmt->display;
                            $sitename = "Drafit.com";
                            $email_message_no_add = $this->Custom->paymentFailedSucritpionsKids($emailTemplatesSubcritionsPmt->value, $name, $kidsname, $sitename);
                            $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                            $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                            $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                            $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                            $subjectSupport = $emailMessageSupport->display;
                            $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);
                        } else {
                            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                            $name = $getUsreDetails->name;
                            $email = $getUsreDetails->email;
                            $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                            $subject = $emailTemplatesSubcritionsPmt->display;
                            $sitename = "Drafit.com";
                            $email_message_no_add = $this->Custom->paymentFailedSucritpions($emailTemplatesSubcritionsPmt->value, $name, $sitename);
                            $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                            $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                            $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                            $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                            $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                           // echo $toSupport;
                            $subjectSupport = $emailMessageSupport->display;

                            $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);
                            //onlytesting cuse
                            // $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupportPayment, $messageSupportPayment);
                        }
                    } else if (@$message['status'] == '1') {
                        $updateId = $paymentId;
                        $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId']], ['id' => $updateId]);
                        $this->LetsPlanYourFirstFix->updateAll(['applay_dt' => date('Y-m-d H:i:s')], ['id' => $planId]);
                        $paymentDetails = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId])->first();
                        $checkUser = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId, 'PaymentGetways.payment_type' => 1])->first();
                        if ($checkUser->payment_type == 1) {
                            if ($getingData->kid_id != '') {
                                @$kidId = $getingData->kid_id;
                                $this->KidsDetails->updateAll(['is_redirect' => 2], ['id' => @$checkUser->kid_id]);
                                $kid_id = $getingData->kid_id;
                            } else {
                                $kid_id = 0;
                                $this->Users->updateAll(['is_redirect' => 2], ['id' => @$checkUser->user_id]);
                            }
                        }


                        if ($paymentDetails->profile_type == 1) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 2) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 3) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE_KID'])->first();
                        }

                        $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.user_id' => $getingData->user_id, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $paymentDetails->profile_type, 'PaymentGetways.payment_type' => 1])->count();
                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'SUCCESS_PAYMENT'])->first();
                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                        $stylefee = $this->Settings->find('all')->where(['Settings.name' => 'style_fee'])->first();
                        //$to = 'bibhu268.phpdeveloper@gmail.com';
                        $feeprice = $stylefee->value;
                        $name = $getUsreDetails->name;
                        $from = $fromMail->value;
                        $subject = $emailMessage->display;
                        $sitename = SITE_NAME;
                        $usermessage = $message['Success'];
                        $sumitted_date = date_format($checkUser->created_dt, 'm/d/Y');
                        $paid_amount = "$ " . number_format($checkUser->price, 2);
                        $last_4_digit = substr($cardDetails->card_number, -4);
                        //echo $getUsreDetails->email;exit;
                        $email_message = $this->Custom->paymentEmail($emailMessage->value, $name, $usermessage, $sitename, $message['TransId'], $paid_amount, $sumitted_date, $cardDetails->card_type, $last_4_digit, $usr_name, $full_address, $feeprice);
                        $getUsreDetails->email;
                        $this->Custom->sendEmail($getUsreDetails->email, $from, $subject, $email_message);
                        $subjectProfile = $emailMessageProfile->display;
                        $email_message_profile = $this->Custom->paymentEmailCount($emailMessageProfile->value, $name, $usermessage, $sitename, $paymentCount);
                        $this->Custom->sendEmail($getUsreDetails->email, $from, $subjectProfile, $email_message_profile);
                        // $this->Custom->sendEmail('devadash143@gmail', $from, $subjectProfile, $email_message_profile);
                         $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                        $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);

                        //$this->Custom->sendEmail('devadash143@gmail', $from, $subjectProfile, $email_message_profile);
                    } else {

                        /*

                          if(($getUsreDetails->usrdet == null) || ($getUsreDetails->usrdet->is_progressbar < 100) ){

                          //profile incomplete mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();



                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Complete your profile";

                          $message = $this->Custom->userProfileComplete($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $this->Custom->sendEmail($to,$from, $subject, $message);

                          //mail($to, $from, $subject, $message);



                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_PROFILE_NOT_COMPLETE'])->first();

                          $subjectSupport = $emailMessageSupport->display;

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                          $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                          if($getUsreDetails->addressess == null){

                          //address incomplete mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();

                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_ADDRESS_NOT_COMPLATE'])->first();

                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Please complete your shipping address";

                          $message = $this->Custom->addressNotComplate($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                          $this->Custom->sendEmail($to, $from, $subject, $message);

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;



                          $messageSupport = $this->Custom->addressNotComplate($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $subjectSupport = $emailMessageSupport->display;

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                          if($getUsreDetails->card_detl == null){

                          //card not added mail



                          $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();

                          $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'SUPPORT_NOT_PAID_ONCE'])->first();

                          $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                          $to = $getUsreDetails->email;

                          $from = $fromMail->value;

                          $subject = $emailMessage->display;



                          $sitename = SITE_NAME;

                          $mail_msg = "Please complete your payment";

                          $message = $this->Custom->notPaidOnce($emailMessage->value, $getUsreDetails->name, $mail_msg, $sitename);



                          $this->Custom->sendEmail($to, $from, $subject, $message);



                          $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $getUsreDetails->name, $mail_msg, $sitename);

                          $subjectSupport = $emailMessageSupport->display;

                          $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                          $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                          }

                         */

                        //     $getErrorMeg1 ='Please Add a valid card.';
                        // $fromMail1 = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                        // $to1 = $getUsreDetails->email;
                        // $name1 = $getUsreDetails->usrdet->first_name . ' ' . $getUsreDetails->usrdet->last_name;
                        // $from1 = $fromMail1->value;
                        // $sitename1 = SITE_NAME;
                        // $emailMessage1 = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                        // $subject1 = $emailMessage1->display;
                        // $email_message1 = $this->Custom->paymentFaild($emailMessage1->value, $name1, $getErrorMeg1, $sitename1);
                        // $this->Custom->sendEmail($to1, $from1, $subject1, $email_message1);
                        // $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
                        // $this->Custom->sendEmail($toSupport, $from1, $subject1, $email_message1);





                        if (!empty($getingData->kid_id)) {
                            $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                            if ($kidsdetails->kids_first_name == '') {
                                $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                            } else {
                                $kidsname = $kidsdetails->kids_first_name;
                            }
                            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                            $name = $getUsreDetails->name;
                            $email = $getUsreDetails->email;
                            $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subject = $emailTemplatesSubcritionsPmt->display;
                            $sitename = "Drafit.com";
                            $email_message_no_add = $this->Custom->paymentFailedSucritpionsKids($emailTemplatesSubcritionsPmt->value, $name, $kidsname, $sitename);
                            $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                            $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                            $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                            $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);

                            $subjectSupport = $emailMessageSupport->display;
                             $toSupport;
                            $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);
                        } else {
                            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                            $name = $getUsreDetails->name;
                            $email = $getUsreDetails->email;
                            $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                            $subject = $emailTemplatesSubcritionsPmt->display;
                            $sitename = "Drafit.com";
                            $email_message_no_add = $this->Custom->paymentFailedSucritpions($emailTemplatesSubcritionsPmt->value, $name, $sitename);
                            $this->Custom->sendEmail($email, $from, $subject, $email_message_no_add);
                            $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_HELP'])->first()->value;
                            $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                            $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                            $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);

                            $subjectSupport = $emailMessageSupport->display;
                           // echo $toSupport;
                            $this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment);

                            //onlytesting cuse
//                        dd$this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupportPayment, $messageSupportPayment);
                        }
                    }
                }
            
        }
    }

    public function authorizeCreditCard($arr_data = []) {

        extract($arr_data);

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();

        $merchantAuthentication->setName(\SampleCodeConstants::MERCHANT_LOGIN_ID);

        $merchantAuthentication->setTransactionKey(\SampleCodeConstants::MERCHANT_TRANSACTION_KEY);

        $refId = 'ref' . time();

        $creditCard = new AnetAPI\CreditCardType();

        $creditCard->setCardNumber($card_number);

        $creditCard->setExpirationDate($exp_date);

        $creditCard->setCardCode($card_code);

        $paymentOne = new AnetAPI\PaymentType();

        $paymentOne->setCreditCard($creditCard);

        $order = new AnetAPI\OrderType();

        $order->setInvoiceNumber($invice);

        $order->setDescription($product);

        $customerAddress = new AnetAPI\CustomerAddressType();

        $customerAddress->setFirstName($first_name);

        $customerAddress->setLastName($last_name);

        $customerAddress->setCompany($companyName);

        $customerAddress->setAddress($address);

        $customerAddress->setCity($city);

        $customerAddress->setState($state);

        $customerAddress->setZip($zip);

        $customerAddress->setCountry($country);

        $customerData = new AnetAPI\CustomerDataType();

        $customerData->setType("individual");

        $customerData->setId("99999456654");

        $customerData->setEmail($email);

        $duplicateWindowSetting = new AnetAPI\SettingType();

        $duplicateWindowSetting->setSettingName("duplicateWindow");

        $duplicateWindowSetting->setSettingValue("60");

        $merchantDefinedField1 = new AnetAPI\UserFieldType();

        $merchantDefinedField1->setName("Drapefit Inc");

        $merchantDefinedField1->setValue("2093065");

        $merchantDefinedField2 = new AnetAPI\UserFieldType();

        $merchantDefinedField2->setName("favoriteColor");

        $merchantDefinedField2->setValue("blue");

        $transactionRequestType = new AnetAPI\TransactionRequestType();

        $transactionRequestType->setTransactionType("authOnlyTransaction");

        $transactionRequestType->setAmount($amount);

        $transactionRequestType->setOrder($order);

        $transactionRequestType->setPayment($paymentOne);

        $transactionRequestType->setBillTo($customerAddress);

        $transactionRequestType->setCustomer($customerData);

        $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);

        $transactionRequestType->addToUserFields($merchantDefinedField1);

        $transactionRequestType->addToUserFields($merchantDefinedField2);

        $request = new AnetAPI\CreateTransactionRequest();

        $request->setMerchantAuthentication($merchantAuthentication);

        $request->setRefId($refId);

        $request->setTransactionRequest($transactionRequestType);

        $controller = new AnetController\CreateTransactionController($request);

        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        $msg = array();

        if ($response != null) {

            if ($response->getMessages()->getResultCode() == 'Ok') {

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {

                    $msg['status'] = 1;

                    $msg['TransId'] = $tresponse->getTransId();

                    $msg['Success'] = " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";

                    $msg['ResponseCode'] = " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";

                    $msg['MessageCode'] = " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";

                    $msg['AuthCode'] = " Auth Code: " . $tresponse->getAuthCode() . "\n";

                    $msg['Description'] = " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";



                    $msg['msg'] = " Description: " . $tresponse . "\n";
                } else {

                    if ($tresponse->getErrors() != null) {

                        $msg['ErrorCode'] = " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";

                        $msg['ErrorMessage'] = "Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    }
                }
            } else {

                $msg['error'] = 'error';

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {

                    $msg['ErrorCode'] = " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";

                    $msg['ErrorCode'] = " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                } else {

                    $msg['ErrorCode'] = " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";

                    $msg['ErrorMessage'] = " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
                }
            }
        } else {

            echo "No response returned \n";
        }

        return $msg;
    }

//    public function kidAutocheckoutmail() {
//
//        echo date('Y-m-d H:i:s', strtotime('-8 days'));
//        echo date('Y-m-d H:i:s');
//
//        $all_kids = $this->LetsPlanYourFirstFix->find('all')->where(['kid_id !=' => 0]);
//        if (!is_null($all_kids)) {
//            foreach ($all_kids as $getkiddata) {
//                $notpaid_users = $this->PaymentGetways->find('all')->where(['PaymentGetways.user_id' => $getkiddata->user_id, 'PaymentGetways.kid_id' => $getkiddata->kid_id, 'PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.created_dt <=' => date('Y-m-d H:i:s', strtotime('-8 days'))]);
//                // echo date('Y-m-d');
//                // echo date('Y-m-d H:i:s', strtotime('-8 days'));
//                //pj($notpaid_users);exit;
//                foreach ($notpaid_users as $notcheckout) {
//
//                    // pj($notcheckout);
//
//                    $paymentId = $notcheckout->id;
//                    $user_id = $notcheckout->user_id;
//                    $kid_id = $notcheckout->kid_id;
//                    $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
//                    $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
//                    // pj($getUsersDetails); exit;
//                    if ($kid_id != 0) {
//                        $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
//                        $prData = $this->Products->find('all')->where(['Products.kid_id' => $kid_id, 'Products.keep_status' => 0, 'Products.is_complete' => 0, 'Products.checkedout' => 'N', 'Products.payment_id' => $paymentId]);
//                        $kid = $kid_id;
//                        $profileType = 3;
//                    } else {
//                        $kid = 0;
//                        $prData = $this->Products->find('all')->where(['user_id' => $user_id, 'keep_status' => 0, 'Products.is_complete' => 0, 'Products.kid_id =' => 0, 'Products.checkedout' => 'N', 'Products.payment_id' => $paymentId]);
//                        $profileType = $getUsersDetails->gender;
//                    }
//                    //pj($prData);exit;
//                    $style_pick_total = 0;
//                    $discount_amt = $this->Custom->styleFitFee();
//                    foreach ($prData as $pd) {
//                        $style_pick_total += (double) $pd->sell_price;
//                    }
//                    $percentage = 25;
//                    $discount = ($percentage / 100) * $style_pick_total;
//                    $subTotal = $style_pick_total - $discount;
//                    $stylist_picks_subtotal = number_format($style_pick_total, 2);
//                    $keep_all_discount = number_format((!empty($discount) ? $discount : 0), 2);
//                    $style_fit_fee = number_format($discount_amt, 2);
//                    $amount = $style_pick_total - $style_fit_fee - 20;
//                    //echo $amount; exit;
//                    $payment_data = $this->PaymentCardDetails->find('all')->where(['PaymentCardDetails.user_id' => $user_id, 'PaymentCardDetails.use_card' => 1])->first();
//
//                    if ($payment_data == null) {
//
//                        $getErrorMeg = 'Please Add a valid card.';
//                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
//                        $to = $getEmail->email;
//                        $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
//                        $from = $fromMail->value;
//                        $sitename = SITE_NAME;
//                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
//                        $subject = $emailMessage->display;
//                        $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
//                        $this->Custom->sendEmail($to, $from, $subject, $email_message);
//
//                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
//                        $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
//
//                        exit;
//                    }
//
//                    $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.default_set' => 1, 'ShippingAddress.user_id' => $user_id])->first();
//                    $paymentG = $this->PaymentGetways->newEntity();
//                    $table1['user_id'] = $user_id;
//                    $table1['kid_id'] = $kid;
//                    $table1['emp_id'] = 0;
//                    $table1['status'] = 0;
//                    $table1['price'] = $subTotal;
//                    $table1['profile_type'] = $profileType;
//                    $table1['payment_type'] = 2;
//                    $table1['created_dt'] = date('Y-m-d H:i:s');
//                    $paymentG = $this->PaymentGetways->patchEntity($paymentG, $table1);
//                    $lastPymentg = $this->PaymentGetways->save($paymentG);
//                    //pj($lastPymentg); exit;
//                    $arr_user_info = [
//                        'card_number' => $payment_data->card_number,
//                        'exp_date' => $payment_data->card_expire,
//                        'card_code' => "" . $payment_data->cvv,
//                        'product' => 'Check out order',
//                        'first_name' => $billingAddress->full_name,
//                        'last_name' => $billingAddress->full_name,
//                        'address' => $billingAddress->address,
//                        'city' => $billingAddress->city,
//                        'state' => $billingAddress->state,
//                        'zip' => $billingAddress->zipcode,
//                        'country' => 'USA',
//                        'email' => $getUsersDetails->email,
//                        'amount' => $amount,
//                        'invice' => @$lastPymentg->id,
//                        'refId' => 32,
//                        'companyName' => 'Drapefit',
//                    ];
//                    $message = $this->authorizeCreditCard($arr_user_info);
//
//                    //pj($message); 
//                    //exit;
//                    if (@$message['status'] == '1') {
//                        $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId'], 'auto_checkout' => 1], ['id' => $lastPymentg->id]);
//
//                        $this->PaymentGetways->updateAll(['work_status' => 2], ['id' => $paymentId]);
//                        foreach ($prData as $dataMail) {
//                            $priceMail = $dataMail->sell_price;
//                            $productData .= "<tr>
//                                <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       # " . $i . "
//                                    </td>
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                      <img src='" . HTTP_ROOT . PRODUCT_IMAGES . $dataMail->product_image . "' width='85'/>
//                                    </td>
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       " . $dataMail->product_name_one . "
//                                    </td>
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       " . $dataMail->product_name_two . "
//                                    </td>
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       keep
//                                    </td> 
//                                    
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       " . $dataMail->size . "
//                                    </td>                  
//                                                               
//                                    <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
//                                       $" . number_format($priceMail, 2) . "
//                                    </td>
//                                </tr>";
//                            $this->Products->updateAll(['checkedout' => 'Y', 'keep_status' => '3', 'is_complete' => '1'], ['id' => $dataMail->id]);
//
//                            $i++;
//                        }
//                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
//                        $to = $getEmail->email;
//                        $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
//                        $from = $fromMail->value;
//                        $sitename = SITE_NAME;
//                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ORDER_PAYMENT'])->first();
//                        $subject = $emailMessage->display;
//                        $email_message = $this->Custom->order($emailMessage->value, $name, $sitename, $productData, $stylist_picks_subtotal, $subTotal, $style_fit_fee, $keep_all_discount);
//                        $this->Custom->sendEmail($to, $from, $subject, $email_message);
//                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
//                        $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
//
//
//                        if ($kid_id != 0) {
//                            @$kidId = $kid_id;
//                            $this->KidsDetails->updateAll(['is_redirect' => 5], ['id' => @$kidId]);
//                            $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => $kid_id]);
//                        } else {
//                            $this->Users->updateAll(['is_redirect' => 5], ['id' => $user_id]);
//                            $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => 0]);
//                        }
//                    } else {
//                        $getErrorMeg = $this->Custom->getAllMeg($message['ErrorCode']);
//                        $this->Flash->error(__($message['ErrorCode']));
//
//                        $getErrorMeg .= '<br>Please Add a valid card';
//                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
//                        $to = $getEmail->email;
//                        $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
//                        $from = $fromMail->value;
//                        $sitename = SITE_NAME;
//                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
//                        $subject = $emailMessage->display;
//                        $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
//                        $this->Custom->sendEmail($to, $from, $subject, $email_message);
//
//                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;
//                        $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
//                    }
//                }
//            }
//        }
//        exit;
//    }



    public function kidProfileNotComplete() {

        $this->KidsDetails->belongsTo('usr', ['className' => 'Users', 'foreignKey' => 'user_id']);

        $all_kidss = $this->KidsDetails->find('all')->contain(['usr']);

        //pj($all_kidss);exit;



        foreach ($all_kidss as $kid) {

            if (!empty($kid->kids_first_name)) {

                $name = $kid->kids_first_name;
            }

            //

            if (empty($kid->kids_first_name) || ($kid->is_progressbar < 100)) {

                if ($kid->kid_count == '1') {

                    $name = 'First Child';
                }

                if ($kid->kid_count == '2') {

                    $name = 'Second Child';
                }

                if ($kid->kid_count == '3') {

                    $name = 'Third Child';
                }

                if ($kid->kid_count == '4') {

                    $name = 'Four Child';
                }

                $client = $kid->usr;



                $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'KID_PROFILE_NOT_COMPLETE'])->first();



                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

                $to = $client->email;

                $from = $fromMail->value;

                //$subject = $emailMessage->display;

                $subject = "Complete your " . $name . " profile";

                $sitename = SITE_NAME;

                $mail_msg = "Complete your " . $name . " profile";

                $message = $this->Custom->userProfileComplete($emailMessage->value, $client->name, $mail_msg, $sitename);

                $this->Custom->sendEmail($to, $from, $subject, $message);

                //mail($to, $from, $subject, $message);



                $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'KID_PROFILE_NOT_COMPLETE'])->first();

                $subjectSupport = $emailMessageSupport->display;

                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_HELP'])->first()->value;

                $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $client->name, $mail_msg, $sitename);

                $this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport);

                // $this->Custom->sendEmail('devadash143@gmail.com', $from, $subjectSupport, $messageSupport);



                echo "Mail send successfully <br>";
            }
        }

        exit;
    }

}
