<?php

namespace App\Controller;

ob_start();

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\ORM\TableRegistry;
use Cake\Core\App;
use Cake\Core\Configure;

require_once(ROOT . '/vendor' . DS . 'PaymentTransactions' . DS . 'authorize-credit-card.php');

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class CronjobsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Custom');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Mpdf');
        $this->loadModel('Users');
        $this->loadModel('UserDetails');
        $this->loadModel('Payments');
        $this->loadModel('Settings');
        $this->loadmodel('ShippingAddress');
        $this->loadModel('Pages');
        $this->loadModel('Settings');
        $this->loadModel('PaymentGetways');
        $this->loadModel('PersonalizedFix');
        $this->loadModel('rather downplay');
        $this->loadModel('ReferFriends');
        $this->loadModel('ShippingAddress');
        $this->loadModel('SizeChart');
        $this->loadModel('style_quizs');
        $this->loadModel('YourProportions');
        $this->loadModel('PaymentCardDetails');
        $this->loadModel('FitCut');
        $this->loadModel('FlauntArms');
        $this->loadModel('WemenJeansLength');
        $this->loadModel('WomenJeansRise');
        $this->loadModel('WomenJeansStyle');
        $this->loadModel('WomenPrintsAvoid');
        $this->loadModel('WomenTypicalPurchaseCloth');
        $this->loadModel('WomenIncorporateWardrobe');
        $this->loadModel('WomenFabricsAvoid');
        $this->loadModel('WomenColorAvoid');
        $this->loadModel('WomenPrice');
        $this->loadModel('WomenStyle');
        $this->loadModel('WomenInformation');
        $this->loadModel('WomenRatherDownplay');
        $this->loadModel('MensBrands');
        $this->loadModel('MenFit');
        $this->loadModel('MenStats');
        $this->loadModel('MenStyle');
        $this->loadModel('MenStyleSphereSelections');
        $this->loadModel('TypicallyWearMen');
        $this->loadModel('LetsPlanYourFirstFix');
        $this->loadModel('KidsDetails');
        $this->loadModel('KidsPersonality');
        $this->loadModel('KidsPrimary');
        $this->loadModel('KidsSizeFit');
        $this->loadModel('KidClothingType');
        $this->loadModel('FabricsOrEmbellishments');
        $this->loadModel('KidStyles');
        $this->loadModel('KidsPricingShoping');
        $this->loadModel('KidPurchaseClothing');
        $this->loadModel('DeliverDate');
        $this->loadModel('Products');
        $this->loadModel('CustomerProductReview');
        $this->loadModel('UserUsesPromocode');
        $this->loadModel('Promocode');
        $this->loadModel('ChatMessages');
        $this->loadModel('EmailPreferences');
        $this->loadModel('Wallets');
        $this->loadModel('HelpDesks');
        $this->loadModel('Giftcard');
        $this->loadModel('UserMailTemplateGiftcode');
        $this->loadModel('UserUsesGiftcode');
        $this->loadModel('Notifications');
        $this->loadModel('MenAccessories');
        $this->loadModel('CustomDesine');
        $this->loadModel('WomenShoePrefer');
        $this->loadModel('WomenHeelHightPrefer');
        $this->loadModel('WemenStyleSphereSelections');
        $this->loadModel('UserAppliedCodeOrderReview');
        $this->loadModel('Giftcard');
        $this->loadModel('BatchMailingReports');
        $this->loadModel('InProducts');
        $this->loadModel('CronjobsReportsDebasish');
        $this->viewBuilder()->layout('ajax');
    }

    public function beforeFilter(Event $event) {

        $this->Auth->allow(['processInvateryProdcut','boxUpdatex', 'allstatusUpdate', 'autoMentionskid', 'giftMailDelivary', 'authorizeCreditCard', 'autoMentions', 'boxUpdate', 'autocheckoutmail', 'notCompleteProfile', 'notPaidOnce', 'addressNotComplete', 'autocheckoutmail', 'kidAutocheckoutmail', 'kidProfileNotComplete']);
    }

    public function allstatusUpdate() {
        $this->Users->updateAll(['notCompleteProfile_mail' => 0], [1]);
        $this->Users->updateAll(['notPaidOnce' => 0], [1]);
        $this->Users->updateAll(['addressNotComplete' => 0], [1]);
        $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 0], [1]);
        exit;
    }

    public function giftMailDelivary() {
        $getDetails = $this->Giftcard->find('all')->where(['type' => 1, 'delivery_date' => date('Y-m-d'), 'mail_status' => 'NULL', 'is_active' => 0]);
        foreach ($getDetails as $details) {
            $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'GIFTCARD_EMAIL'])->first();
            $fromName = $details->from_name;
            $fromMail = $details->from_email;
            $to = $details->to_email;
            $name = $details->to_name;
            $code = $details->giftcode;
            $price = number_format($details->price, 2);
            $expiry_date = $details->expire_date;
            $msg = $details->msg;
            $subject = $emailMessage->display;
            $sitename = SITE_NAME;
            $message = $this->Custom->giftCardEmail($emailMessage->value, $to, $name, $fromName, $fromMail, $price, $code, $expiry_date, $msg, $sitename);
            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
            $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
            $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'GIFTCARD_EMAIL_SUPPORT'])->first();
            $messageSupport = $this->Custom->giftCardEmailSupport($emailMessage->value, $to, $name, $fromName, $fromMail, $code, $price, $expiry_date, $msg, $sitename);
            $subjectSupport = $emailMessageSupport->display;
            if ($this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport)) {
                $supportstatus = "send";
            } else {
                $supportstatus = "faild";
            }

            if ($this->Custom->sendEmail($to, $from, $subject, $message)) {
                $process = 'giftMailDelivary()';
                $name = $name;
                $kidId = '0';
                $kidName = 'No';
                $email = $to;
                $subject = $subject;
                $status = 'Send';
                $userId = '0';
                $client = 'Client';
                $upportemail = $toSupport;
                $support_subject = $subjectSupport;
                $supportstatus = $supportstatus;
                $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
            } else {
                $process = 'giftMailDelivary()';
                $name = $name;
                $kidId = '0';
                $kidName = 'No';
                $email = $to;
                $subject = $subject;
                $status = 'faild';
                $userId = '0';
                $client = 'Client';
                $upportemail = $toSupport;
                $support_subject = $subjectSupport;
                $supportstatus = $supportstatus;
                $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
            }

            $this->Giftcard->updateAll(array('mail_status' => '1'), array('id' => $details->id));
//$this->checkBydebasish();
        }

        exit;
    }

    public function notCompleteProfile() {
        $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
        $all_clients = $this->Users->find('all')->contain(['UserDetails'])->where(['Users.type' => 2, 'UserDetails.is_progressbar < ' => 100, 'notCompleteProfile_mail' => 0]);
        $count = 1;
        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();
        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
        $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'PROFILE_NOT_COMPLETE'])->first();
        $sitename = SITE_NAME;
        $from = $fromMail->value;

        $mail_msg = "Complete your profile";
        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;

        foreach ($all_clients as $key => $client) {
            if (($client->usrdet == null) || ($client->usrdet->is_progressbar < 100)) {
                $to = '';
                $to = $client->email;
                $subject = $emailMessage->display;
                $message = $this->Custom->userProfileComplete($emailMessage->value, $client->name, $mail_msg, $sitename);
                $subjectSupport = $emailMessageSupport->display;
                $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
                if ($this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport)) {
                    $supportstatus = "Send";
                } else {
                    $supportstatus = 'Faild';
                }

                if ($this->Custom->sendEmail($to, $from, $subject, $message)) {
                    $this->Users->updateAll(['notCompleteProfile_mail' => 1], ['id' => $client->id]);
                    $process = 'notCompleteProfile()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $sx;
                    $status = 'Send';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'notCompleteProfile()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $sx;
                    $status = 'Faild';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }

                $count++;
            }
        }

        exit;
    }

    public function notPaidOnce() {
        $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
        $this->Users->hasOne('paym', ['className' => 'Payments', 'foreignKey' => 'user_id']);
        $this->Users->hasMany('card_detl', ['className' => 'PaymentCardDetails', 'foreignKey' => 'user_id']);
        $all_clients = $this->Users->find('all')->contain(['usrdet', 'paym', 'card_detl'])->where(['Users.type' => 2, 'notPaidOnce' => 0]);
        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();
        $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'NOT_PAID_ONCE'])->first();
        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();

        foreach ($all_clients as $client) {
            if ($client->card_detl == null) {
                $to = $client->email;
                $from = $fromMail->value;
                $subject = $emailMessage->display;
                $sitename = SITE_NAME;
                $mail_msg = "Please complete your payment";
                $message = $this->Custom->notPaidOnce($emailMessage->value, $client->name, $mail_msg, $sitename);
                $messageSupport = $this->Custom->notPaidOnce($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
                $subjectSupport = $emailMessageSupport->display;
                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                if ($this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport)) {
                    $supportstatus = "Send";
                } else {
                    $supportstatus = "faild";
                }
                if ($this->Custom->sendEmail($to, $from, $subject, $message)) {
                    $this->Users->updateAll(['notPaidOnce' => 1], ['id' => $client->id]);
                    $process = 'notPaidOnce()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $subject;
                    $status = 'Send';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'notPaidOnce()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $subject;
                    $status = 'faild';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }
            }
        }

        exit;
    }

    public function addressNotComplete() {
        $this->Users->hasMany('addressess', ['className' => 'ShippingAddress', 'foreignKey' => 'user_id']);
        $all_clients = $this->Users->find('all')->contain(['addressess'])->where(['Users.type' => 2, 'addressNotComplete' => 0]);
        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();
        $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'ADDRESS_NOT_COMPLATE'])->first();
        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
        foreach ($all_clients as $client) {
            if ($client->addressess == null) {
                $to = $client->email;
                $from = $fromMail->value;
                $subject = $emailMessage->display;
                $sitename = SITE_NAME;
                $mail_msg = "Please complete your shipping address";
                $message = $this->Custom->addressNotComplate($emailMessage->value, $client->name, $mail_msg, $sitename);
                $messageSupport = $this->Custom->addressNotComplate($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
                $subjectSupport = $emailMessageSupport->display;

                if ($this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport)) {
                    $supportstatus = 'Send';
                } else {
                    $supportstatus = 'Faild';
                }
                if ($this->Custom->sendEmail($to, $from, $subject, $message)) {
                    $this->Users->updateAll(['addressNotComplete' => 1], ['id' => $client->id]);
                    $process = 'addressNotComplete()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $subject;
                    $status = 'Send';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'addressNotComplete()';
                    $name = $client->name;
                    $kidId = '0';
                    $kidName = 'No';
                    $email = $to;
                    $subject = $subject;
                    $status = 'faild';
                    $userId = $client->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }
            }
        }

        exit;
    }

    public function autocheckoutmail() {
        $notpaid_users = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.auto_checkout' => 0, 'PaymentGetways.finalize_date  <=' => date('Y-m-d', strtotime('-10 days'))]);
        $prData = '';
        $kid_id = 0;
        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
        foreach ($notpaid_users as $notcheckout) {
            $paymentId = $notcheckout->id;
            $user_id = $notcheckout->user_id;
            $kid_id = $notcheckout->kid_id;
            $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
            $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
            if ($kid_id != 0) {
                $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
                $prData = $this->Products->find('all')->where(['kid_id' => $kid_id, 'keep_status' => 0, 'kid_id !=' => 0, 'is_complete' => 0, 'checkedout' => 'N', 'payment_id' => $paymentId]);
                $kid = $kid_id;
                $profileType = 3;
            } else {
                $kid = 0;
                $prData = $this->Products->find('all')->where(['user_id' => $user_id, 'keep_status' => 0, 'is_complete' => 0, 'kid_id =' => '0', 'checkedout' => 'N', 'payment_id' => $paymentId]);
                $profileType = $getUsersDetails->gender;
            }



            $currentPage = $this->Custom->currentPage($user_id);
            if ($currentPage == 4) {
                $payment_data = $this->PaymentCardDetails->find('all')->where(['PaymentCardDetails.user_id' => $user_id, 'PaymentCardDetails.use_card' => 1])->first();
                if ($payment_data == null) {
                    $getErrorMeg = 'Please Add a valid card.';
                    $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                    $to = $getEmail->email;
                    $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                    $from = $fromMail->value;
                    $sitename = SITE_NAME;
                    if ($kid_id != 0) {
                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
                        $subject = $emailMessage->display;
                        $kidsdetails = $this->KidsDetails->find()->where(['id' => $kid_id])->first();
                        if ($kidsdetails->kids_first_name == '') {
                            $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                        } else {
                            $kidsname = $kidsdetails->kids_first_name;
                        }


                        $this->Custom->sendEmail($toSupport, $from, $subject, $email_message);
                        $email_message = $this->Custom->paymentFaildKid($emailMessage->value, $name, $kidsname, $getErrorMeg, $sitename);


                        if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                            $supportstatus = "Send";
                        } else {
                            $supportstatus = "faild";
                        }

                        if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                            $process = 'autocheckoutmail()';
                            $name = $this->Custom->UserName($getEmail->id);
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $to;
                            $subject = $subject;
                            $status = 'Send';
                            $userId = $getEmail->id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $this->Custom->batchprocess($supportstatus, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'autocheckoutmail()';
                            $name = $this->Custom->UserName($getEmail->id);
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $to;
                            $subject = $subject;
                            $status = 'faild';
                            $userId = $getEmail->id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $this->Custom->batchprocess($supportstatus, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }
                    } else {


                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                        $subject = $emailMessage->display;
                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                        $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
                        if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                            $supportstatus = "send";
                        } else {
                            $supportstatus = "faild";
                        }
                        if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                            $process = 'autocheckoutmail()';
                            $name = $this->Custom->UserName($getEmail->id);
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $to;
                            $subject = $subject;
                            $status = 'Send';
                            $userId = $getEmail->id;
                            $client = 'Support';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'autocheckoutmail()';
                            $name = $this->Custom->UserName($getEmail->id);
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $to;
                            $subject = $subject;
                            $status = 'faild';
                            $userId = $getEmail->id;
                            $client = 'Support';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }
                    }
                    exit;
                }
                if ($payment_data != null) {
                    $paidUsers = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.auto_checkout' => 0, 'PaymentGetways.finalize_date <=' => date('Y-m-d', strtotime('-15 days'))]);
                    if (@$paidUsers) {
                        foreach ($paidUsers as $paid) {
                            $paymentId = $paid->id;
                            $user_id = $paid->user_id;
                            $kid_id = $paid->kid_id;
                            $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
                            $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
                            if ($kid_id != 0) {
                                $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
                                $prData = $this->Products->find('all')->where(['kid_id' => $kid_id, 'keep_status' => 0, 'kid_id !=' => 0, 'is_complete' => 0, 'checkedout' => 'N', 'payment_id' => $paymentId]);
                                $kid = $kid_id;
                                $profileType = 3;
                            } else {
                                $kid = 0;
                                $prData = $this->Products->find('all')->where(['user_id' => $user_id, 'keep_status' => 0, 'is_complete' => 0, 'kid_id =' => '0', 'checkedout' => 'N', 'payment_id' => $paymentId]);
                                $profileType = $getUsersDetails->gender;
                            }


                            $style_pick_total = 0;
                            $discount_amt = $this->Custom->styleFitFee();
                            foreach ($prData as $pd) {
                                $style_pick_total += (double) $pd->sell_price;
                            }
                            $percentage = 25;
                            $discount = ($percentage / 100) * $style_pick_total;
                            $subTotal = $style_pick_total - $discount;
                            $stylist_picks_subtotal = number_format($style_pick_total, 2);
                            $keep_all_discount = number_format((!empty($discount) ? $discount : 0), 2);
                            $style_fit_fee = number_format($discount_amt, 2);
                            $amount = $subTotal - 20;
                            $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.default_set' => 1, 'ShippingAddress.user_id' => $user_id])->first();
                            $paymentG = $this->PaymentGetways->newEntity();
                            $table1['user_id'] = $user_id;
                            $table1['kid_id'] = $kid;
                            $table1['emp_id'] = 0;
                            $table1['status'] = 0;
                            $table1['price'] = $amount;
                            $table1['profile_type'] = $profileType;
                            $table1['payment_type'] = 2;
                            $table1['created_dt'] = date('Y-m-d H:i:s');
                            $table1['created_dt'] = date('Y-m-d H:i:s');
                            $table1['auto_check_out_date'] = date('Y-m-d H:i:s');
                            $table1['parent_id'] = $paymentId;
                            $table1['emp_id'] = $this->Custom->previousStyleistNameCron($user_id, $paymentId, $paid->count);

                            $paymentG = $this->PaymentGetways->patchEntity($paymentG, $table1);
                            $lastPymentg = $this->PaymentGetways->save($paymentG);
//pj($lastPymentg); exit;

                            $arr_user_info = [
                                'card_number' => $payment_data->card_number,
                                'exp_date' => $payment_data->card_expire,
                                'card_code' => "" . $payment_data->cvv,
                                'product' => 'Check out order',
                                'first_name' => $billingAddress->full_name,
                                'last_name' => $billingAddress->full_name,
                                'address' => $billingAddress->address,
                                'city' => $billingAddress->city,
                                'state' => $billingAddress->state,
                                'zip' => $billingAddress->zipcode,
                                'country' => 'USA',
                                'email' => $getUsersDetails->email,
                                'amount' => $amount,
                                'invice' => @$lastPymentg->id,
                                'refId' => 32,
                                'companyName' => 'Drapefit',
                            ];

                            $message = $this->authorizeCreditCard($arr_user_info);
                            if (@$message['status'] == '1') {
                                $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId'], 'auto_checkout' => 1], ['id' => $lastPymentg->id]);
                                $this->PaymentGetways->updateAll(['work_status' => 2, 'auto_checkout' => 1], ['id' => $paymentId]);
                                $productData = '';
                                $i = 1;
                                foreach ($prData as $dataMail) {
                                    $priceMail = $dataMail->sell_price;
                                    $productData .= "<tr>
                        <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               # " . $i . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                              <img src='" . HTTP_ROOT . PRODUCT_IMAGES . $dataMail->product_image . "' width='85'/>
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->product_name_one . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->product_name_two . "
                            </td>
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               keep
                            </td> 
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               " . $dataMail->size . "
                            </td>                                            
                            <td style='padding: 10px 10px;font-size: 13px;border-bottom: 1px solid #ccc;'>
                               $" . number_format($priceMail, 2) . "
                            </td>
                        </tr>";
                                    $this->Products->updateAll(['checkedout' => 'Y', 'keep_status' => '3', 'is_complete' => '1'], ['id' => $dataMail->id]);

                                    $i++;
                                }

                                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                                $to = $getEmail->email;
                                $from = $fromMail->value;
                                $sitename = SITE_NAME;
                                if ($kid_id != 0) {
                                    $kname = $this->KidsDetails->find()->where(['id' => $kid_id])->first()->kids_first_name;
                                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ORDER_PAYMENT_KID'])->first();
                                    $subject = $emailMessage->display;
                                    $email_message = $this->Custom->order($emailMessage->value, $kname, $sitename, $productData, $style_pick_total, $amount, $style_fit_fee, $keep_all_discount, $refundamount = '', $amount, $offerData = '');

                                    if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getEmail->id);
                                        $kidId = $kid_id;
                                        $kidName = $kname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'Send';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = 'nill';
                                        $support_subject = 'nill';
                                        $supportstatus = '';
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    } else {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getEmail->id);
                                        $kidId = $kid_id;
                                        $kidName = $kname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'Faild';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = 'Nill';
                                        $support_subject = 'nill';
                                        $supportstatus = '';
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    }

                                    $KidsDetails = $this->KidsDetails->newEntity();
                                    $k['id'] = $kid_id;
                                    $k['is_redirect'] = 5;
                                    $KidsDetails = $this->KidsDetails->patchEntity($KidsDetails, $k);
                                    $KidsDetails = $this->KidsDetails->save($KidsDetails);
                                    $this->KidsDetails->updateAll(['is_redirect' => 5], ['id' => $kid_id]);
                                    $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => $kid_id]);
                                } else {
                                    $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'ORDER_PAYMENT'])->first();
                                    $subject = $emailMessage->display;
                                    $email_message = $this->Custom->order($emailMessage->value, $name, $sitename, $productData, $style_pick_total, $amount, $style_fit_fee, $keep_all_discount, $refundamount = '', $amount, $offerData = '');

                                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;

                                    if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                                        $supportstatus = "send";
                                    } else {
                                        $supportstatus = "Faild";
                                    }

                                    if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = 0;
                                        $kidName = '';
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'Send';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $toSupport;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    } else {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = 0;
                                        $kidName = '';
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'faild';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $toSupport;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    }
                                    $this->Users->updateAll(['is_redirect' => 5], ['id' => $user_id]);
                                    $this->Notifications->updateAll(['is_read' => 1], ['user_id' => $user_id, 'kid_id' => 0]);
                                }
                            } else {
                                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                                $to = $getEmail->email;
                                $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                                $from = $fromMail->value;
                                $sitename = SITE_NAME;
                                if ($kid_id != 0) {
                                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemKidPaymeent'])->first();
                                    $subject = $emailMessage->display;
                                    $kidsdetails = $this->KidsDetails->find()->where(['id' => $kid_id])->first();
                                    if ($kidsdetails->kids_first_name == '') {
                                        $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                                    } else {
                                        $kidsname = $kidsdetails->kids_first_name;
                                    }
                                    $email_message = $this->Custom->paymentFaildKid($emailMessage->value, $name, $kidsname, $getErrorMeg, $sitename);

                                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                                    if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                                        $supportstatus = "Send";
                                    } else {
                                        $supportstatus = "Faild";
                                    }

                                    if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = $kid_id;
                                        $kidName = $kidsname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'Send';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $toSupport;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    } else {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = $kid_id;
                                        $kidName = $kidsname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'faild';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $toSupport;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    }
                                } else {
                                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'someThingsProblemPaymeent'])->first();
                                    $subject = $emailMessage->display;
                                    $email_message = $this->Custom->paymentFaild($emailMessage->value, $name, $getErrorMeg, $sitename);
                                    $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                                    if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                                        $supportstatus = "Send";
                                    } else {
                                        $supportstatus = "Faild";
                                    }
                                    if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = $kid_id;
                                        $kidName = $kidsname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'Send';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $toSupport;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    } else {
                                        $process = 'autocheckoutmail()';
                                        $name = $this->Custom->UserName($getUsreDetails->id);
                                        $kidId = $kid_id;
                                        $kidName = $kidsname;
                                        $email = $to;
                                        $subject = $subject;
                                        $status = 'faild';
                                        $userId = $user_id;
                                        $client = 'Client';
                                        $upportemail = $upportemail;
                                        $support_subject = $subject;
                                        $supportstatus = $supportstatus;
                                        $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



        $currentPage = $this->Custom->currentPage($user_id);
        if ($currentPage == 4) {
            $checkout_between = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1, 'auto_checkout' => 0, 'mail_status' => 1, 'PaymentGetways.auto_checkout' => 0, 'PaymentGetways.finalize_date <=' => date('Y-m-d', strtotime('-10 days')), 'PaymentGetways.finalize_date >=' => date('Y-m-d', strtotime('-15 days'))]);
            foreach ($checkout_between as $chkBET) {
                $paymentId = $chkBET->id;
                $user_id = $chkBET->user_id;
                $kid_id = $chkBET->kid_id;
                $getUsersDetails = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $user_id])->first();
                $getEmail = $this->Users->find('all')->where(['id' => $user_id])->first();
                $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                $to = $getEmail->email;
                if ($kid_id != 0) {
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'CHECKOUTBETWEENDAYS_KIDS'])->first();
                    $getKidsDetails = $this->KidsDetails->find('all')->where(['id' => $kid_id])->first();
                    $name = $getKidsDetails->kids_first_name;
                    $kid = $kid_id;
                    $profileType = 3;
                } else {
                    $kid = 0;
                    $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'CHECKOUTBETWEENDAYS'])->first();
                    $name = $getUsersDetails->first_name . ' ' . $getUsersDetails->last_name;
                    $profileType = $getUsersDetails->gender;
                }
                $from = $fromMail->value;
                $sitename = SITE_NAME;
                $email = $to;
                $subject = $emailMessage->display;
                $email_message = $this->Custom->checkoutBetweenDays($emailMessage->value, $name, $email, $sitename);
                $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                    $supportstatus = "Send";
                } else {
                    $supportstatus = 'faild';
                }
                if ($this->Custom->sendEmail($to, $from, $subject, $email_message)) {
                    $process = 'autocheckoutmail()';
                    $name = $this->Custom->UserName($getUsreDetails->id);
                    $kidId = $kid_id;
                    $kidName = $kidsname;
                    $email = $to;
                    $subject = $subject;
                    $status = 'Send';
                    $userId = $user_id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subject;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'autocheckoutmail()';
                    $name = $this->Custom->UserName($getUsreDetails->id);
                    $kidId = $kid_id;
                    $kidName = $kidsname;
                    $email = $to;
                    $subject = $subject;
                    $status = 'faild';
                    $userId = $user_id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subject;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }
            }
        }

        exit;
    }

    public function autoMentions() {
        $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['try_new_items_with_scheduled_fixes' => 1, 'kid_id' => '0', 'autoMentions' => 0]);
        foreach ($getingData as $data) {
            $planId = $data->id;
            if ($data->how_often_would_you_lik_fixes == 1) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                //echo $data->user_id;
               // echo "<br>";
               $days_between = round($datediff / (60 * 60 * 24));
                // echo "<br>";
              
                if ($days_between == '30') {
                    $this->boxUpdate($planId);
                } else if ($days_between >= '31') {
                    $day = date("l");
                    //echo $day;
                    
                    if ($day == 'Monday') {
                      echo "<br>";
                      echo $data->user_id;
                     echo "<br>";
                        $this->boxUpdate($planId);
                    }
                }
            }

            if ($data->how_often_would_you_lik_fixes == 2) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                $days_between = round($datediff / (60 * 60 * 24));
                if ($days_between == '60') {
                    $this->boxUpdate($planId);
                } else if ($days_between >= '61') {
                    $day = date("l");
                    if ($day == 'Friday') {
                        $this->boxUpdate($planId);
                    }
                }
            }

            if ($data->how_often_would_you_lik_fixes == 3) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                $days_between = round($datediff / (60 * 60 * 24));
                if ($days_between == '90') {
                    $this->boxUpdate($planId);
                } else if ($days_between >= '91') {
                    $day = date("l");
                    if ($day == 'Friday') {
                        $this->boxUpdate($planId);
                    }
                }
            }
        }

        exit;
    }

     public function boxUpdate($planId) {
        if ($planId) {
            $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['id' => $planId])->first();
            $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('addressess', ['className' => 'ShippingAddress', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('card_detl', ['className' => 'PaymentCardDetails', 'foreignKey' => 'user_id']);
            $getUsreDetails = $this->Users->find('all')->contain(['addressess', 'card_detl', 'usrdet'])->where(['Users.id' => $getingData->user_id])->first();
            if (($getingData->user_id != '') && ($getingData->kid_id != '')) {
                $profile_type = '3';
            } else {
                $getGender = $this->UserDetails->find('all')->where(['user_id' => $getingData->user_id])->first();
                $getEmail = $this->Users->find('all')->where(['id' => $getingData->user_id])->first()->email;
                $getUsreDetails = $this->Users->find('all')->where(['id' => $getingData->user_id])->first();
                if ($getGender->gender == 1) {
                    $profile_type = '1';
                } else {
                    $profile_type = '2';
                }
            }


            $cardDetailsCount = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->count();
           // echo $cardDetailsCount.'usredi'.$getingData->user_id;
            if ($cardDetailsCount <= 0) {
                $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                $name = $getUsreDetails->name;
                $email = $getUsreDetails->email;
                $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                $subject = $emailTemplatesSubcritionsPmt->display;
                $sitename = "Drafit.com";
                $email_message_no_add = $this->Custom->paymentFailedSucritpions($emailTemplatesSubcritionsPmt->value, $name, $sitename);
                $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_BATCH_HELP'])->first()->value;
                $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                $subjectSupport = $emailtemplatesAdminPayment->display;

                if ($this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment)) {

                    $supportstatus = "Send";
                } else {
                    $supportstatus = "faild";
                }
                if ($this->Custom->sendEmail($email, $from, $subject, $email_message_no_add)) {
                    $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                    $process = 'boxUpdate()';
                    @$kidId = $kid_id;
                    @$kidName = $kidsname;
                    $email = $email;
                    $name = $name;
                    $subject = $subject;
                    $status = 'Send';
                    @$userId = $getUsreDetails->id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupportPayment;
                    $supportstatus = $supportstatus;
                    $payment_message = "Card number is empty";
                    $transctions_id = "No transcatins id";
                    $number = 0;
                    $this->Custom->batchprocessPayment($number, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'boxUpdate()';
                    $kidId = $kid_id;
                    $name = $name;
                    $kidName = $kidsname;
                    $email = $email;
                    $subject = $subject;
                    $status = 'faild';
                    $userId = $user_id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupportPayment;
                    $supportstatus = $supportstatus;
                    $payment_message = "Card number is empty";
                    $transctions_id = 'No transctions id';
                    $number = 0;
                    $this->Custom->batchprocessPayment($number, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }
            } else {

                $notCompleteCurrentFit = $this->PaymentGetways->find('all')->where(['PaymentGetways.kid_id' => 0, 'PaymentGetways.user_id' => $getingData->user_id, 'PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1])->first();
                if (empty($notCompleteCurrentFit->id)) {
                    $cardDetails = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->first();
                    $paymentDetails = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $profile_type, 'PaymentGetways.kid_id' => 0, 'user_id' => $getingData->user_id])->order(['PaymentGetways.id' => 'DESC'])->first();
                    $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.status' => 1,  'PaymentGetways.kid_id' => 0, 'user_id' => $getingData->user_id])->count();
                    $newEntity = $this->PaymentGetways->newEntity();
                    $data['user_id'] = $getingData->user_id;
                    $data['kid_id'] = $getingData->kid_id;
                    $data['emp_id'] = $this->Custom->previousStyleistNameCron($getingData->user_id, $paymentDetails->id, $paymentDetails->count);
                    $data['price'] = 20;
                    $data['profile_type'] = $profile_type;
                    $data['payment_type'] = 1;
                    $data['status'] = 0;
                    $data['payment_card_details_id'] = $cardDetails->id;
                    $data['created_dt'] = date('Y-m-d H:i:s');
                    $data['count'] = $paymentCount + 1;
                    $data['work_status'] = 1;
                    $newEntity = $this->PaymentGetways->patchEntity($newEntity, $data);
                    $PaymentIdlast = $this->PaymentGetways->save($newEntity);
                    $paymentId = $PaymentIdlast->id;
                    $userData = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $getingData->user_id])->first();
                    $exitdata = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id])->first();
                    $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id, 'is_billing' => 1])->first();
                    $full_address = $billingAddress->address . ((!empty($billingAddress->address_line_2)) ? '<br>' . $billingAddress->address_line_2 : '') . '<br>' . $billingAddress->city . ', ' . $billingAddress->state . '<br>' . $billingAddress->country . ' ' . $billingAddress->zipcode;
                    $usr_name = $billingAddress->full_name;
                    $arr_user_info = [
                        'card_number' => $cardDetails->card_number,
                        'exp_date' => $cardDetails->card_expire,
                        'card_code' => $cardDetails->cvv,
                        'product' => 'Test Plugin',
                        'first_name' => $billingAddress->full_name,
                        'last_name' => $billingAddress->full_name,
                        'address' => $billingAddress->address,
                        'city' => $billingAddress->city,
                        'state' => $billingAddress->state,
                        'zip' => $billingAddress->zipcode,
                        'country' => $billingAddress->country,
                        'email' => $getUsreDetails->email,
                        'amount' => 20,
                        'invice' => $paymentId,
                        'refId' => $paymentId,
                        'companyName' => 'Drapefit',
                    ];
                    $message = $this->authorizeCreditCard($arr_user_info);
                    $fitnumber = $this->PaymentGetways->find('all')->where(['id' => $paymentId])->first()->count;

                    if (@$message['status'] == '1') {
                        $updateId = $paymentId;
                        $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId']], ['id' => $updateId]);
                        $this->LetsPlanYourFirstFix->updateAll(['applay_dt' => date('Y-m-d H:i:s')], ['id' => $planId]);
                        $paymentDetails = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId])->first();
                        $checkUser = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId, 'PaymentGetways.payment_type' => 1])->first();






                        if ($checkUser->payment_type == 1) {
                            if ($getingData->kid_id != '') {
                                @$kidId = $getingData->kid_id;
                                $this->KidsDetails->updateAll(['is_redirect' => 2], ['id' => @$checkUser->kid_id]);
                                $kid_id = $getingData->kid_id;
                            } else {
                                $kid_id = 0;
                                $this->Users->updateAll(['is_redirect' => 2], ['id' => @$checkUser->user_id]);
                            }
                        }


                        if ($paymentDetails->profile_type == 1) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 2) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 3) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE_KID'])->first();
                        }

                        $enty = $this->DeliverDate->newEntity();
                        $date = date('Y-m-d');
                        $dateTime = date('l, F d, Y', strtotime($date . ' + 7 days'));
                        $data['date_in_time'] = $dateTime;
                        $data['weeks'] = 0;
                        $data['is_send_me'] = 0;
                        $data['user_id'] = $checkUser->user_id;
                        $data['kid_id'] = @$kidId;
                        $user = $this->DeliverDate->patchEntity($enty, $data);
                        $this->DeliverDate->save($user);
                        $this->PaymentGetways->updateAll(['delivery_id' => $user->id], ['id' => $updateId]);
                       

               

                        $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.user_id' => $getingData->user_id, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $paymentDetails->profile_type, 'PaymentGetways.payment_type' => 1])->count();
                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'SUCCESS_PAYMENT'])->first();
                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                        $stylefee = $this->Settings->find('all')->where(['Settings.name' => 'style_fee'])->first();
                        $feeprice = $stylefee->value;
                        $name = $getUsreDetails->name;
                        $from = $fromMail->value;
                        $subject = $emailMessage->display;
                        $sitename = SITE_NAME;
                        $usermessage = $message['Success'];
                        $sumitted_date = date_format($checkUser->created_dt, 'm/d/Y');
                        $paid_amount = "$ " . number_format($checkUser->price, 2);
                        $last_4_digit = substr($cardDetails->card_number, -4);
                        $email_message = $this->Custom->paymentEmail($emailMessage->value, $name, $usermessage, $sitename, $message['TransId'], $paid_amount, $sumitted_date, $cardDetails->card_type, $last_4_digit, $usr_name, $full_address, $feeprice);
                        $getUsreDetails->email;
                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                        
                        
                        if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                            $supportstatus = "Send";
                        } else {
                            $supportstatus = "Faild";
                        }
                        if ($this->Custom->sendEmail($getUsreDetails->email, $from, $subject, $email_message)) {
                            $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subject;
                            $status = 'Send';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subject;
                            $status = 'faild';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }

                        $subjectProfile = $emailMessageProfile->display;
                        $email_message_profile = $this->Custom->paymentEmailCount($emailMessageProfile->value, $name, $usermessage, $sitename, $paymentCount);
                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                        if ($this->Custom->sendEmail($toSupport, $from, $subjectProfile, $email_message)) {
                            $supportstatus = "send";
                        } else {
                            $supportstatus = "faild";
                        }

                        if ($this->Custom->sendEmail($getUsreDetails->email, $from, $subjectProfile, $email_message_profile)) {
                            $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subjectProfile;
                            $status = 'Send';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectProfile;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subjectProfile;
                            $status = 'faild';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectProfile;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }
                    } else {
                        $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                        $name = $getUsreDetails->name;
                        $email = $getUsreDetails->email;
                        $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                        $subject = $emailTemplatesSubcritionsPmt->display;
                        $sitename = "Drafit.com";
                        $email_message_no_add = $this->Custom->paymentFailedSucritpions($emailTemplatesSubcritionsPmt->value, $name, $sitename);
                        $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_BATCH_HELP'])->first()->value;
                        $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'paymentFaildSubcritions'])->first();
                        $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                        $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                        $subjectSupport = $emailMessageSupport->display;
                        if ($this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment)) {
                            $supportstatus = "Send";
                        } else {
                            $supportstatus = "faild";
                        }
                        if ($this->Custom->sendEmail($email, $from, $subject, $email_message_no_add)) {
                            $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $email;
                            $subject = $subject;
                            $status = 'Send';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectSupportPayment;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'boxUpdate()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $email;
                            $subject = $subject;
                            $status = 'faild';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectSupportPayment;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }
                    }
                }
            }
        }
    }

    public function autoMentionskid() {
        $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['try_new_items_with_scheduled_fixes' => 1, 'kid_id !=' => 0, 'autoMentions' => 0]);
        foreach ($getingData as $data) {
            $planId = $data->id;
            if ($data->how_often_would_you_lik_fixes == 1) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                $days_between = round($datediff / (60 * 60 * 24));

                if ($days_between == '30') {
                    $this->boxUpdatekid($planId);
                } else if ($days_between >= '31') {
                    $day = date("l");
                    if ($day == 'Tuesday') {
                        $this->boxUpdatekid($planId);
                    }
                }
            }

            if ($data->how_often_would_you_lik_fixes == 2) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                $days_between = round($datediff / (60 * 60 * 24));

                if ($days_between == '30') {
                    $this->boxUpdatekid($planId);
                } else if ($days_between >= '31') {
                    $day = date("l");
                    if ($day == 'Friday') {
                        $this->boxUpdatekid($planId);
                    }
                }
            }

            if ($data->how_often_would_you_lik_fixes == 3) {
                $currentDate = time();
                $oneMonth = strtotime(date('Y-m-d', strtotime($data->applay_dt)));
                $datediff = $currentDate - $oneMonth;
                $days_between = round($datediff / (60 * 60 * 24));

                if ($days_between == '90') {
                    $this->boxUpdatekid($planId);
                } else if ($days_between >= '91') {
                    $day = date("l");
                    if ($day == 'Friday') {
                        $this->boxUpdatekid($planId);
                    }
                }
            }
        }

        exit;
    }

    public function boxUpdateKid($planId) {
        if ($planId) {
//echo $planId;
            $getingData = $this->LetsPlanYourFirstFix->find('all')->where(['id' => $planId])->first();
            $this->Users->hasOne('usrdet', ['className' => 'UserDetails', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('addressess', ['className' => 'ShippingAddress', 'foreignKey' => 'user_id']);
            $this->Users->hasMany('card_detl', ['className' => 'PaymentCardDetails', 'foreignKey' => 'user_id']);
            $getUsreDetails = $this->Users->find('all')->contain(['addressess', 'card_detl', 'usrdet'])->where(['Users.id' => $getingData->user_id])->first();
            if (($getingData->user_id != '') && ($getingData->kid_id != '')) {
                $profile_type = '3';
            } else {
                $getGender = $this->UserDetails->find('all')->where(['user_id' => $getingData->user_id])->first();
                $getEmail = $this->Users->find('all')->where(['id' => $getingData->user_id])->first()->email;
                $getUsreDetails = $this->Users->find('all')->where(['id' => $getingData->user_id])->first();
                if ($getGender->gender == 1) {
                    $profile_type = '1';
                } else {
                    $profile_type = '2';
                }
            }

            $cardDetailsCount = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->count();
            if ($cardDetailsCount <= 0) {
//echo $cardDetailsCount;
                if (!empty($getingData->kid_id)) {
                    $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                    $name = $getUsreDetails->name;
                    $email = $getUsreDetails->email;
                    $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                    $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                    $subject = $emailTemplatesSubcritionsPmt->display;
                    $sitename = "Drafit.com";

                    if ($kidsdetails->kids_first_name == '') {
                        $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                    } else {
                        $kidsname = $kidsdetails->kids_first_name;
                    }
                    $email_message_no_add = $this->Custom->paymentFailedSucritpionsKids($emailTemplatesSubcritionsPmt->value, $name, $kidsname, $sitename);
                    $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_BATCH_HELP'])->first()->value;
                    $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                    $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                    $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                    $subjectSupport = $emailMessageSupport->display;
                    if ($this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment)) {
                        $supportstatus = "Send";
                    } else {
                        $supportstatus = "faild";
                    }

                    if ($this->Custom->sendEmail($email, $from, $subject, $email_message_no_add)) {
                        $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                        $process = 'boxUpdateKid()';
                        $kidId = $kid_id;
                        $kidName = $kidsname;
                        $email = $email;
                        $subject = $subject;
                        $status = 'Send';
                        $userId = $user_id;
                        $client = 'Client';
                        $upportemail = $toSupport;
                        $support_subject = $subjectSupportPayment;
                        $supportstatus = $supportstatus;
                        $payment_message = "Card number is empty";
                        $transctions_id = "No transcatins id";
                        $this->Custom->batchprocessPayment($number = 0, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                    } else {
                        $process = 'boxUpdateKid()';
                        $kidId = $kid_id;
                        $kidName = $kidsname;
                        $email = $email;
                        $subject = $subject;
                        $status = 'faild';
                        $userId = $user_id;
                        $client = 'Client';
                        $upportemail = $toSupport;
                        $support_subject = $subjectSupportPayment;
                        $supportstatus = $supportstatus;
                        $payment_message = "Card number is empty";
                        $transctions_id = "No transcatins id";
                        $this->Custom->batchprocessPayment($number = 0, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                    }
                }
            } else {
                $notCompleteCurrentFit = $this->PaymentGetways->find('all')->where(['PaymentGetways.kid_id' => $getingData->kid_id, 'PaymentGetways.user_id' => $getingData->user_id, 'PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 1, 'PaymentGetways.status' => 1])->first();
                if (empty($notCompleteCurrentFit->id)) {
                    $cardDetails = $this->PaymentCardDetails->find('all')->where(['user_id' => $getingData->user_id, 'use_card' => 1])->first();
                    $paymentDetails = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $profile_type, 'PaymentGetways.kid_id' => $getingData->kid_id, 'user_id' => $getingData->user_id])->order(['PaymentGetways.id' => 'DESC'])->first();
                    $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.status' => 1, 'PaymentGetways.kid_id' => $getingData->kid_id])->count();
                    $newEntity = $this->PaymentGetways->newEntity();
                    $data['user_id'] = $getingData->user_id;
                    $data['kid_id'] = $getingData->kid_id;
                    $data['emp_id'] = $this->Custom->previousStyleistNameCron($getingData->user_id, $paymentDetails->id, $paymentDetails->count);
                    $data['price'] = 20;
                    $data['profile_type'] = $profile_type;
                    $data['payment_type'] = 1;
                    $data['status'] = 0;
                    $data['payment_card_details_id'] = $cardDetails->id;
                    $data['created_dt'] = date('Y-m-d H:i:s');
                    $data['count'] = $paymentCount + 1;
                    $data['work_status'] = 1;
                    $newEntity = $this->PaymentGetways->patchEntity($newEntity, $data);
                    $PaymentIdlast = $this->PaymentGetways->save($newEntity);
                    $paymentId = $PaymentIdlast->id;
                    $userData = $this->UserDetails->find('all')->where(['UserDetails.user_id' => $getingData->user_id])->first();
                    $exitdata = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id])->first();
                    $billingAddress = $this->ShippingAddress->find('all')->where(['ShippingAddress.user_id' => $getingData->user_id, 'is_billing' => 1])->first();
                    $full_address = $billingAddress->address . ((!empty($billingAddress->address_line_2)) ? '<br>' . $billingAddress->address_line_2 : '') . '<br>' . $billingAddress->city . ', ' . $billingAddress->state . '<br>' . $billingAddress->country . ' ' . $billingAddress->zipcode;
                    $usr_name = $billingAddress->full_name;
                    $arr_user_info = [
                        'card_number' => $cardDetails->card_number,
                        'exp_date' => $cardDetails->card_expire,
                        'card_code' => $cardDetails->cvv,
                        'product' => 'Test Plugin',
                        'first_name' => $billingAddress->full_name,
                        'last_name' => $billingAddress->full_name,
                        'address' => $billingAddress->address,
                        'city' => $billingAddress->city,
                        'state' => $billingAddress->state,
                        'zip' => $billingAddress->zipcode,
                        'country' => $billingAddress->country,
                        'email' => $getUsreDetails->email,
                        'amount' => 20,
                        'invice' => $paymentId,
                        'refId' => $paymentId,
                        'companyName' => 'Drapefit',
                    ];
                    $message = $this->authorizeCreditCard($arr_user_info);
                    $fitnumber = $this->PaymentGetways->find('all')->where(['id' => $paymentId])->first()->count;
                    if (@$message['status'] == '1') {
                        $updateId = $paymentId;
                        $this->PaymentGetways->updateAll(['status' => 1, 'transactions_id ' => $message['TransId']], ['id' => $updateId]);
                        $this->LetsPlanYourFirstFix->updateAll(['applay_dt' => date('Y-m-d H:i:s')], ['id' => $planId]);
                        $paymentDetails = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId])->first();
                        $checkUser = $this->PaymentGetways->find('all')->where(['PaymentGetways.id' => $updateId, 'PaymentGetways.payment_type' => 1])->first();
                        if ($checkUser->payment_type == 1) {
                            if ($getingData->kid_id != '') {
                                @$kidId = $getingData->kid_id;
                                $this->KidsDetails->updateAll(['is_redirect' => 2], ['id' => @$checkUser->kid_id]);
                                $kid_id = $getingData->kid_id;
                            }
                        }
                        if ($paymentDetails->profile_type == 1) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 2) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE'])->first();
                        } elseif ($paymentDetails->profile_type == 3) {
                            $emailMessageProfile = $this->Settings->find('all')->where(['Settings.name' => 'PAYMENT_COUNT_PROFILE_KID'])->first();
                        }
                        
                        
                        $enty = $this->DeliverDate->newEntity();
                        $date = date('Y-m-d');
                        $dateTime = date('l, F d, Y', strtotime($date . ' + 7 days'));
                        $data['date_in_time'] = $dateTime;
                        $data['weeks'] = 0;
                        $data['is_send_me'] = 0;
                        $data['user_id'] = $checkUser->user_id;
                        $data['kid_id'] = @$kidId;
                        $user = $this->DeliverDate->patchEntity($enty, $data);
                        $this->DeliverDate->save($user);
                        $this->PaymentGetways->updateAll(['delivery_id' => $user->id], ['id' => $updateId]);
                        

                        $paymentCount = $this->PaymentGetways->find('all')->where(['PaymentGetways.user_id' => $getingData->user_id, 'PaymentGetways.status' => 1, 'PaymentGetways.profile_type' => $paymentDetails->profile_type, 'PaymentGetways.payment_type' => 1])->count();
                        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'SUCCESS_PAYMENT'])->first();
                        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
                        $stylefee = $this->Settings->find('all')->where(['Settings.name' => 'style_fee'])->first();
                        $feeprice = $stylefee->value;
                        $name = $getUsreDetails->name;
                        $from = $fromMail->value;
                        $subject = $emailMessage->display;
                        $sitename = SITE_NAME;
                        $usermessage = $message['Success'];
                        $sumitted_date = date_format($checkUser->created_dt, 'm/d/Y');
                        $paid_amount = "$ " . number_format($checkUser->price, 2);
                        $last_4_digit = substr($cardDetails->card_number, -4);
//echo $getUsreDetails->email;exit;
                        $email_message = $this->Custom->paymentEmail($emailMessage->value, $name, $usermessage, $sitename, $message['TransId'], $paid_amount, $sumitted_date, $cardDetails->card_type, $last_4_digit, $usr_name, $full_address, $feeprice);
                        $getUsreDetails->email;
                        $subjectProfile = $emailMessageProfile->display;
                        $email_message_profile = $this->Custom->paymentEmailCount($emailMessageProfile->value, $name, $usermessage, $sitename, $paymentCount);


                        $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                        if ($kidsdetails->kids_first_name == '') {
                            $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                        } else {
                            $kidsname = $kidsdetails->kids_first_name;
                        }

                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                        if ($this->Custom->sendEmail($toSupport, $from, $subject, $email_message)) {
                            $supportstatus = "Send";
                        } else {
                            $supportstatus = "faild";
                        }

                        if ($this->Custom->sendEmail($getUsreDetails->email, $from, $subject, $email_message)) {
                            $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                            $process = 'boxUpdateKid()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subject;
                            $status = 'Send';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'boxUpdateKid()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subject;
                            $status = 'faild';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subject;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }

                        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;
                        if ($this->Custom->sendEmail($toSupport, $from, $subjectProfile, $email_message)) {
                            $supportstatus = 'Send';
                        } else {
                            $supportstatus = "Faild";
                        }


                        if ($this->Custom->sendEmail($getUsreDetails->email, $from, $subjectProfile, $email_message_profile)) {
                            $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                            $process = 'boxUpdateKid()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subjectProfile;
                            $status = 'Send';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectProfile;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        } else {
                            $process = 'boxUpdateKid()';
                            $kidId = $kid_id;
                            $kidName = $kidsname;
                            $email = $getUsreDetails->email;
                            $subject = $subjectProfile;
                            $status = 'faild';
                            $userId = $user_id;
                            $client = 'Client';
                            $upportemail = $toSupport;
                            $support_subject = $subjectProfile;
                            $supportstatus = $supportstatus;
                            $payment_message = json_encode($message);
                            $transctions_id = $message['TransId'];
                            $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                        }
                    } else {
                        if (!empty($getingData->kid_id)) {
                            $kidsdetails = $this->KidsDetails->find('all')->where(['id' => @$getingData->kid_id])->first();
                            if ($kidsdetails->kids_first_name == '') {
                                $kidsname = $this->Custom->kidsNumber($kidsdetails->kid_count);
                            } else {
                                $kidsname = $kidsdetails->kids_first_name;
                            }
                            $from = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first()->value;
                            $name = $getUsreDetails->name;
                            $email = $getUsreDetails->email;
                            $emailTemplatesSubcritionsPmt = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subject = $emailTemplatesSubcritionsPmt->display;
                            $sitename = "Drafit.com";
                            $email_message_no_add = $this->Custom->paymentFailedSucritpionsKids($emailTemplatesSubcritionsPmt->value, $name, $kidsname, $sitename);

                            $toSupport = $this->Settings->find('all')->where(['Settings.name' => 'TO_BATCH_HELP'])->first()->value;
                            $emailtemplatesAdminPayment = $this->Settings->find('all')->where(['Settings.name' => 'kidPaymentFaildSubcritions'])->first();
                            $subjectSupportPayment = $emailtemplatesAdminPayment->display;
                            $messageSupportPayment = $this->Custom->paymentFailedSucritpionsAdmin($emailtemplatesAdminPayment->value, $name, $email, $sitename, $sitename);
                            $subjectSupport = $emailMessageSupport->display;
                            if ($this->Custom->sendEmail($toSupport, $from, $subjectSupportPayment, $messageSupportPayment)) {
                                $supportstatus = "Send";
                            } else {
                                $supportstatus = "faild";
                            }

                            if ($this->Custom->sendEmail($email, $from, $subject, $email_message_no_add)) {
                                $this->LetsPlanYourFirstFix->updateAll(['autoMentions' => 1], ['id' => $planId]);
                                $process = 'boxUpdateKid()';
                                $kidId = $kid_id;
                                $kidName = $kidsname;
                                $email = $email;
                                $subject = $subject;
                                $status = 'Send';
                                $userId = $user_id;
                                $client = 'Client';
                                $upportemail = $toSupport;
                                $support_subject = $subjectSupportPayment;
                                $supportstatus = $supportstatus;
                                $payment_message = json_encode($message);
                                $transctions_id = $message['TransId'];
                                $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                            } else {
                                $process = 'boxUpdateKid()';
                                $kidId = $kid_id;
                                $kidName = $kidsname;
                                $email = $email;
                                $subject = $subject;
                                $status = 'faild';
                                $userId = $user_id;
                                $client = 'client';
                                $upportemail = $toSupport;
                                $support_subject = $subjectSupportPayment;
                                $supportstatus = $supportstatus;
                                $payment_message = json_encode($message);
                                $transctions_id = $message['TransId'];
                                $this->Custom->batchprocessPayment($fitnumber, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                            }
                        }
                    }
                }
            }
        }
    }

    public function kidProfileNotComplete() {
        $this->KidsDetails->belongsTo('usr', ['className' => 'Users', 'foreignKey' => 'user_id']);
        $all_kidss = $this->KidsDetails->find('all')->contain(['usr'])->where(['KidsDetails.kidProfileNotComplete' => 0]);

        $emailMessage = $this->Settings->find('all')->where(['Settings.name' => 'KID_PROFILE_NOT_COMPLETE'])->first();
        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
        $emailMessageSupport = $this->Settings->find('all')->where(['Settings.name' => 'KID_PROFILE_NOT_COMPLETE'])->first();
        $subjectSupport = $emailMessageSupport->display;
        $toSupport = $this->Settings->find('all')->where(['name' => 'TO_BATCH_HELP'])->first()->value;

        foreach ($all_kidss as $kid) {
            if (!empty($kid->kids_first_name)) {
                $name = $kid->kids_first_name;
            }
            if (empty($kid->kids_first_name) || ($kid->is_progressbar < 100)) {
                if ($kid->kid_count == '1') {
                    $name = 'First Child';
                }
                if ($kid->kid_count == '2') {
                    $name = 'Second Child';
                }
                if ($kid->kid_count == '3') {
                    $name = 'Third Child';
                }
                if ($kid->kid_count == '4') {
                    $name = 'Four Child';
                }
                $client = $kid->usr;
                $to = $client->email;
                $from = $fromMail->value;
                $subject = "Complete your " . $name . " profile";
                $sitename = SITE_NAME;
                $mail_msg = "Complete your " . $name . " profile";
                $message = $this->Custom->userProfileComplete($emailMessage->value, $client->name, $mail_msg, $sitename);
                $messageSupport = $this->Custom->userProfileComplete($emailMessageSupport->value, $client->name, $mail_msg, $sitename);
                if ($this->Custom->sendEmail($toSupport, $from, $subjectSupport, $messageSupport)) {
                    $supportstatus = "Send";
                } else {
                    $supportstatus = "Faild";
                }
                if ($this->Custom->sendEmail($to, $from, $subject, $message)) {
                    $this->KidsDetails->updateAll(['kidProfileNotComplete' => 1], ['id' => $kid->id]);
                    $process = 'kidProfileNotComplete()';
                    $name = $client->name;
                    $kidId = $kid_id;
                    $kidName = $kidsname;
                    $email = $to;
                    $subject = $subject;
                    $status = 'Send';
                    $userId = $user_id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $subjectSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                } else {
                    $process = 'kidProfileNotComplete()';
                    $name = $client->name;
                    $kidId = $kid_id;
                    $kidName = $kidsname;
                    $email = $to;
                    $subject = $subject;
                    $status = 'faild';
                    $userId = $user_id;
                    $client = 'Client';
                    $upportemail = $toSupport;
                    $support_subject = $toSupport;
                    $supportstatus = $supportstatus;
                    $this->Custom->batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status);
                }
            }
        }

        exit;
    }

  public function authorizeCreditCard($arr_data = []) {

        extract($arr_data);

        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();

        $merchantAuthentication->setName(\SampleCodeConstants::MERCHANT_LOGIN_ID);

        $merchantAuthentication->setTransactionKey(\SampleCodeConstants::MERCHANT_TRANSACTION_KEY);

        $refId = 'ref' . time();

        $creditCard = new AnetAPI\CreditCardType();

        $creditCard->setCardNumber($card_number);

        $creditCard->setExpirationDate($exp_date);

        $creditCard->setCardCode($card_code);

        $paymentOne = new AnetAPI\PaymentType();

        $paymentOne->setCreditCard($creditCard);

        $order = new AnetAPI\OrderType();

        $order->setInvoiceNumber($invice);

        $order->setDescription($product);

        $customerAddress = new AnetAPI\CustomerAddressType();

        $customerAddress->setFirstName($first_name);

        $customerAddress->setLastName($last_name);

        $customerAddress->setCompany($companyName);

        $customerAddress->setAddress($address);

        $customerAddress->setCity($city);

        $customerAddress->setState($state);

        $customerAddress->setZip($zip);

        $customerAddress->setCountry($country);

        $customerData = new AnetAPI\CustomerDataType();

        $customerData->setType("individual");

        $customerData->setId("99999456654");

        $customerData->setEmail($email);

        $duplicateWindowSetting = new AnetAPI\SettingType();

        $duplicateWindowSetting->setSettingName("duplicateWindow");

        $duplicateWindowSetting->setSettingValue("60");

        $merchantDefinedField1 = new AnetAPI\UserFieldType();

        $merchantDefinedField1->setName("Drapefit Inc");

        $merchantDefinedField1->setValue("2093065");

        $merchantDefinedField2 = new AnetAPI\UserFieldType();

        $merchantDefinedField2->setName("favoriteColor");

        $merchantDefinedField2->setValue("blue");

        $transactionRequestType = new AnetAPI\TransactionRequestType();

        $transactionRequestType->setTransactionType("authOnlyTransaction");

        $transactionRequestType->setAmount($amount);

        $transactionRequestType->setOrder($order);

        $transactionRequestType->setPayment($paymentOne);

        $transactionRequestType->setBillTo($customerAddress);

        $transactionRequestType->setCustomer($customerData);

        $transactionRequestType->addToTransactionSettings($duplicateWindowSetting);

        $transactionRequestType->addToUserFields($merchantDefinedField1);

        $transactionRequestType->addToUserFields($merchantDefinedField2);

        $request = new AnetAPI\CreateTransactionRequest();

        $request->setMerchantAuthentication($merchantAuthentication);

        $request->setRefId($refId);

        $request->setTransactionRequest($transactionRequestType);

        $controller = new AnetController\CreateTransactionController($request);

        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

        $msg = array();

        if ($response != null) {

            if ($response->getMessages()->getResultCode() == 'Ok') {

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {

                    $msg['status'] = 1;

                    $msg['TransId'] = $tresponse->getTransId();

                    $msg['Success'] = " Successfully created transaction with Transaction ID: " . $tresponse->getTransId() . "\n";

                    $msg['ResponseCode'] = " Transaction Response Code: " . $tresponse->getResponseCode() . "\n";

                    $msg['MessageCode'] = " Message Code: " . $tresponse->getMessages()[0]->getCode() . "\n";

                    $msg['AuthCode'] = " Auth Code: " . $tresponse->getAuthCode() . "\n";

                    $msg['Description'] = " Description: " . $tresponse->getMessages()[0]->getDescription() . "\n";



                    $msg['msg'] = " Description: " . $tresponse . "\n";
                } else {

                    if ($tresponse->getErrors() != null) {

                        $msg['ErrorCode'] = " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";

                        $msg['ErrorMessage'] = "Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                    }
                }
            } else {

                $msg['error'] = 'error';

                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getErrors() != null) {

                    $msg['ErrorCode'] = " Error Code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";

                    $msg['ErrorCode'] = " Error Message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
                } else {

                    $msg['ErrorCode'] = " Error Code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";

                    $msg['ErrorMessage'] = " Error Message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
                }
            }
        } else {

            echo "No response returned \n";
        }

        return $msg;
    }

 public function processInvateryProdcut() {
        $previousworklist = $this->PaymentGetways->find('all')->where(['PaymentGetways.payment_type' => 1, 'PaymentGetways.work_status' => 2, 'PaymentGetways.status' => 1]);
        if (!empty($previousworklist)) {
            foreach ($previousworklist as $woklist) {

                if (!empty($woklist->id)) {
                   
                    if ($woklist->profile_type == 1) {
                        $productList = $this->Products->find('all')->where(['payment_id' => $woklist->id, 'keep_status In' => [1, 2]]);
                        if (!empty($productList)) {
                            foreach ($productList as $dt) {
                                $productCheck = $this->InProducts->find('all')->where(['prodcut_id' => $dt->id])->first();
                                if (@$productCheck->id == '') {
                                    $c = $this->InProducts->newEntity();
                                    $data1['user_id'] = '49';
                                    $data1['profile_type'] = 1;
                                    $data1['product_name_one'] = $dt->product_name_one;
                                    $data1['product_name_two'] = $dt->product_name_two;
                                    $data1['brand_id'] = 49;
                                    $data1['tall_feet'] = $this->Custom->tallFeet($dt->user_id, 1);
                                    $data1['tall_inch'] = $this->Custom->tellInch($dt->user_id, 1);
                                    $data1['best_fit_for_weight'] = $this->Custom->weightLbs($dt->user_id, 1);
                                    $data1['best_size_fit'] = $this->Custom->bestSizeFit($dt->user_id);
                                    $data1['waist_size'] = $this->Custom->waistSize($dt->user_id);
                                    $data1['waist_size_run'] = $this->Custom->waistSizeRun($dt->user_id);
                                    $data1['shirt_size'] = $this->Custom->shirtSize($dt->user_id);
                                    $data1['shirt_size_run'] = $this->Custom->shirtSizeRun($dt->user_id);
                                    $data1['inseam_size'] = $this->Custom->inseamSize($dt->user_id);
                                    $data1['shoe_size'] = $this->Custom->shoeSize($dt->user_id, 1);
                                    $data1['shoe_size_run'] = $this->Custom->shoeSizeRun($dt->user_id);
                                    $data1['better_body_shape'] = $this->Custom->betterBodyShape($dt->user_id,1);
                                    $data1['skin_tone'] = $this->Custom->skinTone($dt->user_id, 1);
                                    $data1['work_type'] = $this->Custom->workType($dt->user_id, 1);
                                    $data1['casual_shirts_type'] = $this->Custom->casualShirtsType($dt->user_id);
                                    $data1['bottom_up_shirt_fit'] = $this->Custom->bottomUpShirtFit($dt->user_id);
                                    $data1['jeans_Fit'] = $this->Custom->jeansFit($dt->user_id);
                                    $data1['shorts_long'] = 0;
                                    $data1['color'] = 0;
                                    $data1['outfit_matches'] = 0;
                                    $data1['pants'] = 0;
                                    $data1['bra'] = 0;
                                    $data1['bra_recomend'] = 0;
                                    $data1['skirt'] = 0;
                                    $data1['jeans'] = 0;
                                    $data1['dress'] = 0;
                                    $data1['dress_recomended'] = 0;
                                    $data1['shirt_blouse'] = 0;
                                    $data1['shirt_blouse_recomend'] = 0;
                                    $data1['pantsr1'] = 0;
                                    $data1['womenHeelHightPrefer'] = 0;
                                    $data1['proportion_shoulders'] = 0;
                                    $data1['proportion_legs'] = 0;
                                    $data1['proportion_arms'] = 0;
                                    $data1['proportion_hips'] = 0;
                                    $data1['top_size'] = 0;
                                    $data1['purchase_price'] = $dt->sell_price;;
                                    $data1['quantity'] = 1;
                                    $data1['note'] = 0;
                                    $data1['product_image'] = $dt->product_image;
                                    $data1['available_status'] = 1;
                                    $data1['is_active'] = 1;
                                    $data1['created'] = date('Y-m-d H:i:s');
                                    $data1['prodcut_id'] = $dt->id;
                                    $c = $this->InProducts->patchEntity($c, $data1);
                                    $this->InProducts->save($c);
                                }
                            }
                        }
                    }
                } if ($woklist->profile_type == 2) {
                    //echo $woklist->id;
                    
                    $productList = $this->Products->find('all')->where(['payment_id' => $woklist->id, 'keep_status In' => [1, 2]]);
                    if (!empty($productList)) {
                        foreach ($productList as $dt) {
                            
                            $productCheck = $this->InProducts->find('all')->where(['prodcut_id' => $dt->id])->first();
                            if (@$productCheck->id == '') {
                                
                                $c = $this->InProducts->newEntity();
                                $data1['user_id'] = '49';
                                $data1['profile_type'] = 2;
                                $data1['product_name_one'] = $dt->product_name_one;
                                $data1['product_name_two'] = $dt->product_name_two;
                                $data1['brand_id'] = 49;
                                $data1['tall_feet'] = $this->Custom->tallFeet($dt->user_id, 2);
                                $data1['tall_inch'] = $this->Custom->tellInch($dt->user_id, 2);
                                $data1['best_fit_for_weight'] = $this->Custom->weightLbs($dt->user_id, 2);
                                $data1['best_size_fit'] = 0;
                                $data1['waist_size'] = 0;
                                $data1['waist_size_run'] = 0;
                                $data1['shirt_size'] = 0;
                                $data1['shirt_size_run'] = 0;
                                $data1['inseam_size'] = 0;
                                $data1['shoe_size'] = $this->Custom->shoeSize($dt->user_id, 2);
                                $data1['shoe_size_run'] = 0;
                                $data1['better_body_shape'] = $this->Custom->betterBodyShape($dt->user_id, 2);
                                $data1['skin_tone'] = $this->Custom->skinTone($dt->user_id, 2);
                                $data1['work_type'] = $this->Custom->workType($dt->user_id, 2);
                                $data1['casual_shirts_type'] = 0;
                                $data1['bottom_up_shirt_fit'] = 0;
                                $data1['jeans_Fit'] = 0;
                                $data1['shorts_long'] = 0;
                                $data1['color'] = $this->Custom->color($dt->user_id, 2);
                                $data1['outfit_matches'] = 0;
                                $data1['pants'] = $this->Custom->pants($dt->user_id, 2);
                                $data1['bra'] = $this->Custom->bra($dt->user_id);
                                $data1['bra_recomend'] = $this->Custom->braRecomend($dt->user_id);
                                $data1['skirt'] = $this->Custom->skirt($dt->user_id);
                                $data1['jeans'] = $this->Custom->jeans($dt->user_id);
                                $data1['dress'] = $this->Custom->dress($dt->user_id);
                                $data1['dress_recomended'] = $this->Custom->dressRecomended($dt->user_id);
                                $data1['shirt_blouse'] = $this->Custom->shirtBlouse($dt->user_id);
                                $data1['shirt_blouse_recomend'] = $this->Custom->shirtBlouseRecomend($dt->user_id);
                                $data1['pantsr1'] = $this->Custom->pantsr1($dt->user_id);
                                $data1['womenHeelHightPrefer'] = $this->Custom->womenHeelHightPrefer($dt->user_id);
                                $data1['proportion_shoulders'] = $this->Custom->proportionShoulders($dt->user_id);
                                $data1['proportion_legs'] = $this->Custom->proportionLegs($dt->user_id);
                                $data1['proportion_arms'] = $this->Custom->proportionArms($dt->user_id);
                                $data1['proportion_hips'] = $this->Custom->proportionHips($dt->user_id);
                                $data1['top_size'] = 0;
                                $data1['purchase_price'] = $dt->sell_price;
                                $data1['quantity'] = 1;
                                $data1['note'] = '';
                                $data1['product_image'] = $dt->product_image;
                                $data1['available_status'] = 1;
                                $data1['is_active'] = 1;
                                $data1['created'] = date('Y-m-d H:i:s');
                                $data1['prodcut_id'] = $dt->id;
                                $c = $this->InProducts->patchEntity($c, $data1);
                                //pj($c);
                                $this->InProducts->save($c);
                            }
                        }
                    }
                } if ($woklist->profile_type == 3) {
                    
                    $productList = $this->Products->find('all')->where(['payment_id' => $woklist->id, 'keep_status In' => [1, 2]]);
                    if (!empty($productList)) {
                        foreach ($productList as $dt) {
                            $productCheck = $this->InProducts->find('all')->where(['prodcut_id' => $dt->id])->first();
                            if (@$productCheck->id == '') {
                                $c = $this->InProducts->newEntity();
                                $data1['user_id'] = '49';
                                $data1['profile_type'] = $this->Custom->getProfileKid($dt->kid_id);
                                $data1['product_name_one'] = $dt->product_name_one;
                                $data1['product_name_two'] = $dt->product_name_two;
                                $data1['brand_id'] = 49;
                                $data1['tall_feet'] = $this->Custom->tallFeetkid($dt->kid_id);
                                $data1['tall_inch'] = $this->Custom->tellInchkid($dt->kid_id);
                                $data1['best_fit_for_weight'] = $this->Custom->weightLbskid($dt->kid_id);
                                $data1['best_size_fit'] = $this->Custom->bestSizeFitkid($dt->kid_id);
                                $data1['waist_size'] = 0;
                                $data1['waist_size_run'] = 0;
                                $data1['shirt_size'] = 0;
                                $data1['shirt_size_run'] = 0;
                                $data1['inseam_size'] = 0;
                                $data1['shoe_size'] = $this->Custom->shoeSizekid($dt->kid_id);
                                $data1['shoe_size_run'] = 0;
                                $data1['better_body_shape'] = $this->Custom->betterBodyShapekid($dt->kid_id);
                                $data1['skin_tone'] = 0;
                                $data1['work_type'] = 0;
                                $data1['casual_shirts_type'] = 0;
                                $data1['bottom_up_shirt_fit'] = 0;
                                $data1['jeans_Fit'] = 0;
                                $data1['shorts_long'] = 0;
                                $data1['color'] = $this->Custom->colorkid($dt->kid_id);
                                $data1['outfit_matches'] = 0;
                                $data1['pants'] = $this->Custom->paintskid($dt->kid_id);
                                $data1['bra'] = 0;
                                $data1['bra_recomend'] = 0;
                                $data1['skirt'] = $this->Custom->skirtkid($dt->kid_id);
                                $data1['jeans'] = $this->Custom->jeanskid($dt->kid_id);
                                $data1['dress'] = 0;
                                $data1['dress_recomended'] = 0;
                                $data1['shirt_blouse'] = 0;
                                $data1['shirt_blouse_recomend'] = 0;
                                $data1['pantsr1'] = 0;
                                $data1['womenHeelHightPrefer'] = 0;
                                $data1['proportion_shoulders'] = 0;
                                $data1['proportion_legs'] = 0;
                                $data1['proportion_arms'] = 0;
                                $data1['proportion_hips'] = 0;
                                $data1['top_size'] = $this->Custom->topSizekid($dt->kid_id);
                                $data1['purchase_price'] = $dt->sell_price;
                                $data1['quantity'] = 1;
                                $data1['note'] = 0;
                                $data1['product_image'] = $dt->product_image;
                                $data1['available_status'] = 0;
                                $data1['is_active'] = 1;
                                $data1['created'] = date('Y-m-d H:i:s');
                                $data1['prodcut_id'] = $dt->id;
                                $c = $this->InProducts->patchEntity($c, $data1);
                                $this->InProducts->save($c);
                            }
                        }
                    }
                }
            }
        }
        exit;
    }

}
