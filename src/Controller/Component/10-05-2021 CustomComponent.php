<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;

class CustomComponent extends Component {

    function __construct($prompt = null) {
        
    }

   function getProfile($paymentId = null) {
        $table = TableRegistry::get('PaymentGetways');
        $query = $table->find('all')->where(['id' => $paymentId])->first();
        if (!empty($query->profile_type)) {
            return $query->profile_type;
        } else {
            return 1;
        }
    }
    
    function getProfileKid($id = null) {
        $table = TableRegistry::get('kidsDetails');
        $query = $table->find('all')->where(['id' => $id])->first();
        if ($query->kids_clothing_gender=='boys') {
            return 3;
        } else if($query->kids_clothing_gender=='girls') {
            return 4;
        }
    }

    function isstylefee($paymentId = null) {
        $table = TableRegistry::get('PaymentGetways');
        $query = $table->find('all')->where(['id' => $paymentId])->first()->is_style_fee;
        return $query;
    }

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    function formatText($value) {
        $value = str_replace("“", "\"", $value);
        $value = str_replace("�?", "\"", $value);
        //$value = preg_replace('/[^(\x20-\x7F)\x0A]*/','', $value);
        $value = stripslashes($value);
        $value = html_entity_decode($value, ENT_QUOTES);
        $trans = get_html_translation_table(HTML_ENTITIES, ENT_QUOTES);
        $value = strtr($value, $trans);
        $value = stripslashes(trim($value));
        return $value;
    }

    function shortLength($value, $len) {
        $value_format = $this->formatText($value);
        $value_raw = html_entity_decode($value_format, ENT_QUOTES);
        if (strlen($value_raw) > $len) {
            $value_strip = substr($value_raw, 0, $len);
            $value_strip = $this->formatText($value_strip);
            $lengthvalue = "<span title='" . $value_format . "' rel='tooltip'>" . $value_strip . "...</span>";
        } else {
            $lengthvalue = $value_format;
        }
        return $lengthvalue;
    }

    function makeSeoUrl($url) {
        if ($url) {
            $url = trim($url);
            $value = preg_replace("![^a-z0-9]+!i", "-", $url);
            $value = trim($value, "-");
            return strtolower($value);
        }
    }

    function generateUniqNumber($id = NULL) {
        $uniq = uniqid(rand());
        if ($id) {
            return md5($uniq . time() . $id);
        } else {
            return md5($uniq . time());
        }
    }

    function generateUniqGiftCode($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }

    function getRealIpAddress() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    function get_ip_address() {
        if (isSet($_SERVER)) {
            if (isSet($_SERVER["HTTP_X_FORWARDED_FOR"])) {
                $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
            } elseif (isSet($_SERVER["HTTP_CLIENT_IP"])) {
                $realip = $_SERVER["HTTP_CLIENT_IP"];
            } else {
                $realip = $_SERVER["REMOTE_ADDR"];
            }
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $realip = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP')) {
                $realip = getenv('HTTP_CLIENT_IP');
            } else {
                $realip = getenv('REMOTE_ADDR');
            }
        }



        return $realip;
    }

    function uploadImage($tmp_name, $name, $large, $medium, $thumb) {
        if ($name) {
            $image = strtolower($name);
            //          $extname = substr(strrchr($image, "."), 1);

            $extname = $this->getExtension($image);

            if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'bmp')) {

                return false;
            } else {

                if ($extname == "jpg" || $extname == "jpeg") {

                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "png") {

                    $src = imagecreatefrompng($tmp_name);
                } else {

                    $src = imagecreatefromgif($tmp_name);
                }



                list($width, $height) = getimagesize($tmp_name);





                $newwidth = 500;

                $newheight = ($height / $width) * $newwidth;

                $tmp = imagecreatetruecolor($newwidth, $newheight);



                $newwidth1 = 291;

                $newheight1 = ($height / $width) * $newwidth1;

                $tmp1 = imagecreatetruecolor($newwidth1, $newheight1);



                $newwidth2 = 100;

                $newheight2 = ($height / $width) * $newwidth2;

                $tmp2 = imagecreatetruecolor($newwidth2, $newheight2);

                imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                imagecopyresampled($tmp1, $src, 0, 0, 0, 0, $newwidth1, $newheight1, $width, $height);

                imagecopyresampled($tmp2, $src, 0, 0, 0, 0, $newwidth2, $newheight2, $width, $height);

                $time = time();

                $filepath = md5($time) . "." . $extname;

                $filename = $large . $filepath;

                $filename1 = $medium . "medium_" . $filepath;

                $filename2 = $thumb . "thumb_" . $filepath;

                imagejpeg($tmp, $filename, 100);



                imagejpeg($tmp1, $filename1, 100);



                imagejpeg($tmp2, $filename2, 100);



                imagedestroy($src);

                imagedestroy($tmp);

                imagedestroy($tmp1);

                imagedestroy($tmp2);



                return $filepath;
            }
        }
    }

    function uploadImageBanner($tmp_name, $name, $path, $imgWidth) {

        if ($name) {

            $image = strtolower($name);

            $extname = $this->getExtension($image); //$extname = substr(strrchr($image, "."), 1);

            if (($extname != 'jfif') && ($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'bmp')) {

                return false;
            } else {

                if ($extname == "jpg" || $extname == "jpeg") {

                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "jfif" || $extname == "JFIF") {

                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "png") {

                    $src = imagecreatefrompng($tmp_name);
                } else {
                    $src = imagecreatefromgif($tmp_name);
                }

                list($width, $height) = getimagesize($tmp_name);



                if ($extname == 'gif' || $width <= $imgWidth) {

                    $time = time() . rand(100, 999);

                    $filepath = md5($time) . "." . $extname;

                    $targetpath = $path . $filepath;

                    if (!is_dir($path)) {

                        mkdir($path);
                    }

                    move_uploaded_file($tmp_name, $targetpath);

                    return $filepath;
                } else {

                    $newwidth = $imgWidth;

                    $newheight = ($height / $width) * $newwidth;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                    $time = time();

                    $filepath = md5($time) . "." . $extname;

                    $filename = $path . $filepath;

                    imagejpeg($tmp, $filename, 100);



                    imagedestroy($src);

                    imagedestroy($tmp);

                    return $filepath;
                }
            }
        }
    }

    function uploadBlogImage($tmp_name, $name, $path, $imgWidth) {

        if ($name) {

            $image = strtolower($name);

            $extname = $this->getExtension($image); //$extname = substr(strrchr($image, "."), 1);

            if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'bmp')) {

                return false;
            } else {

                if ($extname == "jpg" || $extname == "jpeg") {

                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "png") {

                    $src = imagecreatefrompng($tmp_name);
                } else {

                    $src = imagecreatefromgif($tmp_name);
                }

                list($width, $height) = getimagesize($tmp_name);



                if ($extname == 'gif' || $width <= $imgWidth) {

                    $time = time() . rand(100, 999);

                    $filepath = md5($time) . "." . $extname;

                    $targetpath = $path . $filepath;

                    if (!is_dir($path)) {

                        mkdir($path);
                    }

                    move_uploaded_file($tmp_name, $targetpath);

                    return $filepath;
                } else {

                    $newwidth = $imgWidth;

                    $newheight = ($height / $width) * $newwidth;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                    $time = time();

                    $filepath = md5($time) . "." . $extname;

                    $filename = $path . $filepath;

                    imagejpeg($tmp, $filename, 100);



                    imagedestroy($src);

                    imagedestroy($tmp);

                    return $filepath;
                }
            }
        }
    }

    function uploadAvatarImage($tmp_name, $name, $path, $imgWidth) {

        if ($name) {

            $image = strtolower($name);

            $extname = $this->getExtension($image); //$extname = substr(strrchr($image, "."), 1);

            if (($extname != 'gif') && ($extname != 'jpg') && ($extname != 'jpeg') && ($extname != 'png') && ($extname != 'bmp')) {

                return false;
            } else {

                if ($extname == "jpg" || $extname == "jpeg") {

                    $src = imagecreatefromjpeg($tmp_name);
                } else if ($extname == "png") {

                    $src = imagecreatefrompng($tmp_name);
                } else {

                    $src = imagecreatefromgif($tmp_name);
                }

                list($width, $height) = getimagesize($tmp_name);



                if ($extname == 'gif' || $width <= $imgWidth) {

                    $time = time() . rand(100, 999);

                    $filepath = md5($time) . '1' . "." . $extname;

                    $targetpath = $path . $filepath;

                    if (!is_dir($path)) {

                        mkdir($path);
                    }

                    move_uploaded_file($tmp_name, $targetpath);

                    return $filepath;
                } else {

                    $newwidth = $imgWidth;

                    $newheight = ($height / $width) * $newwidth;

                    $tmp = imagecreatetruecolor($newwidth, $newheight);

                    imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                    $time = time();

                    $filepath = md5($time) . '1' . "." . $extname;

                    $filename = $path . $filepath;

                    imagejpeg($tmp, $filename, 100);



                    imagedestroy($src);

                    imagedestroy($tmp);

                    return $filepath;
                }
            }
        }
    }

    function lastValue($string) {

        $explode = explode('-', $string);

        $lastArrayValue = end($explode);

        return $lastArrayValue;
    }

    function number_pad($number, $n = 4) {

        $number = intval($number, 10);

        $number = (string) $number;

        return str_pad((int) $number, $n, "0", STR_PAD_LEFT);
    }

    function emailText($value) {

        $value = stripslashes(trim($value));

        $value = str_replace('"', "\"", $value);

        $value = str_replace('"', "\"", $value);

        $value = preg_replace('/[^(\x20-\x7F)\x0A]*/', '', $value);

        return stripslashes($value);
    }

    function paymentCanceLTemplete($msg, $name, $ticket, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[TICKET_NO]")) {

            $msg = str_replace("[TICKET_NO]", $ticket, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", $sitename, $msg);
        }

        return $msg;
    }

    function paymentSuccessTemplete($msg, $name, $ticket, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[TICKET_NO]")) {

            $msg = str_replace("[TICKET_NO]", $ticket, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", $sitename, $msg);
        }

        return $msg;
    }

    function contactUs($msg, $name, $email, $phone, $subject, $body_subject, $uMessage, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[PHONE]")) {

            $msg = str_replace("[PHONE]", $phone, $msg);
        }

        if (strstr($msg, "[SUBJECT]")) {

            $msg = str_replace("[SUBJECT]", $subject, $msg);
        }

        if (strstr($msg, "[BODY_SUBJECT]")) {

            $msg = str_replace("[BODY_SUBJECT]", $body_subject, $msg);
        }

        if (strstr($msg, "[UMSG]")) {

            $msg = str_replace("[UMSG]", $uMessage, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", $sitename, $msg);
        }

        return $msg;
    }

    function customerContactUs($msg, $name, $email, $phone, $subject1, $body_subject, $uMessage, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[PHONE]")) {

            $msg = str_replace("[PHONE]", $phone, $msg);
        }

        if (strstr($msg, "[SUBJECT]")) {

            $msg = str_replace("[SUBJECT]", $subject1, $msg);
        }

        if (strstr($msg, "[BODY_SUBJECT]")) {

            $msg = str_replace("[BODY_SUBJECT]", $body_subject, $msg);
        }

        if (strstr($msg, "[UMSG]")) {

            $msg = str_replace("[UMSG]", $uMessage, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", $sitename, $msg);
        }

        return $msg;
    }

    function sendEmail($to, $from, $subject, $message, $header = 1, $footer = 1) {

        if ($header) {

            $hdr = '';
        }

        if ($footer) {



            $ftr = '';
        }



        //echo $from;exit;

        $subscripbe = '';

        if ($to) {

            $table = TableRegistry::get('Users');

            @$unique = $table->find('all')->where(['email' => $to])->first()->unique_id;

            @$userId = $table->find('all')->where(['email' => $to])->first()->id;

            $subscripbe = '<a href="' . SITE_NAME . 'unsubscrib?id=' . @$unique . '" target="_blank" style="text-algin:center;color:#777777;text-decoration:underline;" >Unsubscribe </a>&nbsp;&nbsp;';
        }

        if (strstr($message, "[SUBCRIBE]")) {

            $message = str_replace("[SUBCRIBE]", $subscripbe, $message);
        }







        $message = $hdr . $message . $ftr;

        $to = $this->emailText($to);

        $subject = $this->emailText($subject);

        $message = $this->emailText($message);
        $message = str_replace("<script>", "&lt;script&gt;", $message);
        $message = str_replace("</script>", "&lt;/script&gt;", $message);

        $message = str_replace("<SCRIPT>", "&lt;script&gt;", $message);

        $message = str_replace("</SCRIPT>", "&lt;/script&gt;", $message);
        //$message=str_replace(".","8228",$message);
        //   // $email = new Email('default');
        //     $email = new Email();
        //     //$email->transport('default');
        //     $res = $email->from($from)
        //             ->to($to)
        //             ->emailFormat('html')
        //             ->subject($subject)
        //             ->viewVars(array('$msg' => $message))
        //             ->send($message);
        // $headers = "From: support@drapefittest.com" . "\r\n"."CC: debmicrofinet@gmail.com";
        // // $headers = 'MIME-Version: 1.0' . "\r\n";
        // // $headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        // // $headers.= 'From:' . $from . "\r\n";
        // if (mail($to, 'subject', 'hello', $headers)) {
        //     return true;
        // } else {
        //     return false;
        // }
        //$headers.= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        ////$headers.= 'From: Drepefittest<' . $from . "> \r\n";
        //  $headers .= 'Content-type: text/html; UTF-8' . "";
        // $headers = 'From: Drepefittest<' . $from . "> \r\n";
        // $headers .= "Reply-To: " . $from . "\r\n";
        // $headers .= "Return-Path: " . $from . "\r\n";
        // $headers .= "CC: suprakash8906@gmail.com\r\n";
        // $headers .= "BCC: suprakash8906@gmail.com\r\n";
        // $headers .= 'Content-type: text/html; UTF-8' . "";
        // $headers .= "X-Priority: 3\r\n";
        // $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
        // if (mail($to, $subject, $message, $headers, ' -f' . $from)) {
        //     return true;
        // } else {
        //     return false;
        // }
        $email = new Email();
        $email->transport('mailjet');
        //$email->transport('default');

        $res = $email->from($from)
                ->to($to)
                ->bcc('devadash143@gmail.com')
                ->emailFormat('html')
                ->subject($subject)
                ->viewVars(array('$msg' => $message))
                ->send($message);
        return $res;
    }

    function filterData($data) {

        /* this function is meant for filtering whole data received from the screen */

        $filteredData = array_map(function($v) {

            if (is_array($v)) {

                return $this->filterData($v);
            } else {

                return trim(strip_tags($v));
            }
        }, $data);



        return $filteredData;
    }

    function formatForgetPassword($msg, $name, $email, $link, $site_name) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $link, $msg);
        }

        if (strstr($msg, "[SITELINK]")) {

            $msg = str_replace("[SITELINK]", HTTP_ROOT, $msg);
        }

        if (strstr($msg, "[SITENAME]")) {

            $msg = str_replace("[SITENAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function helpformat($msg, $name, $email, $message, $date) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[MSG]")) {

            $msg = str_replace("[MSG]", $message, $msg);
        }



        if (strstr($msg, "[DATE]")) {

            $msg = str_replace("[DATE]", $date, $msg);
        }

        return $msg;
    }

    function helpclientformat($msg, $name, $email, $message, $date) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[MSG]")) {

            $msg = str_replace("[MSG]", $message, $msg);
        }



        if (strstr($msg, "[DATE]")) {

            $msg = str_replace("[DATE]", $date, $msg);
        }

        return $msg;
    }

    function helpCustomerformat($msg, $name, $email, $message, $date) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[MSG]")) {

            $msg = str_replace("[MSG]", $message, $msg);
        }



        if (strstr($msg, "[DATE]")) {

            $msg = str_replace("[DATE]", $date, $msg);
        }

        return $msg;
    }

    function createAdminFormat($msg, $name, $email, $pwd, $site_name, $url_link = null) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[PASSWORD]")) {

            $msg = str_replace("[PASSWORD]", $pwd, $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $url_link, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function kidProfileStart($msg, $name, $kidname, $sitename, $kidlink) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KIDNAME]")) {

            $msg = str_replace("[KIDNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $kidlink, $msg);
        }

        return $msg;
    }

    function kidProfileComplete($msg, $name, $kidname, $sitename, $kidlink) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KIDNAME]")) {

            $msg = str_replace("[KIDNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $kidlink, $msg);
        }

        return $msg;
    }

    function yourSubscription($msg, $name, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function yourKidsSubscription($msg, $name, $kidname, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KIDNAME]")) {

            $msg = str_replace("[KIDNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentEmail($msg, $name, $message, $site_name, $transaction_id = null, $price = null, $submit_date = null, $card_name = null, $last_4_digit = null, $usr_name = null, $full_address = null, $feeprice = null) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $message, $msg);
        }

        if (strstr($msg, "[TRANSACTION_ID]")) {

            $msg = str_replace("[TRANSACTION_ID]", $transaction_id, $msg);
        }

        if (strstr($msg, "[AMOUNT]")) {

            $msg = str_replace("[AMOUNT]", $price, $msg);
        }

        if (strstr($msg, "[SUBMITTED_DATE]")) {

            $msg = str_replace("[SUBMITTED_DATE]", $submit_date, $msg);
        }

        if (strstr($msg, "[CARD_NAME]")) {

            $msg = str_replace("[CARD_NAME]", $card_name, $msg);
        }

        if (strstr($msg, "[LAST_FOUR_DIGIT]")) {

            $msg = str_replace("[LAST_FOUR_DIGIT]", $last_4_digit, $msg);
        }

        if (strstr($msg, "[USER_NAME]")) {

            $msg = str_replace("[USER_NAME]", $usr_name, $msg);
        }

        if (strstr($msg, "[USER_ADDRESS]")) {

            $msg = str_replace("[USER_ADDRESS]", $full_address, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        if (strstr($msg, "[STYLEFEE]")) {

            $msg = str_replace("[STYLEFEE]", $feeprice, $msg);
        }

        return $msg;
    }

    function paymentEmailCount($msg, $name, $message, $site_name, $paymentCount) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $message, $msg);
        }

        if (strstr($msg, "[COUNT]")) {

            $msg = str_replace("[COUNT]", $paymentCount, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function referenceEmail($msg, $name, $message, $site_name, $refer) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $message, $msg);
        }



        if (strstr($msg, "[REFER_NAME]")) {

            $msg = str_replace("[REFER_NAME]", "<a href='" . HTTP_ROOT . 'refer/' . $refer . "'>Click here </a>", $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function create_image($name) {

        $im = @imagecreate(200, 200) or die("Cannot Initialize new GD image stream");

        $background_color = imagecolorallocate($im, 255, 255, 0);  // yellow

        imagepng($im, BARCODE . $name);

        imagedestroy($im);
    }

    function create_profile_image($name) {

        $im = @imagecreate(200, 200) or die("Cannot Initialize new GD image stream");

        $background_color = imagecolorallocate($im, 255, 255, 0);  // yellow

        imagepng($im, BARCODE_PROFILE . $name);

        imagedestroy($im);
    }

    function getKidsId($userId = null) {

        $table = TableRegistry::get('Products');

        $query = $table->find('all')->where(['Products.user_id' => $userId, 'Products.kid_id !=' => 0])->select(['Products.kid_id']);

//        pj($query);exit;

        return $query;
    }

    function order($msg, $name, $site_name, $productData, $total, $subtotal, $style_fit, $kip_al, $refundamount, $gtotal, $offerData) {
        if (strstr($msg, "[CUSTOMER_NAME]")) {
            $msg = str_replace("[CUSTOMER_NAME]", $name, $msg);
        }
        if (strstr($msg, "[NAME]")) {
            $msg = str_replace("[NAME]", $name, $msg);
        }
        if (strstr($msg, "[SITE_NAME]")) {
            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }
        if (strstr($msg, "[PRODUCTDATA]")) {
            $msg = str_replace("[PRODUCTDATA]", $productData, $msg);
        }
        if (strstr($msg, "[TOTAL]")) {
            $msg = str_replace("[TOTAL]", $total, $msg);
        }
        if (strstr($msg, "[SUBTOTAL]")) {
            $msg = str_replace("[SUBTOTAL]", number_format($subtotal, 2), $msg);
        }
        if (strstr($msg, "[KEEP_ALL]")) {
            $msg = str_replace("[KEEP_ALL]", $kip_al, $msg);
        }
        if (strstr($msg, "[STYLE_FIT]")) {
            $msg = str_replace("[STYLE_FIT]", $style_fit, $msg);
        }
//        if (strstr($msg, "[REFUND]")) {
//            $msg = str_replace("[REFUND]", number_format($refundamount, 2), $msg);
//        }
        if (strstr($msg, "[GTOTAL]")) {
            $msg = str_replace("[GTOTAL]", number_format($gtotal, 2), $msg);
        }
        if (strstr($msg, "[OFFERDATA]")) {
            $msg = str_replace("[OFFERDATA]", $offerData, $msg);
        }
        return $msg;
    }

    function promocodesend($msg, $promocode, $price, $comment, $sitename, $created = null, $expiry = null) {

        if (strstr($msg, "[PROMOCODE]")) {

            $msg = str_replace("[PROMOCODE]", $promocode, $msg);
        }

        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[COMMENT]")) {

            $msg = str_replace("[COMMENT]", $comment, $msg);
        }

        if (strstr($msg, "[CREATED_DATE]")) {

            $msg = str_replace("[CREATED_DATE]", $created, $msg);
        }

        if (strstr($msg, "[EXPIRY_DATE]")) {

            $msg = str_replace("[EXPIRY_DATE]", $expiry, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function giftcodesend($msg, $giftcode, $price, $comment, $sitename, $expiry = null) {

        if (strstr($msg, "[GIFTCODE]")) {

            $msg = str_replace("[GIFTCODE]", $giftcode, $msg);
        }

        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[COMMENT]")) {

            $msg = str_replace("[COMMENT]", $comment, $msg);
        }

        if (strstr($msg, "[EXPIRY_DATE]")) {

            $msg = str_replace("[EXPIRY_DATE]", $expiry, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function orderK($msg, $name, $site_name, $customer_data, $kid_data, $detailsKid, $total, $subtotal, $sales_tax) {



        $usersData = '';

        $style_pick_total = 0;

        $i = 1;

        //pj($detailsKid); exit;

        foreach ($customer_data as $customer_data_review) {



            if ($customer_data_review->keep_status == 3) {

                $price = $customer_data_review->sell_price;
            } else {

                $price = 0;
            }



            if ($customer_data_review->keep_status == 3) {

                $keep = 'Keeps';
            } elseif ($customer_data_review->keep_status == 2) {

                $keep = 'Exchange';
            } else {

                $keep = 'Return';
            }

            $style_pick_total += (double) $customer_data_review->sell_price;

            $usersData .= "<tr style='border-bottom: 1px solid black;text-align: left;'>

                        <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               #or " . $i++ . "

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               <img src='" . HTTP_ROOT . PRODUCT_IMAGES . $customer_data_review->product_image . "' style='width: 100px;'/>

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $customer_data_review->product_name_one . "

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $customer_data_review->product_name_two . "

                            </td>

                           <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                                " . $keep . "

                            </td>



                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $customer_data_review->size . "

                            </td>



                            <td style='text-align: center;padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               $" . number_format($price, 2) . "

                            </td>

                    </tr>";
        }



        $style_pick_totalkids = 0;

        $i = 1;

        $namek = '';

        foreach ($kid_data as $customer_data_review) {

            $namek .= "<tr><th colspan='5' style = 'text-align: center;'>

                       Kids Name:- " . $customer_data_review->kids_detail->kids_first_name .
                    " </th></tr>";

            $s = 1;

            foreach ($detailsKid[$customer_data_review->kid_id] as $kidsp) {



                if ($kidsp->keep_status == 3) {

                    $keep = 'Keeps';
                } elseif ($kidsp->keep_status == 2) {

                    $keep = 'Exchange';
                } else {

                    $keep = 'Return';
                }



                if ($kidsp->keep_status == 3) {

                    $price = $kidsp->sell_price;
                } else {

                    $price = 0;
                }





                $namek .= "<tr style='border-bottom: 1px solid black;text-align: left;'>

                        <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               # " . $s . "

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                              <img src='" . HTTP_ROOT . PRODUCT_IMAGES . $kidsp->product_image . "' style='width: 100px;'/>

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $kidsp->product_name_one . "

                            </td>

                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $kidsp->product_name_two . "

                            </td>

                           <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                                " . $keepkid . "

                            </td>



                            <td style='padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               " . $kidsp->size . "

                            </td>



                            <td style='text-align: center;padding: 10px 0px;border-bottom: 1px solid #ddd;'>

                               $" . number_format($kidsp->sell_price, 2) . "

                            </td>

                        </tr>";



                $s++;
            }

            $i++;
        }



















        $msg = "<div style='width: 100%;text-align: center;'>

    <h5 style='color:#5a5656;'>Your check out receipt</h5>

    <table align='center' style='width:90%;'>

        <thead>

            <tr>

                <th style='padding: 10px 0px;font-size:30px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;background: #000;'>

                    <span style='font-weight: 100;color: #fff;'>DrapeFit</span>

                </th>

            </tr>

            <tr>

                <th>

                    <h2 style='padding-top: 30px;margin:0;font-size: 30px;color:#d64ade'>Checkout Receipt</h2>

                    <h4 style='font-size: 13px;color:#5a5656;'>Here's the receipt</h4>

                </th>

            </tr>

        </thead>

        <tbody>

            <tr>

                <td>

                    <table style='width: 100%;text-align: right;'>

                        <tr >

                            <th  colspan='6' style='text-align: center;'>

                                Your Stylist Picks

                            </th>

                        </tr>

                        <tr style='border: 1px solid black;text-align: left;'>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Sno

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Image

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Product name 1

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Product name 2

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                KEEPING

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Size

                            </th>

                            <th style='padding: 10px 10px;border-bottom: 1px solid #434141;'>

                                Cost

                            </th>

                        </tr>

                         <tr>

                            <th colspan='5'  style = 'text-align: center;'>

                                Customer Name:- " . $name . "

                            </th>





                        </tr>" . $usersData . "





                        " . $namek . "





                        <tr>

                            <td colspan='5' style='text-align: right;padding-bottom: 20px;'>

                                Stylist Picks Subtotal

                            </td>

                            <td style='text-align: center;padding-bottom: 30px;padding-top: 10px;'>

                                $" . $subtotal . "

                            </td>

                        </tr>







                        <tr>

                            <td colspan='5' style='text-align: right;padding-bottom: 10px;'>

                                order Subtotal

                            </td>

                            <td style='text-align: center;padding-bottom: 10px;'>

                                $" . $subtotal . "

                            </td>

                        </tr>

                        <tr>

                            <td colspan='5' style='text-align: right;padding-bottom: 10px;'>

                                Sales tax

                            </td>

                            <td style='text-align: center;padding-bottom: 10px;'>

                                $" . $sales_tax . "

                            </td>

                        </tr>

                        <tr>

                            <td colspan='5 style='text-align: right;padding-bottom: 10px;border-bottom: 1px solid #ddd;'>

                                <strong>order total</strong>

                            </td>

                            <td style='text-align: center;padding-bottom: 10px;border-bottom: 1px solid #ddd;'>

                                $" . $total . "

                            </td>

                        </tr>

                        <tr>

                            <td colspan='5' style='text-align: center;padding-bottom: 120px;border-bottom: 1px solid #ddd;'>

                                <em><strong style='font-size: 13px;'>Thanks for letting us style you.</strong></em>

                            </td>

                        </tr>

                    </table>

                </td>

            </tr>

        </tbody>

    </table>

</div>";



        return $msg;
    }

    function timeElapsedString($datetime) {

        $now = date_create(date("Y-m-d H:i:s"));  //current date

        $your_date = date_create($datetime);   //Your Date

        $datediff = date_diff($now, $datetime);



        if ($datediff->format('%d') == 0) {

            $date = date_format($datetime, "H:i:s A");
        } else {

            $date = date_format($your_date, "M j, y");
        }

        return $date;
    }

    function EmployeeAssignedFormat($msg, $profile_name, $name, $site_name) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }



        if (strstr($msg, "[PNAME]")) {

            $msg = str_replace("[PNAME]", $profile_name, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function EmployeeAssignedKidFormat($msg, $profile_name, $name, $site_name, $kidname) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }



        if (strstr($msg, "[PNAME]")) {

            $msg = str_replace("[PNAME]", $profile_name, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }



        if (strstr($msg, "[KIDNAME]")) {

            $msg = str_replace("[KIDNAME]", $kidname, $msg);
        }

        return $msg;
    }

    function kidName($id) {
        $table = TableRegistry::get('KidsDetails');
        $query = $table->find('all')->where(['id' => $id])->first();
        $name = '';
        if (!empty($query->kids_first_name)) {
            $name = $query->kids_first_name;
        }
        return $name;
    }

//     function productFinalize($msg, $profile_name, $name, $site_name, $track_number = null, $purchase_date = null, $address1 = null, $address2 = null, $address3 = null) {
//
//        if (strstr($msg, "[NAME]")) {
//
//            $msg = str_replace("[NAME]", $name, $msg);
//        }
//
//
//
//        if (strstr($msg, "[PNAME]")) {
//
//            $msg = str_replace("[PNAME]", $profile_name, $msg);
//        }
//
//
//
//        if (strstr($msg, "[TRACK_NUMBER]")) {
//
//            $msg = str_replace("[TRACK_NUMBER]", $track_number, $msg);
//        }
//
//
//
//        if (strstr($msg, "[ARRIVE_DATE]")) {
//
//            $msg = str_replace("[ARRIVE_DATE]", $purchase_date, $msg);
//        }
//
//
//
//        if (strstr($msg, "[ADDRESS1]")) {
//
//            $msg = str_replace("[ADDRESS1]", $address1, $msg);
//        }
//
//        if (strstr($msg, "[ADDRESS]")) {
//
//            $msg = str_replace("[ADDRESS]", $address3, $msg);
//        }
//
//
//
//        if (strstr($msg, "[STATE_PIN_COUNTRY]")) {
//
//            $msg = str_replace("[STATE_PIN_COUNTRY]", $address2, $msg);
//        }
//
//
//
//        if (strstr($msg, "[SITE_NAME]")) {
//
//            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
//        }
//
//        return $msg;
//    }

    function productFinalize($msg, $profile_name, $STYLEIST_NAME, $track_number, $PRODUCT_DETAILS, $FITCOUNT, $SUB_TOTAL, $ALL_KEEP_PERCENTAGE, $PERCENTAGE_VALUE, $ORDER_SUB_TOTAL, $STYLE_FIT, $ORDER_TOTAL, $sitename, $SITE_NAME_LINK) {
        if (strstr($msg, "[NAME]")) {
            $msg = str_replace("[NAME]", $name, $msg);
        }
        if (strstr($msg, "[PNAME]")) {
            $msg = str_replace("[PNAME]", $profile_name, $msg);
        }
        if (strstr($msg, "[TRACK_NUMBER]")) {
            $msg = str_replace("[TRACK_NUMBER]", $track_number, $msg);
        }
        if (strstr($msg, "[STYLEIST_NAME]")) {
            $msg = str_replace("[STYLEIST_NAME]", $STYLEIST_NAME, $msg);
        }
        if (strstr($msg, "[PRODUCT_DETAILS]")) {
            $msg = str_replace("[PRODUCT_DETAILS]", $PRODUCT_DETAILS, $msg);
        }
        if (strstr($msg, "[FITCOUNT]")) {
            $msg = str_replace("[FITCOUNT]", $FITCOUNT, $msg);
        }
        if (strstr($msg, "[SUB_TOTAL]")) {
            $msg = str_replace("[SUB_TOTAL]", $SUB_TOTAL, $msg);
        }
        if (strstr($msg, "[ALL_KEEP_PERCENTAGE]")) {
            $msg = str_replace("[ALL_KEEP_PERCENTAGE]", $ALL_KEEP_PERCENTAGE, $msg);
        }
        if (strstr($msg, "[PERCENTAGE_VALUE]")) {
            $msg = str_replace("[PERCENTAGE_VALUE]", $PERCENTAGE_VALUE, $msg);
        }
        if (strstr($msg, "[ORDER_SUB_TOTAL]")) {
            $msg = str_replace("[ORDER_SUB_TOTAL]", $ORDER_SUB_TOTAL, $msg);
        }
        if (strstr($msg, "[STYLE_FIT]")) {
            $msg = str_replace("[STYLE_FIT]", $STYLE_FIT, $msg);
        }
        if (strstr($msg, "[ORDER_TOTAL]")) {
            $msg = str_replace("[ORDER_TOTAL]", $ORDER_TOTAL, $msg);
        }
        if (strstr($msg, "[SITE_NAME_LINK]")) {
            $msg = str_replace("[SITE_NAME_LINK]", $SITE_NAME_LINK, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function encrypt_decrypt($action = null, $string = null) {

        $output = false;

        $encrypt_method = "AES-256-CBC";

        $secret_key = 'Debendra11dfg89we@wre#wejew12#gfsdg#gvxdc$hdbff%sjhd.swsw9760122';

        $secret_iv = 'debendraT22df67his@vcv89cvcvc#dsd12&ccv$v90ve12t%vbiv';

        // hash

        $key = hash('sha256', $secret_key);



        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {

            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);

            $output = base64_encode($output);
        }

        if ($action == 'decrypt') {

            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    function loginRedirectAjax($userId) {

        $table = TableRegistry::get('Users');

        $table2 = TableRegistry::get('UserDetails');

        $afterLoginCheck = $table->find('all')->where(['id' => $userId])->first();

        $afterLoginCheck2 = $table2->find('all')->where(['user_id' => $userId])->first();



        if ($afterLoginCheck->is_redirect == 0 && $afterLoginCheck2->is_progressbar != 100) {

            $url = 'welcome/style/';
        } elseif ($afterLoginCheck->is_redirect == 0 && $afterLoginCheck2->is_progressbar == 100) {



            $url = 'welcome/schedule/';
        } elseif ($afterLoginCheck->is_redirect == 1) {

            $url = 'welcome/schedule/';
        } elseif ($afterLoginCheck->is_redirect == 2) {

            $url = 'not-yet-shipped';
        } elseif ($afterLoginCheck->is_redirect == 3) {

            $url = 'profile-review/';
        } elseif ($afterLoginCheck->is_redirect == 4) {

            $url = 'order_review/';
        } elseif ($afterLoginCheck->is_redirect == 5) {

            $url = 'calendar-sechedule/';
        } elseif ($afterLoginCheck->is_redirect == 6) {

            $url = 'customer-order-review';
        }

        return $url;
    }

    function notifications($userId, $kid_id, $msg) {

        $notifications = TableRegistry::get('Notifications');

        $notificationsTable = $notifications->newEntity();

        $notificationsTable->user_id = $userId;

        $notificationsTable->$msg = $msg;

        $notificationsTable->is_read = 0;

        $notificationsTable->created = data('Y-m-d H:i:s');

        $notificationsTable->kid_id = $kid_id;

        $this->$notifications->save($notificationsTable);

        return 1;
    }

    function getAllMeg($errorCode = null) {

        $msg = '';

        if ($errorCode == 'I00001') {

            return $msg = "Successful";
        } else if ($errorCode == 'I00002') {

            return $msg = "The subscription has already been canceled.";
        } else if ($errorCode == 'I00003') {

            return $msg = "The record has already been deleted.";
        } else if ($errorCode == 'I00004') {

            return $msg = "No records found";
        } else if ($errorCode == 'I00005') {

            return $msg = "he mobile device has been submitted for approval by the account administrator.";
        } else if ($errorCode == 'I00006') {

            return $msg = "The mobile device is approved and ready for use";
        } else if ($errorCode == 'I00007') {

            return $msg = "The Payment Gateway Account service (id=8) has already been accepted.";
        } else if ($errorCode == 'I00008') {

            return $msg = "The Payment Gateway Account service (id=8) has already been declined.";
        } else if ($errorCode == 'I00009') {

            return $msg = "The APIUser already exists.";
        } else if ($errorCode == 'I00010') {

            return $msg = "The merchant is activated successfully";
        } else if ($errorCode == 'I00011') {

            return $msg = "The merchant is not activated.";
        } else if ($errorCode == 'I99999') {

            return $msg = "This feature has not yet been completed. One day it will be but it looks like today is not that day.";
        } else if ($errorCode == 'E00001') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == 'E00002') {

            return $msg = "The content-type specified is not supported.";
        } else if ($errorCode == 'E00003') {

            return $msg = "An error occurred while parsing the XML request.";
        } else if ($errorCode == 'E00004') {

            return $msg = "The name of the requested API method is invalid.";
        } else if ($errorCode == 'E00005') {

            return $msg = "The transaction key or API key is invalid or not present.";
        } else if ($errorCode == 'E00006') {

            return $msg = "The API user name is invalid or not present.";
        } else if ($errorCode == 'E00007') {

            return $msg = "User authentication failed due to invalid authentication values.";
        } else if ($errorCode == 'E00008') {

            return $msg = "User authentication failed. The account or API user is inactive.";
        } else if ($errorCode == 'E00009') {

            return $msg = "The payment gateway account is in Test Mode. The request cannot be processed.";
        } else if ($errorCode == 'E00010') {

            return $msg = "User authentication failed. You do not have the appropriate permissions";
        } else if ($errorCode == 'E00011') {

            return $msg = "Access denied. You do not have the appropriate permissions.";
        } else if ($errorCode == 'E00012') {

            return $msg = "A duplicate subscription already exists.";
        } else if ($errorCode == 'E00013') {

            return $msg = "The field is invalid";
        } else if ($errorCode == 'E00014') {

            return $msg = "A required field is not present.";
        } else if ($errorCode == 'E00015') {

            return $msg = "The field length is invalid.";
        } else if ($errorCode == 'E00016') {

            return $msg = "The field type is invalid.";
        } else if ($errorCode == 'E00017') {

            return $msg = "The start date cannot occur in the past.";
        } else if ($errorCode == 'E00018') {

            return $msg = "The credit card expires before the subscription start date.";
        } else if ($errorCode == 'E00019') {

            return $msg = "The customer tax id or drivers license information is required.";
        } else if ($errorCode == 'E00020') {

            return $msg = "The payment gateway account is not enabled for eCheck.Net subscriptions";
        } else if ($errorCode == 'E00021') {

            return $msg = "The payment gateway account is not enabled for credit card subscriptions.";
        } else if ($errorCode == 'E00022') {

            return $msg = "The interval length cannot exceed 365 days or 12 months";
        } else if ($errorCode == 'E00023') {

            return $msg = "The subscription duration cannot exceed three years";
        } else if ($errorCode == 'E00024') {

            return $msg = "Trial Occurrences is required when Trial Amount is specified.";
        } else if ($errorCode == 'E00025') {

            return $msg = "Automated Recurring Billing is not enabled.";
        } else if ($errorCode == 'E00026') {

            return $msg = "Both Trial Amount and Trial Occurrences are required.";
        } else if ($errorCode == 'E00027') {

            return $msg = "The transaction was unsuccessful.";
        } else if ($errorCode == 'E00028') {

            return $msg = "Trial Occurrences must be less than Total Occurrences.";
        } else if ($errorCode == 'E00029') {

            return $msg = "Payment information is required.";
        } else if ($errorCode == 'E00030') {

            return $msg = "The payment schedule is required.";
        } else if ($errorCode == 'E00031') {

            return $msg = "The amount is required.";
        } else if ($errorCode == 'E00032') {

            return $msg = "The start date is required.";
        } else if ($errorCode == 'E00033') {

            return $msg = "The start date cannot be changed.";
        } else if ($errorCode == 'E00034') {

            return $msg = "The interval information cannot be changed.";
        } else if ($errorCode == 'E00035') {

            return $msg = "The subscription cannot be found.";
        } else if ($errorCode == 'E00036') {

            return $msg = "The payment type cannot be changed.";
        } else if ($errorCode == 'E00037') {

            return $msg = "The subscription cannot be updated.";
        } else if ($errorCode == 'E00038') {

            return $msg = "The subscription cannot be canceled.";
        } else if ($errorCode == 'E00039') {

            return $msg = "A duplicate record already exists.";
        } else if ($errorCode == 'E00040') {

            return $msg = "The record cannot be found.";
        } else if ($errorCode == 'E00041') {

            return $msg = "One or more fields must contain a value.";
        } else if ($errorCode == 'E00042') {

            return $msg = "You cannot add more than {0} payment profiles.";
        } else if ($errorCode == 'E00043') {

            return $msg = "You cannot add more than {0} shipping addresses.";
        } else if ($errorCode == 'E00044') {

            return $msg = "Customer Information Manager is not enabled.";
        } else if ($errorCode == 'E00045') {

            return $msg = "The root node does not reference a valid XML namespace.";
        } else if ($errorCode == 'E00046') {

            return $msg = "Generic InsertNewMerchant failure.";
        } else if ($errorCode == 'E00047') {

            return $msg = "Merchant Boarding API is not enabled.";
        } else if ($errorCode == 'E00048') {

            return $msg = "At least one payment method must be set in payment types or an echeck service must be provided";
        } else if ($errorCode == 'E00049') {

            return $msg = "The operation timed out before it could be completed.";
        } else if ($errorCode == 'E00050') {

            return $msg = "Sell Rates cannot be less than Buy Rates";
        } else if ($errorCode == 'E00051') {

            return $msg = "The original transaction was not issued for this payment profile.";
        } else if ($errorCode == 'E00052') {

            return $msg = "The maximum number of elements for an array {0} is {1}.";
        } else if ($errorCode == 'E00053') {

            return $msg = "Server too busy";
        } else if ($errorCode == 'E00054') {

            return $msg = "The mobile device is not registered with this merchant account.";
        } else if ($errorCode == 'E00055') {

            return $msg = "The mobile device has already been registered but is pending approval by the account administrator.";
        } else if ($errorCode == 'E00056') {

            return $msg = "The mobile device has been disabled for use with this account.";
        } else if ($errorCode == 'E00057') {

            return $msg = "The user does not have permissions to submit requests from a mobile device.";
        } else if ($errorCode == 'E00058') {

            return $msg = "The merchant has met or exceeded the number of pending mobile devices permitted for this account.";
        } else if ($errorCode == 'E00059') {

            return $msg = "The authentication type is not allowed for this method call.";
        } else if ($errorCode == 'E00060') {

            return $msg = "The transaction type is invalid.";
        } else if ($errorCode == 'E00062') {

            return $msg = "Fatal error when calling web service.";
        } else if ($errorCode == 'E00063') {

            return $msg = "Calling web service return error.";
        } else if ($errorCode == 'E00064') {

            return $msg = "Client authorization denied.";
        } else if ($errorCode == 'E00065') {

            return $msg = "Prerequisite failed.";
        } else if ($errorCode == 'E00066') {

            return $msg = "Invalid value.";
        } else if ($errorCode == 'E00067') {

            return $msg = "An error occurred while parsing the XML request. Too many {0} specified.";
        } else if ($errorCode == 'E00068') {

            return $msg = "An error occurred while parsing the XML request. {0} is invalid.";
        } else if ($errorCode == 'E00069') {

            return $msg = "The Payment Gateway Account service (id=8) has already been accepted. Decline is not allowed.";
        } else if ($errorCode == 'E00070') {

            return $msg = "The Payment Gateway Account service (id=8) has already been declined. Agree is not allowed.";
        } else if ($errorCode == 'E00071') {

            return $msg = "{0} must contain data.";
        } else if ($errorCode == 'E00072') {

            return $msg = "Required node missing.";
        } else if ($errorCode == 'E00073') {

            return $msg = "One of the field values is not valid.";
        } else if ($errorCode == 'E00074') {

            return $msg = "This merchant is not associated with this reseller.";
        } else if ($errorCode == 'E00075') {

            return $msg = "This is the result of an XML parser error. Missing field(s).";
        } else if ($errorCode == 'E00076') {

            return $msg = "Invalid value.";
        } else if ($errorCode == 'E00077') {

            return $msg = "Value too long.";
        } else if ($errorCode == 'E00078') {

            return $msg = "Pending Status (not completed).";
        } else if ($errorCode == 'E00079') {

            return $msg = "The impersonation login ID is invalid or not present.";
        } else if ($errorCode == 'E00080') {

            return $msg = "The impersonation API Key is invalid or not present.";
        } else if ($errorCode == 'E00081') {

            return $msg = "Partner account is not authorized to impersonate the login account.";
        } else if ($errorCode == 'E00082') {

            return $msg = "Country is not valid.";
        } else if ($errorCode == 'E00083') {

            return $msg = "Bank payment method is not accepted for the selected business country.";
        } else if ($errorCode == 'E00084') {

            return $msg = "Credit card payment method is not accepted for the selected business country.";
        } else if ($errorCode == 'E00085') {

            return $msg = "State for {0} is not valid.";
        } else if ($errorCode == 'E00086') {

            return $msg = "Merchant has declined authorization to resource.";
        } else if ($errorCode == 'E00087') {

            return $msg = "No subscriptions found for the given request.";
        } else if ($errorCode == 'E00088') {

            return $msg = "ProfileIds cannot be sent when requesting CreateProfile";
        } else if ($errorCode == 'E00089') {

            return $msg = "Payment data is required when requesting CreateProfile.";
        } else if ($errorCode == 'E00090') {

            return $msg = "PaymentProfile and PaymentData are mutually exclusive, only one of them can be provided at a time.";
        } else if ($errorCode == 'E00091') {

            return $msg = "PaymentProfileId cannot be sent with payment data.";
        } else if ($errorCode == 'E00092') {

            return $msg = "ShippingProfileId cannot be sent with ShipTo data.";
        } else if ($errorCode == 'E00093') {

            return $msg = "PaymentProfile cannot be sent with billing data.";
        } else if ($errorCode == 'E00094') {

            return $msg = "Paging Offset exceeds the maximum allowed value";
        } else if ($errorCode == 'E00095') {

            return $msg = "ShippingProfileId is not provided within Customer Profile.";
        } else if ($errorCode == 'E00096') {

            return $msg = "Finger Print value is not valid.";
        } else if ($errorCode == 'E00097') {

            return $msg = "Finger Print can't be generated.";
        } else if ($errorCode == 'E00098') {

            return $msg = "Customer Profile ID or Shipping Profile ID not found.";
        } else if ($errorCode == 'E00099') {

            return $msg = "Customer profile creation failed. This transaction ID is invalid.";
        } else if ($errorCode == 'E00100') {

            return $msg = "Customer profile creation failed. This transaction type does not support profile creation.";
        } else if ($errorCode == 'E00101') {

            return $msg = "Customer profile creation failed.";
        } else if ($errorCode == 'E00102') {

            return $msg = "Customer Info is missing.";
        } else if ($errorCode == 'E00103') {

            return $msg = "Customer profile creation failed. This payment method does not support profile creation";
        } else if ($errorCode == 'E00104') {

            return $msg = "Server in maintenance. Please try again later.";
        } else if ($errorCode == 'E00105') {

            return $msg = "The specified payment profile is associated with an active or suspended subscription and cannot be deleted.";
        } else if ($errorCode == 'E00106') {

            return $msg = "The specified customer profile is associated with an active or suspended subscription and cannot be deleted";
        } else if ($errorCode == 'E00107') {

            return $msg = "The specified shipping profile is associated with an active or suspended subscription and cannot be deleted.";
        } else if ($errorCode == 'E00108') {

            return $msg = "CustomerProfileId cannot be sent with customer data";
        } else if ($errorCode == 'E00109') {

            return $msg = "CustomerAddressId cannot be sent with shipTo data.";
        } else if ($errorCode == 'E00110') {

            return $msg = "CustomerPaymentProfileId is not provided within Customer Profile.";
        } else if ($errorCode == 'E00111') {

            return $msg = "The original subscription was not created with this Customer Profile.";
        } else if ($errorCode == 'E00112') {

            return $msg = "The specified month should not be in the future.";
        } else if ($errorCode == 'E00113') {

            return $msg = "Invalid OTS Token Data.";
        } else if ($errorCode == 'E00114') {

            return $msg = "Invalid OTS Token.";
        } else if ($errorCode == 'E00115') {

            return $msg = "Expired OTS Token.";
        } else if ($errorCode == 'E00116') {

            return $msg = "OTS Token access violation";
        } else if ($errorCode == 'E00117') {

            return $msg = "OTS Service Error '{0}'";
        } else if ($errorCode == 'E00118') {

            return $msg = "The transaction has been declined.";
        } else if ($errorCode == 'E00119') {

            return $msg = "Payment information should not be sent to Hosted Payment Page request.";
        } else if ($errorCode == 'E00120') {

            return $msg = "Payment and Shipping Profile IDs cannot be specified when creating new profiles.";
        } else if ($errorCode == 'E00121') {

            return $msg = "No default payment/shipping profile found.";
        } else if ($errorCode == 'E00122') {

            return $msg = "Please use Merchant Interface settings (API Credentials and Keys) to generate a signature key.";
        } else if ($errorCode == 'E00123') {

            return $msg = "The provided access token has expired";
        } else if ($errorCode == 'E00124') {

            return $msg = "The provided access token is invalid";
        } else if ($errorCode == 'E00125') {

            return $msg = "Hash doesnâ€™t match";
        } else if ($errorCode == 'E00126') {

            return $msg = "Failed shared key validation";
        } else if ($errorCode == 'E00127') {

            return $msg = "Invoice does not exist";
        } else if ($errorCode == 'E00128') {

            return $msg = "Requested action is not allowed";
        } else if ($errorCode == 'E00129') {

            return $msg = "Failed sending email";
        } else if ($errorCode == 'E00130') {

            return $msg = "Valid Customer Profile ID or Email is required";
        } else if ($errorCode == 'E00131') {

            return $msg = "Invoice created but not processed completely";
        } else if ($errorCode == 'E00132') {

            return $msg = "Invoicing or CIM service is not enabled.";
        } else if ($errorCode == 'E00133') {

            return $msg = "Server error.";
        } else if ($errorCode == 'E00134') {

            return $msg = "Due date is invalid";
        } else if ($errorCode == 'E00135') {

            return $msg = "Merchant has not provided processor information.";
        } else if ($errorCode == 'E00136') {

            return $msg = "Processor account is still in process, please try again later.";
        } else if ($errorCode == 'E00137') {

            return $msg = "Multiple payment types are not allowed.";
        } else if ($errorCode == 'E00138') {

            return $msg = "Payment and Shipping Profile IDs cannot be specified when requesting a hosted payment page.";
        } else if ($errorCode == 'E00139') {

            return $msg = "Access denied. Access Token does not have correct permissions for this API.";
        } else if ($errorCode == 'E00140') {

            return $msg = "Reference Id not found";
        } else if ($errorCode == 'E00141') {

            return $msg = "Payment Profile creation with this OpaqueData descriptor requires transactionMode to be set to liveMode.";
        } else if ($errorCode == 'E00142') {

            return $msg = "RecurringBilling setting is a required field for recurring tokenized payment transactions.";
        } else if ($errorCode == 'E00143') {

            return $msg = "Failed to parse MerchantId to integer";
        } else if ($errorCode == 'E00144') {

            return $msg = "We are currently holding the last transaction for review. Before you reactivate the subscription, review the transaction.";
        } else if ($errorCode == 'E00145') {

            return $msg = "This invoice has been canceled by the sender. Please contact the sender directly if you have questions. ";
        } else if ($errorCode == '0') {

            return $msg = "Unknown Error";
        } else if ($errorCode == '1') {

            return $msg = "This transaction has been approved.";
        } else if ($errorCode == '2') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '3') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '4') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '5') {

            return $msg = "A valid amount is required.";
        } else if ($errorCode == '6') {

            return $msg = "The credit card number is invalid.";
        } else if ($errorCode == '7') {

            return $msg = "Credit card expiration date is invalid.";
        } else if ($errorCode == '8') {

            return $msg = "The credit card has expired";
        } else if ($errorCode == '9') {

            return $msg = "The ABA code is invalid";
        } else if ($errorCode == '10') {

            return $msg = "The account number is invalid";
        } else if ($errorCode == '11') {

            return $msg = "A duplicate transaction has been submitted.";
        } else if ($errorCode == '12') {

            return $msg = "An authorization code is required but not present.";
        } else if ($errorCode == '13') {

            return $msg = "The merchant login ID or password is invalid or the account is inactive. 	";
        } else if ($errorCode == '14') {

            return $msg = "The referrer, relay response or receipt link URL is invalid.";
        } else if ($errorCode == '15') {

            return $msg = "The transaction ID is invalid or not present.";
        } else if ($errorCode == '16') {

            return $msg = "The transaction cannot be found.";
        } else if ($errorCode == '17') {

            return $msg = "The merchant does not accept this type of credit card.";
        } else if ($errorCode == '18') {

            return $msg = "ACH transactions are not accepted by this merchant.";
        } else if ($errorCode == '19') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '20') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '21') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '22') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '23') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '24') {

            return $msg = "The Elavon bank number or terminal ID is incorrect. Call Merchant Service Provider.";
        } else if ($errorCode == '25') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '26') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '27') {

            return $msg = "The transaction has been declined because of an AVS mismatch. The address provided does not match billing address of cardholder.";
        } else if ($errorCode == '28') {

            return $msg = "The merchant does not accept this type of credit card.";
        } else if ($errorCode == '29') {

            return $msg = "The Paymentech identification numbers are incorrect. Call Merchant Service Provider.";
        } else if ($errorCode == '30') {

            return $msg = "The configuration with processor is invalid. Call Merchant Service Provider.";
        } else if ($errorCode == '31') {

            return $msg = "The FDC Merchant ID or Terminal ID is incorrect. Call Merchant Service Provider.";
        } else if ($errorCode == '32') {

            return $msg = "The merchant password is invalid or not present.";
        } else if ($errorCode == '33') {

            return $msg = "%s cannot be left blank.";
        } else if ($errorCode == '34') {

            return $msg = "The VITAL identification numbers are incorrect. Call Merchant Service Provider.";
        } else if ($errorCode == '35') {

            return $msg = "An error occurred during processing. Call Merchant Service Provider.";
        } else if ($errorCode == '36') {

            return $msg = "The authorization was approved but settlement failed.";
        } else if ($errorCode == '37') {

            return $msg = "The credit card number is invalid.";
        } else if ($errorCode == '38') {

            return $msg = "The Global Payment System identification numbers are incorrect. Call Merchant Service Provider.";
        } else if ($errorCode == '39') {

            return $msg = "The supplied currency code is either invalid, not supported, not allowed for this merchant or doesnt have an exchange rate.";
        } else if ($errorCode == '40') {

            return $msg = "This transaction must be encrypted.";
        } else if ($errorCode == '41') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '42') {

            return $msg = "There is missing or invalid information in a required field.";
        } else if ($errorCode == '43') {

            return $msg = "The merchant was incorrectly set up at the processor. Call Merchant Service Provider.";
        } else if ($errorCode == '44') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '45') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '46') {

            return $msg = "Your session has expired or does not exist. You must log in again to continue working.";
        } else if ($errorCode == '47') {

            return $msg = "The amount requested for settlement cannot be greater than the original amount authorized.";
        } else if ($errorCode == '48') {

            return $msg = "This processor does not accept partial reversals.";
        } else if ($errorCode == '49') {

            return $msg = "The transaction amount submitted was greater than the maximum amount allowed.";
        } else if ($errorCode == '50') {

            return $msg = "This transaction is awaiting settlement and cannot be refunded.";
        } else if ($errorCode == '51') {

            return $msg = "The sum of all credits against this transaction is greater than the original transaction amount.";
        } else if ($errorCode == '52') {

            return $msg = "The transaction was authorized but the client could not be notified; it will not be settled.";
        } else if ($errorCode == '53') {

            return $msg = "The transaction type is invalid for ACH transactions.";
        } else if ($errorCode == '54') {

            return $msg = "The referenced transaction does not meet the criteria for issuing a credit.";
        } else if ($errorCode == '55') {

            return $msg = "The sum of credits against the referenced transaction would exceed original debit amount.";
        } else if ($errorCode == '56') {

            return $msg = "Credit card transactions are not accepted by this merchant.";
        } else if ($errorCode == '57') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '58') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '59') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '60') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '61') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '62') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '63') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '64') {

            return $msg = "The referenced transaction was not approved.";
        } else if ($errorCode == '65') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '66') {

            return $msg = "This transaction cannot be accepted for processing.";
        } else if ($errorCode == '67') {

            return $msg = "This transaction cannot be accepted for processing.";
        } else if ($errorCode == '68') {

            return $msg = "The version parameter is invalid";
        } else if ($errorCode == '69') {

            return $msg = "The transaction type is invalid";
        } else if ($errorCode == '70') {

            return $msg = "The transaction method is invalid.";
        } else if ($errorCode == '71') {

            return $msg = "The bank account type is invalid.";
        } else if ($errorCode == '72') {

            return $msg = "The authorization code is invalid.";
        } else if ($errorCode == '73') {

            return $msg = "The drivers license date of birth is invalid.";
        } else if ($errorCode == '74') {

            return $msg = "The duty amount is invalid.";
        } else if ($errorCode == '75') {

            return $msg = "The freight amount is invalid.";
        } else if ($errorCode == '76') {

            return $msg = "The tax amount is invalid.";
        } else if ($errorCode == '77') {

            return $msg = "The SSN or tax ID is invalid.";
        } else if ($errorCode == '78') {

            return $msg = "The card code is invalid.";
        } else if ($errorCode == '79') {

            return $msg = "The drivers license number is invalid.";
        } else if ($errorCode == '80') {

            return $msg = "The drivers license state is invalid.";
        } else if ($errorCode == '81') {

            return $msg = "The requested form type is invalid.";
        } else if ($errorCode == '82') {

            return $msg = "Scripts are only supported in version 2.5.";
        } else if ($errorCode == '83') {

            return $msg = "The requested script is either invalid or no longer supported.";
        } else if ($errorCode == '84') {

            return $msg = "The device type is invalid or missing.";
        } else if ($errorCode == '85') {

            return $msg = "The market type is invalid";
        } else if ($errorCode == '86') {

            return $msg = "The Response Format is invalid";
        } else if ($errorCode == '87') {

            return $msg = "Transactions of this market type cannot be processed on this system.";
        } else if ($errorCode == '88') {

            return $msg = "Track1 data is not in a valid format.";
        } else if ($errorCode == '89') {

            return $msg = "Track2 data is not in a valid format.";
        } else if ($errorCode == '90') {

            return $msg = "ACH transactions cannot be accepted by this system.";
        } else if ($errorCode == '91') {

            return $msg = "Version 2.5 is no longer supported.";
        } else if ($errorCode == '92') {

            return $msg = "The gateway no longer supports the requested method of integration.";
        } else if ($errorCode == '93') {

            return $msg = "A valid country is required.";
        } else if ($errorCode == '94') {

            return $msg = "The shipping state or country is invalid.";
        } else if ($errorCode == '95') {

            return $msg = "A valid state is required.";
        } else if ($errorCode == '96') {

            return $msg = "This country is not authorized for buyers.";
        } else if ($errorCode == '97') {

            return $msg = "This transaction cannot be accepted.";
        } else if ($errorCode == '98') {

            return $msg = "This transaction cannot be accepted.";
        } else if ($errorCode == '99') {

            return $msg = "This transaction cannot be accepted.";
        } else if ($errorCode == '100') {

            return $msg = "The eCheck type parameter is invalid.";
        } else if ($errorCode == '101') {

            return $msg = "The given name on the account and/or the account type does not match the actual account.";
        } else if ($errorCode == '102') {

            return $msg = "This request cannot be accepted.";
        } else if ($errorCode == '103') {

            return $msg = "This transaction cannot be accepted.";
        } else if ($errorCode == '104') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '105') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '106') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '107') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '108') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '109') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '110') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '111') {

            return $msg = "A valid billing country is required.";
        } else if ($errorCode == '112') {

            return $msg = "A valid billing state/province is required.";
        } else if ($errorCode == '113') {

            return $msg = "The commercial card type is invalid.";
        } else if ($errorCode == '114') {

            return $msg = "The merchant account is in test mode. This automated payment will not be processed.";
        } else if ($errorCode == '115') {

            return $msg = "The merchant account is not active. This automated payment will not be processed.";
        } else if ($errorCode == '116') {

            return $msg = "The authentication indicator is invalid.";
        } else if ($errorCode == '117') {

            return $msg = "The cardholder authentication value is invalid.";
        } else if ($errorCode == '118') {

            return $msg = "The combination of card type, authentication indicator and cardholder authentication value is invalid.";
        } else if ($errorCode == '119') {

            return $msg = "Transactions having cardholder authentication values cannot be marked as recurring.";
        } else if ($errorCode == '120') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '121') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '122') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '123') {

            return $msg = "This account has not been given the permission(s) required for this request.";
        } else if ($errorCode == '124') {

            return $msg = "This processor does not accept recurring transactions.";
        } else if ($errorCode == '125') {

            return $msg = "The surcharge amount is invalid.";
        } else if ($errorCode == '126') {

            return $msg = "The Tip amount is invalid.";
        } else if ($errorCode == '127') {

            return $msg = "The transaction resulted in an AVS mismatch. The address provided does not match billing address of cardholder.";
        } else if ($errorCode == '128') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '130') {

            return $msg = "This merchant account has been closed.";
        } else if ($errorCode == '131') {

            return $msg = "This transaction cannot be accepted at this time.";
        } else if ($errorCode == '132') {

            return $msg = "This transaction cannot be accepted at this time.";
        } else if ($errorCode == '141') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '145') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '153') {

            return $msg = "There was an error processing the payment data.";
        } else if ($errorCode == '154') {

            return $msg = "Processing Apple Payments is not enabled for this merchant account";
        } else if ($errorCode == '155') {

            return $msg = "This processor does not support this method of submitting payment data.";
        } else if ($errorCode == '156') {

            return $msg = "The cryptogram is either invalid or cannot be used in combination with other parameters.";
        } else if ($errorCode == '157') {

            return $msg = "";
        } else if ($errorCode == '158') {

            return $msg = "";
        } else if ($errorCode == '159') {

            return $msg = "";
        } else if ($errorCode == '160') {

            return $msg = "";
        } else if ($errorCode == '161') {

            return $msg = "";
        } else if ($errorCode == '162') {

            return $msg = "";
        } else if ($errorCode == '163') {

            return $msg = "";
        } else if ($errorCode == '164') {

            return $msg = "";
        } else if ($errorCode == '165') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '166') {

            return $msg = "";
        } else if ($errorCode == '167') {

            return $msg = "";
        } else if ($errorCode == '168') {

            return $msg = "";
        } else if ($errorCode == '169') {

            return $msg = "";
        } else if ($errorCode == '170') {

            return $msg = "An error occurred during processing. Please contact the merchant.";
        } else if ($errorCode == '171') {

            return $msg = "An error occurred during processing. Please contact the merchant.";
        } else if ($errorCode == '172') {

            return $msg = "An error occurred during processing. Please contact the merchant.";
        } else if ($errorCode == '173') {

            return $msg = "An error occurred during processing. Please contact the merchant.";
        } else if ($errorCode == '174') {

            return $msg = "The transaction type is invalid. Please contact the merchant.";
        } else if ($errorCode == '175') {

            return $msg = "This processor does not allow voiding of credits.";
        } else if ($errorCode == '176') {

            return $msg = "";
        } else if ($errorCode == '177') {

            return $msg = "";
        } else if ($errorCode == '178') {

            return $msg = "";
        } else if ($errorCode == '179') {

            return $msg = "";
        } else if ($errorCode == '180') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '181') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '182') {

            return $msg = "One or more of the sub-merchant values are invalid.";
        } else if ($errorCode == '183') {

            return $msg = "One or more of the required sub-merchant values are missing.";
        } else if ($errorCode == '184') {

            return $msg = "Invalid Token Requestor Name.";
        } else if ($errorCode == '185') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '186') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '187') {

            return $msg = "Invalid Token Requestor ECI Length.";
        } else if ($errorCode == '188') {

            return $msg = "";
        } else if ($errorCode == '189') {

            return $msg = "";
        } else if ($errorCode == '190') {

            return $msg = "";
        } else if ($errorCode == '191') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '192') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '193') {

            return $msg = "The transaction is currently under review.";
        } else if ($errorCode == '194') {

            return $msg = "";
        } else if ($errorCode == '195') {

            return $msg = "One or more of the HTML type configuration fields do not appear to be safe.";
        } else if ($errorCode == '196') {

            return $msg = "";
        } else if ($errorCode == '197') {

            return $msg = "";
        } else if ($errorCode == '198') {

            return $msg = "";
        } else if ($errorCode == '199') {

            return $msg = "";
        } else if ($errorCode == '200') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '201') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '202') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '203') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '204') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '205') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '206') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '207') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '208') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '209') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '210') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '211') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '212') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '213') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '214') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '215') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '216') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '217') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '218') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '219') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '220') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '221') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '222') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '223') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '224') {

            return $msg = "This transaction has been declined";
        } else if ($errorCode == '225') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '226') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '227') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '228') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '229') {

            return $msg = "Conversion rate for this card is available.";
        } else if ($errorCode == '230') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '231') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '232') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '233') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '234') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '235') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '236') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '237') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '238') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '239') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '240') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '241') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '242') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '243') {

            return $msg = "Recurring billing is not allowed for this eCheck.Net type.";
        } else if ($errorCode == '244') {

            return $msg = "This eCheck.Net type is not allowed for this Bank Account Type.";
        } else if ($errorCode == '245') {

            return $msg = "This eCheck.Net type is not allowed when using the payment gateway hosted payment form.";
        } else if ($errorCode == '246') {

            return $msg = "This eCheck.Net type is not allowed.";
        } else if ($errorCode == '247') {

            return $msg = "This eCheck.Net type is not allowed.";
        } else if ($errorCode == '248') {

            return $msg = "The check number is invalid.";
        } else if ($errorCode == '250') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '251') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '252') {

            return $msg = "Your order has been received. Thank you for your business!";
        } else if ($errorCode == '253') {

            return $msg = "Your order has been received. Thank you for your business!";
        } else if ($errorCode == '254') {

            return $msg = "This transaction has been declined.";
        } else if ($errorCode == '260') {

            return $msg = "Reversal not supported for this transaction type.";
        } else if ($errorCode == '261') {

            return $msg = "An error occurred during processing. Please try again.";
        } else if ($errorCode == '265') {

            return $msg = "The PayformMask is invalid.";
        } else if ($errorCode == '270') {

            return $msg = "Line item %1 is invalid.";
        } else if ($errorCode == '271') {

            return $msg = "The number of line items submitted is not allowed. A maximum of %1 line items can be submitted.";
        } else if ($errorCode == '280') {

            return $msg = "The auction platform name is invalid.";
        } else if ($errorCode == '281') {

            return $msg = "The auction platform ID is invalid.";
        } else if ($errorCode == '282') {

            return $msg = "The auction listing type is invalid.";
        } else if ($errorCode == '283') {

            return $msg = "The auction listing ID is invalid.";
        } else if ($errorCode == '283') {

            return $msg = "The auction seller ID is invalid.";
        } else if ($errorCode == '285') {

            return $msg = "The auction buyer ID is invalid.";
        } else if ($errorCode == '286') {

            return $msg = "One or more required auction values were not submitted.";
        } else if ($errorCode == '287') {

            return $msg = "The combination of auction platform ID and auction platform name is invalid.";
        } else if ($errorCode == '288') {

            return $msg = "This transaction cannot be accepted.";
        } else if ($errorCode == '289') {

            return $msg = "This processor does not accept zero dollar authorization for this card type.";
        } else if ($errorCode == '290') {

            return $msg = "There is one or more missing or invalid required fields.";
        } else if ($errorCode == '295') {

            return $msg = "The amount of this request was only partially approved on the given prepaid card. An additional payment is required to fulfill the balance of this transaction.";
        } else if ($errorCode == '296') {

            return $msg = "The specified SplitTenderID is invalid.";
        } else if ($errorCode == '297') {

            return $msg = "Transaction ID and Split Tender ID cannot both be used in the same request.";
        } else if ($errorCode == '298') {

            return $msg = "This order has already been released or voided therefore new transaction associations cannot be accepted.";
        } else if ($errorCode == '300') {

            return $msg = "The device ID is invalid.";
        } else if ($errorCode == '301') {

            return $msg = "The device batch ID is invalid.";
        } else if ($errorCode == '302') {

            return $msg = "The reversal flag is invalid.";
        } else if ($errorCode == '303') {

            return $msg = "The device batch is full. Please close the batch.";
        } else if ($errorCode == '304') {

            return $msg = "The original transaction is in a closed batch.";
        } else if ($errorCode == '305') {

            return $msg = "The merchant is configured for auto-close.";
        } else if ($errorCode == '306') {

            return $msg = "The batch is already closed.";
        } else if ($errorCode == '307') {

            return $msg = "The reversal was processed successfully.";
        } else if ($errorCode == '308') {

            return $msg = "Original transaction for reversal not found.";
        } else if ($errorCode == '309') {

            return $msg = "The device has been disabled.";
        } else if ($errorCode == '310') {

            return $msg = "This transaction has already been voided.";
        } else if ($errorCode == '311') {

            return $msg = "This transaction has already been captured.";
        } else if ($errorCode == '312') {

            return $msg = "The specified security code was invalid.";
        } else if ($errorCode == '313') {

            return $msg = "A new security code was requested.";
        } else if ($errorCode == '314') {

            return $msg = "This transaction cannot be processed.";
        } else if ($errorCode == '315') {

            return $msg = "The credit card number is invalid.";
        } else if ($errorCode == '316') {

            return $msg = "Credit card expiration date is invalid.";
        } else if ($errorCode == '317') {

            return $msg = "The credit card has expired.";
        } else if ($errorCode == '318') {

            return $msg = "A duplicate transaction has been submitted.";
        } else if ($errorCode == '319') {

            return $msg = "The transaction cannot be found.";
        } else if ($errorCode == '320') {

            return $msg = "The device identifier is either not registered or not enabled.";
        } else if ($errorCode == '325') {

            return $msg = "The request data did not pass the required fields check for this application.";
        } else if ($errorCode == '326') {

            return $msg = "The request field(s) are either invalid or missing.";
        } else if ($errorCode == '327') {

            return $msg = "The void request failed. Either the original transaction type does not support void, or the transaction is in the process of being settled.";
        } else if ($errorCode == '328') {

            return $msg = "A validation error occurred at the processor.";
        } else if ($errorCode == '330') {

            return $msg = "V.me transactions are not accepted by this merchant.";
        } else if ($errorCode == '331') {

            return $msg = "The x_call_id value is missing.";
        } else if ($errorCode == '332') {

            return $msg = "The x_call_id value is not found or invalid.";
        } else if ($errorCode == '333') {

            return $msg = "A validation error was returned from V.me.";
        } else if ($errorCode == '334') {

            return $msg = "The V.me transaction is in an invalid state.";
        } else if ($errorCode == '339') {

            return $msg = "Use x_method to specify method or send only x_call_id or card/account information.";
        } else if ($errorCode == '340') {

            return $msg = "V.me by Visa does not support voids on captured or credit transactions. Please allow the transaction to settle, then process a refund for the captured transaction.";
        } else if ($errorCode == '341') {

            return $msg = "The x_discount value is invalid.";
        } else if ($errorCode == '342') {

            return $msg = "The x_giftwrap value is invalid.";
        } else if ($errorCode == '343') {

            return $msg = "The x_subtotal value is invalid.";
        } else if ($errorCode == '344') {

            return $msg = "The x_misc value is invalid.";
        } else if ($errorCode == '350') {

            return $msg = "Country must be a valid two or three-character value if specified.";
        } else if ($errorCode == '351') {

            return $msg = "Employee ID must be 1 to %x characters in length.";
        } else if ($errorCode == '355') {

            return $msg = "An error occurred while parsing the EMV data.";
        } else if ($errorCode == '356') {

            return $msg = "EMV-based transactions are not currently supported for this processor and card type.";
        } else if ($errorCode == '357') {

            return $msg = "Opaque Descriptor is required.";
        } else if ($errorCode == '358') {

            return $msg = "EMV data is not supported with this transaction type.";
        } else if ($errorCode == '359') {

            return $msg = "EMV data is not supported with this market type.";
        } else if ($errorCode == '360') {

            return $msg = "An error occurred while decrypting the EMV data.";
        } else if ($errorCode == '361') {

            return $msg = "The EMV version is invalid.";
        } else if ($errorCode == '362') {

            return $msg = "The EMV version is required.";
        } else if ($errorCode == '363') {

            return $msg = "The POS Entry Mode value is invalid.";
        } else if ($errorCode == '370') {

            return $msg = "Signature data is too large.";
        } else if ($errorCode == '371') {

            return $msg = "Signature must be PNG formatted data.";
        } else if ($errorCode == '375') {

            return $msg = "Terminal/lane number must be numeric.";
        } else if ($errorCode == '380') {

            return $msg = "KSN is duplicated.";
        } else if ($errorCode == '901') {

            return $msg = "This transaction cannot be accepted at this time due to system maintenance. Please try again later.";
        } else if ($errorCode == '2000') {

            return $msg = "Need payer consent.";
        } else if ($errorCode == '2001') {

            return $msg = "PayPal transactions are not accepted by this merchant.";
        } else if ($errorCode == '2002') {

            return $msg = "PayPal transactions require x_version of at least 3.1.";
        } else if ($errorCode == '2003') {

            return $msg = "Request completed successfully";
        } else if ($errorCode == '2004') {

            return $msg = "Success URL is required.";
        } else if ($errorCode == '2005') {

            return $msg = "Cancel URL is required.";
        } else if ($errorCode == '2006') {

            return $msg = "Payer ID is required.";
        } else if ($errorCode == '2007') {

            return $msg = "This processor does not accept zero dollar authorizations.";
        } else if ($errorCode == '2008') {

            return $msg = "The referenced transaction does not meet the criteria for issuing a Continued Authorization.";
        } else if ($errorCode == '2009') {

            return $msg = "The referenced transaction does not meet the criteria for issuing a Continued Authorization w/ Auto Capture.";
        } else if ($errorCode == '2100') {

            return $msg = "PayPal transactions require valid URL for success_url";
        } else if ($errorCode == '2101') {

            return $msg = "PayPal transactions require valid URL for cancel_url";
        } else if ($errorCode == '2102') {

            return $msg = "Payment not authorized. Payment has not been authorized by the user.";
        } else if ($errorCode == '2103') {

            return $msg = "This transaction has already been authorized.";
        } else if ($errorCode == '2104') {

            return $msg = "The totals of the cart item amounts do not match order amounts. Be sure the total of the payment detail item parameters add up to the order total.";
        } else if ($errorCode == '2105') {

            return $msg = "PayPal has rejected the transaction.Invalid Payer ID.";
        } else if ($errorCode == '2106') {

            return $msg = "PayPal has already captured this transaction.";
        } else if ($errorCode == '2107') {

            return $msg = "PayPal has rejected the transaction. This Payer ID belongs to a different customer.";
        } else if ($errorCode == '2108') {

            return $msg = "PayPal has rejected the transaction. x_paypal_hdrimg exceeds maximum allowable length.";
        } else if ($errorCode == '2109') {

            return $msg = "PayPal has rejected the transaction. x_paypal_payflowcolor must be a 6 character hexadecimal value.";
        } else if ($errorCode == '2200') {

            return $msg = "The amount requested for settlement cannot be different than the original amount authorized. Please void transaction if required";
        } else if ($msg == '820') {
            return $msg = " Please check your card details.";
        } else if ($msg == ' Error Message : The credit card has expired.\n') {
            return $msg = "The credit card has expired.";
        }

        return $msg;
    }

    function Refunded($msg, $price, $transctionsId, $name, $email, $rdate, $sitename, $message = null, $last_4_digit = null) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }



        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[TRANS]")) {

            $msg = str_replace("[TRANS]", $transctionsId, $msg);
        }

        if (strstr($msg, "[RDATE]")) {

            $msg = str_replace("[RDATE]", $rdate, $msg);
        }

        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }



        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $message, $msg);
        }

        if (strstr($msg, "[LAST_4_DIGIT]")) {

            $msg = str_replace("[LAST_4_DIGIT]", $last_4_digit, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function SubscriptionCancelationEmail($msg, $name, $email, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function SubscriptionActivatedEmail($msg, $name, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function KIdsSubscriptionActivatedEmail($msg, $name, $kidname, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KIDNAME]")) {

            $msg = str_replace("[KIDNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentFailedSupport($msg, $name, $email, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function styleFitFee() {

        $table = TableRegistry::get('Settings');

        $query = $table->find('all')->where(['name' => 'style_fee'])->first();

        $name = 0;

        if (!empty($query->value)) {

            $name = $query->value;
        }



        return $name;
    }

    function getPromoCode($paymentId = null) {
        $table = TableRegistry::get('UserAppliedCodeOrderReview');
        $query = $table->find('all')->where(['payment_id' => $paymentId])->order(['id' => 'ASC']);
        return $query;
    }

    function careersform($msg, $first_name, $last_name, $email, $phone, $location, $school, $degree, $discipline, $linkedin, $hearabt_job, $employee_referral, $compensation, $authorized_usa, $sponsorship_usa, $work_morning, $during_datetime, $work_evening, $work_weekend, $fulltime_capacity, $gender, $hispanic_latino, $veteran_status, $disability_status, $date) {
        if (strstr($msg, "[FIRSTNAME]")) {
            $msg = str_replace("[FIRSTNAME]", $first_name, $msg);
        }
        if (strstr($msg, "[LASTNAME]")) {
            $msg = str_replace("[LASTNAME]", $last_name, $msg);
        }
        if (strstr($msg, "[EMAIL]")) {
            $msg = str_replace("[EMAIL]", $email, $msg);
        }
        if (strstr($msg, "[PHONE]")) {
            $msg = str_replace("[PHONE]", $phone, $msg);
        }
        if (strstr($msg, "[LOCATION]")) {
            $msg = str_replace("[LOCATION]", $location, $msg);
        }
        if (strstr($msg, "[SCHOOL]")) {

            $msg = str_replace("[SCHOOL]", $school, $msg);
        }
        if (strstr($msg, "[DEGREE]")) {
            $msg = str_replace("[DEGREE]", $degree, $msg);
        }
        if (strstr($msg, "[DISCIPLINE]")) {
            $msg = str_replace("[DISCIPLINE]", $discipline, $msg);
        }
        if (strstr($msg, "[LINKEDIN]")) {

            $msg = str_replace("[LINKEDIN]", $linkedin, $msg);
        }
        if (strstr($msg, "[HRTABTJOB]")) {
            $msg = str_replace("[HRTABTJOB]", $hearabt_job, $msg);
        }
        if (strstr($msg, "[EMPLYREFRAL]")) {
            $msg = str_replace("[EMPLYREFRAL]", $employee_referral, $msg);
        }

        if (strstr($msg, "[AUTHUSA]")) {
            $msg = str_replace("[AUTHUSA]", $authorized_usa, $msg);
        }
        if (strstr($msg, "[SPNSUSA]")) {

            $msg = str_replace("[SPNSUSA]", $sponsorship_usa, $msg);
        }
        if (strstr($msg, "[MORNINGTIME]")) {
            $msg = str_replace("[MORNINGTIME]", $work_morning, $msg);
        }
        if (strstr($msg, "[DAYGTIME]")) {
            $msg = str_replace("[DAYGTIME]", $during_datetime, $msg);
        }
        if (strstr($msg, "[EVENINGTIME]")) {
            $msg = str_replace("[EVENINGTIME]", $work_evening, $msg);
        }
        if (strstr($msg, "[ONWEEKEND]")) {

            $msg = str_replace("[ONWEEKEND]", $work_weekend, $msg);
        }
        if (strstr($msg, "[INTERFULLTIME]")) {

            $msg = str_replace("[INTERFULLTIME]", $fulltime_capacity, $msg);
        }

        if (strstr($msg, "[GENDER]")) {

            $msg = str_replace("[GENDER]", $gender, $msg);
        }

        if (strstr($msg, "[HISLAT]")) {

            $msg = str_replace("[HISLAT]", $hispanic_latino, $msg);
        }

        if (strstr($msg, "[VETESTAT]")) {

            $msg = str_replace("[VETESTAT]", $veteran_status, $msg);
        }

        if (strstr($msg, "[DISBSTAT]")) {

            $msg = str_replace("[DISBSTAT]", $disability_status, $msg);
        }

        if (strstr($msg, "[DATE]")) {

            $msg = str_replace("[DATE]", $date, $msg);
        }

        return $msg;
    }

    function giftCardEmail($msg, $to, $name, $fromName, $fromMail, $price, $code, $expiry_date, $msSg, $sitename) {

        if (strstr($msg, "[TONAME]")) {

            $msg = str_replace("[TONAME]", $name, $msg);
        }
        if (strstr($msg, "[TOEMAIL]")) {

            $msg = str_replace("[TOEMAIL]", $to, $msg);
        }

        if (strstr($msg, "[FROMNAME]")) {

            $msg = str_replace("[FROMNAME]", $fromName, $msg);
        }

        if (strstr($msg, "[FROMEMAIL]")) {

            $msg = str_replace("[FROMEMAIL]", $fromMail, $msg);
        }
        if (strstr($msg, "[CODE]")) {

            $msg = str_replace("[CODE]", $code, $msg);
        }



        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[EXPIREON]")) {

            $msg = str_replace("[EXPIREON]", $expiry_date, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $msSg, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }



        return $msg;
    }

    function giftCardEmailSupport($msg, $to, $name, $fromName, $fromMail, $code, $price, $expiry_date, $msSg, $sitename) {

        if (strstr($msg, "[TONAME]")) {

            $msg = str_replace("[TONAME]", $toname, $msg);
        }

        if (strstr($msg, "[FROMNAME]")) {

            $msg = str_replace("[FROMNAME]", $fromname, $msg);
        }

        if (strstr($msg, "[FROMEMAIL]")) {

            $msg = str_replace("[FROMEMAIL]", $fromMail, $msg);
        }



        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[CODE]")) {

            $msg = str_replace("[CODE]", $code, $msg);
        }

        if (strstr($msg, "[EXPIREON]")) {

            $msg = str_replace("[EXPIREON]", $expiry_date, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $msSg, $msg);
        }



        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }



        return $msg;
    }

    function giftCardMail($msg, $toname, $fromname, $sitename, $price, $code, $expireon, $giftmessage) {

        if (strstr($msg, "[TONAME]")) {

            $msg = str_replace("[TONAME]", $toname, $msg);
        }

        if (strstr($msg, "[FROMNAME]")) {

            $msg = str_replace("[FROMNAME]", $fromname, $msg);
        }
        if (strstr($msg, "[GIFTCODE]")) {

            $msg = str_replace("[GIFTCODE]", $code, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[EXPIREON]")) {

            $msg = str_replace("[EXPIREON]", $expireon, $msg);
        }

        if (strstr($msg, "[MESSAGE]")) {

            $msg = str_replace("[MESSAGE]", $giftmessage, $msg);
        }

        return $msg;
    }

    function giftCardPrint($msg, $toname, $fromname, $sitename, $price, $code, $expireon) {

        if (strstr($msg, "[TONAME]")) {

            $msg = str_replace("[TONAME]", $toname, $msg);
        }

        if (strstr($msg, "[FROMNAME]")) {

            $msg = str_replace("[FROMNAME]", $fromname, $msg);
        }
        if (strstr($msg, "[CODE]")) {

            $msg = str_replace("[CODE]", $code, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        if (strstr($msg, "[PRICE]")) {

            $msg = str_replace("[PRICE]", $price, $msg);
        }

        if (strstr($msg, "[EXPIREON]")) {

            $msg = str_replace("[EXPIREON]", $expireon, $msg);
        }



        return $msg;
    }

    function userProfileComplete($msg, $name, $mail_msg, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MAIL_MESSAGE]")) {

            $msg = str_replace("[MAIL_MESSAGE]", $mail_msg, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function notPaidOnce($msg, $name, $mail_msg, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MAIL_MESSAGE]")) {

            $msg = str_replace("[MAIL_MESSAGE]", $mail_msg, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    /* Debendra 31-3-2020 */

    function paymentFaild($msg, $name, $link, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $link, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    /* 31-03-2020 end */

    function addressNotComplate($msg, $name, $mail_msg, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[MAIL_MESSAGE]")) {

            $msg = str_replace("[MAIL_MESSAGE]", $mail_msg, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentFailedSucritpions($msg, $name, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentFailedSucritpionsAdmin($msg, $name, $email, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function checkoutBetweenDays($msg, $name, $email, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[EMAIL]")) {

            $msg = str_replace("[EMAIL]", $email, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentFailedSucritpionsKids($msg, $name, $kidname, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KNAME]")) {

            $msg = str_replace("[KNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function paymentFaildKid($msg, $name, $kidname, $link, $sitename) {

        if (strstr($msg, "[NAME]")) {

            $msg = str_replace("[NAME]", $name, $msg);
        }

        if (strstr($msg, "[KNAME]")) {

            $msg = str_replace("[KNAME]", $kidname, $msg);
        }

        if (strstr($msg, "[LINK]")) {

            $msg = str_replace("[LINK]", $link, $msg);
        }

        if (strstr($msg, "[SITE_NAME]")) {

            $msg = str_replace("[SITE_NAME]", "<a href='" . HTTP_ROOT . "'>" . SITE_NAME . "</a>", $msg);
        }

        return $msg;
    }

    function kidsNumber($count) {

        if ($count == 1) {

            $data = "First child";
        }

        if ($count == 2) {

            $data = "Second child";
        }

        if ($count == 3) {

            $data = "Third child";
        }

        if ($count == 4) {

            $data = "Fourth child";
        }

        if ($count == 5) {

            $data = "Fifth child";
        }

        return $data;
    }

    function ChcckPaid($user_id) {
        $table = TableRegistry::get('PaymentGetways');
        $data = $table->find('all')->where(['user_id' => $user_id, 'payment_type' => 1, 'status' => 1, 'count' => 1, 'kid_id' => 0])->first();
        if (@$data->user_id != '') {
            @$result = $data->user_id;
        } else {
            @$result = 0;
        }
        return @$result;
    }

    function ChcckPaidKid($kid_id) {
        $id = 0;
        $table = TableRegistry::get('PaymentGetways');
        $data = $table->find('all')->where(['payment_type' => 1, 'status' => 1, 'count' => 1, 'kid_id' => $kid_id])->first();
        if (@$data->kid_id != '') {
            $id = $data->kid_id;
        } else {
            $id = '0';
        }
        return $id;
    }

    function UserGender($id) {
        $table = TableRegistry::get('UserDetails');
        $query = $table->find('all')->where(['user_id' => $id])->first();
        return @$query->gender;
    }

    function downloadCustomerReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Asign");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Created date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kids name.");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Gender");


        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['asign']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['created']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kidsname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['gender']);

            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);

            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function customerpaidReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Rq Date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Profile");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Fit number");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Previous Stylist");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Assign Customer stylist");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Assign Kid stylist");


        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['RqDate']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['fullName']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['profile']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Fitnumber']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['PreviousStylist']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['AssignCustomerstylist']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['KidName']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['AssignKidstylist']);


            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);

            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function downloadpreviousworklistReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//

        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Gender");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Profile");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Order date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Previous Stylist");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Assign Customer stylist");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Assign Kid stylist");


        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;

            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['fullName']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['gender']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['profile']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['RqDate']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['PreviousStylist']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['AssignCustomerstylist']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['KidName']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['AssignKidstylist']);


            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);

            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function UserName($id) {
        $table = TableRegistry::get('UserDetails');
        $query = $table->find('all')->where(['user_id' => $id])->first();
        return @$query->first_name . ' ' . @$query->last_name;
    }

    function havingKid($user_id) {
        $table = TableRegistry::get('KidsDetails');
        $data = $table->find('all')->where(['user_id' => $user_id])->first();
        if (@$data->id == '') {
            $result = 0;
        } else {
            $result = 1;
        }
        return $result;
    }

    function getStylistIdName($id = null) {
        $table = TableRegistry::get('CustomerStylist');
        $query = $table->find('all')->where(['user_id' => $id, 'kid_id' => 0])->first();
        $Utable = TableRegistry::get('Users');
        $name = $Utable->find('all')->where(['id' => @$query->employee_id])->first()->name;
        if ($name != '') {
            $stylistName = $name;
        } else {
            $stylistName = "Not assinge yet";
        }
        return @$stylistName;
    }

    function getStylistIdKidName($id = null) {
        $table = TableRegistry::get('CustomerStylist');
        $query = $table->find('all')->where(['kid_id' => $id])->first();
        $Utable = TableRegistry::get('Users');
        $name = $Utable->find('all')->where(['id' => @$query->employee_id])->first()->name;
        if ($name != '') {
            $stylistName = $name;
        } else {
            $stylistName = "Not assinge yet";
        }
        return @$stylistName;
    }

    function requestDate($id = null) {
        $table = TableRegistry::get('DeliverDate');
        $query = $table->find('all')->where(['id' => $id])->first()->date_in_time;
        return $query;
    }

    function previousStyleistName($userId = null, $paymenId, $count) {
        $table = TableRegistry::get('PaymentGetways');
        $user = TableRegistry::get('Users');
        $query1 = $table->find('all')->where(['user_id' => $userId, 'id' => $paymenId, 'count' => $count])->first();
        $c = $count - 1;
        if (@$query1->kid != '') {
            $query = $table->find('all')->where(['user_id' => $userId, 'status' => 1, 'count' => $c, 'kid_id' => $query1->kid_id])->first()->emp_id;
        } else {
            @$query = $table->find('all')->where(['user_id' => $userId, 'status' => 1, 'count' => $c, 'kid_id' => '0'])->first()->emp_id;
        }
        @$name = $user->find('all')->where(['id' => $query])->first()->name;
        if ($name != '') {
            return $name;
        } else {
            return "No yet";
        }
    }

    function previousStyleistNameCron($userId = null, $paymenId, $count) {
        $table = TableRegistry::get('PaymentGetways');
        $user = TableRegistry::get('Users');
        $query1 = $table->find('all')->where(['user_id' => $userId, 'id' => $paymenId, 'count' => $count])->first();
        $c = $count - 1;
        if (@$query1->kid != '') {
            $query = $table->find('all')->where(['user_id' => $userId, 'status' => 1, 'count' => $c, 'kid_id' => $query1->kid_id])->first()->emp_id;
        } else {
            @$query = $table->find('all')->where(['user_id' => $userId, 'status' => 1, 'count' => $c, 'kid_id' => '0'])->first()->emp_id;
        }
        @$id = $user->find('all')->where(['id' => $query])->first()->id;
        if (@$id != '') {
            return @$id;
        } else {
            return "No yet";
        }
    }

    function emaplyeName($id) {
        $table = TableRegistry::get('Users');
        $query = $table->find('all')->where(['id' => $id])->first();
        return $query->name;
    }

    function ToOrdinal($n) {
        $ordinal = "";
        if ($n >= 0 && $n <= 999)
            null;
        else {
            null;
            return;
        }
        $u = $n % 10;
        $t = floor(($n / 10) % 10);
        $h = floor($n / 100);
        if ($h > 0) {
            $ordinal .= ToCardinalUnits($h);
            $ordinal .= " hundred";
            if ($t == 0 && $u == 0) {
                $ordinal .= "th";
            } else {
                $ordinal .= " ";
            }
        }
        if ($t >= 2 && $u != 0) {
            switch ($t) {
                case 2:
                    $ordinal .= "twenty-";
                    break;
                case 3:
                    $ordinal .= "thirty-";
                    break;
                case 4:
                    $ordinal .= "forty-";
                    break;
                case 5:
                    $ordinal .= "fifty-";
                    break;
                case 6:
                    $ordinal .= "sixty-";
                    break;
                case 7:
                    $ordinal .= "seventy-";
                    break;
                case 8:
                    $ordinal .= "eighty-";
                    break;
                case 9:
                    $ordinal .= "ninety-";
                    break;
            }
        }
        if ($t >= 2 && $u == 0) {
            switch ($t) {
                case 2:
                    $ordinal .= "twentieth";
                    break;
                case 3:
                    $ordinal .= "thirtieth";
                    break;
                case 4:
                    $ordinal .= "fortieth";
                    break;
                case 5:
                    $ordinal .= "fiftieth";
                    break;
                case 6:
                    $ordinal .= "sixtieth";
                    break;
                case 7:
                    $ordinal .= "seventieth";
                    break;
                case 8:
                    $ordinal .= "eightieth";
                    break;
                case 9:
                    $ordinal .= "ninetieth";
                    break;
            }
        }
        if ($t == 1) {
            switch ($u) {
                case 0:
                    $ordinal .= "tenth";
                    break;
                case 1:
                    $ordinal .= "eleventh";
                    break;
                case 2:
                    $ordinal .= "twelfth";
                    break;
                case 3:
                    $ordinal .= "thirteenth";
                    break;
                case 4:
                    $ordinal .= "fourteenth";
                    break;
                case 5:
                    $ordinal .= "fifteenth";
                    break;
                case 6:
                    $ordinal .= "sixteenth";
                    break;
                case 7:
                    $ordinal .= "seventeenth";
                    break;
                case 8:
                    $ordinal .= "eighteenth";
                    break;
                case 9:
                    $ordinal .= "nineteenth";
                    break;
            }
        }

        if ($t != 1) {
            switch ($u) {
                case 0:
                    if ($n == 0)
                        $ordinal .= "zeroth";
                    break;
                case 1:
                    $ordinal .= "first";
                    break;
                case 2:
                    $ordinal .= "second";
                    break;
                case 3:
                    $ordinal .= "third";
                    break;
                case 4:
                    $ordinal .= "fourth";
                    break;
                case 5:
                    $ordinal .= "fifth";
                    break;
                case 6:
                    $ordinal .= "sixth";
                    break;
                case 7:
                    $ordinal .= "seventh";
                    break;
                case 8:
                    $ordinal .= "eighth";
                    break;
                case 9:
                    $ordinal .= "ninth";
                    break;
            }
        }
        return $ordinal;
    }

    function customerName($id) {
        $table = TableRegistry::get('Users');
        $query = $table->find('all')->where(['id' => $id])->first();
        $name = '';
        if (!empty($query->name)) {
            $name = $query->name;
        }

        return $name;
    }

    function customerEmail($id) {
        $table = TableRegistry::get('Users');
        $query = $table->find('all')->where(['id' => $id])->first();
        $email = '';
        if (!empty($query->email)) {
            $email = $query->email;
        }

        return $email;
    }

    function workStatus($status) {
        if (@$status == '0') {
            $data = 'not assign';
        }
        if (@$status == '2') {
            $data = 'Previous work list';
        }
        if (@$status == '1') {
            $data = 'currentworklist';
        }
        return @$data;
    }

    function downloadstylistwiseReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Stylist Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Assigned customer");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kids name.");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Created date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Status");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['assignedcustomer']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kidname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['created']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['status']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function downloadstateReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kids name.");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "State");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "City");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Country");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Zipcode");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kidname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['state']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['city']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['country']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['zipcode']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function downloadsallcustomerReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Gender");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Asign");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid count");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Paid status");
        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['date']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['fullname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Gender']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['asign']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kitcount']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['paidstatus']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function downloadnotpaidreport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Gender");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Full Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Asign");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid name");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['date']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Gender']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['fullname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['asign']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Kids']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function autocheckoutReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Order Date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Profile Type");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Stylist name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Finalize Date");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Product Count");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Product price");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Auto check Out date");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['OrderDate']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['KidName']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['ProfileType']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Stylistname']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['FinalizeDate']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['ProductCount']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['Productprice']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['AutocheckOutdate']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function paymentMode() {
        $table = TableRegistry::get('Paymentmode');
        $query = $table->find('all')->where(['id' => $id])->first();
        $val = '';
        if (!empty($query->value)) {
            $val = $query->value;
        }

        return $val;
    }

    function getStylistName($id = null) {
        $table = TableRegistry::get('CustomerStylist');
        $Utable = TableRegistry::get('Users');
        @$getEmpId = $table->find('all')->where(['user_id' => @$id])->first()->employee_id;
        if (@$getEmpId != '') {
            $name = $Utable->find('all')->where(['id' => @$getEmpId])->first()->name;
        } else {
            $name = "Not assigne";
        }

        return $name;
    }

    function countKid($user_id) {
        $table = TableRegistry::get('KidsDetails');
        $data = $table->find('all')->where(['user_id' => $user_id])->count();
        if (@$data == '') {
            $data = 0;
        }
        return $data;
    }

    function paidStatus($id = null) {
        $tablename = TableRegistry::get('PaymentGetways');
        $getPaidStatusCount = $tablename->find('all')->where(['user_id' => $id, 'status' => 1])->count();
        if ($getPaidStatusCount != 0) {
            $status = "PAID";
            return $status;
        } else {
            $getPaidNotStatusCount = $tablename->find('all')->where(['user_id' => $id, 'status' => 0])->count();
            if ($getPaidNotStatusCount == 0) {
                $status = 'Not paid';
                return $status;
            }
        }
    }

    function GenderName($id) {
        $table = TableRegistry::get('UserDetails');
        $query = $table->find('all')->where(['user_id' => $id])->first();
        if (!empty($query->gender)) {
            if ($query->gender == 1) {
                $gender = 'Men';
            }
            if ($query->gender == 2) {
                $gender = 'Women';
            }
            if ($query->gender == 3) {
                $gender = 'Kid';
            }
            return $gender;
        }
    }

    function getStylistIdNameKid($id = null) {
        $table = TableRegistry::get('CustomerStylist');
        $query = $table->find('all')->where(['kid_id' => $id])->first();
        $Utable = TableRegistry::get('Users');
        @$name = $Utable->find('all')->where(['id' => @$query->employee_id])->first()->name;
        if (@$name != '') {
            $stylistName = $name;
        } else {
            $stylistName = "Not assinge yet";
        }
        return @$stylistName;
    }

    function productCountPrice($id) {
        $table = TableRegistry::get('Products');
        $table1 = TableRegistry::get('PaymentGetways');
        $data = $table1->find('all')->where(['id' => $id])->first();
        $product_count = $table->find('all')->where(['payment_id' => $data->parent_id])->count();
        return $product_count;
    }

    function productPrice($id) {
        $table = TableRegistry::get('Products');
        $product_ls = $table->find('all')->where(['payment_id' => $id]);
        $paymentGetwayAmount = 0;
        foreach ($product_ls as $ls) {
            $paymentGetwayAmount += $ls->sell_price;
        }
        return $paymentGetwayAmount;
    }

    function applydate($id, $kid) {
        $table = TableRegistry::get('LetsPlanYourFirstFix');
        $table2 = TableRegistry::get('Users');
        $table3 = TableRegistry::get('PaymentGetways');
        if ($kid != '') {
            $kid1 = $kid;
        } else {
            $kid1 = 0;
        }

        $dataCheck = $table->find('all')->where(['user_id' => $id, 'kid_id' => $kid1])->first();
        //pj($dataCheck);
        //exit;

        if (!empty($dataCheck->id)) {
            if ($dataCheck->applay_dt == '0000-00-00 00:00:00' && $dataCheck->try_new_items_with_scheduled_fixes == 1) {
                $getPayment = $table3->find('all')->where(['user_id' => $id, 'kid_id' => $kid1])->first();
                if ($getPayment->status == 0) {
                    $users = $table2->find('all')->where(['id' => $id])->first();
                    $dataDate = $users->created_dt;
                } else {
                    $dataDate = $getPayment->created_dt;
                }
            } else {
                $users = $table2->find('all')->where(['id' => $id])->first();

                $dataDate = $users->created_dt;
            }
        } else {
            $users = $table2->find('all')->where(['id' => $id])->first();
            $dataDate = $users->created_dt;
        }

        return date('Y-m-d h:i:s', strtotime($dataDate));
    }

    function batchprocess($supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status) {
        if (1) {
            $table = TableRegistry::get('BatchMailingReports');
            $Enty = $table->newEntity();
            $data['support_status'] = $supportstatus;
            $data['support_email'] = $upportemail;
            $data['support_subject'] = $support_subject;
            $data['client'] = @$client;
            $data['process'] = @$process;
            $data['name'] = @$name;
            $data['kid_id'] = @$kidId;
            $data['kid_name'] = @$kidName;
            $data['email'] = @$email;
            $data['sending_datetime'] = date('Y-m-d H:I:S');
            $data['subject'] = @$subject;
            $data['status'] = @$status;
            $data['user_id'] = @$userId;
            $data['day'] = date('l');
            $data['payment_message'] = '';
            $data['transctions_id'] = '';
            $Enty = $table->patchEntity($Enty, $data);
            $table->save($Enty);
            // return true;
        } else {
            //return "error";
        }
    }

    function batchprocessPayment($fit_number, $payment_message, $transctions_id, $supportstatus, $upportemail, $support_subject, $client, $process, $userId, $name, $kidId, $kidName, $email, $subject, $status) {
        if (1) {
            $table = TableRegistry::get('BatchMailingReports');
            $Enty = $table->newEntity();
            $data['fit_number'] = $fit_number;
            $data['support_status'] = $supportstatus;
            $data['support_email'] = $upportemail;
            $data['support_subject'] = $support_subject;
            $data['client'] = @$client;
            $data['process'] = @$process;
            $data['name'] = @$name;
            $data['kid_id'] = @$kidId;
            $data['kid_name'] = @$kidName;
            $data['email'] = @$email;
            $data['sending_datetime'] = date('Y-m-d H:I:S');
            $data['subject'] = @$subject;
            $data['status'] = @$status;
            $data['user_id'] = @$userId;
            $data['day'] = date('l');
            $data['payment_message'] = $payment_message;
            $data['transctions_id'] = $transctions_id;
            $Enty = $table->patchEntity($Enty, $data);
            $table->save($Enty);
            // return true;
        } else {
            //return "error";
        }
    }

    function downloadbetchprocessReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Subject");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Status");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Subject");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Sending date time");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Status");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Day");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['subject']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kid_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['status']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_subject']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['sending_datetime']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_status']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function downloadbetchprocessSubcriptionReport($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Fit number");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Subject");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Kid Name");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Client Status");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Email");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Subject");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Sending date time");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Support Status");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Process");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Day");

        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['fit_number']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['subject']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['kid_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['status']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_subject']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['sending_datetime']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['support_status']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['process']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['day']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = 'Drapfit-' . $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }

    function paymentProfileCount($type) {
        if ($type == 1) {
            $ptype = 'st';
        } elseif ($type == 2) {
            $ptype = 'nd';
        } elseif ($type == 3) {
            $ptype = 'rd';
        } else {
            $ptype = 'th';
        }
        return $type . $ptype;
    }
    
    function uploadFiles($tmp_name, $name, $path) {
        if ($name) {
            $extname = $extname = $this->getExtension($name);
            $time = time() . rand(100, 999);
            $filepath = md5($time) . "." . $extname;
            $targetpath = $path . $filepath;
            $targetpath = move_uploaded_file($tmp_name, $targetpath);
            return $filepath;
        }
    }
    
     function customDebasish($payments, $fileName) {
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
         $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "#");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Website");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email ID");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Email Server");
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Traffic Status");
        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
             $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['s_n']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['d_a']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['email_server']);
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['trafic_status']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);

            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        return $filename;
    }
    
    function customDebasish2($payments, $fileName) {
        //echo $fileName;exit;
        $objPHPExcel = new \PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $style = [
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);
        foreach (range('A', 'F') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        $head = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S'];
        $count = 0;
        ///SetHeading//
         
        $objPHPExcel->getActiveSheet()->SetCellValue($head[$count++] . '1', "Website");
       
        //Set Content
        $rowCount = 2;
        $total = count($payments);
        for ($i = 0; $i < $total; $i++) {
            $count = -1;
            $objPHPExcel->getActiveSheet()->SetCellValue($head[++$count] . $rowCount, $payments[$i]['web']);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $objPHPExcel->getActiveSheet()->getStyle($head[$count] . $rowCount)->applyFromArray($style);
            $rowCount++;
        }
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $filename = $fileName . ".xlsx";
        $objWriter->save("files/temp_excel/$filename");
        //echo $filename; exit;
        return $filename;
    }
    
    function tallFeet($userId, $profile) {
        if (@$profile == 1) {
            $table = TableRegistry::get('MenStats');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->tall_feet)) {
                return $status->tall_feet;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('personalized_fix');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->tell_in_feet)) {
                return $status->tell_in_feet;
            } else {
                return 0;
            }
        }
    }

    function tellInch($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('MenStats');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->tell_inch)) {
                return $status->tell_inch;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('PersonalizedFix');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->tell_in_inch)) {
                return $status->tell_in_inch;
            } else {
                return 0;
            }
        }
    }

    function weightLbs($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('MenStats');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->weight_lb)) {
                return $status->weight_lb;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('PersonalizedFix');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->weight_lbs)) {
                return $status->weight_lbs;
            } else {
                return 0;
            }
        }
    }

    function bestSizeFit($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->size)) {
            return $status->size;
        } else {
            return 0;
        }
    }

    function waistSize($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->waist)) {
            return $status->waist;
        } else {
            return 0;
        }
    }

    function waistSizeRun($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->waist_size_run)) {
            return $status->waist_size_run;
        } else {
            return 0;
        }
    }

    function shirtSize($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->size)) {
            return $status->size;
        } else {
            return 0;
        }
    }

    function shirtSizeRun($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->shirt)) {
            return $status->shirt;
        } else {
            return 0;
        }
    }

    function inseamSize($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->inseam)) {
            return $status->inseam;
        } else {
            return 0;
        }
    }

    function shoeSize($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('TypicallyWearMen');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->shoe)) {
                return $status->shoe;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('SizeChart');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->shoe)) {
                return $status->shoe;
            } else {
                return 0;
            }
        }
    }

    function shoeSizeRun($userId) {
        $table = TableRegistry::get('TypicallyWearMen');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->shoe_medium)) {
            return $status->shoe_medium;
        } else {
            return 0;
        }
    }

    function betterBodyShape($userId, $profile) {
       
        if ($profile == 1) {
            $table = TableRegistry::get('TypicallyWearMen');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
          
            if (!empty($status->body_type)) {
                return $status->body_type;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('WomenInformation');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            
            if (!empty($status->body_type)) {
                return $status->body_type;
            } else {
                return 0;
            }
        }
    }

    function skinTone($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('TypicallyWearMen');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->skin_tone)) {
                return $status->skin_tone;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('WomenInformation');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->skin_tone)) {
                return $status->skin_tone;
            } else {
                return 0;
            }
        }
    }

    function workType($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('MenStats');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->your_occupation)) {
                return $status->your_occupation;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('WomenInformation');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->occupation_v2)) {
                return $status->occupation_v2;
            } else {
                return 0;
            }
        }
    }

    function casualShirtsType($userId) {
        $table = TableRegistry::get('menFit');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->casual_shirts_to_fit)) {
            return $status->casual_shirts_to_fit;
        } else {
            return 0;
        }
    }

    function bottomUpShirtFit($userId) {
        $table = TableRegistry::get('menFit');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->button_up_shirts_to_fit)) {
            return $status->button_up_shirts_to_fit;
        } else {
            return 0;
        }
    }

    function jeansFit($userId) {
        $table = TableRegistry::get('menFit');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->jeans_to_fit)) {
            return $status->jeans_to_fit;
        } else {
            return 0;
        }
    }

    function shortsLong($userId) {
        $table = TableRegistry::get('menFit');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->jeans_to_fit)) {
            return $status->jeans_to_fit;
        } else {
            return 0;
        }
    }

    function color($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('MenFit');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->prefer_color)) {
                return $status->prefer_color;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('WemenStyleSphereSelections');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->color_prefer)) {
                return $status->color_prefer;
            } else {
                return 0;
            }
        }
    }

    function pants($userId, $profile) {
        if ($profile == 1) {
            $table = TableRegistry::get('MenFit');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->prefer_color)) {
                return $status->prefer_color;
            } else {
                return 0;
            }
        } else {
            $table = TableRegistry::get('SizeChart');
            $status = $table->find('all')->where(['user_id' => $userId])->first();
            if (!empty($status->pants)) {
                return $status->pants;
            } else {
                return 0;
            }
        }
    }

    function bra($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->bra)) {
            return $status->bra;
        } else {
            return 0;
        }
    }

    function braRecomend($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->bra_recomend)) {
            return $status->bra_recomend;
        } else {
            return 0;
        }
    }

    function skirt($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->skirt)) {
            return $status->skirt;
        } else {
            return 0;
        }
    }

    function jeans($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->jeans)) {
            return $status->jeans;
        } else {
            return 0;
        }
    }

    function dress($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->dress)) {
            return $status->dress;
        } else {
            return 0;
        }
    }

    function dressRecomended($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->dress_recomended)) {
            return $status->dress_recomended;
        } else {
            return 0;
        }
    }

    function shirtBlouse($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->shirt_blouse)) {
            return $status->shirt_blouse;
        } else {
            return 0;
        }
    }

    function shirtBlouseRecomend($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->shirt_blouse_recomend)) {
            return $status->shirt_blouse_recomend;
        } else {
            return 0;
        }
    }

    function pantsr1($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->pantsr1)) {
            return $status->pantsr1;
        } else {
            return 0;
        }
    }

    function womenHeelHightPrefer($userId) {
        $table = TableRegistry::get('WomenHeelHightPrefer');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->height)) {
            return $status->height;
        } else {
            return 0;
        }
    }

    function proportionShoulders($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->proportion_shoulders)) {
            return $status->proportion_shoulders;
        } else {
            return 0;
        }
    }

    function proportionLegs($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->proportion_legs)) {
            return $status->proportion_legs;
        } else {
            return 0;
        }
    }

    function proportionArms($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->proportion_arms)) {
            return $status->proportion_arms;
        } else {
            return 0;
        }
    }

    function proportionHips($userId) {
        $table = TableRegistry::get('SizeChart');
        $status = $table->find('all')->where(['user_id' => $userId])->first();
        if (!empty($status->proportion_hips)) {
            return $status->proportion_hips;
        } else {
            return 0;
        }
    }

    //kidssections
    function topSizekid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['user_id' => $userId, 'kid_id' => $kid])->first();
        if (!empty($status->top_size)) {
            return $status->top_size;
        } else {
            return 0;
        }
    }

    function tallFeetkid($kid) {
        $table = TableRegistry::get('KidsDetails');
        $status = $table->find('all')->where(['id' => $kid])->first();
        if (!empty($status->tall_feet)) {
            return $status->tall_feet;
        } else {
            return 0;
        }
    }

    function tellInchkid($kid) {
        $table = TableRegistry::get('KidsDetails');
        $status = $table->find('all')->where(['id' => $kid])->first();
        if (!empty($status->tell_inch)) {
            return $status->tell_inch;
        } else {
            return 0;
        }
    }

    function weightLbskid($kid) {
        $table = TableRegistry::get('KidsDetails');
        $status = $table->find('all')->where(['id' => $kid])->first();
        if (!empty($status->weight_lb)) {
            return $status->weight_lb;
        } else {
            return 0;
        }
    }

    function bestSizeFitkid($kid) {
        $table = TableRegistry::get('KidsDetails');
        $status = $table->find('all')->where(['id' => $kid])->first();
        if (!empty($status->size_prefer_wear)) {
            return $status->size_prefer_wear;
        } else {
            return 0;
        }
    }

    function shoeSizekid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['kid_id' => $kid])->first();
        if (!empty($status->shoes)) {
            return $status->shoes;
        } else {
            return 0;
        }
    }

    function betterBodyShapekid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['kid_id' => $kid])->first();
        if (!empty($status->body_shape)) {
            return $status->body_shape;
        } else {
            return 0;
        }
    }

    function colorkid($kid) {
        $table = TableRegistry::get('KidsDetails');
        $status = $table->find('all')->where(['id' => $kid_id])->first();
        if (!empty($status->color)) {
            return $status->color;
        } else {
            return 0;
        }
    }

    function skirtkid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['kid_id' => $kid])->first();
        if (!empty($status->skirts)) {
            return $status->skirts;
        } else {
            return 0;
        }
    }

    function paintskid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['kid_id' => $kid])->first();
        if (!empty($status->paint)) {
            return $status->paint;
        } else {
            return 0;
        }
    }
    function jeanskid($kid) {
        $table = TableRegistry::get('KidsSizeFit');
        $status = $table->find('all')->where(['kid_id' => $kid])->first();
        if (!empty($status->jeans)) {
            return $status->jeans;
        } else {
            return 0;
        }
    }
    
    function birthDayMen($id) {
        $table = TableRegistry::get('MenStats');
        $query = $table->find('all')->where(['user_id' => $id])->first();
        return @$query->birthday;
    }

    function birthDayWomenMen($id) {
        $table = TableRegistry::get('WomenInformation');
        $query = $table->find('all')->where(['user_id' => $id])->first();
      
        return @$query->birthday;
    }

    function kidBirthDay($id) {
        $table = TableRegistry::get('kidsDetails');
        $query = $table->find('all')->where(['id' => $id])->first();
        return @$query->kids_birthdate;
    }

    function kidDeatils($id) {
        $table = TableRegistry::get('kidsDetails');
        $query = $table->find('all')->where(['user_id' => $id]);
        return @$query;
    }
    
   function menMatching($uId) {
        $MenStats = TableRegistry::get('MenStats');
        $InProducts = TableRegistry::get('InProducts');
        $products = TableRegistry::get('Products');
        $men_stats = $MenStats->find('all')->where(['user_id' => $uId])->first();
        $matching = [];

        $products = $products->find('all')->where(['user_id' => $uId]);
        //pj($products); exit;
        $product_Id = [];
        foreach ($products as $pd) {
            $product_Id[] = $pd->id;
        }
//echo "<pre>";
    //print_r($product_Id);  exit;
   //echo $men_stats->tall_feet;
        if (!empty($men_stats->tall_feet)) {
            $in_products = $InProducts->find('all')->where(['profile_type'=>1,'tall_feet' => $men_stats->tall_feet]);
            //pj($in_products);

            foreach ($in_products as $ip) {
                
                
                if (!empty($ip->id)) {
                    if (@!in_array($ip->prodcut_id, @$product_Id)) {
                     // echo "<br>";
                      // echo $ip->prodcut_id;
                        $productId = $ip->id;
                        $matching[$ip->id]['tall_feet'] = 1;
                        $matching[$ip->id]['product_id'] = $ip->prodcut_id;
                    }
                }
            }
           // exit;
        }
     
  
        if (!empty($men_stats->tell_inch)) {
            $in_products_inch = $InProducts->find('all')->where(['profile_type'=>1,'tall_inch' => $men_stats->tell_inch]);
            foreach ($in_products_inch as $inch)
                if (!empty($inch->id)) {
                  // echo $inch->prodcut_id; 
                    //echo "<br>";
                    if (@!in_array(@$inch->prodcut_id, $product_Id)) {
                         // echo $inch->prodcut_id; 
                        // echo "<br>";
                        $productId = $inch->id;
                        $matching[$inch->id]['tall_inch'] = 1;
                        $matching[$inch->id]['product_id'] = $inch->prodcut_id;
                    }
                }
               
        }

        if (!empty($men_stats->weight_lb)) {
            $in_products_weight = $InProducts->find('all')->where(['profile_type'=>1,'best_fit_for_weight' => $men_stats->weight_lb]);
            foreach ($in_products_weight as $weight)
                if (!empty($weight->id)) {
                   
                    if (@!in_array(@$weight->prodcut_id, $product_Id)) {
                        //echo $weight->prodcut_id; 
                        $productId = $weight->id;
                        $matching[$weight->id]['best_fit_for_weight'] = 1;
                        $matching[$weight->id]['product_id'] = $weight->prodcut_id;
                    }
                }
                //exit;
        }

        $TypicallyWearMen = TableRegistry::get('TypicallyWearMen');
        $TypicallyWearMenData = $TypicallyWearMen->find('all')->where(['user_id' => $uId])->first();
        if (!empty($TypicallyWearMenData->waist)) {
            $in_products_typically = $InProducts->find('all')->where(['profile_type'=>1,'waist_size' => $TypicallyWearMenData->waist]);
            foreach ($in_products_typically as $typically)
                if (!empty($typically->id)) {
                    
                    if (@!in_array(@$typically->prodcut_id, $product_Id)) {
                        $productId = $typically->id;
                        //echo $typically->prodcut_id;
                       // echo "<br>";
                        $matching[$typically->id]['waist_size'] = 1;
                        $matching[$typically->id]['product_id'] = $typically->prodcut_id;
                    }
                }
        }


      

        if (!empty($TypicallyWearMenData->size)) {
            $in_products_size = $InProducts->find('all')->where(['profile_type'=>1,'shirt_size' => $TypicallyWearMenData->size]);
            foreach ($in_products_size as $size)
                if (!empty($size->id)) {
                  //echo $size->prodcut_id; 
                       // echo "<br>";
                    if (@!in_array(@$size->prodcut_id, $product_Id)) {
                        $productId = $size->id;
                        //echo $size->prodcut_id; 
                        //echo "<br>";
                        $matching[$size->id]['size'] = 1;
                        $matching[$size->id]['product_id'] = $size->prodcut_id;
                    }
                }
                //exit;
        }



        if (!empty($TypicallyWearMenData->inseam)) {
            $in_products_inseam = $InProducts->find('all')->where(['profile_type'=>1,'inseam_size' => $TypicallyWearMenData->inseam]);
            foreach ($in_products_inseam as $inseam)
                if (!empty($inseam->id)) {
                    //echo $inseam->prodcut_id;
                    if (@!in_array(@$inseam->prodcut_id, $product_Id)) {
                        //echo $inseam->prodcut_id;
                        $productId = $inseam->id;
                        $matching[$inseam->id]['inseam'] = 1;
                        $matching[$inseam->id]['product_id'] = $inseam->prodcut_id;
                    }
                }
               //exit;
        }

        if (!empty($TypicallyWearMenData->shoe)) {
            $in_products_shoe = $InProducts->find('all')->where(['profile_type'=>1,'shoe_size' => $TypicallyWearMenData->shoe]);
            foreach ($in_products_shoe as $shoe)
                if (!empty($shoe->id)) {
                    //echo $shoe->prodcut_id;
                    if (@!in_array(@$shoe->prodcut_id, $product_Id)) {
                     //echo $shoe->prodcut_id;
                        $productId = $shoe->id;
                        $matching[$shoe->id]['shoe'] = 1;
                        $matching[$shoe->id]['product_id'] = $shoe->prodcut_id;
                    }
                }
                //exit;
        }
        if (!empty($TypicallyWearMenData->body_type)) {
            $in_products_better_body_shape = $InProducts->find('all')->where(['profile_type'=>1,'better_body_shape' => $TypicallyWearMenData->body_type]);
            foreach ($in_products_better_body_shape as $better_body_shape)
                if (!empty($better_body_shape->id)) {
                    //echo $better_body_shape->prodcut_id; 
                    //echo "<pre>";
                    //print_r($productId); 
                    if (@!in_array(@$better_body_shape->prodcut_id, $product_Id)) {
                        //echo $better_body_shape->prodcut_id; 
                        $productId = $better_body_shape->id;
                        $matching[$better_body_shape->id]['better_body_shape'] = 1;
                        $matching[$better_body_shape->id]['product_id'] = $better_body_shape->prodcut_id;
                    }
                }
                //exit;
        }

        if (!empty($TypicallyWearMenData->skin_tone)) {
            $in_products_skin_tone = $InProducts->find('all')->where(['profile_type'=>1,'skin_tone' => $TypicallyWearMenData->skin_tone]);
            foreach ($in_products_skin_tone as $skin_tone)
            
                if (!empty($skin_tone->id)) {
                    //echo $skin_tone->prodcut_id;
                    if (@!in_array(@$skin_tone->prodcut_id, $product_Id)) {
                        //echo $skin_tone->prodcut_id;
                        $productId = $skin_tone->id;
                        $matching[$skin_tone->id]['skin_tone'] = 1;
                        $matching[$skin_tone->id]['product_id'] = $skin_tone->prodcut_id;
                    }
                }
                //exit;
        }


    //echo "<pre>";
    //print_r($matching); exit;

        return $matching;
    }
    
   function womenMatching($uId) {
        $stat = TableRegistry::get('PersonalizedFix');
        $stats = $stat->find('all')->where(['user_id' => $uId])->first();
        $InProducts = TableRegistry::get('InProducts');
        $products = TableRegistry::get('Products');
        $matching = [];
        $products = $products->find('all')->where(['user_id' => $uId]);
        $product_Id = [];
        foreach ($products as $pd) {
            $product_Id[] = $pd->id;
        }
        if (!empty($stats->tell_in_feet)) {
            $in_products = $InProducts->find('all')->where(['profile_type' => 2, 'tall_feet' => $stats->tell_in_feet]);
            foreach ($in_products as $ip) {
                if (!empty($ip->id)) {
                    if (@!in_array($ip->prodcut_id, @$product_Id)) {
                        $productId = $ip->id;
                        $matching[$ip->id]['tall_feet'] = 1;
                        $matching[$ip->id]['product_id'] = $ip->prodcut_id;
                    }
                }
            }
        }
        if (!empty($stats->tell_in_inch)) {
            $in_products_inch = $InProducts->find('all')->where(['profile_type' => 2, 'tall_inch' => $stats->tell_in_inch]);
            foreach ($in_products_inch as $inch)
                if (!empty($inch->id)) {
                    if (@!in_array(@$inch->prodcut_id, $product_Id)) {
                        $productId = $inch->id;
                        $matching[$inch->id]['tall_inch'] = 1;
                        $matching[$inch->id]['product_id'] = $inch->prodcut_id;
                    }
                }
        }
        if (!empty($stats->weight_lbs)) {
            $in_products_weight = $InProducts->find('all')->where(['profile_type' => 2, 'best_fit_for_weight' => $stats->weight_lbs]);
            foreach ($in_products_weight as $weight)
                if (!empty($weight->id)) {
                    if (@!in_array(@$weight->prodcut_id, $product_Id)) {
                        $productId = $weight->id;
                        $matching[$weight->id]['best_fit_for_weight'] = 1;
                        $matching[$weight->id]['product_id'] = $weight->prodcut_id;
                    }
                }
        }

        $sizeChart = TableRegistry::get('SizeChart');
        $sizeChartData = $sizeChart->find('all')->where(['user_id' => $uId])->first();
        if (!empty($sizeChartData->pants)) {
            $in_products_size = $InProducts->find('all')->where(['profile_type' => 2, 'pants' => $sizeChartData->pants]);
            foreach ($in_products_size as $size)
                if (!empty($size->id)) {
                    if (@!in_array(@$size->prodcut_id, $product_Id)) {
                        $productId = $size->id;
                        $matching[$size->id]['pants'] = 1;
                        $matching[$size->id]['product_id'] = $size->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->bra)) {
            $in_products_bra = $InProducts->find('all')->where(['profile_type' => 2, 'bra' => $sizeChartData->bra]);
            foreach ($in_products_bra as $bra)
                if (!empty($bra->id)) {
                    if (@!in_array(@$bra->prodcut_id, $product_Id)) {
                        $productId = $bra->id;
                        $matching[$bra->id]['bra'] = 1;
                        $matching[$bra->id]['product_id'] = $bra->prodcut_id;
                    }
                }
        }



        if (!empty($sizeChartData->skirt)) {
            $in_products_skirt = $InProducts->find('all')->where(['profile_type' => 2, 'skirt' => $sizeChartData->skirt]);
            foreach ($in_products_skirt as $skirt)
                if (!empty($skirt->id)) {
                    if (@!in_array(@$skirt->prodcut_id, $product_Id)) {
                        $productId = $skirt->id;
                        $matching[$skirt->id]['skirt'] = 1;
                        $matching[$skirt->id]['product_id'] = $skirt->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->shoe)) {
            $in_products_shoe = $InProducts->find('all')->where(['profile_type' => 2, 'shoe_size' => $sizeChartData->shoe]);
            foreach ($in_products_shoe as $shoe)
                if (!empty($shoe->id)) {
                    if (@!in_array(@$shoe->prodcut_id, $product_Id)) {
                        $productId = $shoe->id;
                        $matching[$shoe->id]['shoe'] = 1;
                        $matching[$shoe->id]['product_id'] = $shoe->prodcut_id;
                    }
                }
        }
        if (!empty($sizeChartData->dress)) {
            $in_products_dress = $InProducts->find('all')->where(['profile_type' => 2, 'dress' => $sizeChartData->dress]);
            foreach ($in_products_dress as $dress)
                if (!empty($dress->id)) {
                    if (@!in_array(@$dress->prodcut_id, $product_Id)) {
                        $productId = @$dress->id;
                        $matching[$dress->id]['dress'] = 1;
                        $matching[$dress->id]['product_id'] = $dress->prodcut_id;
                    }
                }
        }

        $womenInformations = TableRegistry::get('WomenInformation');
        $womenInformationsData = $womenInformations->find('all')->where(['user_id' => $uId])->first();
        if (!empty($womenInformationsData->skin_tone)) {
            $in_products_skin_tone = $InProducts->find('all')->where(['profile_type' => 2, 'skin_tone' => $womenInformationsData->skin_tone]);
            foreach ($in_products_skin_tone as $skin_tone)
                if (!empty($skin_tone->id)) {
                    if (@!in_array(@$skin_tone->prodcut_id, $product_Id)) {
                        $productId = $skin_tone->id;
                        $matching[$skin_tone->id]['skin_tone'] = 1;
                        $matching[$skin_tone->id]['product_id'] = $skin_tone->prodcut_id;
                    }
                }
        }

        if (!empty($womenInformationsData->body_type)) {
            $in_products_better_body_shape = $InProducts->find('all')->where(['profile_type' => 2, 'better_body_shape' => $womenInformationsData->body_type]);
            foreach ($in_products_better_body_shape as $better_body_shape)
                if (!empty($better_body_shape->id)) {
                    if (@!in_array(@$better_body_shape->prodcut_id, $product_Id)) {
                        $productId = $better_body_shape->id;
                        $matching[$better_body_shape->id]['better_body_shape'] = 1;
                        $matching[$better_body_shape->id]['product_id'] = $better_body_shape->prodcut_id;
                    }
                }
        }


        return $matching;
    }

    function girlsMatching($uId, $kidId) {
        $stat = TableRegistry::get('KidsDetails');
        $stats = $stat->find('all')->where(['user_id' => $uId, 'id' => $kidId])->first();
        $InProducts = TableRegistry::get('InProducts');
        $products = TableRegistry::get('Products');
        $matching = [];
        $products = $products->find('all')->where(['user_id' => $uId, 'kid_id' => $kidId]);
        $product_Id = [];
        foreach ($products as $pd) {
            $product_Id[] = $pd->id;
        }
        if (!empty($stats->tall_feet)) {
            $in_products = $InProducts->find('all')->where(['profile_type' => 4, 'tall_feet' => $stats->tall_feet]);
            foreach ($in_products as $ip) {
                if (!empty($ip->id)) {
                    if (@!in_array($ip->prodcut_id, @$product_Id)) {
                        $productId = $ip->id;
                        $matching[$ip->id]['tall_feet'] = 1;
                        $matching[$ip->id]['product_id'] = $ip->prodcut_id;
                    }
                }
            }
        }
        if (!empty($stats->tell_inch)) {
            $in_products_inch = $InProducts->find('all')->where(['profile_type' => 4, 'tall_inch' => $stats->tell_inch]);
            foreach ($in_products_inch as $inch)
                if (!empty($inch->id)) {
                    if (@!in_array(@$inch->prodcut_id, $product_Id)) {
                        $productId = $inch->id;
                        $matching[$inch->id]['tall_inch'] = 1;
                        $matching[$inch->id]['product_id'] = $inch->prodcut_id;
                    }
                }
        }
        if (!empty($stats->weight_lb)) {
            $in_products_weight = $InProducts->find('all')->where(['profile_type' => 4, 'best_fit_for_weight' => $stats->weight_lb]);
            foreach ($in_products_weight as $weight)
                if (!empty($weight->id)) {
                    if (@!in_array(@$weight->prodcut_id, $product_Id)) {
                        $productId = $weight->id;
                        $matching[$weight->id]['best_fit_for_weight'] = 1;
                        $matching[$weight->id]['product_id'] = $weight->prodcut_id;
                    }
                }
        }

        $sizeChart = TableRegistry::get('KidsSizeFit');
        $sizeChartData = $sizeChart->find('all')->where(['user_id' => $uId, 'kid_id' => $kidId])->first();
        if (!empty($sizeChartData->top_size)) {
            $in_products_size = $InProducts->find('all')->where(['profile_type' => 4, 'top_size' => $sizeChartData->top_size]);
            foreach ($in_products_size as $size)
                if (!empty($size->id)) {
                    if (@!in_array(@$size->prodcut_id, $product_Id)) {
                        $productId = $size->id;
                        $matching[$size->id]['top_size'] = 1;
                        $matching[$size->id]['product_id'] = $size->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->bottom_size)) {
            $in_products_bottom = $InProducts->find('all')->where(['profile_type' => 4, 'bottom_size' => $sizeChartData->bottom_size]);
            foreach ($in_products_bottom as $bt)
                if (!empty($bra->id)) {
                    if (@!in_array($bt->prodcut_id, $product_Id)) {
                        $productId = $bt->id;
                        $matching[$bra->id]['bottom_size'] = 1;
                        $matching[$bra->id]['product_id'] = $bt->prodcut_id;
                    }
                }
        }





        if (!empty($sizeChartData->shoe_size)) {
            $in_products_shoe = $InProducts->find('all')->where(['profile_type' => 4, 'shoe_size' => $sizeChartData->shoe_size]);
            foreach ($in_products_shoe as $shoe)
                if (!empty($shoe->id)) {
                    if (@!in_array(@$shoe->prodcut_id, $product_Id)) {
                        $productId = $shoe->id;
                        $matching[$shoe->id]['shoe'] = 1;
                        $matching[$shoe->id]['product_id'] = $shoe->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->t_shirts)) {
            $in_products_shirts = $InProducts->find('all')->where(['profile_type' => 4, 'shirt_size' => $sizeChartData->t_shirts]);
            foreach ($in_products_shirts as $shirts)
                if (!empty($shirts->id)) {
                    if (@!in_array(@$shirts->prodcut_id, $product_Id)) {
                        $productId = @$shirts->id;
                        $matching[$shirts->id]['dress'] = 1;
                        $matching[$shirts->id]['product_id'] = $shirts->prodcut_id;
                    }
                }
        }



        if (!empty($sizeChartData->paint)) {
            $in_products_skin_tone = $InProducts->find('all')->where(['profile_type' => 4, 'pants' => $sizeChartData->paint]);
            foreach ($in_products_paint as $paint)
                if (!empty($paint->id)) {
                    if (@!in_array(@$$paint->prodcut_id, $product_Id)) {
                        $productId = $paint->id;
                        $matching[$paint->id]['skin_tone'] = 1;
                        $matching[$paint->id]['product_id'] = $paint->prodcut_id;
                    }
                }
        }


        if (!empty($sizeChartData->body_shape)) {
            $in_products_better_body_shape = $InProducts->find('all')->where(['profile_type' => 4, 'better_body_shape' => $sizeChartData->body_shape]);
            foreach ($in_products_better_body_shape as $better_body_shape)
                if (!empty($better_body_shape->id)) {
                    if (@!in_array(@$better_body_shape->prodcut_id, $product_Id)) {
                        $productId = $better_body_shape->id;
                        $matching[$better_body_shape->id]['better_body_shape'] = 1;
                        $matching[$better_body_shape->id]['product_id'] = $better_body_shape->prodcut_id;
                    }
                }
        }



        if (!empty($sizeChartData->skirt)) {
            $in_products_better_body_skrit = $InProducts->find('all')->where(['profile_type' => 4, 'skirt' => $sizeChartData->skirt]);
            foreach ($in_products_better_body_skrit as $better_skirt)
                if (!empty($better_skirt->id)) {
                    if (@!in_array(@$better_skirt->prodcut_id, $product_Id)) {
                        $productId = $better_skirt->id;
                        $matching[$better_skirt->id]['skirt'] = 1;
                        $matching[$better_skirt->id]['product_id'] = $better_skirt->prodcut_id;
                    }
                }
        }
        if (!empty($sizeChartData->skirt)) {
            $in_products_better_body_skrit = $InProducts->find('all')->where(['profile_type' => 4, 'skirt' => $sizeChartData->skirt]);
            foreach ($in_products_better_body_skrit as $better_skirt)
                if (!empty($better_skirt->id)) {
                    if (@!in_array(@$better_skirt->prodcut_id, $product_Id)) {
                        $productId = $better_skirt->id;
                        $matching[$better_skirt->id]['skirt'] = 1;
                        $matching[$better_skirt->id]['product_id'] = $better_skirt->prodcut_id;
                    }
                }
        }



        return $matching;
    }
    
    function boyMatching($uId, $kidId) {
        $stat = TableRegistry::get('KidsDetails');
        $stats = $stat->find('all')->where(['user_id' => $uId, 'id' => $kidId])->first();
        $InProducts = TableRegistry::get('InProducts');
        $products = TableRegistry::get('Products');
        $matching = [];
        $products = $products->find('all')->where(['user_id' => $uId, 'kid_id' => $kidId]);
        $product_Id = [];
        foreach ($products as $pd) {
            $product_Id[] = $pd->id;
        }
        if (!empty($stats->tall_feet)) {
            $in_products = $InProducts->find('all')->where(['profile_type' => 4, 'tall_feet' => $stats->tall_feet]);
            foreach ($in_products as $ip) {
                if (!empty($ip->id)) {
                    if (@!in_array($ip->prodcut_id, @$product_Id)) {
                        $productId = $ip->id;
                        $matching[$ip->id]['tall_feet'] = 1;
                        $matching[$ip->id]['product_id'] = $ip->prodcut_id;
                    }
                }
            }
        }
        if (!empty($stats->tell_inch)) {
            $in_products_inch = $InProducts->find('all')->where(['profile_type' => 4, 'tall_inch' => $stats->tell_inch]);
            foreach ($in_products_inch as $inch)
                if (!empty($inch->id)) {
                    if (@!in_array(@$inch->prodcut_id, $product_Id)) {
                        $productId = $inch->id;
                        $matching[$inch->id]['tall_inch'] = 1;
                        $matching[$inch->id]['product_id'] = $inch->prodcut_id;
                    }
                }
        }
        if (!empty($stats->weight_lb)) {
            $in_products_weight = $InProducts->find('all')->where(['profile_type' => 4, 'best_fit_for_weight' => $stats->weight_lb]);
            foreach ($in_products_weight as $weight)
                if (!empty($weight->id)) {
                    if (@!in_array(@$weight->prodcut_id, $product_Id)) {
                        $productId = $weight->id;
                        $matching[$weight->id]['best_fit_for_weight'] = 1;
                        $matching[$weight->id]['product_id'] = $weight->prodcut_id;
                    }
                }
        }

        $sizeChart = TableRegistry::get('KidsSizeFit');
        $sizeChartData = $sizeChart->find('all')->where(['user_id' => $uId, 'kid_id' => $kidId])->first();
        if (!empty($sizeChartData->top_size)) {
            $in_products_size = $InProducts->find('all')->where(['profile_type' => 4, 'top_size' => $sizeChartData->top_size]);
            foreach ($in_products_size as $size)
                if (!empty($size->id)) {
                    if (@!in_array(@$size->prodcut_id, $product_Id)) {
                        $productId = $size->id;
                        $matching[$size->id]['top_size'] = 1;
                        $matching[$size->id]['product_id'] = $size->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->bottom_size)) {
            $in_products_bottom = $InProducts->find('all')->where(['profile_type' => 4, 'bottom_size' => $sizeChartData->bottom_size]);
            foreach ($in_products_bottom as $bt)
                if (!empty($bra->id)) {
                    if (@!in_array($bt->prodcut_id, $product_Id)) {
                        $productId = $bt->id;
                        $matching[$bra->id]['bottom_size'] = 1;
                        $matching[$bra->id]['product_id'] = $bt->prodcut_id;
                    }
                }
        }





        if (!empty($sizeChartData->shoe_size)) {
            $in_products_shoe = $InProducts->find('all')->where(['profile_type' => 4, 'shoe_size' => $sizeChartData->shoe_size]);
            foreach ($in_products_shoe as $shoe)
                if (!empty($shoe->id)) {
                    if (@!in_array(@$shoe->prodcut_id, $product_Id)) {
                        $productId = $shoe->id;
                        $matching[$shoe->id]['shoe'] = 1;
                        $matching[$shoe->id]['product_id'] = $shoe->prodcut_id;
                    }
                }
        }

        if (!empty($sizeChartData->t_shirts)) {
            $in_products_shirts = $InProducts->find('all')->where(['profile_type' => 4, 'shirt_size' => $sizeChartData->t_shirts]);
            foreach ($in_products_shirts as $shirts)
                if (!empty($shirts->id)) {
                    if (@!in_array(@$shirts->prodcut_id, $product_Id)) {
                        $productId = @$shirts->id;
                        $matching[$shirts->id]['dress'] = 1;
                        $matching[$shirts->id]['product_id'] = $shirts->prodcut_id;
                    }
                }
        }



        if (!empty($sizeChartData->paint)) {
            $in_products_skin_tone = $InProducts->find('all')->where(['profile_type' => 4, 'pants' => $sizeChartData->paint]);
            foreach ($in_products_paint as $paint)
                if (!empty($paint->id)) {
                    if (@!in_array(@$$paint->prodcut_id, $product_Id)) {
                        $productId = $paint->id;
                        $matching[$paint->id]['skin_tone'] = 1;
                        $matching[$paint->id]['product_id'] = $paint->prodcut_id;
                    }
                }
        }


        if (!empty($sizeChartData->body_shape)) {
            $in_products_better_body_shape = $InProducts->find('all')->where(['profile_type' => 4, 'better_body_shape' => $sizeChartData->body_shape]);
            foreach ($in_products_better_body_shape as $better_body_shape)
                if (!empty($better_body_shape->id)) {
                    if (@!in_array(@$better_body_shape->prodcut_id, $product_Id)) {
                        $productId = $better_body_shape->id;
                        $matching[$better_body_shape->id]['better_body_shape'] = 1;
                        $matching[$better_body_shape->id]['product_id'] = $better_body_shape->prodcut_id;
                    }
                }
        }



        if (!empty($sizeChartData->skirt)) {
            $in_products_better_body_skrit = $InProducts->find('all')->where(['profile_type' => 4, 'skirt' => $sizeChartData->skirt]);
            foreach ($in_products_better_body_skrit as $better_skirt)
                if (!empty($better_skirt->id)) {
                    if (@!in_array(@$better_skirt->prodcut_id, $product_Id)) {
                        $productId = $better_skirt->id;
                        $matching[$better_skirt->id]['skirt'] = 1;
                        $matching[$better_skirt->id]['product_id'] = $better_skirt->prodcut_id;
                    }
                }
        }
        if (!empty($sizeChartData->skirt)) {
            $in_products_better_body_skrit = $InProducts->find('all')->where(['profile_type' => 4, 'skirt' => $sizeChartData->skirt]);
            foreach ($in_products_better_body_skrit as $better_skirt)
                if (!empty($better_skirt->id)) {
                    if (@!in_array(@$better_skirt->prodcut_id, $product_Id)) {
                        $productId = $better_skirt->id;
                        $matching[$better_skirt->id]['skirt'] = 1;
                        $matching[$better_skirt->id]['product_id'] = $better_skirt->prodcut_id;
                    }
                }
        }



        return $matching;
    }
   


}
