<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Http\Exception\NotFoundException;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/*
 *  IMPORTANT 
 *          "stripe-php" folder added in vendor file 
 */

class StripesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');

        $this->loadComponent('Custom');

        $this->loadModel('Users');
        $this->loadModel('UserDetails');
        $this->loadModel('Settings');
        $this->loadModel('Optionals');

        $this->viewBuilder()->setLayout('ajax');
    }
    
     public function beforeFilter(Event $event) {

        $this->Auth->allow(['webhook']);
    }

    public function index() {
        
    }

    public function process() {
        //$this->request->allowMethod(['post']);
        if ($this->request->is('post')) {
            $postData = $this->request->getData();
            // get token and user details
            $stripeToken = $postData['stripeToken'];
            $custName = $postData['custName'];
            $custEmail = $postData['custEmail'];
            $cardNumber = $postData['cardNumber'];
            $cardCVC = $postData['cardCVC'];
            $cardExpMonth = $postData['cardExpMonth'];
            $cardExpYear = $postData['cardExpYear'];
            //include Stripe PHP library
            require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
            //require_once('stripe-php/init.php');
            //set stripe secret key and publishable key
            $stripe = array(
                // "secret_key"      => "Your_Stripe_API_Secret_Key",
                // "publishable_key" => "Your_API_Publishable_Key"
                "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
                "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            $stripe_clint = new \Stripe\StripeClient($stripe['secret_key']);

            //add customer to stripe 
            //Create Once for eacy manage in stripe end            
            $customer = \Stripe\Customer::create(array(
                        'name' => $custName,
                        'email' => $custEmail,
                        'source' => $stripeToken,
                        'description' => 'test description',
                        "address" => ["city" => 'Bhubneswar', "country" => 'India', "line1" => 'bbsr', "line2" => "", "postal_code" => "751019", "state" => "Odisha"]
            ));

            $stripe_clint->customers->update(
                    $customer->id
            );

            // item details for which payment made
            $itemName = "php test item";
            $itemNumber = "PHPZAG987654321";
            $itemPrice = 50;
            $currency = "USD";
            $orderID = "SKA987654321";
            // details for which payment performed
            $payDetails = \Stripe\Charge::create(array(
                        'customer' => $customer->id,
                        'amount' => $itemPrice * 100,
                        'currency' => $currency,
                        'description' => $itemName,
                        'metadata' => array(
                            'order_id' => $orderID
                        )
            ));
            // get payment details
            $paymenyResponse = $payDetails->jsonSerialize();
            // check whether the payment is successful
            if ($paymenyResponse['amount_refunded'] == 0 && empty($paymenyResponse['failure_code']) && $paymenyResponse['paid'] == 1 && $paymenyResponse['captured'] == 1) {
                // transaction details 
                $amountPaid = $paymenyResponse['amount'];
                $balanceTransaction = $paymenyResponse['balance_transaction'];
                $charged_id = $paymenyResponse['id']; //used for refund
                $receipt_url = $paymenyResponse['receipt_url'];
                $paidCurrency = $paymenyResponse['currency'];
                $paymentStatus = $paymenyResponse['status'];
                $paymentDate = date("Y-m-d H:i:s");
                //insert tansaction details into database
                /* include_once("db_connect.php");
                  $insertTransactionSQL = "INSERT INTO transaction(cust_name, cust_email, card_number, card_cvc, card_exp_month, card_exp_year,item_name, item_number, item_price, item_price_currency, paid_amount, paid_amount_currency, txn_id, payment_status, created, modified)
                  VALUES('" . $custName . "','" . $custEmail . "','" . $cardNumber . "','" . $cardCVC . "','" . $cardExpMonth . "','" . $cardExpYear . "','" . $itemName . "','" . $itemNumber . "','" . $itemPrice . "','" . $paidCurrency . "','" . $amountPaid . "','" . $paidCurrency . "','" . $balanceTransaction . "','" . $paymentStatus . "','" . $paymentDate . "','" . $paymentDate . "')";
                  mysqli_query($conn, $insertTransactionSQL) or die("database error: " . mysqli_error($conn));
                  $lastInsertId = mysqli_insert_id($conn); */
                $lastInsertId = $balanceTransaction;
                //if order inserted successfully
                if ($lastInsertId && $paymentStatus == 'succeeded') {
                    $paymentMessage = "<strong>The payment was successful.</strong><strong> Order ID: {$lastInsertId}</strong>";
                    $paymentMessage .= "<br><strong> Charger ID: {$charged_id}<small>Required for refund</small></strong>";
                    $paymentMessage .= "<br><strong> Receipt url : {$receipt_url}</strong>";
                    echo "<pre>";
                    print_r($payDetails);
                    echo "</pre>";
                } else {
                    $paymentMessage = "Payment failed!";
                }
            } else {
                $paymentMessage = "Payment failed!";
            }
        } else {
            $paymentMessage = "Payment failed!";
        }
        echo $paymentMessage;
    }

    public function refund($key = null) {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php" . DS . "init.php");
        //require_once('stripe-php/init.php');
        //set stripe secret key and publishable key
        $stripe = array(
            // "secret_key"      => "Your_Stripe_API_Secret_Key",
            // "publishable_key" => "Your_API_Publishable_Key"
            "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
            "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        try {

            $res = \Stripe\Refund::create([
//                        'amount' => 1000,
                        'charge' => $key,
            ]);

            echo "<pre>";
            print_r($res);
            echo "</pre>";
        } catch (\Stripe\Error\Base $e) {
            // Code to do something with the $e exception object when an error occurs

            echo('Err : ' . $e->getMessage());
            $body = $e->getJsonBody();
            $err = $body['error'];

            echo("<br>" . 'Status is:' . $e->getHttpStatus() . "<br>");
            echo('Type is:' . $err['type'] . "<br>");
            echo('Code is:' . $err['code'] . "<br>");
            echo('Param is:' . $err['param'] . "<br>");
            echo('Message is:' . $err['message'] . "<br>");

            echo "<pre>";
            print_r(['er' => $e]);
            echo "</pre>";
        } catch (Exception $e) {
            // Catch any other non-Stripe exceptions
            echo "<pre>";
            print_r(['error' => $e]);
            echo "</pre>";
        }
        exit;
    }

    public function directPay() {
        // This is your real test secret API key.
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = array(
            // "secret_key"      => "Your_Stripe_API_Secret_Key",
            // "publishable_key" => "Your_API_Publishable_Key"
            "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
            "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        header('Content-Type: application/json');

        try {



            $paymentIntent = \Stripe\PaymentIntent::create([
                        'amount' => 20 * 100,
                        'currency' => 'usd',
            ]);

            $output = [
                'clientSecret' => $paymentIntent->client_secret,
            ];

            echo json_encode($output);
        } catch (Error $e) {

            http_response_code(500);

            echo json_encode(['error' => $e->getMessage()]);
        }
        exit;
    }

    public function stripeKeys() {
        $stripe = array(
            // "secret_key"      => "Your_Stripe_API_Secret_Key",
            // "publishable_key" => "Your_API_Publishable_Key"
            "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
            "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
        );
        return $stripe;
    }

    /* --------------STRIPE API Customer CREATE ---------------------------- */

    public function stripePaymentTest() {
        echo "<pre>";
        $name = 'Dev Ss';
        $email = "debmicrofinet@gmail.com";
//        $customer_dtl['custName']= $name;
//        $customer_dtl['custEmail']= $email;
//        $customer_dtl['city']= "Tucson";
//        $customer_dtl['state']= 'TX';
//        $customer_dtl['country']= "United states";
//        $customer_dtl['address']= "7805 S Placita Senora Maria";
//        $customer_dtl['zip_code']= "350048";
//        $customer = $this->createCustomer($customer_dtl);
//        
//        echo "Customer <br>";
//        print_r($customer);
//        
//        $card_dtl['card_number']='4242424242424242';
//        $card_dtl['exp_month']='05';
//        $card_dtl['exp_year']='2029';
//        $card_dtl['card_code']='123';
//        $card = $this->createCard($card_dtl);
//        
//        echo "CArd <br>";
//        print_r($card);

        $payment_d['amount'] = 10;
        $payment_d['customer_id'] = 'cus_LnjjU9aPB6m72R'; //$customer['customer_id'];
        $payment_d['payment_methode_id'] = 'pm_1L68G2ITPrbxGSMcJrSx3Cr4'; //$card['payment_methode_id'];
        $payment_d['name'] = $name;
        $payment_d['ord_idd'] = 12211;
        $payment = $this->paymentProcess($payment_d);

        echo "Payment <br>";
        print_r($payment);
        exit;
    }

    public function createCustomer($customer = []) {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = $this->stripeKeys();
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $stripe_clint = new \Stripe\StripeClient($stripe['secret_key']);

        //add customer to stripe 
        $customer_ret = [];
        try {
            $customer = \Stripe\Customer::create(array(
                        'name' => $customer['custName'],
                        'email' => $customer['custEmail'],
                        'description' => 'API CREATE ',
                        "address" => ["city" => $customer['city'], "country" => $customer['country'], "line1" => $customer['address'], "line2" => "", "postal_code" => $customer['zip_code'], "state" => $customer['state']]
            ));
            $customer_ret['customer_id'] = $customer->id;
            $customer_ret['status'] = 'success';
        } catch (Exception $e) {
            $customer_ret['msg'] = 'No response returned';
            $customer_ret['status'] = 'error';
        }
        return $customer_ret;
    }

    /* --------------STRIPE API Card CREATE ---------------------------- */

    public function createCard($card = []) {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = $this->stripeKeys();
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $stripe_clint = new \Stripe\StripeClient($stripe['secret_key']);
        $payment_ky = [];
        try {
            $payment_methode = $stripe_clint->paymentMethods->create([
                'type' => 'card',
                'card' => [
                    'number' => $card['card_number'],
                    'exp_month' => $card['exp_month'],
                    'exp_year' => $card['exp_year'],
                    'cvc' => $card['card_code'],
                ],
            ]);
            $payment_ky['payment_methode_id'] = $payment_methode->id;
            $payment_ky['status'] = 'success';
        } catch (\Stripe\Exception\CardException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Error\Base $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (Exception $e) {
            $payment_ky['msg'] = 'No response returned';
            $payment_ky['status'] = 'error';
        }

        return $payment_ky;
    }

    /* --------------STRIPE API Payment PROCESS---------------------------- */

    public function paymentProcess($payment_d = []) {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = $this->stripeKeys();
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $stripe_clint = new \Stripe\StripeClient($stripe['secret_key']);
        $payment_ky = [];
        try {
            $payment_init = $stripe_clint->paymentIntents->create([
                'amount' => $payment_d['amount'] * 100,
                'currency' => 'usd',
                'payment_method_types' => ['card'],
                'customer' => $payment_d['customer_id'],
                'payment_method' => $payment_d['payment_methode_id'],
                'confirm' => true,
                'description' => $payment_d['name'] . ' api Payment',
                'metadata' => array(
                    'order_id' => $payment_d['ord_idd']
                )
            ]);

            $pay_res = $payment_init->jsonSerialize();
//            echo($pay_res['charges']['data'][0]['id']."<br>");
//            echo($pay_res['charges']['data'][0]['balance_transaction']."<br>");
//            echo($pay_res['charges']['data'][0]['receipt_url']."<br>");

            $payment_ky['TransId'] = $pay_res['charges']['data'][0]['balance_transaction'];
            $payment_ky['receipt_url'] = $pay_res['charges']['data'][0]['receipt_url'];
            $payment_ky['charge_id'] = $pay_res['charges']['data'][0]['id'];

            $payment_ky['Success'] = " Successfully created transaction with Transaction ID: " . $pay_res['charges']['data'][0]['balance_transaction'];
            $payment_ky['ResponseCode'] = " Transaction Response Code: 200 ";
            $payment_ky['MessageCode'] = " Message Code: 200 ";
            $payment_ky['AuthCode'] = " Auth Code: 200";
            $payment_ky['Description'] = " Description: The payment was successful";
            $payment_ky['msg'] = " Description: The payment was successful";

            $payment_ky['payment_methode_id'] = $payment_d['payment_methode_id'];
            $payment_ky['status'] = 'success';
        } catch (\Stripe\Exception\CardException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\RateLimitException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\AuthenticationException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Exception\ApiErrorException $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (\Stripe\Error\Base $e) {
            $body = $e->getJsonBody();
            $err = $body['error'];
            $payment_ky['msg'] = " Error Code  :" . $e->getError()->code . " <br> Error Message : " . $err['message'] . "\n";
            $payment_ky['status'] = 'error';
        } catch (Exception $e) {
            $payment_ky['msg'] = 'No response returned';
            $payment_ky['status'] = 'error';
        }

        return $payment_ky;
    }

    /* -------------------------END------------------------------------------ */

    public function webhook() {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");

// This is your Stripe CLI webhook secret for testing your endpoint locally.
//        $endpoint_secret = 'whsec_cd34cf8ec00f66b7967ee2f963b2fbdf0cbbde6dba8d170b8db4239bae23b72d';
        $endpoint_secret = 'whsec_SqPh1rqxDynZoKSV6mu8WwGiJdckJ3nB';

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                            $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }
// Handle the event


        $message = $event->type;
        switch ($event->type) {
            case 'account.updated':
//                $account = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'account.external_account.created':
//                $externalAccount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'account.external_account.deleted':
//                $externalAccount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'account.external_account.updated':
//                $externalAccount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'balance.available':
//                $balance = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'billing_portal.configuration.created':
//                $configuration = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'billing_portal.configuration.updated':
//                $configuration = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'billing_portal.session.created':
//                $session = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'capability.updated':
//                $capability = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'cash_balance.funds_available':
//                $cashBalance = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.captured':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.expired':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.failed':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.pending':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.refunded':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.succeeded':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.updated':
//                $charge = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.dispute.closed':
//                $dispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.dispute.created':
//                $dispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.dispute.funds_reinstated':
//                $dispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.dispute.funds_withdrawn':
//                $dispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.dispute.updated':
//                $dispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'charge.refund.updated':
//                $refund = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'checkout.session.async_payment_failed':
//                $session = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'checkout.session.async_payment_succeeded':
//                $session = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'checkout.session.completed':
//                $session = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'checkout.session.expired':
//                $session = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'coupon.created':
//                $coupon = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'coupon.deleted':
//                $coupon = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'coupon.updated':
//                $coupon = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'credit_note.created':
//                $creditNote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'credit_note.updated':
//                $creditNote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'credit_note.voided':
//                $creditNote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.created':
//                $customer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.deleted':
//                $customer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.updated':
//                $customer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.discount.created':
//                $discount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.discount.deleted':
//                $discount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.discount.updated':
//                $discount = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.source.created':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.source.deleted':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.source.expiring':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.source.updated':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.created':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.deleted':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.pending_update_applied':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.pending_update_expired':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.trial_will_end':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.subscription.updated':
//                $subscription = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.tax_id.created':
//                $taxId = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.tax_id.deleted':
//                $taxId = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'customer.tax_id.updated':
//                $taxId = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'file.created':
//                $file = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.canceled':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.created':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.processing':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.redacted':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.requires_input':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'identity.verification_session.verified':
//                $verificationSession = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.created':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.deleted':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.finalization_failed':
//                $invoice = $event->data->object;v
            case 'invoice.finalized':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.marked_uncollectible':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.paid':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.payment_action_required':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.payment_failed':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.payment_succeeded':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.sent':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.upcoming':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.updated':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoice.voided':
//                $invoice = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoiceitem.created':
//                $invoiceitem = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoiceitem.deleted':
//                $invoiceitem = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'invoiceitem.updated':
//                $invoiceitem = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_authorization.created':
//                $issuingAuthorization = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_authorization.request':
//                $issuingAuthorization = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_authorization.updated':
//                $issuingAuthorization = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_card.created':
//                $issuingCard = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_card.updated':
//                $issuingCard = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_cardholder.created':
//                $issuingCardholder = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_cardholder.updated':
//                $issuingCardholder = $event->data->object;v
                $message .= json_encode($event->data->object);
            case 'issuing_dispute.closed':
//                $issuingDispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_dispute.created':
//                $issuingDispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_dispute.funds_reinstated':
//                $issuingDispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_dispute.submitted':
//                $issuingDispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_dispute.updated':
//                $issuingDispute = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_transaction.created':
//                $issuingTransaction = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'issuing_transaction.updated':
//                $issuingTransaction = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'mandate.updated':
//                $mandate = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'order.created':
//                $order = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'order.payment_failed':
//                $order = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'order.payment_succeeded':
//                $order = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'order.updated':
//                $order = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'order_return.created':
//                $orderReturn = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.amount_capturable_updated':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.canceled':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.created':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.partially_funded':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.payment_failed':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.processing':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.requires_action':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_intent.succeeded':
//                $paymentIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_link.created':
//                $paymentLink = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_link.updated':
//                $paymentLink = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_method.attached':
//                $paymentMethod = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_method.automatically_updated':
//                $paymentMethod = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_method.detached':
//                $paymentMethod = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payment_method.updated':
//                $paymentMethod = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payout.canceled':
//                $payout = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payout.created':
//                $payout = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payout.failed':
//                $payout = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payout.paid':
//                $payout = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'payout.updated':
//                $payout = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'person.created':
//                $person = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'person.deleted':
//                $person = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'person.updated':
//                $person = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'plan.created':
//                $plan = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'plan.deleted':
//                $plan = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'plan.updated':
//                $plan = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'price.created':
//                $price = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'price.deleted':
//                $price = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'price.updated':
//                $price = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'product.created':
//                $product = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'product.deleted':
//                $product = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'product.updated':
//                $product = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'promotion_code.created':
//                $promotionCode = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'promotion_code.updated':
//                $promotionCode = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'quote.accepted':
//                $quote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'quote.canceled':
//                $quote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'quote.created':
//                $quote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'quote.finalized':
//                $quote = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'radar.early_fraud_warning.created':
//                $earlyFraudWarning = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'radar.early_fraud_warning.updated':
//                $earlyFraudWarning = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'recipient.created':
//                $recipient = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'recipient.deleted':
//                $recipient = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'recipient.updated':
//                $recipient = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'reporting.report_run.failed':
//                $reportRun = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'reporting.report_run.succeeded':
//                $reportRun = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'reporting.report_type.updated':
//                $reportType = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'review.closed':
//                $review = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'review.opened':
//                $review = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'setup_intent.canceled':
//                $setupIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'setup_intent.created':
//                $setupIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'setup_intent.requires_action':
//                $setupIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'setup_intent.setup_failed':
//                $setupIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'setup_intent.succeeded':
//                $setupIntent = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'sigma.scheduled_query_run.created':
//                $scheduledQueryRun = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'sku.created':
//                $sku = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'sku.deleted':
//                $sku = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'sku.updated':
//                $sku = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.canceled':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.chargeable':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.failed':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.mandate_notification':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.refund_attributes_required':
//                $source = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.transaction.created':
//                $transaction = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'source.transaction.updated':
//                $transaction = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.aborted':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.canceled':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.completed':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.created':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.expiring':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.released':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'subscription_schedule.updated':
//                $subscriptionSchedule = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'tax_rate.created':
//                $taxRate = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'tax_rate.updated':
//                $taxRate = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'terminal.reader.action_failed':
//                $reader = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'terminal.reader.action_succeeded':
//                $reader = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'test_helpers.test_clock.advancing':
//                $testClock = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'test_helpers.test_clock.created':
//                $testClock = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'test_helpers.test_clock.deleted':
//                $testClock = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'test_helpers.test_clock.internal_failure':
//                $testClock = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'test_helpers.test_clock.ready':
//                $testClock = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'topup.canceled':
//                $topup = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'topup.created':
//                $topup = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'topup.failed':
//                $topup = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'topup.reversed':
//                $topup = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'topup.succeeded':
//                $topup = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'transfer.created':
//                $transfer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'transfer.failed':
//                $transfer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'transfer.paid':
//                $transfer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'transfer.reversed':
//                $transfer = $event->data->object;
                $message .= json_encode($event->data->object);
            case 'transfer.updated':
//                $transfer = $event->data->object;
                $message .= json_encode($event->data->object);
            // ... handle other event types
            default:
                echo 'Received unknown event type ' . $event->type;
                $message .= json_encode($event->data->object);
        }

        $fromMail = $this->Settings->find('all')->where(['Settings.name' => 'FROM_EMAIL'])->first();
        $from = $fromMail->value;
        $subject = 'Stripe event....';
        $to = 'debmicrofinet@gmail.com';
        $this->Custom->sendEmail($to, $from, $subject, $message);

        echo json_encode(true);
        http_response_code(200);
        exit;
    }

    public function cusUpdtChk() {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = new \Stripe\StripeClient('sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg'
        );
        $dd = $stripe->customers->update(
                'cus_LjxAn2TZweK9Yw'
        );
        echo "<pre>";
        print_r($dd);
        echo "</pre>";
    }

}
