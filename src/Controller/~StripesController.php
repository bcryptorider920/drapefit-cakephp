<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Network\Request;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Http\Exception\NotFoundException;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/*
 *  IMPORTANT 
 *          "stripe-php" folder added in vendor file 
 */

class StripesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Flash');

        $this->loadComponent('Custom');

        $this->loadModel('Users');
        $this->loadModel('UserDetails');
        $this->loadModel('Settings');
        $this->loadModel('Optionals');

        $this->viewBuilder()->setLayout('ajax');
    }

    public function index() {
        
    }

    public function process() {
        //$this->request->allowMethod(['post']);
        if ($this->request->is('post')) {
            $postData = $this->request->getData();
            // get token and user details
            $stripeToken = $postData['stripeToken'];
            $custName = $postData['custName'];
            $custEmail = $postData['custEmail'];
            $cardNumber = $postData['cardNumber'];
            $cardCVC = $postData['cardCVC'];
            $cardExpMonth = $postData['cardExpMonth'];
            $cardExpYear = $postData['cardExpYear'];
            //include Stripe PHP library
            require_once(ROOT . DS . 'vendor' . DS . "stripe-php" . DS . "init.php");
            //require_once('stripe-php/init.php');
            //set stripe secret key and publishable key
            $stripe = array(
                // "secret_key"      => "Your_Stripe_API_Secret_Key",
                // "publishable_key" => "Your_API_Publishable_Key"
                "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
                "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
            );
            \Stripe\Stripe::setApiKey($stripe['secret_key']);

            //add customer to stripe 
            //Create Once for eacy manage in stripe end            
            $customer = \Stripe\Customer::create(array(
                        'name' => $custName,
                        'email' => $custEmail,
                        'source' => $stripeToken,
                        'description' => 'test description',
                        "address" => ["city" => 'Bhubneswar', "country" => 'India', "line1" => 'bbsr', "line2" => "", "postal_code" => "751019", "state" => "Odisha"]
            ));

            // item details for which payment made
            $itemName = "php test item";
            $itemNumber = "PHPZAG987654321";
            $itemPrice = 50;
            $currency = "USD";
            $orderID = "SKA987654321";
            // details for which payment performed
            $payDetails = \Stripe\Charge::create(array(
                        'customer' => $customer->id,
                        'amount' => $itemPrice * 100,
                        'currency' => $currency,
                        'description' => $itemName,
                        'metadata' => array(
                            'order_id' => $orderID
                        )
            ));
            // get payment details
            $paymenyResponse = $payDetails->jsonSerialize();
            // check whether the payment is successful
            if ($paymenyResponse['amount_refunded'] == 0 && empty($paymenyResponse['failure_code']) && $paymenyResponse['paid'] == 1 && $paymenyResponse['captured'] == 1) {
                // transaction details 
                $amountPaid = $paymenyResponse['amount'];
                $balanceTransaction = $paymenyResponse['balance_transaction'];
                $charged_id = $paymenyResponse['id']; //used for refund
                $receipt_url = $paymenyResponse['receipt_url'];
                $paidCurrency = $paymenyResponse['currency'];
                $paymentStatus = $paymenyResponse['status'];
                $paymentDate = date("Y-m-d H:i:s");
                //insert tansaction details into database
                /* include_once("db_connect.php");
                  $insertTransactionSQL = "INSERT INTO transaction(cust_name, cust_email, card_number, card_cvc, card_exp_month, card_exp_year,item_name, item_number, item_price, item_price_currency, paid_amount, paid_amount_currency, txn_id, payment_status, created, modified)
                  VALUES('" . $custName . "','" . $custEmail . "','" . $cardNumber . "','" . $cardCVC . "','" . $cardExpMonth . "','" . $cardExpYear . "','" . $itemName . "','" . $itemNumber . "','" . $itemPrice . "','" . $paidCurrency . "','" . $amountPaid . "','" . $paidCurrency . "','" . $balanceTransaction . "','" . $paymentStatus . "','" . $paymentDate . "','" . $paymentDate . "')";
                  mysqli_query($conn, $insertTransactionSQL) or die("database error: " . mysqli_error($conn));
                  $lastInsertId = mysqli_insert_id($conn); */
                $lastInsertId = $balanceTransaction;
                //if order inserted successfully
                if ($lastInsertId && $paymentStatus == 'succeeded') {
                    $paymentMessage = "<strong>The payment was successful.</strong><strong> Order ID: {$lastInsertId}</strong>";
                    $paymentMessage .= "<br><strong> Charger ID: {$charged_id}<small>Required for refund</small></strong>";
                    $paymentMessage .= "<br><strong> Receipt url : {$receipt_url}</strong>";
                    echo "<pre>";
                    print_r($payDetails);
                    echo "</pre>";
                } else {
                    $paymentMessage = "Payment failed!";
                }
            } else {
                $paymentMessage = "Payment failed!";
            }
        } else {
            $paymentMessage = "Payment failed!";
        }
        echo $paymentMessage;
    }

    public function refund($key = null) {
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php" . DS . "init.php");
        //require_once('stripe-php/init.php');
        //set stripe secret key and publishable key
        $stripe = array(
            // "secret_key"      => "Your_Stripe_API_Secret_Key",
            // "publishable_key" => "Your_API_Publishable_Key"
            "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
            "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        try {

            $res = \Stripe\Refund::create([
//                        'amount' => 1000,
                        'charge' => $key,
            ]);

            echo "<pre>";
            print_r($res);
            echo "</pre>";
        } catch (\Stripe\Error\Base $e) {
            // Code to do something with the $e exception object when an error occurs

            echo('Err : ' . $e->getMessage());
            $body = $e->getJsonBody();
            $err = $body['error'];

            echo("<br>" . 'Status is:' . $e->getHttpStatus() . "<br>");
            echo('Type is:' . $err['type'] . "<br>");
            echo('Code is:' . $err['code'] . "<br>");
            echo('Param is:' . $err['param'] . "<br>");
            echo('Message is:' . $err['message'] . "<br>");

            echo "<pre>";
            print_r(['er' => $e]);
            echo "</pre>";
        } catch (Exception $e) {
            // Catch any other non-Stripe exceptions
            echo "<pre>";
            print_r(['error' => $e]);
            echo "</pre>";
        }
        exit;
    }

    public function directPay() {
        // This is your real test secret API key.
        require_once(ROOT . DS . 'vendor' . DS . "stripe-php2" . DS . "init.php");
        $stripe = array(
            // "secret_key"      => "Your_Stripe_API_Secret_Key",
            // "publishable_key" => "Your_API_Publishable_Key"
            "secret_key" => "sk_test_51JY90jITPrbxGSMcpa6GFAxK96iCUrRjwWpJPY0gbh53l1EXf1F5aLYkNqc8V3h6baqk0gm9N79qazLZrp6bNg1H00TRuPEAeg",
            "publishable_key" => "pk_test_51JY90jITPrbxGSMcuo8bhxqQhCbSvHghLQaYIxtqVSe9u2xxm80SDtIVQ9acsLTW4WyPJX5G0nIMxaLXwtXbsN0N00vkBYmYDU"
        );
        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        header('Content-Type: application/json');

        try {



            $paymentIntent = \Stripe\PaymentIntent::create([
                        'amount' => 20 * 100,
                        'currency' => 'usd',
            ]);

            $output = [
                'clientSecret' => $paymentIntent->client_secret,
            ];

            echo json_encode($output);
        } catch (Error $e) {

            http_response_code(500);

            echo json_encode(['error' => $e->getMessage()]);
        }
        exit;
    }

}
