<?php

use Cake\ORM\TableRegistry;
?>
<style>
    .btn.btn-info.hint--top.hint .fa.fa-fw.fa-user-plus {
        width: 3.286em !important;
    }
    .hide{
        display: none;
    }
    .active{
        display: block;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= __('EXT') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo HTTP_ROOT . 'appadmins' ?>"><i class="fa fa-dashboard"></i> Home</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="box-header with-border1">
                            <div class="col-xs-12"> 
                                <?php echo $this->Html->link($this->Html->tag('i', ' Download excel', array('class' => 'fa fa-download')), ['action' => 'testdebasishexce2'], ['escape' => false, "data-placement" => "top", "data-hint" => "Download pdf", 'class' => 'btn btn-info  hint--top  hint', 'style' => 'padding: 0 12px!important;']); ?>
                                <?php echo $this->Html->link($this->Html->tag('i', ' Empty tbale', array('class' => 'fa fa-download')), ['action' => 'emptytbldebaish3'], ['escape' => false, "data-placement" => "top", "data-hint" => "Download pdf", 'class' => 'btn btn-info  hint--top  hint', 'style' => 'padding: 0 12px!important;']); ?>
                                <a href="<?php echo HTTP_ROOT.'appadmins/filterdebasish'?>" data-placement="top"  class="btn btn-info  hint--top  hint" style="padding: 0 12px!important;">Fillter</a>
                                <a href="<?php echo HTTP_ROOT.'appadmins/rowdebasish'?>" data-placement="top"  class="btn btn-info  hint--top  hint" style="padding: 0 12px!important;">Row upload</a>
                                 <a href="<?php echo HTTP_ROOT.'appadmins/testdebasish/'?>" data-placement="top"  class="btn btn-info  hint--top  hint" style="padding: 0 12px!important;">main upload</a>
                            </div>

                            <div class="col-xs-12"> 

                            </div>
                        </div>

                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>

                                    <th>Sno</th>
                                    <th>Domain</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($data as $dt) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $dt->web; ?></td>

                                    </tr>

                                    <?php
                                    $count++;
                                }
                                ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

