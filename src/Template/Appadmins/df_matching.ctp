<div class="content-wrapper">
    <section class="content-header">
        <?php if ($getData->kid_id != '') { ?>
            <h1> Drapefit Matching Listing of <?php echo $this->Custom->kidName($getData->kid_id); ?></h1>
        <?php } else { ?>
            <h1> Drapefit Matching Listing of <?php echo $userDetails->first_name; ?></h1>
        <?php } ?>
        <ol class="breadcrumb">
            <li><a href="<?php echo HTTP_ROOT . 'appadmins' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a class="active-color" href="<?= h(HTTP_ROOT) ?>appadmins/matching/<?php echo $id; ?>">   <i class="fa  fa-user-plus"></i> Drapefit Matching Product </a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Product Name 1</th>
                                    <th>Product Name 2</th>
                                    <th>Product Image</th>
                                    <th>Size</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($getProducts as $key => $prod) { ?>
                                    <tr>
                                        <td><?= $prod->product_name_one; ?></td>  
                                        <td><?= $prod->product_name_two; ?></td>  
                                        <td><img src="<?= HTTP_ROOT . 'files/product_img/' . $prod->product_image; ?>" width='100'></td>
                                        <td><?= $prod->size; ?></td>
                                        <td><?= $prod->sell_price; ?></td>
                                        <td>
                                            <?php if (!empty($_GET['exchange'])) { ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-plus-square-o')), ['action' => 'dfAddMatchProduct', $id, $prod->id, '?' => ['exchange' => $_GET['exchange']]], ['escape' => false, "data-placement" => "top", "data-hint" => "Add Product", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                            <?php } else { ?>
                                                <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-plus-square-o')), ['action' => 'dfAddMatchProduct', $id, $prod->id], ['escape' => false, "data-placement" => "top", "data-hint" => "Add Product", 'class' => 'btn btn-info hint--top  hint', 'style' => 'padding: 0 7px!important;']); ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>


</div>