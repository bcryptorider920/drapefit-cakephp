<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= __('Matching products') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo HTTP_ROOT . 'appadmins' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo HTTP_ROOT . 'appadmins/view_users' ?>"> Matching product listing</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <?php
                        if ($gender == 3) {
                            echo "<h3>" . $userDetails->kids_first_name . "</h3>";
                            !empty($kids_size_fit->kids_weight) ? 'Weight : ' . $kids_size_fit->kids_weight . '<br>' : '';
                        }
                        ?>
                        <?php
                        if ($gender == 2) {
                            echo "<h3>" . $userDetails->first_name . "</h3>";
                        }
                        ?>
                        <?php
                        if ($gender == 1) {
                            echo "<h3>" . $userDetails->first_name . "</h3>";
                        }
                        ?>
                        <div>

                            <ul>
                                <?php foreach ($getProducts as $pro) { ?>
                                    <li><?= $pro->product_name_one ?></li>
                                <?php } ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>