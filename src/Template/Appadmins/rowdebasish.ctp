<?php

use Cake\ORM\TableRegistry;
?>
<style>
    .btn.btn-info.hint--top.hint .fa.fa-fw.fa-user-plus {
        width: 3.286em !important;
    }
    .hide{
        display: none;
    }
    .active{
        display: block;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= __('EXT') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo HTTP_ROOT . 'appadmins' ?>"><i class="fa fa-dashboard"></i> Home</a></li>

        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="box-header with-border1">
                            <div class="col-xs-12"> 
                                
                              
                                <a href="<?php echo HTTP_ROOT.'appadmins/copydebaish/'?>" data-placement="top"  class="btn btn-info  hint--top  hint" style="padding: 0 12px!important;">find missing</a>
                                <a href="<?php echo HTTP_ROOT.'appadmins/testdebasish/'?>" data-placement="top"  class="btn btn-info  hint--top  hint" style="padding: 0 12px!important;">Upload main files</a>
                            </div>

                            <div class="col-xs-12"> 

                            </div>
                        </div>



                        <div class="col-xs-12">
                            <!-- general form elements -->
                            <div class="box box-info">

                                <?= $this->Form->create(null, array('data-toggle' => "validator", 'type' => 'file')) ?>
                                <div class="box-body">

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="exampleInputName">excel</label>
                                            <?= $this->Form->input('excel[]', ['value' => '', 'multiple' => 'multiple', 'type' => 'file', 'label' => false, 'kl_virtual_keyboard_secure_input' => "on"]); ?>
                                        </div>
                                    </div>


                                </div>

                                <div class="box-footer">
                                    <?= $this->Form->button('SUBMIT', ['class' => 'btn btn-success', 'style' => 'float:left;margin-left:17px;']) ?>
                                </div>
                                <?= $this->Form->end() ?>
                            </div>
                        </div>

                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>

                                    <th>Sno</th>
                                    <th>Domain</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $count = 1;
                                foreach ($data as $dt) {
                                    ?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $dt->web; ?></td>

                                    </tr>

                                    <?php
                                    $count++;
                                }
                                ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

