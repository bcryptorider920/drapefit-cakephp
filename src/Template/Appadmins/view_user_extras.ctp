<style>
    .btn.btn-info.hint--top.hint .fa.fa-fw.fa-user-plus {
        width: 3.286em !important;
    }
    .hide{
        display: none;
    }
    .active{
        display: block;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <?= __('User Extras') ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo HTTP_ROOT . 'appadmins' ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo HTTP_ROOT . 'appadmins/view_users' ?>"> Order listing</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="box-header with-border1">
                            <div class="col-xs-12" id="formDiv"> </div>
                        </div>

                        <div class="row">

                            <?php
                            $color_arr = $this->Custom->inColor();
                            if (!empty($all_data)) {
                                foreach ($all_data as $data_li) {
                                    $product_details = $data_li->in_product;

                                    if (empty($product_details->prodcut_id)) {
                                        $img = HTTP_ROOT_INV . PRODUCT_IMAGES . $product_details->product_image;
                                    } else {
                                        $img = HTTP_ROOT . PRODUCT_IMAGES . $product_details->product_image;
                                    }
                                    ?> 
                                    <div class="col-sm-12 col-sm-6 col-md-6">
                                        <div class="cart-list-box" style="<?= ($data_li->status == 2) ? 'border: red 5px solid;' : ''; ?>">
                                            <div class="cart-list-box-left">
                                                <img src="<?= $img; ?>">
                                            </div>
                                            <div class="cart-list-box-right">
                                                <div class="first-text">
                                                    <div class="first-text-left">
                                                        <h3><?= $product_details->product_name_one; ?></h3>
                                                        <p><?php
                                                            if (!empty($product_details->note)) {
                                                                echo $product_details->note;
                                                            }
                                                            ?></p>
                                                        <h6>
                                                            <?php
                                                            if (!empty($product_details->picked_size)) {
                                                                $li_size = explode('-', $product_details->picked_size);
                                                                foreach ($li_size as $sz_l) {
                                                                    if (($product_details->$sz_l == 0) || ($product_details->$sz_l == 00)) {
                                                                        echo $product_details->$sz_l;
                                                                    } else {
                                                                        echo!empty($product_details->$sz_l) ? $product_details->$sz_l . '&nbsp;&nbsp;' : '';
                                                                    }
                                                                }
                                                            }
                                                            if (!empty($product_details->primary_size) && ($product_details->primary_size == 'free_size')) {
                                                                echo "Free Size";
                                                            }
                                                            ?>                                            
                                                        </h6>

                                                        <div>
                                                            <strong>Style number</strong>
                                                            <span><?php 
                                                            $pid_li = explode('-',$data_li->prod_id);
                                                            $pid_last_data = end($pid_li);
                                                            echo str_replace('-'.$pid_last_data, '', $data_li->prod_id);
                                                            
                                                            ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="first-text-right">
                                                        <h5><?php
                                                            if (!empty($product_details->sale_price)) {
                                                                echo '$' . number_format($product_details->sale_price, 2);
                                                            }
                                                            ?></h5>
                                                    </div>
                                                </div>
                                                <div class="qty-box">
                                                    <div class="qty-box-left">
                                                        <label>Qty:</label>
                                                        <select>
                                                            <option><?= $data_li->quantity; ?></option>                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>   
                                    <?php
                                }
                            } else {
                                echo "<div class='col-sm-12'>   <h3 class='text text-danger'>No data found</h3></div>   ";
                            }
                            
                            if (!empty($all_prevs_data)) {
                                echo "<div class='col-sm-12'>   <h3 class='text text-info'>Previous selected extra products</h3></div>   ";
                                foreach ($all_prevs_data as $data_li) {
                                    $product_details = $data_li->in_product;

                                    if (empty($product_details->prodcut_id)) {
                                        $img = HTTP_ROOT_INV . PRODUCT_IMAGES . $product_details->product_image;
                                    } else {
                                        $img = HTTP_ROOT . PRODUCT_IMAGES . $product_details->product_image;
                                    }
                                    ?> 
                                    <div class="col-sm-12 col-sm-6 col-md-6">
                                        <div class="cart-list-box" style="<?= ($data_li->status == 2) ? 'border: red 5px solid;' : ''; ?>">
                                            <div class="cart-list-box-left">
                                                <img src="<?= $img; ?>">
                                            </div>
                                            <div class="cart-list-box-right">
                                                <div class="first-text">
                                                    <div class="first-text-left">
                                                        <h3><?= $product_details->product_name_one; ?></h3>
                                                        <p><?php
                                                            if (!empty($product_details->note)) {
                                                                echo $product_details->note;
                                                            }
                                                            ?></p>
                                                        <h6>
                                                            <?php
                                                            if (!empty($product_details->picked_size)) {
                                                                $li_size = explode('-', $product_details->picked_size);
                                                                foreach ($li_size as $sz_l) {
                                                                    if (($product_details->$sz_l == 0) || ($product_details->$sz_l == 00)) {
                                                                        echo $product_details->$sz_l;
                                                                    } else {
                                                                        echo!empty($product_details->$sz_l) ? $product_details->$sz_l . '&nbsp;&nbsp;' : '';
                                                                    }
                                                                }
                                                            }
                                                            if (!empty($product_details->primary_size) && ($product_details->primary_size == 'free_size')) {
                                                                echo "Free Size";
                                                            }
                                                            ?>                                            
                                                        </h6>

                                                        <div>
                                                            <strong>Style number</strong>
                                                            <span><?php 
                                                            $pid_li = explode('-',$data_li->prod_id);
                                                            $pid_last_data = end($pid_li);
                                                            echo str_replace('-'.$pid_last_data, '', $data_li->prod_id);
                                                            
                                                            ?></span>
                                                        </div>

                                                        <?php if ($data_li->status == 2) { ?> 
                                                            <div>
                                                                <strong>Fit number</strong>
                                                                <span><?= $data_li->fit_cnt; ?></span>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="first-text-right">
                                                        <h5><?php
                                                            if (!empty($product_details->sale_price)) {
                                                                echo '$' . number_format($product_details->sale_price, 2);
                                                            }
                                                            ?></h5>
                                                    </div>
                                                </div>
                                                <div class="qty-box">
                                                    <div class="qty-box-left">
                                                        <label>Qty:</label>
                                                        <select>
                                                            <option><?= $data_li->quantity; ?></option>                                           
                                                        </select>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>   
                                    <?php
                                }
                            } 
                            ?>

                        </div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->