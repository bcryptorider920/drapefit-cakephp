<section class="banner">
    <div id="banner" class="owl-carousel">
      <div class="item">

              <img src="<?= $this->Url->image('banner8.jpg'); ?>">
              <div class="untitled__slideContent slide-text">
                <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <span>Experience Styling Service </span> 
        <a class="button" href="https://www.google.com" target="/black">Take Your Style Quiz</a>
      </div>
    </div>
  </div>
      </div>
            </div>
            <!--TESTIMONIAL 1 -->
             <div class="item">
              <img src="<?= $this->Url->image('banner10.jpg'); ?>">
              <div class="untitled__slideContent slide-text">
                <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <span>Style Delivered To Your Door</span> 
        <a class="button" href="https://www.google.com" target="/black">Take Your Style Quiz</a>
      </div>
    </div>
  </div>
      </div>
            </div>
            
            <!--END OF TESTIMONIAL 1 -->
            <!--TESTIMONIAL 2 -->
           <div class="item">
              <img src="<?= $this->Url->image('banner6.jpg'); ?>">
              <div class="untitled__slideContent slide-text slide-text2">
                <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <span>Build A Look You Love</span> 
        <a class="button" href="https://www.google.com" target="/black">Take Your Style Quiz</a>
      </div>
    </div>
  </div>
      </div>
            </div>
            <!--END OF TESTIMONIAL 2 -->
            <!--TESTIMONIAL 3 -->
            <div class="item">
              <img src="<?= $this->Url->image('banner2.jpg'); ?>" style="margin-top: 0;">
      <div class="untitled__slideContent slide-text">
        <div class="container">
      <div class="row">
        <div class="col-sm-12">
        <span>Enjoy Effortless Shopping</span> 
        <a class="button" href="https://www.google.com" target="/black">Take Your Style Quiz</a>
      </div>
    </div>
  </div>
      </div>
            </div>
          </div>
  </section>
