<section class="expect-box">    
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="section-head">
                    <h2>What To Expect From Us</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <ul>
                    <li>
                        <div class="expect-box-small">
                            <div class="boxes1">
                                <span>01</span>
                                <h4>ORDER ON DEMAND</h4>
                                <p>You can <a href="https://www.drapefit.com/">order your FIT Box</a> on your need.Schedule online and cancel any time.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="expect-box-small">
                            <div class="boxes1">
                            <span>02</span>
                            <h4>TRY IN HOME</h4>
                            <p>A FIT Box will be delivered in your door.Try at home and return if you don’t like.You have 5 days to buy or return.It’s that easy.</p>
                        </div>
                        </div>
                    </li>
                    <li>
                        <div class="expect-box-small">
                            <div class="boxes1">
                            <span>03</span>
                            <h4>SAVE UP TO 25%</h4>
                            <p>Plenty options to save.If you keep all items you can save up to 25%.</p>
                        </div>
                        </div>
                    </li>
                    <li>
                        <div class="expect-box-small">
                            <div class="boxes1">
                            <span>04</span>
                            <h4>FREE SHIPPING AND RETURNS</h4>
                            <p>Free shipping both the ways.Keep what you like. Return what doesn’t FIT you.</p>
                        </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="tell-us-box">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="section-head">
                    <h2>Tell Us What You Like</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <ul>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                 <img src="<?= $this->Url->image('img_a.jpg'); ?>">
                            </div>
                            <h4>Women <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                     <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_c.jpg'); ?>">
                            </div>
                            <h4>Men <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_b.jpg'); ?>">
                            </div>
                            <h4>Plus Size <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                   
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_e.jpg'); ?>">
                            </div>
                            <h4>Big and Tall <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_d.jpg'); ?>">
                            </div>
                            <h4>Kids <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_f.jpg'); ?>">
                            </div>
                            <h4>Women Active wear <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_g.jpg'); ?>">
                            </div>
                            <h4>Plus Size Active Wear <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_h.jpg'); ?>">
                            </div>
                            <h4>Women Jeans <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="tell-img-box">
                                <img src="<?= $this->Url->image('img_i.jpg'); ?>">
                            </div>
                            <h4>Women Business wear <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></h4>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>