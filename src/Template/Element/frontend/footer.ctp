<?php

use Cake\ORM\TableRegistry;

$action = $this->request->params['action'];
if ($this->request->session()->read('PROFILE') == 'KIDS') {
    if ($this->request->session()->read('KID_ID')) {
        $kidsDetails = TableRegistry::get('kidsDetails');
        $Usersdata = $kidsDetails->find('all')->where(['id' => $this->request->session()->read('KID_ID')])->first();

        if ($Usersdata->is_redirect == 0 && @$Usersdata->is_progressbar != 100) {
            $url = 'welcome/style/';
        } elseif ($Usersdata->is_redirect == 0 && $Usersdata->is_progressbar == 100) {
            $url = 'welcome/schedule/';
        } elseif ($Usersdata->is_redirect == 0) {
            $url = 'welcome/style/';
        } elseif ($Usersdata->is_redirect == 1) {
            $url = 'welcome/schedule/';
        } elseif ($Usersdata->is_redirect == 2) {
            $url = 'not-yet-shipped';
        } elseif ($Usersdata->is_redirect == 3) {
            $url = 'profile-review/';
        } elseif ($Usersdata->is_redirect == 4) {
            $url = 'order_review/';
        } elseif ($Usersdata->is_redirect == 5) {
            $url = 'calendar-sechedule/';
        } elseif ($Usersdata->is_redirect == 6) {
            $url = 'customer-order-review';
        }
    }
} else {
    $Users = TableRegistry::get('Users');
    $UserDetails = TableRegistry::get('UserDetails');
    $Usersdata = $Users->find('all')->where(['id' => $this->request->getSession()->read('Auth.User.id')])->first();
    $UserDetailsdata = $UserDetails->find('all')->where(['user_id' => $this->request->getSession()->read('Auth.User.id')])->first();
    if (@$Usersdata->is_redirect == 0 && @$UserDetailsdata->is_progressbar != 100) {
        $url = 'welcome/style/';
    } elseif (@$Usersdata->is_redirect == 0 && @$UserDetailsdata->is_progressbar == 100) {
        $url = 'welcome/schedule/';
    } elseif ($Usersdata->is_redirect == 0) {
        $url = 'welcome/style/';
    } elseif ($Usersdata->is_redirect == 1) {
        $url = 'welcome/schedule/';
    } elseif ($Usersdata->is_redirect == 2) {
        $url = 'not-yet-shipped';
    } elseif ($Usersdata->is_redirect == 3) {
        $url = 'profile-review/';
    } elseif ($Usersdata->is_redirect == 4) {
        $url = 'order_review/';
    } elseif ($Usersdata->is_redirect == 5) {
        $url = 'calendar-sechedule/';
    } elseif ($Usersdata->is_redirect == 6) {
        $url = 'customer-order-review';
    }
}
?>
</div>

<!--   <center>-->
<!-- Begin Mailchimp Signup Form -->
<!--<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">-->
<!--<style type="text/css">-->
<!--	#mc_embed_signup{ clear:left; font:14px Helvetica,Arial,sans-serif;  width:500px;}-->
	<!--/* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.-->
	<!--   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */-->
<!--</style>-->
<!--<div class="footer-subscribe" style="text-align: center;-->
<!--padding: 50px 0 40px;-->
<!--background-color: #f7f7f7;">-->
<!--<div id="mc_embed_signup" style="margin:auto;">-->
<!--<form action="https://drapefit.us4.list-manage.com/subscribe/post?u=c4d312025b5781525c229b745&amp;id=0d6810bbe6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>-->
<!--    <div id="mc_embed_signup_scroll" style="margin-bottom: 40px;">-->
<!--	<h2>Join the Drape Fit Community</h2>-->
<!--<div class="indicates-required"><span class="asterisk">*</span> indicates required</div>-->
<!--<P>Subscribe to our newsletter to receive update exclusive offer, style tips and more to your inbox</P>-->
<!--<div class="mc-field-group">-->
<!--	<label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>-->
<!--</label>-->
<!--	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">-->
<!--</div>-->
<!--	<div id="mce-responses" class="clear">-->
<!--		<div class="response" id="mce-error-response" style="display:none"></div>-->
<!--		<div class="response" id="mce-success-response" style="display:none"></div>-->
<!--	</div>    real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<!--    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c4d312025b5781525c229b745_0d6810bbe6" tabindex="-1" value=""></div>-->
<!--    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>-->
<!--    </div>-->
<!--</form>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>-->
<!--End mc_embed_signup-->
<!--</center>-->




<?php if ($action != 'adminlogin') { ?>
    <section class="footer">
        <div class="container"> 
            <div class="row">
                <div class="col-md-3">
                    <div class="footer-logo">
    <?php if (@$this->request->session()->read('Auth.User.id')) { ?>

                            <a href="<?= HTTP_ROOT . $url ?>">    

                                <?php } else { ?>
                                <a href="<?= HTTP_ROOT; ?>"> 
    <?php } ?>
                                <img src="<?= $this->Url->image('logo.png'); ?>" alt="drapfit logo">
                            </a>
                            </br>
                            <p>WE DO BEST FIT</p>
                            <div class="footer-social">
                                <a class="us-country" href="#">United States</a>
                                <h3>Follow Us :</h3>
    <?php echo $this->cell('Chat::social_media'); ?>
                            </div> 


                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-links">
                        <h3>About Us</h3>
                        <ul>
                            <!-- <a href="footer.ctp"></a> -->
                            <li><a href="<?php echo HTTP_ROOT . 'who-we-are' ?>">Who We Are</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'our-mission' ?>">Our Mission</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'our-stylist' ?>">Our Stylist</a></li>
                            <!-- <li><a href="<?php echo HTTP_ROOT . 'news' ?>">News</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'investors' ?>">Investors</a></li> -->
                            <!--<li><a href="<?php echo HTTP_ROOT . 'executive-team' ?>">Executive Team</a></li>-->
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-links">
                        <h3>Service</h3>
                        <ul>
                            <?php // if($action == "women" || $action == "plusSize" || $action == "maternity" || $action == "petite" || $action == "womenJeans" || $action == "womenBusiness"){ ?>

                            <?php //} else if($action == "men" || $action == "bigTall" || $action == "styleGuide") { ?>

                            <?php /* ?><li><a href="<?php echo HTTP_ROOT . 'men/style-guide' ?>">The Style Guide</a></li><?php */ ?>
    <?php //}else{  ?>
                            <li><a href="<?php echo HTTP_ROOT . 'men' ?>">Men</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'men/big-tall' ?>">Big & Tall</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women' ?>">Women</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women/plus-size' ?>">Plus size Fits</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women/maternity' ?>">Maternity Fits</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women/petite' ?>">Petite</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women/women-jeans' ?>">Women Jeans</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'women/women-business' ?>">Women Business Fits</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'kids' ?>">Kids</a></li>
    <?php //} ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-links">
                        <h3>The Company</h3>
                        <ul>
                            <li><a href="http://styleguide.drapefittest.com/<?php //echo HTTP_ROOT . 'style-blog'  ?>" target="_blank">The Style Guide</a></li> 
                            <li><a href="<?php echo HTTP_ROOT . 'news' ?>">News</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'investors' ?>">Investors Relation</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'careers' ?>">Careers</a></li>
    <!--                         <li><a href="<?php echo HTTP_ROOT . 'feedback-review' ?>">Feedback & Reviews</a></li>
                            
                            <li><a href="<?php echo HTTP_ROOT . 'feedback-review' ?>">Feedback & Reviews</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'feedback-review' ?>">Feedback & Reviews</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'feedback-review' ?>">Feedback & Reviews</a></li> -->
    <?php /* ?><li><a href="<?php echo HTTP_ROOT . 'terms-conditions' ?>">Terms & Conditions</a></li><?php */ ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer-links">
                        <h3>Recommended</h3>
                        <ul>
                            <li><a href="<?php echo HTTP_ROOT . 'how-it-works' ?>">How Drape Fit Works</a></li>                
                            <li><a href="<?php echo HTTP_ROOT . 'box-pricing' ?>">FIT Box Pricing</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'personal-stylist' ?>">Personal Stylist</a></li>
                        </ul>
                    </div>           
                </div>   
                <div class="col-md-2">
                    <div class="footer-links">
                        <h3>Customer Care</h3>
                        <ul>
                            <li><a href="<?php echo HTTP_ROOT . 'faq' ?>">FAQ</a></li>                
                            <li><a href="<?php echo HTTP_ROOT . 'gifts' ?>">Gift Card</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'return-exchange' ?>">Return & Exchange</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'track-order' ?>">Track Order</a></li>
                            <!--<li><a href="<?php echo HTTP_ROOT . 'help-center' ?>">Help Center</a></li>-->
                            <li><a href="#">Help Center</a></li>
                            <li><a href="<?php echo HTTP_ROOT . 'contact-us' ?>">Contact Us</a></li>
                            <!-- <li><a href="<?php echo HTTP_ROOT . 'privacy-policy' ?>">Track Order</a></li> -->
                        </ul>
                    </div>
                </div>

            </div>
        </div>
     
    </section>

    <!--<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6931110.js"></script>-->
<?php
}?>