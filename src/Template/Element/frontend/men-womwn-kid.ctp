<section class="three-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-4 col-md-4">
                <a href="<?php echo HTTP_ROOT.'women'?>">
                <div class="pr-box">
                    <div class="pr-box-img">
                        <img src="<?= $this->Url->image('Women14.jpg'); ?>">
                    </div>
                    <div class="pr-box-text">
                        <h4>WOMEN</h4>
                        <p>Receive a FIT monthly</p>
                    </div>
                    <!-- <span class="view"><i class="fa fa-eye" aria-hidden="true"></i></span> -->
                </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 col-md-4">
                <a href="<?php echo HTTP_ROOT.'men'?>">
                <div class="pr-box">
                    <div class="pr-box-img">
                        <img src="<?= $this->Url->image('Men15.jpg'); ?>">
                    </div>
                    <div class="pr-box-text">
                        <h4>MEN</h4>
                        <p>Receive a FIT monthly</p>
                    </div>
                    <!-- <span class="view"><i class="fa fa-eye" aria-hidden="true"></i></span> -->
                </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 col-md-4">
                <a href="<?php echo HTTP_ROOT.'kids'?>">
                <div class="pr-box">
                    <div class="pr-box-img">
                        <img src="<?= $this->Url->image('Kids8.jpg'); ?>">
                    </div>
                    <div class="pr-box-text">
                        <h4>KIDS</h4>
                        <p>Receive a FIT monthly</p>
                    </div>
                    <!-- <span class="view"><i class="fa fa-eye" aria-hidden="true"></i></span> -->
                </div>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="brand">
    <div class="container">
    <div class="row">
            <div class="col-sm-12">
                <div class="section-head">
                    <h2>Brands Are Ready For You</h2>
                    <p>We are working with many brands.According to your selection we will ship a complete FIT Box that will FIT under your budget.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="brand-image">
                    <ul>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>penguin.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>nike.png" alt="">
                            </div>
                        </li>

                        <li>
                            <div class="big-images">
                                <img src="<?= HTTP_ROOT . MAN ?>scotch.png" alt="">
                            </div>
                        </li>

                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>gap.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>pata.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="big-images">
                                <img src="<?= HTTP_ROOT . MAN ?>tommy.png" alt="">
                            </div>
                        </li>

                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>boss.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>vineyard.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>vans.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>hurley.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>brooks.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>zara.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>levis.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>armour.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>bonobos.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>landsend.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>jcrew.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>asos.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>mango.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>hm.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>uniqlo.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>northface.png" alt="">
                            </div>
                        </li>
                        <!-- <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>h&m.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>eagle.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>ragnbone.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>bensharma.png" alt="">
                            </div>
                        </li>
                        <li>
                            <div class="small-images">
                                <img src="<?= HTTP_ROOT . MAN ?>express.png" alt="">
                            </div>
                        </li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
 <!-- testimonial section added by suprakash 16-12-2020 -->
<div class="clearfix"></div>


<section class="full-section testimonial-kids testim" id="section-3">
   <div class="full-section-container">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <div class="owl-carousel testimonials-slider style-2">
                  <div class="item">
                     <div class="text-box">
                        <div class="testimonial">
                           <blockquote>
                                      <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                              <p class="pt-15">Drape Fit has been a blast . The stylist I have been working with did a great job of analyzing my needs . The styling and fit of all the outfits have exceeded my expectations. Love the prices of all outfits. The quality has been good with unique styles.It’s been fun! </p>
                           </blockquote>
                           <!--<img src="images/testimonials/image-8.jpg" alt="">-->
                            <h5>
                              - Jennifer H.
                              <!--<span>Clients</span>-->
                              <small>Bozeman, MT</small>
                           </h5>
                        </div>
                        <!-- testimonial -->
                     </div>
                     <!-- text-box -->
                  </div>
                  <!-- item -->
                  <div class="item">
                     <div class="text-box">
                        <div class="testimonial">
                           <blockquote>
                                      <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                              <p class="pt-15">Drape Fit incredibly convenient & time savings for those of us who don’t have much free time to go shopping. I just got my 2nd Fit Box with unique spring styles and very much affordable prices. Love their customer service which is absolutely amazing. </p>
                           </blockquote>
                           <!--<img src="images/testimonials/image-8.jpg" alt="">-->
                            <h5>
                              - Rebecca M.
                              <!--<span>Clients</span>-->
                              <small>Miami , FL</small>
                           </h5>
                        </div>
                        <!-- testimonial -->
                     </div>
                     <!-- text-box -->
                  </div>
                  <!-- item -->
                  
                  <!-- item -->
                  <div class="item">
                     <div class="text-box">
                        <div class="testimonial">
                           <blockquote>
                                      <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                              <p class="pt-15">I love this personal styling service . My stylist has been attentive to my style choices and price points . If you complete your style quiz properly, you will receive beautiful outfits. </p>
                           </blockquote>
                           <!--<img src="images/testimonials/image-8.jpg" alt="">-->
                            <h5>
                              - David L . 
                              <!--<span>Clients</span>-->
                              <small>Toledo , OH</small>
                           </h5>
                        </div>
                        <!-- testimonial -->
                     </div>
                     <!-- text-box -->
                  </div>
                  <!-- item -->
                  
                  <!-- item -->
                  <div class="item">
                     <div class="text-box">
                        <div class="testimonial">
                           <blockquote>
                                      <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                              <p class="pt-15">I started with a Fit Box for my son . Did the style quiz which is very specific. Customer service is awesome and so are the boxes. I think it’s really useful for your kids and your family. $20/ month isn’t not at all bad for styling fees. Definitely keeping Drape Fit ! </p>
                           </blockquote>
                           <!--<img src="images/testimonials/image-8.jpg" alt="">-->
                            <h5>
                              - Kate S. 
                              <!--<span>Clients</span>-->
                              <small>Austin , IN</small>
                           </h5>
                        </div>
                        <!-- testimonial -->
                     </div>
                     <!-- text-box -->
                  </div>
                  <!-- item -->
                  
                  <!-- item -->
                  <div class="item">
                     <div class="text-box">
                        <div class="testimonial">
                           <blockquote>
                                      <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                            <span class="fa fa-star checked" style="color: orange;"></span>
                              <p class="pt-15">I started using Drape Fit few months before. I hate shopping. So Drape Fit seemed like the perfect way to get some good quality clothes I liked - and to step out my comfort zone a little bit by trying something different styles. I love this service.</p>
                           </blockquote>
                           <!--<img src="images/testimonials/image-8.jpg" alt="">-->
                            <h5>
                             - Al V.
                              <!--<span>Clients</span>-->
                              <small>Hudson , NY </small>
                           </h5>
                        </div>
                        <!-- testimonial -->
                     </div>
                     <!-- text-box -->
                  </div>
                  <!-- item -->
                  
                  
                  <!-- item -->
               </div>
               <!-- testimonials-slider -->
            </div>
            <!-- col -->
         </div>
         <!-- row -->
      </div>
      <!-- container -->
   </div>
   <!-- full-section-container -->
</section>
<!-- full-section -->
<section class="maternity-box fax-boxes">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                <div class="section-head">
                <h2>Faq</h2>
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div aria-multiselectable="true" class="panel-group" id="accordion" role="tablist">
                        <div class="row">
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="panel panel-default">
                            <div class="panel-heading active" id="headingOne" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseOne" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseOne" role="button">How many Fits come in a DrapeFit Box?</a></h4>
                            </div>
                            <div aria-labelledby="headingOne" class="panel-collapse collapse" id="collapseOne" role="tabpanel">
                                <div class="panel-body">
                                    <p>There are 6 Fits of each DrapeFit Box.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" id="headingTwo" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseTwo" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo" role="button">What sizes do you carry?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingTwo" class="panel-collapse collapse" id="collapseTwo" role="tabpanel">
                                <div class="panel-body">
                                    <p>We currently carry women's sizes 0-12, XS-XL and waist size 24-32. We  also offer maternity or plus size at this time.</p>
                                    <p>We currently offer men’s sizes  S, M, L , XL, XXL and waist size 30-34 .We also Provide Big & Tall Sizes .</p>
                                    <p>We currently offer kid’s size  newborn - 4T and Boy & Girl 5-14 </p>
                                </div>
                            </div>
                        </div>
    
                            </div>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                 <div class="panel panel-default">
                            <div class="panel-heading" id="headingThree" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseThree" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseThree" role="button">How many DrapeFit Box do I get each month?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingThree" class="panel-collapse collapse" id="collapseThree" role="tabpanel">
                                <div class="panel-body">
                                    <p>we have subscription plan that include per month, every two month, every three months. To get your next Fit Box, keep what you love from your previous Fit Box and return rest in the USPS prepaid return envelop provided for you and drop it in the mail. As soon as you are eligible, we’ll send you an email letting you know it’s time to pick out the next Fit Box.</p>
    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" id="headingfour" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapsefour" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapsefour" role="button">How is the billing policy?</a></h4>
                            </div>
    
                            <div aria-labelledby="collapsefour" class="panel-collapse collapse" id="collapsefour" role="tabpanel">
                                <div class="panel-body">
                                    <p>For your first box, a $20 styling fee will be charged as soon as you order . For each subsequently scheduled box, a $20 styling fee will be charged after your Stylist begins styling your FIT Box.</p>
                                </div>
                            </div>
                        </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="panel panel-default">
                            <div class="panel-heading" id="headingfive" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapsefive" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapsefive" role="button">How are my items chosen?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingfive" class="panel-collapse collapse" id="collapsefive" role="tabpanel">
                                <div class="panel-body">
                                    <p>First take your style quiz and your stylist will analyze your profile as per your age, size and budget. Stylist will handpicked and make a perfect style Fit Box for you.</p> 
<p>You can pin, screenshot and share what you love most from our social media platforms and share it with your Stylist. Add your Instagram or Facebook username to your style profile!</p>
<p>Remember to follow us on Facebook, Instagram, Pinterest, and Twitter to get the latest styles and sneak peeks of what's new at DRAPE FIT.
</p>
    
                                </div>
                            </div>
                        </div>
                            </div>

                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="panel panel-default">
                            <div class="panel-heading" id="headingsix" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapsesix" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapsesix" role="button">What if I need a break or want to change my plan?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingsix" class="panel-collapse collapse" id="collapsesix" role="tabpanel">
                                <div class="panel-body">
                                    <p>There is no commitment with DrapeFit. You can change, pause or cancel your subscription at any time. Or you can email us to <a href="mailto:support@drapefit.com">support@drapefit.com</a> for pause your subscription.
</p>
    
                                </div>
                            </div>
                        </div>
                            </div>


                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>

 <!-- end suprakash -->