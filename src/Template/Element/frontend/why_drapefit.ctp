
<section class="fabulous-ways-to fabulous-ways-to2">
    <div class="find-left-right-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="fabulous-main">
                        <div class="fabulous-img">
                            <img src="<?= $this->Url->image('fab-img4.jpg'); ?>">
                        </div>
                    <div class="find-left">
                        <div>
                        <h3>WHY DRAPE FIT?</h3>
                        <p>Drape Fit is a personal styling service that sends the best clothing subscription boxes in the market. We offer the latest hand-picked styles right to your door every month in the monthly fashion box. We do personalized style creation for Men, Women, and Kids for a monthly clothing box for you. Save your time and money with Drape Fit's affordable fashion subscription box.</p>
                        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display = 'block'" class="sign-up-btn">Take your style quiz</a>
                        </div>
                    </div>
                    </div>
                    <div class="fabulous-main"> 
                    <div class="find-left">
                        <div>
                         <h3>HOW WE ARE DIFFERENT FROM OTHERS?</h3>
                        <p>We provide an effortless personal styling experience that perfectly Fits your body shape and size. Client's satisfaction is our satisfaction. We offer to try in-home, order on-demand, and free shipping & returns. We offer trendy, high-quality outFits in the monthly fashion box to your needs and Fits. We have the best team of professional stylist who loves to help you get the perfect style you want.</p>
                        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display = 'block'" class="sign-up-btn">Take your style quiz</a>
                        </div>
                    </div>
                    <div class="fabulous-img">
                            <img src="<?= $this->Url->image('fab-img3.jpg'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

