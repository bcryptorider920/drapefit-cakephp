<section class="how-it-work">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="it-work-top">
                        <div class="section-head">
                            <h2>How it Works</h2>
                        </div>
<div class="section-head2">
                        <div class="row">
                            <div class="col-sm-12 col-lg-4 col-md-4">
                                <div class="how-it-item">
                                    <div class="item-round-images">
                                        <span class="round-img">
                                            <img src="<?= $this->Url->image('keep-icon3.png'); ?>">
                                        </span>
                                    </div>
                    <h3>01. Fill out your style quiz</h3>
                    <p>Share your style, price, size and style preferences with your personal stylist.</p>
                                    
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-4 col-md-4">
                                <div class="how-it-item">
                                    <div class="item-round-images">
                                        <span class="round-img">
                                            <img src="<?= $this->Url->image('keep-icon2.png'); ?>">
                                        </span>
                                    </div>
                                    <h3>02. Request A Fit Box</h3>
                    <p>Get up to 5 hand-picked  pieces
of clothing by your personal stylist  delivered to your
home, monthly or quarterly.Save money and time.</p>
                                    
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-4 col-md-4">
                                <div class="how-it-item">
                                    <div class="item-round-images">
                                        <span class="round-img">
                                        <img src="<?= $this->Url->image('keep-icon.png'); ?>">
                                        </span>
                                    </div>
                                    <h3>03. Keep What Love</h3>
                    <p>Keep which fit you. Send back rest.  You’re only billed for what you keep, and your styling fee is applied to your purchase. Shipping is free both ways.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="text-center pb-40 mt-5">
                            <div class="take-box">
                            <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display = 'block'" class="sign-up-btn">Take your style quiz</a>
                        </div>
                            <br>
                            <span>
                            <a href="javascript:void(0)" class="sign-up-member">Already have an account?</a>
                            <a href="javascript:void(0)" class="sign-in" onclick="document.getElementById('id01').style.display = 'block'">Sign in</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="fabulous-ways-to">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-head">
                    <h2>Two fabulous ways to find what you love</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="find-left-right-box">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="fabulous-main">
                        <div class="fabulous-img">
                            <img src="<?= $this->Url->image('fab-img1.jpg'); ?>">
                        </div>
                    <div class="find-left">
                        <div>
                        <h3>Get pieces hand-selected by our professional stylists</h3>
                        <p>Try on the best pieces at home when you get a Drape Fit clothing subscription box, keep the favourite apparels and send back the rest to us. A $20 Drape Fit styling fee covers your best online personal stylist in USA time and expertise – it gets credited toward anything you buy.</p>
                        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display = 'block'" class="sign-up-btn">Take your style quiz</a>
                        </div>
                    </div>
                    </div>
                    <div class="fabulous-main">
                        
                    <div class="find-left">
                        <div>
                         <h3>On the instant buy pieces curated just for your style</h3>
                        <p>Leap hours of browsing and discover pieces curated just for your look, style and life. After getting your first Drape Fit Monthly Fashion Box, on the instant buy what you want and when you want it, to feel your best.</p>
                        <a href="javascript:void(0)" onclick="document.getElementById('id01').style.display = 'block'" class="sign-up-btn">Take your style quiz</a>
                        </div>
                    </div>
                    <div class="fabulous-img">
                            <img src="<?= $this->Url->image('fab-img2.jpg'); ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>