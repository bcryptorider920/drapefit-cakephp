<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta https-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title><?php echo!empty($title_for_layout) ? $title_for_layout : SITE_NAME; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?= $this->fetch('content'); ?>
    </body>
</html>