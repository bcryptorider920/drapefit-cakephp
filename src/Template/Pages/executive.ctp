

<link rel="stylesheet" type="text/css" href="<?php echo HTTP_ROOT ?>assets/css/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="demo-banner-box">
   <!-- <img src="assets/images/FIS_Global_Banner_Executive_Team.jpg">-->
   <h1>Executive Team</h1>
</div>
<div class="officer-list-box">
   <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="it-work-top">
      <div class="row">
         <div class="col-sm-12 cl-lg-12 col-md-12 df-detls team-heading">
          <div class="section-head" data-aos="fade-down">
                            <h2><span>Our</span> Executive Team</h2>
                            <img src="/img/header-booten.png">
                        </div>
         </div>
      <div class="col-md-8 col-md-offset-2">
      <div class="row team-gggg">
         <div class="col-sm-6">
            <div class="your-team-yyzzzzz">
               <picture class="src-components-ResponsiveImage">
                  <img src="<?php echo HTTP_ROOT ?>assets/images/shukhendu.jpg">
               </picture>
               <div class="team-contan">
                  <h3>Sukhendu Mukherjee</h3>
                  <h6>CEO & CO-FOUNDER</h6>
                  <p>Sukhendu always love about style and technology . His Drape Fit  idea actually came about from the time wanting to create a platform that made it really easy to shop entire  perfect  outfits from head-to-toe for everyone as per their styles ,needs and  budget.Prior to Co-founding Drape Fit Sukhendu developed better experience in styles , retails and technologies </p>
                  <button class="bbt-read bbt-read-1" href="#">Read More</button>
               </div>
            </div>
         </div>
         <div class="team-tetail-pup-box box-1" style="display: none;">
            <div class="detail-box">
               <span class="closees bbt-read bbt-read-1"><i class="fa fa-times" aria-hidden="true"></i></span>
               <h4>Sukhendu Mukherjee</h4>
               <p>CEO & CO-FOUNDER</p>
               <p>Sukhendu always love about style and technology . His Drape Fit  idea actually came about from the time wanting to create a platform that made it really easy to shop entire  perfect  outfits from head-to-toe for everyone as per their styles ,needs and  budget.Prior to Co-founding Drape Fit Sukhendu developed better experience in styles , retails and technologies .He is very passionate about styles.Previously  He consulted and experienced with dozens of Leading companies in USA including e-commerce and retailers . He worked as a Lead Engineer  at  different IT  industry powerhouses in worldwide.Sukhendu holds an engineering degree in Information Technology. He involves with lots of social work for  developing our society.He helps people to experience <a href="https://drapefit.com/executive-team">personal styling everyday</a> through Drape Fit .</p>
               <p style="font-size: 18px;color:orange;font-weight: bold;font-style: italic;font-family:Comic Sans MS,cursive;">
                  "Drape  Fit is about helping people to find the best Fit and Try before Buy to save time and money from Work to Workout.“
               </p>
               <h4>Sukhendu Mukherjee</h4>
               <p>CEO & CO-FOUNDER</p>
            </div>
         </div>
         <div class="col-sm-6">
            <div class="your-team-yyzzzzz">
               <picture class="src-components-ResponsiveImage">
                  <img src="<?php echo HTTP_ROOT ?>assets/images/monalisha.jpg">
               </picture>
               <div class="team-contan">
                  <h3>Monomita Chakraborty</h3>
                  <h6>CEO & CO-FOUNDER</h6>
                  <p>Monomita is the Co-Founder and Chief Information Officer at Drape Fit , overseeing operations , styling and client experience and merchandising . She Co-founded  Drape Fit to deliver a superior customer styling experience that helping people to achieve everyday confidence.She thinks Drape Fit helps everyone to explore their shopping experience through their</p>
                  <button class="bbt-read bbt-read-2" href="#">Read More</button>
               </div>
            </div>
         </div>
         <div class="team-tetail-pup-box box-2" style="display: none;">
            <div class="detail-box">
               <span class="closees bbt-read bbt-read-2"><i class="fa fa-times" aria-hidden="true"></i></span>
               <h4>Monomita Chakraborty</h4>
               <p>CEO & CO-FOUNDER</p>
               <p>Sukhendu always love about style and technology . His Drape Fit  idea actually came about from the time wanting to create a platform that made it really easy to shop entire  perfect  outfits from head-to-toe for everyone as per their styles ,needs and  budget.Prior to Co-founding Drape Fit Sukhendu developed better experience in styles , retails and technologies .He is very passionate about styles.Previously  He consulted and experienced with dozens of Leading companies in USA including e-commerce and retailers . He worked as a Lead Engineer  at  different IT  industry powerhouses in worldwide.Sukhendu holds an engineering degree in Information Technology. He involves with lots of social work for  developing our society.He helps people to experience <a href="https://drapefit.com/executive-team">personal styling everyday</a> through Drape Fit .</p>
               <p style="font-size: 18px;color:orange;font-weight: bold;font-style: italic;font-family:Comic Sans MS,cursive;">
                  "Drape  Fit is about helping people to find the best Fit and Try before Buy to save time and money from Work to Workout.“
               </p>
               <h4>Monomita Chakraborty</h4>
               <p>CEO & CO-FOUNDER</p>
            </div>
         </div>
      </div>
      </div>
    </div>
      <div class="row">
         <div class="col-sm-12 cl-lg-12 col-md-12 df-detls team-heading">
            <h2>Advisory Board</h2>
         </div>
      
      <div class="col-md-8 col-md-offset-2">
      <div class="row team-gggg">
         <div class="col-sm-6 col-centered">
            <div class="your-team-yyzzzzz">
               <picture class="src-components-ResponsiveImage">
                  <img src="<?php echo HTTP_ROOT ?>assets/images/doug.jpg">
               </picture>
               <div class="team-contan">
                  <h3>Doug Thorpe </h3>
                  <h6>Executive Leader</h6>
                  <p>Executive Leader & Coach | Breaking through the Invisible Wall between Management and Leadership</p>
                  <button class="bbt-read bbt-read-3" href="#">Read More</button>
               </div>
            </div>
         </div>
         <div class="team-tetail-pup-box box-3" style="display: none;">
            <div class="detail-box">
                
               <span class="closees bbt-read bbt-read-3"><i class="fa fa-times" aria-hidden="true"></i></span>
               <h4>Doug Thorpe </h4>
               <p>Executive Leader</p>
               <p>Executive Leader & Coach | Breaking through the Invisible Wall between Management and Leadership</p>
               
              <h4>Doug Thorpe </h4>
               <p>Executive Leader</p>
            </div>
         </div>
         
      </div>
      </div>
    </div>
      <!-- <ul>
         <li>
             <div class="img-box">
                 <img src="<?php echo HTTP_ROOT ?>assets/images/shukhendu.jpg">
             </div>
             <div class="detail-box">
             <h4>Sukhendu Mukherjee</h4>
             <p>CEO & CO-FOUNDER</p>
             <p>Sukhendu always love about style and technology . His Drape Fit  idea actually came about from the time wanting to create a platform that made it really easy to shop entire  perfect  outfits from head-to-toe for everyone as per their styles ,needs and  budget.Prior to Co-founding Drape Fit Sukhendu developed better experience in styles , retails and technologies .He is very passionate about styles.Previously  He consulted and experienced with dozens of Leading companies in USA including e-commerce and retailers . He worked as a Lead Engineer  at  different IT  industry powerhouses in worldwide.Sukhendu holds an engineering degree in Information Technology. He involves with lots of social work for  developing our society.He helps people to experience <a href="https://drapefit.com/executive-team">personal styling everyday</a> through Drape Fit .</p>
             <p style="font-size: 18px;color:orange;font-weight: bold;font-style: italic;font-family:Comic Sans MS,cursive;">
                 "Drape  Fit is about helping people to find the best Fit and Try before Buy to save time and money from Work to Workout.“</p>
             <h4>Sukhendu Mukherjee</h4>
             <p>CEO & CO-FOUNDER</p>
             </div>
         </li>
         <li>
             <div class="img-box">
                 <img src="<?php echo HTTP_ROOT ?>assets/images/monalisha.jpg">
             </div>
             <div class="detail-box">
             <h4>Monomita Chakraborty</h4>
             <p>CIO & CO-FOUNDER</p>
             <p>Monomita is the Co-Founder and Chief Information Officer at Drape Fit , overseeing operations , styling and client experience and merchandising . She Co-founded  Drape Fit to deliver a superior customer styling experience that helping people to achieve everyday confidence.She thinks Drape Fit helps everyone to explore their shopping experience through their own affordable personal stylists or personal shopper. She helps  people not to stick on their single style or color palette ,for  discover everybody’s  different styles  with some trendy , some classy through their own Drape Fit’s Stylist. As a woman She  started Drape Fit where styles meet our daily lives  . She is very passionate about high quality styles and technology . Previously She had working experience as a Business Analyst with lots of different leading companies in USA. Monomita holds an engineering degree in Information Technology. </p>
             <p style="font-size: 18px;color:orange;font-weight: bold;font-style: italic;font-family:Comic Sans MS,cursive;">
                 "I co-founded Drape Fit as a way for women just like me to embrace their individuality. It grew out of a personal need and now, Drape Fit is a place where everyone can explore all the incredible things that style can really do."</p>
             <h4>Monomita Chakraborty"</h4>
             <p>CIO & CO-FOUNDER</p>
             </div>
         </li>
         
         </ul> -->
       </div>
     </div>
   </div>
   </div>
</div>
</div>
</div>
<div class="lets-talk-box">
   <div class="lets-talk-text">
      <h2>Let's Talk</h2>
      <p>Together, we can work to solve your business goals. Feel free to provide your email address below and a representative will reach out to you soon.</p>
      <a href="https://www.drapefit.com/contact-us">Contact Us</a>
   </div>
   <div class="shutter-container">
      <div class="shutter-blade shutter-blade-left "></div>
      <div class="shutter-blade shutter-blade-right "></div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
   $(document).ready(function(){
     $(".bbt-read-1").click(function(){
       $(".box-1").toggle();
     });
   });
   
   $(document).ready(function(){
     $(".bbt-read-2").click(function(){
       $(".box-2").toggle();
     });
   });
   
    $(document).ready(function(){
     $(".bbt-read-3").click(function(){
       $(".box-3").toggle();
     });
   });
   
   
   
   
   // function myFunction() {
   //   var x = document.getElementById("myDIV");
   //   if (x.style.display === "none") {
   //     x.style.display = "block";
   //   } else {
   //     x.style.display = "none";
   //   }
   // }
</script>

