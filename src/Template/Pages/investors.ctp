 
<body>
 <?php /* ?><link rel="stylesheet" type="text/css" href="<?php echo HTTP_ROOT ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo HTTP_ROOT ?>assets/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<section class="inner-banner inner-banner2"><img src="https://www.drapefit.com/images/banner-final1.jpg"></section>
<section class="how-it-work inner-b">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="it-work-top">
<div class="section-head" data-aos="fade-down">
<ul>
  <li><a href="https://drapefittest.com/">Home</a></li>
  <li>&gt;</li>
  <li>Investors</li>
</ul>

<h2>Investors</h2>
<img src="/img/header-booten.png">
</div>
</div>
</div>
</div>
</div>
</section>
<!-----Events Calendar ----->
<div class="events-calender-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <p>DrapeFit is working hard to be listed in NASDAQ.</p>
                <!-- <div class="row">
                    <div class="col-sm-6 col-lg-6 col-md-6">
                        <h3>Stock Quote</h3>
                        <ul class="common-stock">
                            <li>FIS (COMMON STOCK)</li>
                            <li><span>Exchange</span> NYSE (USD)</li>
                            <li><span>Price</span><strong>$137.67</strong></li>
                            <li style="color: red;"><span style="color: #232f3e;">Change (%)</span>-1.38 (0.00%)</li>
                            <li><span>Volume</span>2,187,397</li>
                            <li>Data as of 09/06/19 6:30 pm EDT</li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-6 col-md-6 right-call">
                        <h3>Q2 2019 FIS Earnings Conference Call</h3>
                        <h5>August 6, 2019 8:30 AM EDT</h5>
                        <ul>
                            <li><a href="#"><i class="fa fa-volume-up"></i> Click here for webcast</a></li>
                            <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Click here to access the Q2 2019 earnings release</a></li>
                            <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Click here to access the supplemental slides</a></li>
                        </ul>
                        <h3>FIS at Cowen 47th Annual Technology, Media & Telecom Conference</h3>
                        <h5>May 30, 2019 10:50 AM EDT</h5>
                        <ul>
                            <li><a href="#"><i class="fa fa-volume-up"></i> Click here for webcast</a></li>
                        </ul>
                        <h3>Q1 2019 FIS Earnings Conference Call</h3>
                        <h5>April 30, 2019 8:30 AM EDT</h5>
                        <ul>
                            <li><a href="#"><i class="fa fa-volume-up"></i> Click here for webcast</a></li>
                            <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Click here to access the Q1 2019 earnings release</a></li>
                            <li><a href="#"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Click here to access the supplemental slides</a></li>
                        </ul>
                    </div>
                </div> -->

            </div>
        </div>
    </div>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="<?php echo HTTP_ROOT ?>assets/js/index.js"></script><?php */ ?>
<section style="" class="page-sections">
    <?php echo $pageDetails->description ?>
</section>
<section style="float:left; width:100%;">
    <section class="drafit-presonal">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<h2>DRAPE FIT - A Personal Styling Service For Everyone</h2>
    				<p>Drape FIT is the world's leading online personal styling service. We combine data science and human judgment to deliver apparel, shoes, and accessories personalized to our clients™ unique tastes, lifestyles, and budgets. Our service is available for women, men, and kids, and designed to help all our clients look, feel, and be their best selves.We have also included maternity, plus size , petite and Big & Tall outfits keeping every size and shape in mind.</p>

    				<p>We provide a personal stylist facility to our customers, helping them select their favourite items to complete their look.</p>

    				<p>We send Fit Box of hand-picked styles right to the customer's door bi-weekly , every month , every two months frequency depends on customer selection. We have met with a sudden rise in the service demand more than we have expected, due to which we would like to expand our business. </p>

    				<p>We want to inform you that our valued customers are satisfied with our customized service provided for their shopping experience. Our personal stylist help and suggest them to select the best pieces as per their requirement.</p>

    				<p>We believe in creating and sustaining a smoothly run business that would give substantial financial benefit to us both if we have the right amount of funds backing. To expand our business, we would be requiring more logistics and services.</p>

    				<p>We request you to contact us for any clarification or query at any time through email or call or for any further information.</p>

    				<p>For Contact details please follow <b>Investors FAQ</b> .</p>
    				<p>                                 <a class="fa fa-file-pdf-o"  href="https://drapefittest.com/files/investors/DrapeFit-InvestorsPresentation-converted.pdf" target="_blank">  DrapeFit-InvestorsPresentation-converted.pdf</a>
                                  </p>
							
    			</div>
    		</div>
    	</div>
    </section>
    <section class="maternity-box fax-boxes">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div class="section-head" data-aos="fade-down">
                <h2><span>Investor  </span> FAQs</h2>
                <img src="/img/header-booten.png">
                </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div aria-multiselectable="true" class="panel-group" id="accordion" role="tablist">
                        <div class="row">
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="panel panel-default">
                            <div class="panel-heading active" id="headingOne" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseOne" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseOne" role="button">When and how will you charge my card for my FIT Box?</a></h4>
                            </div>
                            <div aria-labelledby="headingOne" class="panel-collapse collapse" id="collapseOne" role="tabpanel">
                                <div class="panel-body">
                                    <p>For each order which we receive from the customer, then the initial $20 styling fee will be charged before our stylist start styling for you. The said styling fee will also be credited to your account for the things you purchase from the FIT Box you receive. After your FIT Box arrives, you can log in to your account with us and buy the items you would like to keep with you. And the rest of the things you can return with the prepaid shipping envelope sent with your FIT Box.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" id="headingTwo" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseTwo" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseTwo" role="button">Why is it important to add the credit card details?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingTwo" class="panel-collapse collapse" id="collapseTwo" role="tabpanel">
                                <div class="panel-body">
                                    <p>We require substantial credit card details to send you your FIT Box. You won't be charged till you order your FIT request (a $20 styling fee will be charged when you order and when your FIT Box ships).</p>
                                </div>
                            </div>
                        </div>
    
                            </div>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                 <div class="panel panel-default">
                            <div class="panel-heading" id="headingThree" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapseThree" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapseThree" role="button">What is the standard cost of the FIT Box sent?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingThree" class="panel-collapse collapse" id="collapseThree" role="tabpanel">
                                <div class="panel-body">
                                    <p>Well out standard FIT Box, our standard cost for the FIT Box is around $60, which is further adjusted in accordance with your budget preference by your personal stylist. Thus when you receive your FIT Box, you may find a very slight difference in the value of your Fit as most of the time, it will be under your budget preferences.</p>
    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" id="headingfour" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapsefour" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapsefour" role="button">Does the style charge turn over if I return all the items of my FIT  Box?</a></h4>
                            </div>
    
                            <div aria-labelledby="collapsefour" class="panel-collapse collapse" id="collapsefour" role="tabpanel">
                                <div class="panel-body">
                                    <p>Your styling charge must be applied to your current purchase. Thus, it doesn't turn over to your next shipment. The styling fee is non-refundable and cannot be used by any other account. This applies whether you return all your things, or buy an item which is less the cost of the styling fee. </p>
                                    <p>Thus, we aim to provide you the items which are in accordance with your styling needs and preferences in the specially customized FIT Box. Therefore, you will definitely find most of the items of your use. </p>
                                    <p>But still, in any case, you return all the items from the FIT Box, then we would appreciate you leave feedback every time for each item. It will help us to know you better and to improve our picks in accordance to your need for future FIT Boxes.</p>
                                    <p>Further, the credit of the styling fee is valid for 30 days from the date of your fit order (the date and time on which the styling fee is charged). Further, all the unused styling credits are entirely non-refundable. Whereas the same can be used on, the drape fit the online outlet.</p>
                                </div>
                            </div>
                        </div>
                            </div>
                            <div class="col-sm-4 col-lg-4 col-md-4">
                                <div class="panel panel-default">
                            <div class="panel-heading" id="headingsix" role="tab">
                                <h4 class="panel-title"><a aria-controls="collapsesix" aria-expanded="false" class="collapsed" data-parent="#accordion" data-toggle="collapse" href="#collapsesix" role="button">What is the standard cost of the FIT Box sent?</a></h4>
                            </div>
    
                            <div aria-labelledby="headingsix" class="panel-collapse collapse" id="collapsesix" role="tabpanel">
                                <div class="panel-body">
                                    <p>Well out standard FIT Box, our standard cost for the FIT Box is around $60, which is further adjusted in accordance with your budget preference by your personal stylist. Thus when you receive your FIT Box, you may find a very slight difference in the value of your Fit as most of the time, it will be under your budget preferences.</p>
    
                                </div>
                            </div>
                        </div>
                            </div>
                    </div>
                        
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12 more-faq">
                    <a href="#" class="sign-up-btn">See More Faq</a>
                </div>
            </div> -->
        </div>
    </section>
    
	<div class="inquiryForm" tyle="width: 100%; margin: 10px 0;">
	<section class="investor-email-alerts">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-12 col-md-12">
					<div class="email-alearts">
                        <div class="foram-box">
						<!-- <form> -->
                              <?= $this->Flash->render() ?>
  
   <?php echo $this->Form->create(@$user, ['url' => ['action' => 'investors'], 'data-toggle' => "validator", 'novalidate' => "true", 'id' => 'inventorsForm']); ?>
							<h2>Email Alerts</h2>
							<p>Required fields denoted by an asterisk.</p>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="Quote">
                                    <label class="chack-jt" for="Quote">Quote</label>
                                  </div>
								</div>
								<!-- <div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="Filing">
                                    <label class="chack-jt" for="Filing">SEC Filing</label>
                                  </div>
								</div> -->
								<div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="Events">
                                    <label class="chack-jt" for="Events">Events</label>
                                  </div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="News">
                                    <label class="chack-jt" for="News"> News</label>
                                  </div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="EOD">
                                    <label class="chack-jt" for="EOD">EOD SEC Filing</label>
                                  </div>
								</div>
							</div>
								
							<div class="row">
								<!-- <div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="News">
                                    <label class="chack-jt" for="News"> News</label>
                                  </div>
								</div> -->
								<!-- <div class="col-sm-6">
									<div class="form-group">
                                    <input class="checkbox" type="checkbox" id="Weekly">
                                    <label class="chack-jt" for="Weekly">Weekly Summary</label>
                                  </div>
								</div> -->
							</div>
                                  <div class="form-group-emai">
                                  	<label>Email <span class="color-red">*</span></label>
                                  	<input type="text" name="emailAddress"  id='emailAddress' required = 'required' placeholder="Email Address" />
                                  </div>
                                  <div class="form-group-emai">                                  	
                                  	<input type="submit"  value="SUBMIT" name="">
                                  </div>
						<!-- </form> -->
                        <?php echo $this->Form->end(); ?>
                        </div>
					</div>
				</div>
			</div>
	</div>
	</section>
</div>
	</section>
	</body>

    <script src='https://www.google.com/recaptcha/api.js'></script>
<script>
 $(function(){
  $("#inventorsForm").validate({
   ignore:[],
   onkeyup:function(element){
    if(element.name=='email'){
     return false;
    }
   },
   rules:{
    firstName:"required",
    lastName:"required",
    phoneNo:"required",
    emailAddress:"required",
    subject:"required",
    message:"required",
    emailAddress: {
     required:true,
     email:true,
    },
   },
   messages:{
    firstName:"Please enter your first name",
    lastName:"Please enter your last name",
    phoneNo:"Please enter your mobile number",
    emailAddress:{
     required:"Please enter your email address",
     email:"Please enter your valid email address",
    },
    subject:"Please enter your subject",
    message:"Please enter your message",
   }
  });
  $('#phoneNo').keypress(function(event){
   var keycode=event.which;//var x=event.keyCode;
   if(!(event.shiftKey==false&&(keycode==46||keycode==8||keycode==37||keycode==39||keycode==43||(keycode>=48&&keycode<=57)))){
    event.preventDefault();//stops the default action of an element from happening.
   }
  });

 });
</script>

