<style class="cp-pen-styles">

    h1 {
        display: inline-block;
        background-color: #333;
        color: #fff;
        font-size: 20px;
        font-weight: normal;
        text-transform: uppercase;
        padding: 4px 20px;
        float: left;
    }
    .clear {
        clear: both;
    }
    .items {
        display: block;
        margin: 20px 0;
    }
    .item {
        background-color: #fff;
        float: left;
        margin: 0 10px 10px 0;
        width: 205px;
        padding: 10px;
        height: 315px;
    }
    .item img {
        display: block;
        margin: auto;
        height: 200px;
    }
    h2 {
        font-size: 16px;
        display: block;
        border-bottom: 1px solid #ccc;
        margin: 0 0 10px 0;
        padding: 0 0 5px 0;
    }
    .shopping-cart {
        display: inline-block;
        background: url('http://cdn1.iconfinder.com/data/icons/jigsoar-icons/24/_cart.png') no-repeat 0 0;
        width: 24px;
        height: 24px;
        margin: 0 10px 0 0;
    }</style>

<section class="checkout-payment-conformation">
    <div class="style-bar">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <div>
                        <h1>All Products</h1>
                        <span style="float:right;">
                            <select class="selectpicker" data-show-subtext="true">
                                <option data-subtext="French's">Price H-L</option>
                                <option data-subtext="Heinz">Price L-H</option>
                                <option data-subtext="Sweet">NAME ASC</option>
                                <option data-subtext="Miracle Whip">NAME DESC</option>                               
                            </select>
                        </span>
                        <div class="clear"></div>
                        <!-- items -->
                        <div class="items">
                            <?php foreach ($all_products as $pro) { ?>
                                <div class="item">
                                    <img src="<?= HTTP_ROOT . 'inventory/files/product_img/' . $pro->product_image; ?>" alt="item" />
                                    <h2><?= $pro->product_name_one; ?></h2>

                                    <p>Price: <em>$<?= $pro->sale_price; ?></em>
                                    </p>
                                    <button class="add-to-cart btn btn-success" type="button">Add to cart</button>
                                </div>
                            <?php } ?>

                        </div>
                        <div>
                            <?php
                            echo "<div class='center' style='float:left;width:100%;'><ul class='pagination' style='margin:20px auto;'>";
                            echo $this->Paginator->prev('< ' . __('prev'), array('tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'disabled'), null, array('class' => 'prev disabled'));
                            echo $this->Paginator->numbers(array('separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
                            echo $this->Paginator->next(__('next') . ' >', array('tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'disabled'), null, array('class' => 'next disabled'));
                            echo "</div></ul>";
                            ?>
                            <div style="float: left;width: 100%;"><?= $this->Paginator->counter(['format' => 'Page {{page}} of {{pages}}']) ?></div>
                        </div>
                        <!--/ items -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
