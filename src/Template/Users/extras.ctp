<div class="banner-extra">
    <h1>Add Extras To Your Fix</h1>
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever to make a type specimen book.</p>
</div>
<div class="browse-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <!-- <a class="browse-btn" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i> Browse all</a> -->
               
                <a class="count" href="<?= HTTP_ROOT; ?>extras/review_extras?t=<?= $this->request->session()->read('PROFILE'); ?>">Your Extras <i class="fa fa-cube" aria-hidden="true"></i><span id="usr_extr_cnt">0</span></a>
            </div>
        </div>
    </div>
</div>
<div class="extra-main-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-8 col-md-8">
                <div class="extar-boxes">
                    <?php
                    $color_arr = $this->Custom->inColor();
                    if (!empty($extra_products)) {
                        foreach ($extra_products as $pdetails) {
                            ?>
                            <div class="extar-box">
                                <a href="<?= HTTP_ROOT; ?>extras/extras_recommendations?t=<?= $getData['t'] . '&prd=' . $pdetails->prod_id . '&k=' . $pdetails->id . '&pn=' . $this->Custom->makeSeoUrl($pdetails->product_name_one); ?>">
                                    <div class="extra-img">
                                        <?php
                                        if (empty($pdetails->prodcut_id)) {
                                            ?>
                                            <img src="<?php echo HTTP_ROOT_INV . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" />
                                        <?php } else { ?>
                                            <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" />
                                        <?php } ?>
                                    </div>
                                    <h4><?= $pdetails->product_name_one; ?></h4>
                                    <h5> <?php
//                                    if (!empty($pdetails->color)) {
//                                        echo $color_arr[$pdetails->color];
//                                    }
                                        if (!empty($pdetails->sale_price)) {
                                            echo '$' . number_format($pdetails->sale_price, 2);
                                        }
                                        ?>
                                    </h5>
                                    <!--                                <ul>
                                                                        <li><span>01</span></li>
                                                                        <li><span>02</span></li>
                                                                        <li><span>03</span></li>
                                                                        <li><span>04</span></li>
                                                                        <li class="two">+2</li>
                                                                    </ul>-->
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-12 col-sm-4 col-md-4 right-extra">
                <h2>How Extra Work</h2>
                <ul>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
     $(document).ready(function () {
        getUserExtraProduct();
    });
function getUserExtraProduct() {
    var url = $('#pageurl').val();
    $.ajax({
        type: 'GET',
        url: url + 'users/get_user_extra_product_count',
        success: function (response) {
            if (response) {
                $('#usr_extr_cnt').html(response.cnt)
            }
        },
        dataType: 'json'
    });
}
</script>