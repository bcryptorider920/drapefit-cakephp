<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel='stylesheet' href='https://k1ngzed.com/dist/swiper/swiper.min.css'>
<link rel='stylesheet' href='https://k1ngzed.com/dist/EasyZoom/easyzoom.css'>

<div class="browse-box browse-box2">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <a class="browse-btn" href="<?= HTTP_ROOT; ?>extras?t=<?= $this->request->session()->read('PROFILE'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Browse all</a>
                <a class="count" href="<?= HTTP_ROOT; ?>extras/review_extras?t=<?= $this->request->session()->read('PROFILE'); ?>">Your Extras <i class="fa fa-cube" aria-hidden="true"></i><span id="usr_extr_cnt">0</span></a>
            </div>
        </div>
    </div>
</div>
<?php
$color_arr = $this->Custom->inColor();
if (empty($product_details->prodcut_id)) {
    $img = HTTP_ROOT_INV . PRODUCT_IMAGES . $product_details->product_image;
} else {
    $img = HTTP_ROOT . PRODUCT_IMAGES . $product_details->product_image;
}
?>
<div class="extra-main-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-6 col-md-6">
                <div class="product__carousel">
                    <!-- Swiper and EasyZoom plugins start -->
                    <div class="swiper-container gallery-top">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide easyzoom easyzoom--overlay">
                                <a href="<?= $img; ?>">
                                    <img src="<?= $img; ?>" alt=""/>
                                </a>
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next swiper-button-white"></div>
                        <div class="swiper-button-prev swiper-button-white"></div>
                    </div>
                    <div class="swiper-container gallery-thumbs">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="<?= $img; ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <!-- Swiper and EasyZoom plugins end -->
                </div>
            </div>
            <div class="col-sm-12 col-sm-6 col-md-6 detail-right">
                <?= $this->Form->create('', ['type' => 'post']); ?>
                <h4><?= $product_details->product_name_two; ?></h4>
                <h2><?= $product_details->product_name_one; ?></h2>
                <h5> <?php
                    if (!empty($product_details->sale_price)) {
                        echo '$' . number_format($product_details->sale_price, 2);
                    }
                    ?></h5>
                <h6>Color: <?php
                    if (!empty($product_details->color)) {
                        echo $color_arr[$product_details->color];
                    }
                    ?></h6>
                <!--                <ul class="color">
                                    <li><span>01</span></li>
                                    <li><span>02</span></li>
                                    <li><span>03</span></li>
                                    <li><span>04</span></li>
                                    <li><span>05</span></li>
                                    <li><span>06</span></li>
                                </ul>-->
                <div class="select-box">
                    <div class="select-box-left">
                        <label>Size</label>
                        <select name="size">
                            <option> 
                                <?php
                                if (!empty($product_details->picked_size)) {
                                    $li_size = explode('-', $product_details->picked_size);
                                    foreach ($li_size as $sz_l) {
                                        if (($product_details->$sz_l == 0) || ($product_details->$sz_l == 00)) {
                                            echo $product_details->$sz_l;
                                        } else {
                                            echo!empty($product_details->$sz_l) ? $product_details->$sz_l . '&nbsp;&nbsp;' : '';
                                        }
                                    }
                                }
                                if (!empty($product_details->primary_size) && ($product_details->primary_size == 'free_size')) {
                                    echo "Free Size";
                                }
                                ?>
                            </option>
                        </select>
                    </div>
                    <div class="select-box-left">
                        <label>Quantity</label>
                        <select name="qnt">
                            <?php
                            $qnty = $this->Custom->productQuantity($product_details->prod_id);
                            for ($i = 1; $i <= $qnty; $i++) {
                                ?>
                                <option><?= $i; ?></option> 
                                <?php
                            }
                            ?>

                        </select>

                    </div>
                    <input type="hidden" name="color" value="<?php
                    if (!empty($product_details->color)) {
                        echo $color_arr[$product_details->color];
                    }
                    ?>" >
                    <input type="hidden" name="prod_id" value="<?php
                    if (!empty($product_details->prod_id)) {
                        echo $product_details->prod_id;
                    }
                    ?>" >
                    <input type="hidden" name="in_prod_id" value="<?php
                    if (!empty($product_details->id)) {
                        echo $product_details->id;
                    }
                    ?>" >
                    <input type="hidden" name="usr_type" value="<?php
                    if (!empty($getData)) {
                        echo $getData['t'];
                    }
                    ?>" >

                </div>

                <?php
                if (!empty($product_details->note)) {
                    echo '<h3>Product Details</h3><div  class="detail-list">' . $product_details->note . '</div>';
                }
                ?>

                <div>
                    <button type="submit" class="btn btn-warning">Add to fit</button>
                </div>

                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
<script src='https://code.jquery.com/jquery-3.4.1.min.js'></script>
<script src='https://k1ngzed.com/dist/swiper/swiper.min.js'></script>
<script src='https://k1ngzed.com/dist/EasyZoom/easyzoom.js'></script>
<script type="text/javascript">
    
    $(document).ready(function () {
        getUserExtraProduct();
    });
function getUserExtraProduct() {
    var url = $('#pageurl').val();
    $.ajax({
        type: 'GET',
        url: url + 'users/get_user_extra_product_count',
        success: function (response) {
            if (response) {
                $('#usr_extr_cnt').html(response.cnt)
            }
        },
        dataType: 'json'
    });
}
    // product Gallery and Zoom

    // activation carousel plugin
    var galleryThumbs = new Swiper('.gallery-thumbs', {
        spaceBetween: 5,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        breakpoints: {
            0: {
                slidesPerView: 3,
            },
            992: {
                slidesPerView: 4,
            },
        }
    });
    var galleryTop = new Swiper('.gallery-top', {
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        thumbs: {
            swiper: galleryThumbs
        },
    });
    // change carousel item height
    // gallery-top
    let productCarouselTopWidth = $('.gallery-top').outerWidth();
    $('.gallery-top').css('height', productCarouselTopWidth);

    // gallery-thumbs
    let productCarouselThumbsItemWith = $('.gallery-thumbs .swiper-slide').outerWidth();
    $('.gallery-thumbs').css('height', productCarouselThumbsItemWith);

    // activation zoom plugin
    var $easyzoom = $('.easyzoom').easyZoom();
</script>