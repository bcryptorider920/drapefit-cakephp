<style>
    .payment-sucess {
        float: left;
        width: 100%;
        padding: 27px 0 10px;
    }
    .payment-sucess .jumbotron2{
        text-align: center;
        float: left;
        width: 100%;
    }
    .payment-sucess .jumbotron2 .display-3{
        font-size: 50px;
        font-weight: bold;
        color: #5ab72d;
    }
    .payment-sucess .jumbotron2 .display-3 span{
        display: block;
        margin-bottom: 10px;
        font-size: 60px;
    }
    .payment-sucess .jumbotron2 p{
        text-align: center;
    }
    .payment-sucess .jumbotron2 p a.btn-primary {
        display: inline-block;
        background: #d64ade;
        border: none;
        font-size: 19px;
        font-weight: bold;
        letter-spacing: 1px;
        padding: 8px 31px 10px;
        border-radius: 0;
        color:#fff;
    }
    .payment-sucess .jumbotron2 p a.btn-primary:hover{
        background: #d64ade;
        color:#fff;
    }
    .checkout-payment-conformation h5 {
        font-weight: bold;
    }
    .checkout-payment-conformation a {
        display: inline-block;
        background: #000;
        border: 1px solid #000;
        padding: 10px 25px;
        margin-bottom: 30px;
        font-weight: bold;
        letter-spacing: 1px;
    }
    .checkout-payment-conformation a:hover{
        background: none;
        color: #000;
    }
</style>
<?php

use Cake\ORM\TableRegistry;

if ($this->request->session()->read('PROFILE') == 'KIDS') {
    if ($this->request->session()->read('KID_ID')) {
        $kidsDetails = TableRegistry::get('kidsDetails');
        $Usersdata = $kidsDetails->find('all')->where(['id' => $this->request->session()->read('KID_ID')])->first();
        if ($Usersdata->is_redirect == 0 && @$Usersdata->is_progressbar != 100) {
            $curl = 'welcome/style/';
        } elseif ($Usersdata->is_redirect == 0 && $Usersdata->is_progressbar == 100) {
            $curl = 'welcome/schedule/';
        } elseif ($Usersdata->is_redirect == 0) {
            $curl = 'welcome/style/';
        } elseif ($Usersdata->is_redirect == 1) {
            $curl = 'welcome/schedule/';
        } elseif ($Usersdata->is_redirect == 2) {
            $curl = 'not-yet-shipped';
        } elseif ($Usersdata->is_redirect == 3) {
            $curl = 'profile-review/';
        } elseif ($Usersdata->is_redirect == 4) {
            $curl = 'order_review/';
        } elseif ($Usersdata->is_redirect == 5) {
            $curl = 'calendar-sechedule/';
        } elseif ($Usersdata->is_redirect == 6) {
            $curl = 'customer-order-review';
        }
    }
} else {
    $Users = TableRegistry::get('Users');
    $UserDetails = TableRegistry::get('UserDetails');
    $Usersdata = $Users->find('all')->where(['id' => $this->request->getSession()->read('Auth.User.id')])->first();
    $UserDetailsdata = $UserDetails->find('all')->where(['user_id' => $this->request->getSession()->read('Auth.User.id')])->first();
    if ($Usersdata->is_redirect == 0 && $UserDetailsdata->is_progressbar != 100) {
        $curl = 'welcome/style/';
    } elseif ($Usersdata->is_redirect == 0 && $UserDetailsdata->is_progressbar == 100) {
        $curl = 'welcome/schedule/';
    } elseif ($Usersdata->is_redirect == 0) {
        $curl = 'welcome/style/';
    } elseif ($Usersdata->is_redirect == 1) {
        $curl = 'welcome/schedule/';
    } elseif ($Usersdata->is_redirect == 2) {
        $curl = 'not-yet-shipped';
    } elseif ($Usersdata->is_redirect == 3) {
        $curl = 'profile-review/';
    } elseif ($Usersdata->is_redirect == 4) {
        $curl = 'order_review/';
    } elseif ($Usersdata->is_redirect == 5) {
        $curl = 'calendar-sechedule/';
    } elseif ($Usersdata->is_redirect == 6) {
        $curl = 'customer-order-review';
    }
}
?>
<div class="style-bar">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <?php
                echo $this->element('frontend/profile_menu_men');
                ?>
            </div>
        </div>
    </div>
</div>
<section class="checkout-payment-conformation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="conformation-message">                
                    <h3><i class="fa fa-check"></i> Thank You For Choosing Drape Fit! </h3>
                    <!-- <p>Please Keep an eye on your email on the progress we do for your Fit Box.</p> -->
                    <p class="go-home"><a class="btn btn-primary btn-sm" href="<?php echo HTTP_ROOT . $curl ?>" role="button">Continue to homepage</a></p>
                    <?php if ($this->request->session()->read('PROFILE') == "WOMEN") { ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Now’s the time to add Extras</h5>           
                            </div>
                            <div class="col-sm-12">
                                <a  class="btn btn-info" href="<?= HTTP_ROOT; ?>extras?t=WOMEN">Browse all Extras</a>          
                            </div>
                        </div>
                    <?php } ?>
                </div>              
            </div>
        </div>

    </div>
</section>

<?php if ($this->request->session()->read('PROFILE') == "WOMEN") { ?>
    <div class="extra-main-box" style="padding-top: 0;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-sm-12 col-md-12">
                    <div class="browse-all-box">
                        <h2>How Extra Work</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever to make a type specimen book.</p>
                        <div class="extar-boxes">
                            <?php
                            $women_extra_dr_list = $this->Custom->womenExtraList();
                            if (!empty($women_extra_dr_list)) {
                                foreach ($women_extra_dr_list as $pdetails) {
                                    ?>
                                    <div class="extar-box">
                                        <a href="<?= HTTP_ROOT; ?>extras/extras_recommendations?t=<?= $this->request->session()->read('PROFILE') . '&prd=' . $pdetails->prod_id . '&k=' . $pdetails->id . '&pn=' . $this->Custom->makeSeoUrl($pdetails->product_name_one); ?>">
                                            <div class="extra-img">
                                                <?php
                                                if (empty($pdetails->prodcut_id)) {
                                                    ?>
                                                    <img src="<?php echo HTTP_ROOT_INV . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" />
                                                <?php } else { ?>
                                                    <img src="<?php echo HTTP_ROOT . PRODUCT_IMAGES; ?><?php echo $pdetails->product_image; ?>" />
                                                <?php } ?>
                                            </div>
                                            <h4><?= $pdetails->product_name_one; ?></h4>
                                            <h5> <?php
//                                    if (!empty($pdetails->color)) {
//                                        echo $color_arr[$pdetails->color];
//                                    }
                                                if (!empty($pdetails->sale_price)) {
                                                    echo '$' . number_format($pdetails->sale_price, 2);
                                                }
                                                ?>
                                            </h5>
                                            <!--                                <ul>
                                                                                <li><span>01</span></li>
                                                                                <li><span>02</span></li>
                                                                                <li><span>03</span></li>
                                                                                <li><span>04</span></li>
                                                                                <li class="two">+2</li>
                                                                            </ul>-->
                                        </a>
                                    </div>    
                                    <?php
                                }
                            }
                            ?>

                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h5>Now’s the time to add Extras</h5>           
                            </div>
                            <div class="col-sm-12">
                                <a  class="btn btn-info" href="<?= HTTP_ROOT; ?>extras?t=WOMEN">Browse all Extras</a>          
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

<?php } ?>

