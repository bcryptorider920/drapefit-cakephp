<div class="banner-extra">
    <h1>Extras in Your Fix</h1>
    <p>Arrivig in your Fix Scheduled for March 31.</p>
</div>
<div class="browse-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12 col-md-12">
                <a class="browse-btn" href="<?= HTTP_ROOT; ?>extras?t=<?= $this->request->session()->read('PROFILE'); ?>"><i class="fa fa-angle-left" aria-hidden="true"></i> Browse all</a>
                <a class="count" href="<?= HTTP_ROOT; ?>extras/review_extras?t=<?= $this->request->session()->read('PROFILE'); ?>">Your Extras <i class="fa fa-cube" aria-hidden="true"></i><span id="usr_extr_cnt">0</span></a>
            </div>
        </div>
    </div>
</div>
<div class="extra-main-box">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-sm-8 col-md-8">
                <h4>Fix may contain up to 10 Extras</h4>
                <?php
                $color_arr = $this->Custom->inColor();
                if (!empty($all_data->count())) {
                    foreach ($all_data as $data_li) {
                        $product_details = $data_li->in_product;

                        if (empty($product_details->prodcut_id)) {
                            $img = HTTP_ROOT_INV . PRODUCT_IMAGES . $product_details->product_image;
                        } else {
                            $img = HTTP_ROOT . PRODUCT_IMAGES . $product_details->product_image;
                        }
                        ?> 
                        <div class="cart-list-box" id="user_extra_<?= $data_li->id; ?>">
                            <div class="cart-list-box-left">
                                <img src="<?= $img; ?>">
                            </div>
                            <div class="cart-list-box-right">
                                <div class="first-text">
                                    <div class="first-text-left">
                                        <h3><?= $product_details->product_name_one; ?></h3>
                                        <p><?php
                                            if (!empty($product_details->note)) {
                                                echo $product_details->note;
                                            }
                                            ?></p>
                                        <h6>
                                            <?php
                                            if (!empty($product_details->picked_size)) {
                                                $li_size = explode('-', $product_details->picked_size);
                                                foreach ($li_size as $sz_l) {
                                                    if (($product_details->$sz_l == 0) || ($product_details->$sz_l == 00)) {
                                                        echo $product_details->$sz_l;
                                                    } else {
                                                        echo!empty($product_details->$sz_l) ? $product_details->$sz_l . '&nbsp;&nbsp;' : '';
                                                    }
                                                }
                                            }
                                            if (!empty($product_details->primary_size) && ($product_details->primary_size == 'free_size')) {
                                                echo "Free Size";
                                            }
                                            ?>                                            
                                        </h6>
                                    </div>
                                    <div class="first-text-right">
                                        <h5><?php
                                            if (!empty($product_details->sale_price)) {
                                                echo '$' . number_format($product_details->sale_price, 2);
                                            }
                                            ?></h5>
                                    </div>
                                </div>
                                <div class="qty-box">
                                    <div class="qty-box-left">
                                        <label>Qty:</label>
                                        <select>
                                            <option><?= $data_li->quantity; ?></option>                                           
                                        </select>
                                    </div>
                                    <div class="qty-box-right">
                                        <a href="javascript:void(0);" onclick="removeUserExtraItm(<?= $data_li->id; ?>)">Remove</a>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <?php
                    }
                } else {
                    echo "<h3 class='text text-danger'>No data found</h3>";
                }
                ?>
            </div>
            <div class="col-sm-12 col-sm-4 col-md-4 right-extra">
                <h2>How Extra Work</h2>
                <ul>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                    <li>Lorem Ipsum is simply dummy text of the</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        getUserExtraProduct();
    });

    function removeUserExtraItm(itm_id) {
        var url = $('#pageurl').val();
        var cnf_r = confirm('Are you sure wamt to delete?');
        if (cnf_r) {
            $.ajax({
                type: 'GET',
                url: url + 'users/deleteUserExtra/' + itm_id,
                success: function (response) {
                    if (response) {
                        $('#user_extra_' + itm_id).remove();
                        getUserExtraProduct();
                    }
                },
                dataType: 'json'
            });
        }
    }

    function getUserExtraProduct() {
        var url = $('#pageurl').val();
        $.ajax({
            type: 'GET',
            url: url + 'users/get_user_extra_product_count',
            success: function (response) {
                if (response) {
                    $('#usr_extr_cnt').html(response.cnt)
                }
            },
            dataType: 'json'
        });
    }
</script>