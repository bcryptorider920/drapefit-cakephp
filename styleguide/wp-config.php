<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'drapefit_style_guide' );

/** MySQL database username */
define( 'DB_USER', 'drapefit_blog' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Kolkata@2' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8v5gxtsmy4mv45s3wfqmhkmxbzi0dkdjjwbodhe14jypjfndbchledahvhhs19ft' );
define( 'SECURE_AUTH_KEY',  'qqtqmr6lbqewwvco1ny2gbtijhrhnx211dq4cvt4pszwxuotwe19projldnzjiff' );
define( 'LOGGED_IN_KEY',    'zfarkr05z7rr1mlzag6nfdkhtfvantllfbi8xn1g3agoomgbxri140hd3i1qvzye' );
define( 'NONCE_KEY',        'ot1nbkcpwalkx6qoataxrzqdn7rhnojza4mlmeygjpwwf0zlxxjt0fw7bluv9bhc' );
define( 'AUTH_SALT',        'sjvtbweyzlnxpzakpjojzdes5aokamuoxb4liju1dq2m0fwfipdcs3hmiz4qdghq' );
define( 'SECURE_AUTH_SALT', 'tunvegqobvqxc4mihpu6tjtkikrc9adih7fo7ozc3hnstn3d4pttx514vuvyzbwt' );
define( 'LOGGED_IN_SALT',   'dizfnmft3arrrgznqwqpn3wcgxoc7yafmih1cecjar8aanobr4hc7xspvyjovqdg' );
define( 'NONCE_SALT',       'z1yp8tusef8muchw3vyrux2h6v7h3p5waecxbixt2uv1g2klyuxn8xuxs8lcoik9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wpse_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
