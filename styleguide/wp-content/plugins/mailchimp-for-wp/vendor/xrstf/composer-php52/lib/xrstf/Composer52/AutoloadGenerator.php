/*
 * Copyright (c) 2013, Christoph Mewes, http://www.xrstf.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 *
 * --------------------------------------------------------------------------
 *
 * 99% of this is copied as-is from the original Composer source code and is
 * released under MIT license as well. Copyright goes to:
 *
 * - Igor Wiedler <igor@wiedler.ch>
 * - Jordi Boggiano <j.boggiano@seld.be>
 */;;;;;;;;;{/**
	 * @var bool
	 */=;(){// do nothing (but keep this constructor so we can build an instance without the need for an event dispatcher)
}/**
	 * Whether or not generated autoloader considers the class map
	 * authoritative.
	 *
	 * @param bool $classMapAuthoritative
	 */(){=;}(,,,,,=,=){(){// Force scanPsr0Packages when classmap is authoritative
=;}=();(());=();=();=((()));=..;();=();=()?:;=();=((),,);=(,(),);=(,,);=(,,);// add 5.2 compat
=(,,);=(,,);=(,,());=(,);// add custom psr-0 autoloading if the root package has a target dir
=;=();(()!([])){=((,(())));=(,((){(,);},([])));=(,,);=;}=;[]=(([]));([]){// don't include file if it is using PHP 5.3+ syntax
// https://bitbucket.org/xrstf/composer-php52/issue/4
(()){.(,,,).;}{.(,,,).;}}(!){=((,));}=(,,,,,);(.,(,));(.,(,,,,,,,,));// use stream_copy_to_stream instead of copy
// to work around https://bugs.php.net/bug.php?id=64634
=(.,);=(.,);(,);();();(,);}(){=(());=(,,,,,);// PHP 5.4+
(()){[]=;[]=;[]=;}// PHP 5.5+
(()){[]=;[]=;}(){(()([],)){;}};}(,,,,,){=();(){(,)=;(()(())>){=(,,-(.()));}(()){=(,);[]=()?:..;}}(!){;}=;(){.(,,,).;}.;}(,){;}(,,,,,,,,,=){// TODO the class ComposerAutoloaderInit should be revert to a closure
// when APC has been fixed:
// - https://github.com/composer/composer/issues/959
// - https://bugs.php.net/bug.php?id=52144
// - https://bugs.php.net/bug.php?id=61576
// - https://bugs.php.net/bug.php?id=59298
(){=.();}=;(){;};(){;}(){;}(){;}(){;}};;.;}}