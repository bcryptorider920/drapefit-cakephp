/*
 * Copyright (c) 2013, Christoph Mewes, http://www.xrstf.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 *
 * --------------------------------------------------------------------------
 *
 * 99% of this is copied as-is from the original Composer source code and is
 * released under MIT license as well. Copyright goes to:
 *
 * - Fabien Potencier <fabien@symfony.com>
 * - Jordi Boggiano <j.boggiano@seld.be>
 */{=();=();=;=();=;=;/**
	 * @param boolean $flag  true to allow class names with a leading underscore, false to disable
	 */(){=;}/**
	 * @return array
	 */(){;}/**
	 * Turns off searching the prefix and fallback directories for classes
	 * that have not been registered with the class map.
	 *
	 * @param bool $classMapAuthoratative
	 */(){=;}/**
	 * Should class lookup fail if not found in the current class map?
	 *
	 * @return bool
	 */(){;}/**
	 * @return array
	 */(){;}/**
	 * @return array
	 */(){;}/**
	 * @param array $classMap  class to filename map
	 */(){(){=(,);}{=;}}/**
	 * Registers a set of classes, merging with any others previously set.
	 *
	 * @param string       $prefix   the classes prefix
	 * @param array|string $paths    the location(s) of the classes
	 * @param bool         $prepend  prepend the location(s)
	 */(,,=){(!){(){=(,);}{=(,);};}(!([])){[]=;;}(){[]=(,[]);}{[]=([],);}}/**
	 * Registers a set of classes, replacing any others previously set.
	 *
	 * @param string       $prefix  the classes prefix
	 * @param array|string $paths   the location(s) of the classes
	 */(,){(!){=;;}[]=;}/**
	 * Turns on searching the include path for class files.
	 *
	 * @param bool $useIncludePath
	 */(){=;}/**
	 * Can be used to check if the autoloader uses the include path to check
	 * for classes.
	 *
	 * @return bool
	 */(){;}/**
	 * Registers this instance as an autoloader.
	 */(){((,),);}/**
	 * Unregisters this instance as an autoloader.
	 */(){((,));}/**
	 * Loads the given class or interface.
	 *
	 * @param  string $class  the name of the class
	 * @return bool|null      true, if loaded
	 */(){(=()){;;}}/**
	 * Finds the path to the file where the class is defined.
	 *
	 * @param  string $class  the name of the class
	 * @return string|null    the path, if found
	 */(){([]){=(,);}(([])){[];}(){;}=();(){((,)){(){((..)){..;}}}}(){((..)){..;}}(=()){;}[]=;}(){(=(,)){// namespaced class name
=(,,(,,)).;=(,+);}{// PEAR-like class name
=;=;}=(,,);// restore the prefix
([]){[]=;}.;;}(){=(,());(){=(,);(=(..)){;}};}}