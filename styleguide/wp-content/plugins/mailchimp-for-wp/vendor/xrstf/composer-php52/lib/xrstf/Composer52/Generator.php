/*
 * Copyright (c) 2013, Christoph Mewes, http://www.xrstf.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */;;;{(){=();=();=();=();=();=();// We can't gain access to the Command's input object, so we have to look
// for -o / --optimize-autoloader ourselves. Sadly, neither getopt() works
// (always returns an empty array), nor does Symfony's Console Input, as
// it expects a full definition of the current command line and we can't
// provide that.
=[];=(,)(,)(,);=();=();(,,,,,,);}}