;/**
 * Config for Exporter object
 */{/**
     * Delimiter
     * @var string
     */=;/**
     * Enclosure
     * @var string
     */=;/**
     * Escape
     * @var string
     */=;/**
     * Newline code
     * @var string
     */=;/**
     * From charset
     * @var string
     */=;/**
     * To charset
     * @var string
     */=;/**
     * File mode
     * @var string
     */=;/**
     * The column headers.
     * @var array
     */=();/**
     * Set delimiter
     * @param string $delimiter
     * @return ExporterConfig
     */(){=;;}/**
     * Return delimiter
     * @return string
     */(){;}/**
     * Set enclosure
     * @param string $enclosure
     * @return ExporterConfig
     */(){=;;}/**
     * Return enclosure
     * @return string
     */(){;}/**
     * Set escape
     * @param string $escape
     * @return ExporterConfig
     */(){=;;}/**
     * Return escape
     * @return string
     */(){;}/**
     * Set newline
     * @param string $newline
     * @return ExporterConfig
     */(){=;;}/**
     * Return newline
     * @return string
     */(){;}/**
     * Set from-character set
     * @param string $fromCharset
     * @return ExporterConfig
     */(){=;;}/**
     * Return from-character set
     * @return string
     */(){;}/**
     * Set to-character set
     * @param string $toCharset
     * @return ExporterConfig
     */(){=;;}/**
     * Return to-character set
     * @return string
     */(){;}/**
     * Set file mode
     * @param string $fileMode
     * @return ExporterConfig
     */(){=;;}/**
     * Return file mode
     * @return string
     */(){;}/**
     * Set the column headers.
     * @param array $columnHeaders
     * @return ExporterConfig
     */(){=;;}/**
     * Get the column headers.
     * @return array
     */(){;}}