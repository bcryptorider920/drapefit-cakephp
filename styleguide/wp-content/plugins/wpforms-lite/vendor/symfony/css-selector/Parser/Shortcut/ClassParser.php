/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;;;;;/**
 * CSS selector class parser shortcut.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{/**
     * {@inheritdoc}
     */(){// Matches an optional namespace, optional element, and required class
// $source = 'test|input.ab6bd_field';
// $matches = array (size=4)
//     0 => string 'test|input.ab6bd_field' (length=22)
//     1 => string 'test' (length=4)
//     2 => string 'input' (length=5)
//     3 => string 'ab6bd_field' (length=11)
((,(),)){(((([]?:,[]?:),[])),);}();}}