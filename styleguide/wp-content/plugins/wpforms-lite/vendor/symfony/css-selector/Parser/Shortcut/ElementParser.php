/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;;;;/**
 * CSS selector element parser shortcut.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{/**
     * {@inheritdoc}
     */(){// Matches an optional namespace, required element or `*`
// $source = 'testns|testel';
// $matches = array (size=3)
//     0 => string 'testns|testel' (length=13)
//     1 => string 'testns' (length=6)
//     2 => string 'testel' (length=6)
((,(),)){((([]?:,[])));}();}}