/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;;;/**
 * XPath expression translator attribute extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{/**
     * {@inheritdoc}
     */(){((,),(,),(,),(,),(,),(,),(,),(,),);}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){();}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){((,,()));}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){(?(,,(..)):);}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){((,,(),(.)));}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){(?(,,()):);}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){(?(,,()-,()):);}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){(?(,,()):);}/**
     * @param XPathExpr $xpath
     * @param string    $attribute
     * @param string    $value
     *
     * @return XPathExpr
     */(,,){((?:,,()));}/**
     * {@inheritdoc}
     */(){;}}