/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;/**
 * XPath expression translator extension interface.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{/**
     * Returns node translators.
     *
     * These callables will receive the node as first argument and the translator as second argument.
     *
     * @return callable[]
     */();/**
     * Returns combination translators.
     *
     * @return callable[]
     */();/**
     * Returns function translators.
     *
     * @return callable[]
     */();/**
     * Returns pseudo-class translators.
     *
     * @return callable[]
     */();/**
     * Returns attribute operation translators.
     *
     * @return callable[]
     */();/**
     * Returns extension name.
     *
     * @return string
     */();}