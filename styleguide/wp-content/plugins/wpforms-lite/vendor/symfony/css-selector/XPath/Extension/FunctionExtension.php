/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;;;;;;;/**
 * XPath expression translator function extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{/**
     * {@inheritdoc}
     */(){((,),(,),(,),(,),(,),(,),);}/**
     * @param XPathExpr    $xpath
     * @param FunctionNode $function
     * @param bool         $last
     * @param bool         $addNameTest
     *
     * @return XPathExpr
     *
     * @throws ExpressionErrorException
     */(,,=,=){{(,)=(());}(){((,(,())),,);}();(){();}(){(.(?.(-):));}(<){(<){();}=;}{=;}=;(){=.;;}(){.;}=((,,));(-){[]=(,,);}((,));// todo: handle an+b, odd, even
// an+b means every-a, plus b, e.g., 2n+1 means odd
// 0n+b means b
// n+0 means a=1, i.e., all elements
// an means every a elements, i.e., 2n means even
// -n means -1n
// -1n+6 means elements 6 and previous
}/**
     * @return XPathExpr
     */(,){(,,);}/**
     * @return XPathExpr
     */(,){(,,,);}/**
     * @return XPathExpr
     *
     * @throws ExpressionErrorException
     */(,){(()){();}(,,,);}/**
     * @return XPathExpr
     *
     * @throws ExpressionErrorException
     */(,){=();(){(!(()())){(.(,));}}((,([]())));}/**
     * @return XPathExpr
     *
     * @throws ExpressionErrorException
     */(,){=();(){(!(()())){(.(,));}}((,([]())));}/**
     * {@inheritdoc}
     */(){;}}