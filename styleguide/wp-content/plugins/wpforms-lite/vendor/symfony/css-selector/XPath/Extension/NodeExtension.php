/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */;;;;/**
 * XPath expression translator node extension.
 *
 * This component is a port of the Python cssselect library,
 * which is copyright Ian Bicking, @see https://github.com/SimonSapin/cssselect.
 *
 * @author Jean-François Simon <jeanfrancois.simon@sensiolabs.com>
 *
 * @internal
 */{=;=;=;;/**
     * @param int $flags
     */(=){=;}/**
     * @param int  $flag
     * @param bool $on
     *
     * @return $this
     */(,){(!()){;}(!()){;};}/**
     * @param int $flag
     *
     * @return bool
     */(){(&);}/**
     * {@inheritdoc}
     */(){((,),(,),(,),(,),(,),(,),(,),(,),(,),);}/**
     * @return XPathExpr
     */(,){(());}/**
     * @return XPathExpr
     */(,){((),(),());}/**
     * @return XPathExpr
     */(,){=(());=(());();(()){((,()));}();}/**
     * @return XPathExpr
     */(,){=(());(,);}/**
     * @return XPathExpr
     */(,){=(());(,());}/**
     * @return XPathExpr
     */(,){=();=();(()){=();}(()){=(,(),);=(());}=?.:(,());=();=(());(()){=();}(,(),,);}/**
     * @return XPathExpr
     */(,){=(());(,,,());}/**
     * @return XPathExpr
     */(,){=(());(,,,());}/**
     * @return XPathExpr
     */(){=();(()){=();}(){=();}{=;=;}(()){=(,(),);=(());}=(,);(!){();};}/**
     * {@inheritdoc}
     */(){;}/**
     * Tests if given name is safe.
     *
     * @param string $name
     *
     * @return bool
     */(){<(,);}}