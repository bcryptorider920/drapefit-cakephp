;;;{/**
     * Splits a string into separate rules
     *
     * @param string $rulesString
     *
     * @return string[]
     */(){=();(,);}/**
     * @param string $string
     *
     * @return string
     */(){=((,),,);=((),,);=(,,);=(,,);=(,,);=();=(,);;}/**
     * Converts a rule-string into an object
     *
     * @param string $rule
     * @param int    $originalOrder
     *
     * @return Rule[]
     */(,){=();=(,);(!([])){();}=();=();=(,([]));=([]);(){=();=();[]=(,(,),,);};}/**
     * Calculates the specificity based on a CSS Selector string,
     * Based on the patterns from premailer/css_parser by Alex Dunae
     *
     * @see https://github.com/premailer/css_parser/blob/master/lib/css_parser/regexps.rb
     *
     * @param string $selector
     *
     * @return Specificity
     */(){=;=;=;(("}",,),("}",,),("}",,));}/**
     * @param string[] $rules
     * @param Rule[]   $objects
     *
     * @return Rule[]
     */(,=()){=;(){=(,(,));;};}/**
     * Sorts an array on the specificity element in an ascending way
     * Lower specificity will be sorted to the beginning of the array
     *
     * @param Rule $e1 The first element.
     * @param Rule $e2 The second element.
     *
     * @return int
     */(,){=();=(());// if the specificity is the same, use the order in which the element appeared
(){=()-();};}}