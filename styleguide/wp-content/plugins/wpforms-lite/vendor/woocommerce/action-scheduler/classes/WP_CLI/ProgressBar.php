;/**
 * WP_CLI progress bar for Action Scheduler.
 *//**
 * Class ProgressBar
 *
 * @package Action_Scheduler\WP_CLI
 *
 * @since 3.0.0
 *
 * @codeCoverageIgnore
 */{/** @var integer */;/** @var integer */;/** @var integer */;/** @var string */;/** @var \cli\progress\Bar */;/**
	 * ProgressBar constructor.
	 *
	 * @param string  $message    Text to display before the progress bar.
	 * @param integer $count      Total number of ticks to be performed.
	 * @param integer $interval   Optional. The interval in milliseconds between updates. Default 100.
 	 *
	 * @throws Exception When this is not run within WP CLI
	 */(,,=){(!(())){/* translators: %s php class name */(((,),));}=;=;=;=;}/**
	 * Increment the progress bar ticks.
	 */(){(){();}();;(,);}/**
	 * Get the progress bar tick count.
	 *
	 * @return int
	 */(){?():;}/**
	 * Finish the current progress bar.
	 */(){(){();}=;}/**
	 * Set the message used when creating the progress bar.
	 *
	 * @param string $message The message to be used when the next progress bar is created.
	 */(){=;}/**
	 * Set the count for a new progress bar.
	 *
	 * @param integer $count The total number of ticks expected to complete.
	 */(){=;();}/**
	 * Set up the progress bar.
	 */(){=(,,);}}