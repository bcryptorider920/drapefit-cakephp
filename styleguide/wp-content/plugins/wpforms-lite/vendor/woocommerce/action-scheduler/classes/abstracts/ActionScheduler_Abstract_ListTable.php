(!()){(.);}/**
 * Action Scheduler Abstract List Table class
 *
 * This abstract class enhances WP_List_Table making it ready to use.
 *
 * By extending this class we can focus on describing how our table looks like,
 * which columns needs to be shown, filter, ordered by and more and forget about the details.
 *
 * This class supports:
 *	- Bulk actions
 *	- Search
 *  - Sortable columns
 *  - Automatic translations of the columns
 *
 * @codeCoverageIgnore
 * @since  2.0.0
 */{/**
	 * The table name
	 */;/**
	 * Package name, used to get options from WP_List_Table::get_items_per_page.
	 */;/**
	 * How many items do we render per page?
	 */=;/**
	 * Enables search in this table listing. If this array
	 * is empty it means the listing is not searchable.
	 */=();/**
	 * Columns to show in the table listing. It is a key => value pair. The
	 * key must much the table column name and the value is the label, which is
	 * automatically translated.
	 */=();/**
	 * Defines the row-actions. It expects an array where the key
	 * is the column name and the value is an array of actions.
	 *
	 * The array of actions are key => value, where key is the method name
	 * (with the prefix row_action_<key>) and the value is the label
	 * and title.
	 */=();/**
	 * The Primary key of our table
	 */=;/**
	 * Enables sorting, it expects an array
	 * of columns (the column names are the values)
	 */=();=();/**
	 * @var array The status name => count combinations for this table's items. Used to display status filters.
	 */=();/**
	 * @var array Notices to display when loading the table. Array of arrays of form array( 'class' => {updated|error}, 'message' => 'This is the notice text display.' ).
	 */=();/**
	 * @var string Localised string displayed in the <h1> element above the able.
	 */;/**
	 * Enables bulk actions. It must be an array where the key is the action name
	 * and the value is the label (which is translated automatically). It is important
	 * to notice that it will check that the method exists (`bulk_$name`) and will throw
	 * an exception if it does not exists.
	 *
	 * This class will automatically check if the current request has a bulk action, will do the
	 * validations and afterwards will execute the bulk method, with two arguments. The first argument
	 * is the array with primary keys, the second argument is a string with a list of the primary keys,
	 * escaped and ready to use (with `IN`).
	 */=();/**
	 * Makes translation easier, it basically just wraps
	 * `_x` with some default (the package name).
	 * 
	 * @deprecated 3.0.0
	 */(,=){;}/**
	 * Reads `$this->bulk_actions` and returns an array that WP_List_Table understands. It
	 * also validates that the bulk method handler exists. It throws an exception because
	 * this is a library meant for developers and missing a bulk method is a development-time error.
	 */(){=();(){(!((,.))){("");}[]=;};}/**
	 * Checks if the current request has a bulk action. If that is the case it will validate and will
	 * execute the bulk method handler. Regardless if the action is valid or not it will redirect to
	 * the previous page removing the current arguments that makes this request a bulk action.
	 */(){;// Detect when a bulk action is being triggered.
=();(!){;}(.[]);=.;((,)((,))!([])([])){=.(,(,([]),)).;([],(,[]));}(((,,,,),([])));;}/**
	 * Default code for deleting entries. We trust ids_sql because it is
	 * validated already by process_bulk_action()
	 */(,){;("}}");}/**
	 * Prepares the _column_headers property which is used by WP_Table_List at rendering.
	 * It merges the columns and the sortable columns.
	 */(){=((),(),(),);}/**
	 * Reads $this->sort_by and returns the columns name in a format that WP_Table_List
	 * expects
	 */(){=();(){[]=(,);};}/**
	 * Returns the columns names for rendering. It adds a checkbox for selecting everything
	 * as the first column
	 */(){=((),);;}/**
	 * Get prepared LIMIT clause for items query
	 *
	 * @global wpdb $wpdb
	 *
	 * @return string Prepared LIMIT clause for items query.
	 */(){;=(.,);(,);}/**
	 * Returns the number of items to offset/skip for this current view.
	 *
	 * @return int
	 */(){=(.,);=();(<){=*(-);}{=;};}/**
	 * Get prepared OFFSET clause for items query
	 *
	 * @global wpdb $wpdb
	 *
	 * @return string Prepared OFFSET clause for items query.
	 */(){;(,());}/**
	 * Prepares the ORDER BY sql statement. It uses `$this->sort_by` to know which
	 * columns are sortable. This requests validates the orderby $_GET parameter is a valid
	 * column and sortable. It will also use order (ASC|DESC) using DESC by default.
	 */(){(()){;}=(());=(());"}}";}/**
	 * Return the sortable column specified for this request to order the results by, if any.
	 *
	 * @return string
	 */(){=();(!([])([],)){=([]);}{=[];};}/**
	 * Return the sortable column order specified for this request.
	 *
	 * @return string
	 */(){(!([])([])){=;}{=;};}/**
	 * Return the status filter for this request, if any.
	 *
	 * @return string
	 */(){=(!([]))?[]:;;}/**
	 * Return the search filter for this request, if any.
	 *
	 * @return string
	 */(){=(!([]))?[]:;;}/**
	 * Process and return the columns name. This is meant for using with SQL, this means it
	 * always includes the primary key.
	 *
	 * @return array
	 */(){=();(!(,)){[]=;};}/**
	 * Check if the current request is doing a "full text" search. If that is the case
	 * prepares the SQL to search texts using LIKE.
	 *
	 * If the current request does not have any search or if this list table does not support
	 * that feature it will return an empty string.
	 *
	 * TODO:
	 *   - Improve search doing LIKE by word rather than by phrases.
	 *
	 * @return string
	 */(){;(([])()){;}=();(){[]=(..,([]));}(,);}/**
	 * Prepares the SQL to filter rows by the options defined at `$this->filter_by`. Before trusting
	 * any data sent by the user it validates that it is a valid option.
	 */(){;(!([])!([])){;}=();(){(([][])([[][]])){;}[]=("",[][]);}(,);}/**
	 * Prepares the data to feed WP_Table_List.
	 *
	 * This has the core for selecting, sorting and filting data. To keep the code simple
	 * its logic is split among many methods (get_items_query_*).
	 *
	 * Beside populating the items this function will also count all the records that matches
	 * the filtering criteria and will do fill the pagination variables.
	 */(){;();();(!([])){// _wp_http_referer is used only on bulk actions, we remove it to keep the $_GET shorter
(((,),([])));;}();=();=();=();=(((),(),));=.(,()).;(!()){=.(,).;}{=;}="}}}}}";((,));="}}}";=();=(.,);((,,(/),));}(){(!){;};(){=!([][])?[][]:;(([])){=;}.()..().;(){.()..(?:)..().;};}((,),,,,());;}/**
	 * Set the data for displaying. It will attempt to unserialize (There is a chance that some columns
	 * are serialized). This can be override in child classes for futher data transformation.
	 */(){=();(){[[]]=(,);}}/**
	 * Renders the checkbox for each row, this is the first column and it is named ID regardless
	 * of how the primary key is named (to keep the code simpler). The bulk actions will do the proper
	 * name transformation though using `$this->ID`.
	 */(){.([]).;}/**
	 * Renders the row-actions.
	 *
	 * This method renders the action menu, it reads the definition from the $row_actions property,
	 * and it checks that the row action method exists before rendering it.
	 *
	 * @param array $row     Row to render
	 * @param $column_name   Current row
	 * @return
	 */(,){(([])){;}=[];=;=;([]){;(!(,.)){;}=!([])?[]:((,,(..)));=!([])?[]:;=(<([]))?:;(,());(,(),([]),([]));(,);};;}(){=(,,);(){(([])){;}}=.[];([]([]..[])(,)){([]);}(((,,),([])));;}/**
	 * Default column formatting, it will escape everythig for security.
	 */(,){=([]);(,);;}/**
	 * Display the table heading and search query, if any
	 */(){.().;(()){/* translators: %s: search query */.(((,),())).;};}/**
	 * Display the table heading and search query, if any
	 */(){(){.[].;.([]).;;}}/**
	 * Prints the available statuses so the user can click to filter.
	 */(){=();=();// Helper to set 'all' filter when not set on status counts passed in
(!([])){=(())+;}(){(){;}((())){=;}{=;}=()?():(,);=((,),);[]=(,(),(),(()),());}(){;(,);;}}/**
	 * Renders the table list, we override the original class to render the table inside a form
	 * and to render any needed HTML (like the search box). By doing so the callee of a function can simple
	 * forget about any extra HTML.
	 */(){.([]).;(){([]){;}.()..().;}(!()){((),);// WPCS: XSS OK
}();;}/**
	 * Process any pending actions.
	 */(){();();(!([])){// _wp_http_referer is used only on bulk actions, we remove it to keep the $_GET shorter
(((,),([])));;}}/**
	 * Render the list table page, including header, notices, status filters and table.
	 */(){();;();();();();;}/**
	 * Get the text to display in the search box on the list table.
	 */(){(,);}}