/**
 * Abstract class with common Queue Cleaner functionality.
 */{/** @var ActionScheduler_QueueCleaner */;/** @var ActionScheduler_FatalErrorMonitor */;/** @var ActionScheduler_Store */;/**
	 * The created time.
	 *
	 * Represents when the queue runner was constructed and used when calculating how long a PHP request has been running.
	 * For this reason it should be as close as possible to the PHP request start time.
	 *
	 * @var int
	 */;/**
	 * ActionScheduler_Abstract_QueueRunner constructor.
	 *
	 * @param ActionScheduler_Store             $store
	 * @param ActionScheduler_FatalErrorMonitor $monitor
	 * @param ActionScheduler_QueueCleaner      $cleaner
	 */(=,=,=){=();=?:();=?:();=?:();}/**
	 * Process an individual action.
	 *
	 * @param int $action_id The action ID to process.
	 * @param string $context Optional identifer for the context in which this action is being processed, e.g. 'WP CLI' or 'WP Cron'
	 *        Generally, this should be capitalised and not localised as it's a proper noun.
	 */(,=){{=;(,,);(()){(,,);;}=;(,,);=();();();(,,,);();}(){(){();(,,,);}{(,,,);}}(()(,)()()){(,);}}/**
	 * Schedule the next instance of the action if necessary.
	 *
	 * @param ActionScheduler_Action $action
	 * @param int $action_id
	 */(,){{()();}(){(,,,);}}/**
	 * Run the queue cleaner.
	 *
	 * @author Jeremy Pry
	 */(){(*());}/**
	 * Get the number of concurrent batches a runner allows.
	 *
	 * @return int
	 */(){(,);}/**
	 * Check if the number of allowed concurrent batches is met or exceeded.
	 *
	 * @return bool
	 */(){()();}/**
	 * Get the maximum number of seconds a batch can run for.
	 *
	 * @return int The number of seconds.
	 */(){=;// Apply deprecated filter from deprecated get_maximum_execution_time() method
(()){(,,);=(,);}((,));}/**
	 * Get the number of seconds the process has been running.
	 *
	 * @return int The number of seconds.
	 */(){=()-;// Get the CPU time if the hosting environment uses it rather than wall-clock time to calculate a process's execution time.
(()(,())){=();(([],[])){=[]+([]/);}};}/**
	 * Check if the host's max execution time is (likely) to be exceeded if processing more actions.
	 *
	 * @param int $processed_actions The number of actions processed so far - used to determine the likelihood of exceeding the time limit if processing another action
	 * @return bool
	 */(){=();=();=/;=+(*);=>;(,,,,,);}/**
	 * Get memory limit
	 *
	 * Based on WP_Background_Process::get_memory_limit()
	 *
	 * @return int
	 */(){(()){=();}{=;// Sensible default, and minimum required by WooCommerce
}(!-){// Unlimited, set to 32GB.
=;}();}/**
	 * Memory exceeded
	 *
	 * Ensures the batch process never exceeds 90% of the maximum WordPress memory.
	 *
	 * Based on WP_Background_Process::memory_exceeded()
	 *
	 * @return bool
	 */(){=()*;=();=;(,,);}/**
	 * See if the batch limits have been exceeded, which is when memory usage is almost at
	 * the maximum limit, or the time to process more actions will exceed the max time limit.
	 *
	 * Based on WC_Background_Process::batch_limits_exceeded()
	 *
	 * @param int $processed_actions The number of actions processed so far - used to determine the likelihood of exceeding the time limit if processing another action
	 * @return bool
	 */(){()();}/**
	 * Process actions in the queue.
	 *
	 * @author Jeremy Pry
	 * @param string $context Optional identifer for the context in which this action is being processed, e.g. 'WP CLI' or 'WP Cron'
	 *        Generally, this should be capitalised and not localised as it's a proper noun.
	 * @return int The number of actions processed.
	 */(=);}