/**
 * Class ActionScheduler_Abstract_RecurringSchedule
 */{/**
	 * The date & time the first instance of this schedule was setup to run (which may not be this instance).
	 *
	 * Schedule objects are attached to an action object. Each schedule stores the run date for that
	 * object as the start date - @see $this->start - and logic to calculate the next run date after
	 * that - @see $this->calculate_next(). The $first_date property also keeps a record of when the very
	 * first instance of this chain of schedules ran.
	 *
	 * @var DateTime
	 */=;/**
	 * Timestamp equivalent of @see $this->first_date
	 *
	 * @var int
	 */=;/**
	 * The recurrance between each time an action is run using this schedule.
	 * Used to calculate the start date & time. Can be a number of seconds, in the
	 * case of ActionScheduler_IntervalSchedule, or a cron expression, as in the
	 * case of ActionScheduler_CronSchedule. Or something else.
	 *
	 * @var mixed
	 */;/**
	 * @param DateTime $date The date & time to run the action.
	 * @param mixed $recurrence The data used to determine the schedule's recurrance.
	 * @param DateTime|null $first (Optional) The date & time the first instance of this interval schedule ran. Default null, meaning this is the first instance.
	 */(,,=){();=()?:;=;}/**
	 * @return bool
	 */(){;}/**
	 * Get the date & time of the first schedule in this recurring series.
	 *
	 * @return DateTime|null
	 */(){;}/**
	 * @return string
	 */(){;}/**
	 * For PHP 5.2 compat, since DateTime objects can't be serialized
	 * @return array
	 */(){=();=();(,(,));}/**
	 * Unserialize recurring schedules serialized/stored prior to AS 3.0.0
	 *
	 * Prior to Action Scheduler 3.0.0, schedules used different property names to refer
	 * to equivalent data. For example, ActionScheduler_IntervalSchedule::start_timestamp
	 * was the same as ActionScheduler_SimpleSchedule::timestamp. This was addressed in
	 * Action Scheduler 3.0.0, where properties and property names were aligned for better
	 * inheritance. To maintain backward compatibility with scheduled serialized and stored
	 * prior to 3.0, we need to correctly map the old property names.
	 */(){();(>){=();}{=();}}}