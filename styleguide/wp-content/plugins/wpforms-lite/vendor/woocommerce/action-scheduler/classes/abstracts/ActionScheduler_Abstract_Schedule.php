/**
 * Class ActionScheduler_Abstract_Schedule
 */{/**
	 * The date & time the schedule is set to run.
	 *
	 * @var DateTime
	 */=;/**
	 * Timestamp equivalent of @see $this->scheduled_date
	 *
	 * @var int
	 */=;/**
	 * @param DateTime $date The date & time to run the action.
	 */(){=;}/**
	 * Check if a schedule should recur.
	 *
	 * @return bool
	 */();/**
	 * Calculate when the next instance of this schedule would run based on a given date & time.
	 *
	 * @param DateTime $after
	 * @return DateTime
	 */();/**
	 * Get the next date & time when this schedule should run after a given date & time.
	 *
	 * @param DateTime $after
	 * @return DateTime|null
	 */(){=;(>){=();;};}/**
	 * Get the date & time the schedule is set to run.
	 *
	 * @return DateTime|null
	 */(){;}/**
	 * For PHP 5.2 compat, since DateTime objects can't be serialized
	 * @return array
	 */(){=();(,);}(){=();();}}