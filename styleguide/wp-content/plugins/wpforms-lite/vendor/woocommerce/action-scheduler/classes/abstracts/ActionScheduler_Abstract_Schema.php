/**
 * Class ActionScheduler_Abstract_Schema
 *
 * @package Action_Scheduler
 *
 * @codeCoverageIgnore
 *
 * Utility class for creating/updating custom tables
 */{/**
	 * @var int Increment this value in derived class to trigger a schema update.
	 */=;/**
	 * @var array Names of tables that will be registered by this class.
	 */=[];/**
	 * Register tables with WordPress, and create them if needed.
	 *
	 * @return void
	 */(){;// make WP aware of our tables
(){[]=;=();=;}// create the tables
(()){(){();}();}}/**
	 * @param string $table The name of the table
	 *
	 * @return string The CREATE TABLE statement, suitable for passing to dbDelta
	 */();/**
	 * Determine if the database schema is out of date
	 * by comparing the integer found in $this->schema_version
	 * with the option set in the WordPress options table
	 *
	 * @return bool
	 */(){=.;=(,);// Check for schema option stored by the Action Scheduler Custom Tables plugin in case site has migrated from that plugin with an older schema
(){=;(){:;;:;;}=(,);();}(,,);}/**
	 * Update the option in WordPress to indicate that
	 * our schema is now up to date
	 *
	 * @return void
	 */(){=.;// work around race conditions and ensure that our option updates
=..();(,);}/**
	 * Update the schema for the given table
	 *
	 * @param string $table The name of the table to update
	 *
	 * @return void
	 */(){(.);=();(){=();(){((,)){(,,);}}}}/**
	 * @param string $table
	 *
	 * @return string The full name of the table, including the
	 *                table prefix for the current blog
	 */(){[].;}}