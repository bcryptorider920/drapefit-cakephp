/**
 * Abstract class for setting a basic lock to throttle some action.
 *
 * Class ActionScheduler_Lock
 */{/** @var ActionScheduler_Lock */=;/** @var int */=;/**
	 * Check if a lock is set for a given lock type.
	 *
	 * @param string $lock_type A string to identify different lock types.
	 * @return bool
	 */(){(()());}/**
	 * Set a lock.
	 *
	 * @param string $lock_type A string to identify different lock types.
	 * @return bool
	 */();/**
	 * If a lock is set, return the timestamp it was set to expiry.
	 *
	 * @param string $lock_type A string to identify different lock types.
	 * @return bool|int False if no lock is set, otherwise the timestamp for when the lock is set to expire.
	 */();/**
	 * Get the amount of time to set for a given lock. 60 seconds by default.
	 *
	 * @param string $lock_type A string to identify different lock types.
	 * @return int
	 */(){(,,);}/**
	 * @return ActionScheduler_Lock
	 */(){(()){=(,);=();};}}