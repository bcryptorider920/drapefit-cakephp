/**
 * Class ActionScheduler_Logger
 * @codeCoverageIgnore
 */{=;/**
	 * @return ActionScheduler_Logger
	 */(){(()){=(,);=();};}/**
	 * @param string $action_id
	 * @param string $message
	 * @param DateTime $date
	 *
	 * @return string The log entry ID
	 */(,,=);/**
	 * @param string $entry_id
	 *
	 * @return ActionScheduler_LogEntry
	 */();/**
	 * @param string $action_id
	 *
	 * @return ActionScheduler_LogEntry[]
	 */();/**
	 * @codeCoverageIgnore
	 */(){();(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);(,(,),,);}(){(,(,));}(){(,(,));}(){(,(,));}(){(,(,));}(,=){(!()){/* translators: %s: context */=((,),);}{=(,);}(,);}(,=,=){(!()){/* translators: %s: context */=((,),);}{=(,);}(,);}(,,=){(!()){/* translators: 1: context 2: exception message */=((,),,());}{/* translators: %s: exception message */=((,),());}(,);}(,){/* translators: %s: amount of time */(,((,),));}(,){(!()){/* translators: 1: error message 2: filename 3: line */(,((,),[],[],[]));}}(){(,(,));}(,=){(!()){/* translators: %s: context */=((,),);}{=(,);}(,(,));}/**
	 * @param string $action_id
	 * @param Exception|NULL $exception The exception which occured when fetching the action. NULL by default for backward compatibility.
	 *
	 * @return ActionScheduler_LogEntry[]
	 */(,=){(!()){/* translators: %s: exception message */=((,),());}{=(,);}(,);}(,){/* translators: %s: exception message */(,((,),()));}/**
	 * Bulk add cancel action log entries.
	 *
	 * Implemented here for backward compatibility. Should be implemented in parent loggers
	 * for more performant bulk logging.
	 *
	 * @param array $action_ids List of action ID.
	 */(){(()){;}(){();}}}