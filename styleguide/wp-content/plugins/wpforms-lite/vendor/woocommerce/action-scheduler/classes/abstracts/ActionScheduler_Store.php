/**
 * Class ActionScheduler_Store
 * @codeCoverageIgnore
 */{=;=;=;=;=;=;/** @var ActionScheduler_Store */=;/** @var int */=;/**
	 * @param ActionScheduler_Action $action
	 * @param DateTime $scheduled_date Optional Date of the first instance
	 *        to store. Otherwise uses the first date of the action's
	 *        schedule.
	 *
	 * @return string The action ID
	 */(,=);/**
	 * @param string $action_id
	 *
	 * @return ActionScheduler_Action
	 */();/**
	 * @param string $hook Hook name/slug.
	 * @param array  $params Hook arguments.
	 * @return string ID of the next action matching the criteria.
	 */(,=());/**
	 * @param array  $query Query parameters.
	 * @param string $query_type Whether to select or count the results. Default, select.
	 *
	 * @return array|int The IDs of or count of actions matching the query.
	 */(=(),=);/**
	 * Get a count of all actions in the store, grouped by status
	 *
	 * @return array
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 *
	 * @return DateTime The date the action is schedule to run, or the date that it ran.
	 */();/**
	 * @param int      $max_actions
	 * @param DateTime $before_date Claim only actions schedule before the given date. Defaults to now.
	 * @param array    $hooks       Claim only actions with a hook or hooks.
	 * @param string   $group       Claim only actions in the given group.
	 *
	 * @return ActionScheduler_ActionClaim
	 */(=,=,=(),=);/**
	 * @return int
	 */();/**
	 * @param ActionScheduler_ActionClaim $claim
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 */();/**
	 * @param string $action_id
	 *
	 * @return string
	 */();/**
	 * @param string $action_id
	 * @return mixed
	 */();/**
	 * @param string $claim_id
	 * @return array
	 */();/**
	 * @param string $comparison_operator
	 * @return string
	 */(){((,(,,,,,))){;};}/**
	 * Get the time MySQL formated date/time string for an action's (next) scheduled date.
	 *
	 * @param ActionScheduler_Action $action
	 * @param DateTime $scheduled_date (optional)
	 * @return string
	 */(,=){=?()():;(!){;}(());();}/**
	 * Get the time MySQL formated date/time string for an action's (next) scheduled date.
	 *
	 * @param ActionScheduler_Action $action
	 * @param DateTime $scheduled_date (optional)
	 * @return string
	 */(,=){=?()():;(!){;}();();}/**
	 * Validate that we could decode action arguments.
	 *
	 * @param mixed $args      The decoded arguments.
	 * @param int   $action_id The action ID.
	 *
	 * @throws ActionScheduler_InvalidActionException When the decoded arguments are invalid.
	 */(,){// Ensure we have an array of args.
(!()){();}// Validate JSON decoding if possible.
(()()){(,);}}/**
	 * Validate a ActionScheduler_Schedule object.
	 *
	 * @param mixed $schedule  The unserialized ActionScheduler_Schedule object.
	 * @param int   $action_id The action ID.
	 *
	 * @throws ActionScheduler_InvalidActionException When the schedule is invalid.
	 */(,){(()!(,)){(,);}}/**
	 * InnoDB indexes have a maximum size of 767 bytes by default, which is only 191 characters with utf8mb4.
	 *
	 * Previously, AS wasn't concerned about args length, as we used the (unindex) post_content column. However,
	 * with custom tables, we use an indexed VARCHAR column instead.
	 *
	 * @param  ActionScheduler_Action $action Action to be validated.
	 * @throws InvalidArgumentException When json encoded args is too long.
	 */(){(((()))>){(((,),));}}/**
	 * Cancel pending actions by hook.
	 *
	 * @since 3.0.0
	 *
	 * @param string $hook Hook name.
	 *
	 * @return void
	 */(){=;(!()){=((,,,));();}}/**
	 * Cancel pending actions by group.
	 *
	 * @since 3.0.0
	 *
	 * @param string $group Group slug.
	 *
	 * @return void
	 */(){=;(!()){=((,,,));();}}/**
	 * Cancel a set of action IDs.
	 *
	 * @since 3.0.0
	 *
	 * @param array $action_ids List of action IDs.
	 *
	 * @return void
	 */(){(){();}(,);}/**
	 * @return array
	 */(){((,),(,),(,),(,),(,),);}/**
	 * Check if there are any pending scheduled actions due to run.
	 *
	 * @param ActionScheduler_Action $action
	 * @param DateTime $scheduled_date (optional)
	 * @return string
	 */(){=(((),,));!();}/**
	 * Callable initialization function optionally overridden in derived classes.
	 */(){}/**
	 * Callable function to mark an action as migrated optionally overridden in derived classes.
	 */(){}/**
	 * @return ActionScheduler_Store
	 */(){(()){=(,);=();};}}