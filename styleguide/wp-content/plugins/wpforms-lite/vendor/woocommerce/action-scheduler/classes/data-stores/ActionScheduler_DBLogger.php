/**
 * Class ActionScheduler_DBLogger
 *
 * Action logs data table data store.
 *
 * @since 3.0.0
 */{/**
	 * Add a record to an action log.
	 *
	 * @param int      $action_id Action ID.
	 * @param string   $message Message to be saved in the log entry.
	 * @param DateTime $date Timestamp of the log entry.
	 *
	 * @return int     The log entry ID.
	 */(,,=){(()){=();}{=;}=();();=();/** @var \wpdb $wpdb */;(,[,,,,],[,,,]);;}/**
	 * Retrieve an action log entry.
	 *
	 * @param int $entry_id Log entry ID.
	 *
	 * @return ActionScheduler_LogEntry
	 */(){/** @var \wpdb $wpdb */;=(("}",));();}/**
	 * Create an action log entry from a database record.
	 *
	 * @param object $record Log entry database record object.
	 *
	 * @return ActionScheduler_LogEntry
	 */(){(()){();}=();(,,);}/**
	 * Retrieve the an action's log entries from the database.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return ActionScheduler_LogEntry[]
	 */(){/** @var \wpdb $wpdb */;=(("}",));([,],);}/**
	 * Initialize the data store.
	 *
	 * @codeCoverageIgnore
	 */(){=();();();(,[,],,);}/**
	 * Delete the action logs for an action.
	 *
	 * @param int $action_id Action ID.
	 */(){/** @var \wpdb $wpdb */;(,[,],[]);}/**
	 * Bulk add cancel action log entries.
	 *
	 * @param array $action_ids List of action ID.
	 */(){(()){;}/** @var \wpdb $wpdb */;=();=();();=();=(,);=.(,,,).;="}";=[];(){[]=(,);}(,);();}}