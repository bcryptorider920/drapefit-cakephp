/**
 * Class ActionScheduler_DBStore
 *
 * Action data table data store.
 *
 * @since 3.0.0
 */{/** @var int */=;/** @var int */=;/**
	 * Initialize the data store
	 *
	 * @codeCoverageIgnore
	 */(){=();();}/**
	 * Save an action.
	 *
	 * @param ActionScheduler_Action $action Action object.
	 * @param DateTime               $date Optional schedule date. Default null.
	 *
	 * @return int Action ID.
	 */(,=){{();/** @var \wpdb $wpdb */;=[(),(()?:),(,),(,),(()),(()),];=(());(()){[]=;}{[]=();[]=;}(,);=;(()){(());}(()){(?:(,));}(,);;}(){/* translators: %s: error message */(((,),()),);}}/**
	 * Generate a hash from json_encoded $args using MD5 as this isn't for security.
	 *
	 * @param string $args JSON encoded action args.
	 * @return string
	 */(){();}/**
	 * Get action args query param value from action args.
	 *
	 * @param array $args Action args.
	 * @return string
	 */(){=();(()){;}();}/**
	 * Get a group's ID based on its name/slug.
	 *
	 * @param string $slug The string name of a group.
	 * @param bool $create_if_not_exists Whether to create the group if it does not already exist. Default, true - create the group.
	 *
	 * @return int The group's ID, if it exists or is created, or 0 if it does not exist and is not created.
	 */(,=){(()){;}/** @var \wpdb $wpdb */;=(("}",));(()){=();};}/**
	 * Create an action group.
	 *
	 * @param string $slug Group slug.
	 *
	 * @return int Group ID.
	 */(){/** @var \wpdb $wpdb */;(,[]);;}/**
	 * Retrieve an action.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return ActionScheduler_Action
	 */(){/** @var \wpdb $wpdb */;=(("}}",));(()){();}(!()){=;();}{=();}(){(,,);();};}/**
	 * Create a null action.
	 *
	 * @return ActionScheduler_NullAction
	 */(){();}/**
	 * Create an action from a database record.
	 *
	 * @param object $data Action database record.
	 *
	 * @return ActionScheduler_Action|ActionScheduler_CanceledAction|ActionScheduler_FinishedAction
	 */(){=;=(,);=();(,);(,);(()){=();}=?:;()(,,,,);}/**
	 * Find an action.
	 *
	 * @param string $hook Action hook.
	 * @param array  $params Parameters of the action to find.
	 *
	 * @return string|null ID of the next action matching the criteria or NULL if not found.
	 */(,=[]){=(,[,,,]);/** @var wpdb $wpdb */;="}";=[];(!([])){"}";[]=[];};[]=;(!([])){;[]=([]);}=;(!([])){;[]=[];([]){=;// Find the next action that matches.
}{=;// Find the most recent action that matches.
}}"";=(,);=();;}/**
	 * Returns the SQL statement to query (or count) actions.
	 *
	 * @param array  $query Filtering options.
	 * @param string $select_or_count  Whether the SQL should select and return the IDs or just the row count.
	 *
	 * @return string SQL statement already properly escaped.
	 */(,=){(!(,(,))){((,));}=(,[,,,,,,,,,,,,,]);/** @var \wpdb $wpdb */;=()?:;"}";=[];(!([])[]){"}";};(!([])){;[]=[];}([]){;[]=[];}(!([])){;[]=([]);}([]){;[]=[];}([]){=[];(());=();=([]);"";[]=;}([]){=[];(());=();=([]);"";[]=;}([]){;}([]){;}(!([])){;[]=[];}(!([])){;(=;<;){[]=(,[]);}=[];(){;[]=;};}(){([]){:=;;:=;;:=;;::=;;}(([])){=;}{=;}"";([]>){;[]=[];[]=[];}}(!()){=(,);};}/**
	 * Query for action count of list of action IDs.
	 *
	 * @param array  $query Query parameters.
	 * @param string $query_type Whether to select or count the results. Default, select.
	 *
	 * @return null|string|array The IDs of actions matching the query
	 */(=[],=){/** @var wpdb $wpdb */;=(,);()?():();}/**
	 * Get a count of all actions in the store, grouped by status.
	 *
	 * @return array Set of 'status' => int $count pairs for statuses with 1 or more actions of that status.
	 */(){;=;"}";;=();=();(()){// Ignore any actions with invalid status
((,)){[]=;}};}/**
	 * Cancel an action.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return void
	 */(){/** @var \wpdb $wpdb */;=(,[],[],[],[]);(()){/* translators: %s: action ID */(((,),));}(,);}/**
	 * Cancel pending actions by hook.
	 *
	 * @since 3.0.0
	 *
	 * @param string $hook Hook name.
	 *
	 * @return void
	 */(){([]);}/**
	 * Cancel pending actions by group.
	 *
	 * @param string $group Group slug.
	 *
	 * @return void
	 */(){([]);}/**
	 * Bulk cancel actions.
	 *
	 * @since 3.0.0
	 *
	 * @param array $query_args Query parameters.
	 */(){/** @var \wpdb $wpdb */;(!()){;}// Don't cancel actions that are already canceled.
(([])[]){;}=;=(,[,,]);(){=();(()){;}=(,(),);=.(,).;=;(,);((// wpcs: PreparedSQLPlaceholders replacement count ok.
"}}",));(,);}}/**
	 * Delete an action.
	 *
	 * @param int $action_id Action ID.
	 */(){/** @var \wpdb $wpdb */;=(,[],[]);(()){(((,),));}(,);}/**
	 * Get the schedule date for an action.
	 *
	 * @param string $action_id Action ID.
	 *
	 * @throws \InvalidArgumentException
	 * @return \DateTime The local date the action is scheduled to run, or the date that it ran.
	 */(){=();();;}/**
	 * Get the GMT schedule date for an action.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @throws \InvalidArgumentException
	 * @return \DateTime The GMT date the action is scheduled to run, or the date that it ran.
	 */(){/** @var \wpdb $wpdb */;=(("}",));(()){(((,),));}(){();}{();}}/**
	 * Stake a claim on actions.
	 *
	 * @param int       $max_actions Maximum number of action to include in claim.
	 * @param \DateTime $before_date Jobs must be schedule before this date. Defaults to now.
	 *
	 * @return ActionScheduler_ActionClaim
	 */(=,=,=(),=){=();(,,,,);=();(,);}/**
	 * Generate a new action claim.
	 *
	 * @return int Claim ID.
	 */(){/** @var \wpdb $wpdb */;=();(,[()]);;}/**
	 * Mark actions claimed.
	 *
	 * @param string    $claim_id Claim Id.
	 * @param int       $limit Number of action to include in claim.
	 * @param \DateTime $before_date Should use UTC timezone.
	 *
	 * @return int The number of actions that were claimed.
	 * @throws \RuntimeException
	 */(,,=,=(),=){/** @var \wpdb $wpdb */;=();=()?:;// can't use $wpdb->update() because of the <= condition
="}";=(,(),(),);=;[]=();[]=;(!()){=(,(),);.(,).;=(,());}(!()){=(,);// throw exception if no matching group found, this matches ActionScheduler_wpPostStore's behaviour
(()){/* translators: %s: group name */(((,),));};[]=;}=;[]=;=("}}}",);=();(){((,));};}/**
	 * Get the number of active claims.
	 *
	 * @return int
	 */(){;="}";=(,[,]);();}/**
	 * Return an action's claim ID, as stored in the claim_id column.
	 *
	 * @param string $action_id Action ID.
	 * @return mixed
	 */(){/** @var \wpdb $wpdb */;="}";=(,);();}/**
	 * Retrieve the action IDs of action in a claim.
	 *
	 * @param string $claim_id Claim ID.
	 *
	 * @return int[]
	 */(){/** @var \wpdb $wpdb */;="}";=(,);=();(,);}/**
	 * Release actions from a claim and delete the claim.
	 *
	 * @param ActionScheduler_ActionClaim $claim Claim object.
	 */(){/** @var \wpdb $wpdb */;(,[],[()],[],[]);(,[()],[]);}/**
	 * Remove the claim from an action.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return void
	 */(){/** @var \wpdb $wpdb */;(,[],[],[],[]);}/**
	 * Mark an action as failed.
	 *
	 * @param int $action_id Action ID.
	 */(){/** @var \wpdb $wpdb */;=(,[],[],[],[]);(()){(((,),));}}/**
	 * Add execution message to action log.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return void
	 */(){/** @var \wpdb $wpdb */;="}";=(,,(,),(),);();}/**
	 * Mark an action as complete.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return void
	 */(){/** @var \wpdb $wpdb */;=(,[,(,),(),],[],[],[]);(()){(((,),));}}/**
	 * Get an action's status.
	 *
	 * @param int $action_id Action ID.
	 *
	 * @return string
	 */(){/** @var \wpdb $wpdb */;="}";=(,);=();(){((,));}(()){((,));}{;}}}