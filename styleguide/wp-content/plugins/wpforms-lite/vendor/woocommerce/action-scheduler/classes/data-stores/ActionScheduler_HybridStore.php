;;;;/**
 * Class ActionScheduler_HybridStore
 *
 * A wrapper around multiple stores that fetches data from both.
 *
 * @since 3.0.0
 */{=;;;;/**
	 * @var int The dividing line between IDs of actions created
	 *          by the primary and secondary stores.
	 *
	 * Methods that accept an action ID will compare the ID against
	 * this to determine which store will contain that ID. In almost
	 * all cases, the ID should come from the primary store, but if
	 * client code is bypassing the API functions and fetching IDs
	 * from elsewhere, then there is a chance that an unmigrated ID
	 * might be requested.
	 */=;/**
	 * ActionScheduler_HybridStore constructor.
	 *
	 * @param Config $config Migration config object.
	 */(=){=(,);(()){=()();}=();=();=();}/**
	 * Initialize the table data store tables.
	 *
	 * @codeCoverageIgnore
	 */(){(,[,],,);();();(,[,],);}/**
	 * When the actions table is created, set its autoincrement
	 * value to be one higher than the posts table to ensure that
	 * there are no ID collisions.
	 *
	 * @param string $table_name
	 * @param string $table_suffix
	 *
	 * @return void
	 * @codeCoverageIgnore
	 */(,){(){(()){=();}/** @var \wpdb $wpdb */;({},[,,,]);({},[]);}}/**
	 * Store the demarkation id in WP options.
	 *
	 * @param int $id The ID to set as the demarkation point between the two stores
	 *                Leave null to use the next ID from the WP posts table.
	 *
	 * @return int The new ID.
	 *
	 * @codeCoverageIgnore
	 */(=){(()){/** @var \wpdb $wpdb */;=("");;}(,);;}/**
	 * Find the first matching action from the secondary store.
	 * If it exists, migrate it to the primary store immediately.
	 * After it migrates, the secondary store will logically contain
	 * the next matching action, so return the result thence.
	 *
	 * @param string $hook
	 * @param array  $params
	 *
	 * @return string
	 */(,=[]){=(,);(!()){([]);}(,);}/**
	 * Find actions matching the query in the secondary source first.
	 * If any are found, migrate them immediately. Then the secondary
	 * store will contain the canonical results.
	 *
	 * @param array $query
	 * @param string $query_type Whether to select or count the results. Default, select.
	 *
	 * @return int[]
	 */(=[],=){=(,);(!()){();}(,);}/**
	 * Get a count of all actions in the store, grouped by status
	 *
	 * @return array Set of 'status' => int $count pairs for statuses with 1 or more actions of that status.
	 */(){=();=();=();(()){=;(([])){[];}(([])){[];}[]=;}=();;}/**
	 * If any actions would have been claimed by the secondary store,
	 * migrate them immediately, then ask the primary store for the
	 * canonical claim.
	 *
	 * @param int           $max_actions
	 * @param DateTime|null $before_date
	 *
	 * @return ActionScheduler_ActionClaim
	 */(=,=,=(),=){=(,,,);=();(!()){();}();(,,,);}/**
	 * Migrate a list of actions to the table data store.
	 *
	 * @param array $action_ids List of action IDs.
	 */(){();}/**
	 * Save an action to the primary store.
	 *
	 * @param ActionScheduler_Action $action Action object to be saved.
	 * @param DateTime               $date Optional. Schedule date. Default null.
	 */(,=){(,);}/**
	 * Retrieve an existing action whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Cancel an existing action whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Delete an existing action whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Get the schedule date an existing action whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Mark an existing action as failed whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Log the execution of an existing action whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Mark an existing action complete whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/**
	 * Get an existing action status whether migrated or not.
	 *
	 * @param int $action_id Action ID.
	 */(){(<){();}{();}}/* * * * * * * * * * * * * * * * * * * * * * * * * * *
	 * All claim-related functions should operate solely
	 * on the primary store.
	 * * * * * * * * * * * * * * * * * * * * * * * * * * *//**
	 * Get the claim count from the table data store.
	 */(){();}/**
	 * Retrieve the claim ID for an action from the table data store.
	 *
	 * @param int $action_id Action ID.
	 */(){();}/**
	 * Release a claim in the table data store.
	 *
	 * @param ActionScheduler_ActionClaim $claim Claim object.
	 */(){();}/**
	 * Release claims on an action in the table data store.
	 *
	 * @param int $action_id Action ID.
	 */(){();}/**
	 * Retrieve a list of action IDs by claim.
	 *
	 * @param int $claim_id Claim ID.
	 */(){();}}