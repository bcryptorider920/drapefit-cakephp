/**
 * Class ActionScheduler_wpCommentLogger
 */{=;=;/**
	 * @param string $action_id
	 * @param string $message
	 * @param DateTime $date
	 *
	 * @return string The log entry ID
	 */(,,=){(()){=();}{=();}=(,,);;}(,,){=();();=(,(),,,,,,);();}/**
	 * @param string $entry_id
	 *
	 * @return ActionScheduler_LogEntry
	 */(){=();(()){();}=();();(,,);}/**
	 * @param string $action_id
	 *
	 * @return ActionScheduler_LogEntry[]
	 */(){=;(()){=;}=((,,,,,));=();(){=();(!()){[]=;}};}(){();}/**
	 * @param WP_Comment_Query $query
	 */(){((,,,,,,,,)){(!([])){;// don't slow down queries that wouldn't include action_log comments anyway
}}[]=;(,(,),,);}/**
	 * @param array $clauses
	 * @param WP_Comment_Query $query
	 *
	 * @return array
	 */(,){(!([])){[]();};}/**
	 * Make sure Action Scheduler logs are excluded from comment feeds, which use WP_Query, not
	 * the WP_Comment_Query class handled by @see self::filter_comment_queries().
	 *
	 * @param string $where
	 * @param WP_Query $query
	 *
	 * @return string
	 */(,){(()){();};}/**
	 * Return a SQL clause to exclude Action Scheduler comments.
	 *
	 * @return string
	 */(){;("}",);}/**
	 * Remove action log entries from wp_count_comments()
	 *
	 * @param array $stats
	 * @param int $post_id
	 *
	 * @return object
	 */(,){;(){=();};}/**
	 * Retrieve the comment counts from our cache, or the database if the cached version isn't set.
	 *
	 * @return object
	 */(){;=();(!){=();=("}",);=;=();=(,,,,);(){// Don't count post-trashed toward totals
([][]){[];}(([[]])){[[[]]]=[];}}[]=;[]=;(){(([])){[]=;}}=;(,);};}/**
	 * Delete comment count cache whenever there is new comment or the status of a comment changes. Cache
	 * will be regenerated next time ActionScheduler_wpCommentLogger::filter_comment_count() is called.
	 */(){();}/**
	 * @codeCoverageIgnore
	 */(){(,(,),,);(,(,),,);();(,(,),,);(,(,),,);// run after WC_Comments::wp_count_comments() to make sure we exclude order notes and action logs
(,(,),,);// Delete comments count cache whenever there is a new comment or a comment status changes
(,(,));(,(,));}(){();}(){();}}