/**
 * Class ActionScheduler_wpPostStore
 */{=;=;=;=;/** @var DateTimeZone */=;(,=){{();=(,);=();(,());(,());(,);;}(){(((,),()),);}}(,=){=(,(),(()),(()?:),(,),(,),);;}(){(,(,),,);(,(,),,);=(,);(){// Prevent KSES from corrupting JSON in post_content.
();}=();(){();}(,(,),);(,(,),);(()()){((,));};}(){([]){[]=;([]){[]=;}};}/**
	 * Create a (probably unique) post name for scheduled actions in a more performant manner than wp_unique_post_slug().
	 *
	 * When an action's post status is transitioned to something other than 'draft', 'pending' or 'auto-draft, like 'publish'
	 * or 'failed' or 'trash', WordPress will find a unique slug (stored in post_name column) using the wp_unique_post_slug()
	 * function. This is done to ensure URL uniqueness. The approach taken by wp_unique_post_slug() is to iterate over existing
	 * post_name values that match, and append a number 1 greater than the largest. This makes sense when manually creating a
	 * post from the Edit Post screen. It becomes a bottleneck when automatically processing thousands of actions, with a
	 * database containing thousands of related post_name values.
	 *
	 * WordPress 5.1 introduces the 'pre_wp_unique_post_slug' filter for plugins to address this issue.
	 *
	 * We can short-circuit WordPress's wp_unique_post_slug() approach using the 'pre_wp_unique_post_slug' filter. This
	 * method is available to be used as a callback on that filter. It provides a more scalable approach to generating a
	 * post_name/slug that is probably unique. Because Action Scheduler never actually uses the post_name field, or an
	 * action's slug, being probably unique is good enough.
	 *
	 * For more backstory on this issue, see:
	 * - https://github.com/woocommerce/action-scheduler/issues/44 and
	 * - https://core.trac.wordpress.org/ticket/21112
	 *
	 * @param string $override_slug Short-circuit return value.
	 * @param string $slug          The desired slug (post_name).
	 * @param int    $post_ID       Post ID.
	 * @param string $post_status   The post status.
	 * @param string $post_type     Post type.
	 * @return string
	 */(,,,,){(){=(.,)..(,);};}(,){(,,);}(,){(()){(,(),,);}{(,(),,);}}(){=();(()){();}{=();}(){(,,);();};}(){(()){;}();}(){();}(){=;=(,);(,);=(,,);(,);=(,,());=()?:();()((),,,,);}/**
	 * @param string $post_status
	 *
	 * @throws InvalidArgumentException if $post_status not in known status fields returned by $this->get_status_labels()
	 * @return string
	 */(){(){:=;;:=;;:(!(,())){((,));}=;;};}/**
	 * @param string $action_status
	 * @throws InvalidArgumentException if $post_status not in known status fields returned by $this->get_status_labels()
	 * @return string
	 */(){(){:=;;:=;;:(!(,())){((,));}=;;};}/**
	 * @param string $hook
	 * @param array $params
	 *
	 * @return string ID of the next action matching the criteria or NULL if not found
	 */(,=()){=(,(,,,));/** @var wpdb $wpdb */;="}";=();(!([])){"}";"}";"}";[]=[];};[]=;;[]=;(!([])){;[]=([]);}(!([])){;[]=([]);}([]){:::=;// Find the most recent action that matches
;::=;// Find the next action that matches
;}"";=(,);=();;}/**
	 * Returns the SQL statement to query (or count) actions.
	 *
	 * @param array $query Filtering options
	 * @param string $select_or_count  Whether the SQL should select and return the IDs or just the row count
	 * @throws InvalidArgumentException if $select_or_count not count or select
	 * @return string SQL statement. The returned SQL is already properly escaped.
	 */(,=){(!(,(,))){((,));}=(,(,,,,,,,,,,,,,,));/** @var wpdb $wpdb */;=()?:;"}";=();(([])[]){"}";"}";"}";}(!([])){"}";"}";"}";;[]=[];};[]=;([]){;[]=[];}(!([])){;[]=([]);}(!([])){;[]=([]);}([]){=[];(());=();=([]);"";[]=;}([]){=[];(());=();=([]);"";[]=;}([]){;}([]){;}(!([])){;[]=[];}(!([])){;(=;<;){[]=(,[]);}}(){([]){:=;;:=;;:=;;:=;;:=;;:::=;;}(([])){=;}{=;}"";([]>){;[]=[];[]=[];}}(,);}/**
	 * @param array $query
	 * @param string $query_type Whether to select or count the results. Default, select.
	 * @return string|array The IDs of actions matching the query
	 */(=(),=){/** @var wpdb $wpdb */;=(,);()?():();}/**
	 * Get a count of all actions in the store, grouped by status
	 *
	 * @return array
	 */(){=();=();=(,);(){{=();}(){// Ignore any post statuses that aren't for actions
;}((,)){[]=;}};}/**
	 * @param string $action_id
	 *
	 * @throws InvalidArgumentException
	 */(){=();(()()){(((,),));}(,);(,(,),,);();(,(,),);}(){=();(()()){(((,),));}(,);(,);}/**
	 * @param string $action_id
	 *
	 * @throws InvalidArgumentException
	 * @return ActionScheduler_DateTime The date the action is schedule to run, or the date that it ran.
	 */(){=();();}/**
	 * @param string $action_id
	 *
	 * @throws InvalidArgumentException
	 * @return ActionScheduler_DateTime The date the action is schedule to run, or the date that it ran.
	 */(){=();(()()){(((,),));}(){();}{();}}/**
	 * @param int      $max_actions
	 * @param DateTime $before_date Jobs must be schedule before this date. Defaults to now.
	 * @param array    $hooks       Claim only actions with a hook or hooks.
	 * @param string   $group       Claim only actions in the given group.
	 *
	 * @return ActionScheduler_ActionClaim
	 * @throws RuntimeException When there is an error staking a claim.
	 * @throws InvalidArgumentException When the given group is not valid.
	 */(=,=,=(),=){=();(,,,,);=();(,);}/**
	 * @return int
	 */(){;="}";=(,());();}(){=(().(,));(,,);// to fit in db field with 20 char limit
}/**
	 * @param string   $claim_id
	 * @param int      $limit
	 * @param DateTime $before_date Should use UTC timezone.
	 * @param array    $hooks       Claim only actions with a hook or hooks.
	 * @param string   $group       Claim only actions in the given group.
	 *
	 * @return int The number of actions that were claimed
	 * @throws RuntimeException When there is a database error.
	 * @throws InvalidArgumentException When the group is invalid.
	 */(,,=,=(),=){// Set up initial variables.
=?():;=!();=?(,,):();// If limiting by IDs and no posts found, then return early since we have nothing to update.
(()){;}/** @var wpdb $wpdb */;/*
		 * Build up custom query to update the affected posts. Parameters are built as a separate array
		 * to make it easier to identify where they are in the query.
		 *
		 * We can't use $wpdb->update() here because of the "ID IN ..." clause.
		 */="}";=(,(,),(),);// Build initial WHERE clause.
=;[]=;[]=;(!()){=(,(),);.(,).;=(,());}/*
		 * Add the IDs to the WHERE clause. IDs not escaped because they came directly from a prior DB query.
		 *
		 * If we're not limiting by IDs, then include the post_date_gmt clause.
		 */(){.(,).;}{;[]=();}// Add the ORDER BY clause and,ms limit.
=;[]=;// Run the query and gather results.
=(("}}}",));(){((,));};}/**
	 * Get IDs of actions within a certain group and up to a certain date/time.
	 *
	 * @param string   $group The group to use in finding actions.
	 * @param int      $limit The number of actions to retrieve.
	 * @param DateTime $date  DateTime object representing cutoff time for actions. Actions retrieved will be
	 *                        up to and including this DateTime.
	 *
	 * @return array IDs of actions in the appropriate group and before the appropriate time.
	 * @throws InvalidArgumentException When the group does not exist.
	 */(,,){// Ensure the group exists before continuing.
(!(,)){(((,),));}// Set up a query for post IDs to use later.
=();=(,,,,*,,,(,,,),(,(),,),((,,,,),),);();}/**
	 * @param string $claim_id
	 * @return array
	 */(){/** @var wpdb $wpdb */;="}";=(,(,));=();;}(){=(());(()){;// nothing to do
}=(,(,));/** @var wpdb $wpdb */;="}";=(,(()));=();(){/* translators: %s: claim ID */(((,),()));}}/**
	 * @param string $action_id
	 */(){/** @var wpdb $wpdb */;="}";=(,,);=();(){/* translators: %s: action ID */(((,),));}}(){/** @var wpdb $wpdb */;="}";=(,,,);=();(){/* translators: %s: action ID */(((,),));}}/**
	 * Return an action's claim ID, as stored in the post password column
	 *
	 * @param string $action_id
	 * @return mixed
	 */(){(,);}/**
	 * Return an action's status, as stored in the post status column
	 *
	 * @param string $action_id
	 * @return mixed
	 */(){=(,);(){((,));}();}(,){/** @var \wpdb $wpdb */;(("}}",,));}/**
	 * @param string $action_id
	 */(){/** @var wpdb $wpdb */;="}";=(,,(,),(),,);();}/**
	 * Record that an action was completed.
	 *
	 * @param int $action_id ID of the completed action.
	 * @throws InvalidArgumentException|RuntimeException
	 */(){=();(()()){(((,),));}(,(,),,);(,(,),,);=((,,),);(,(,),);(,(,),);(()){(());}}/**
	 * Mark action as migrated when there is an error deleting the action.
	 *
	 * @param int $action_id Action ID.
	 */(){((,));}/**
	 * Determine whether the post store can be migrated.
	 *
	 * @return bool
	 */(){;=();(()){=(,);=(("}",,));=?:;(,,);}?:;}/**
	 * InnoDB indexes have a maximum size of 767 bytes by default, which is only 191 characters with utf8mb4.
	 *
	 * Previously, AS wasn't concerned about args length, as we used the (unindex) post_content column. However,
	 * as we prepare to move to custom tables, and can use an indexed VARCHAR column instead, we want to warn
	 * developers of this impending requirement.
	 *
	 * @param ActionScheduler_Action $action
	 */(){{();}(){=((,),());(,,);}}/**
	 * @codeCoverageIgnore
	 */(){(,(,));=();();=();();=();();}}