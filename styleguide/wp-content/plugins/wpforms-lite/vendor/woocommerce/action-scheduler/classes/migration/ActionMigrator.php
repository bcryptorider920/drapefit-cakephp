;/**
 * Class ActionMigrator
 *
 * @package Action_Scheduler\Migration
 *
 * @since 3.0.0
 *
 * @codeCoverageIgnore
 */{/** var ActionScheduler_Store */;/** var ActionScheduler_Store */;/** var LogMigrator */;/**
	 * ActionMigrator constructor.
	 *
	 * @param ActionScheduler_Store $source_store Source store object.
	 * @param ActionScheduler_Store $destination_store Destination store object.
	 * @param LogMigrator           $log_migrator Log migrator object.
	 */(,,){=;=;=;}/**
	 * Migrate an action.
	 *
	 * @param int $source_action_id Action ID.
	 *
	 * @return int 0|new action ID
	 */(){{=();=();}(){=;=;}(()()!()()){// null action or empty status means the fetch operation failed or the action didn't exist
// null schedule means it's missing vital data
// delete it and move on
{();}(){// nothing to do, it didn't exist in the first place
}(,,,);;}{// Make sure the last attempt date is set correctly for completed and failed actions
=()?():;=(,,);}(){(,,,);;// could not save the action in the new store
}{(){:();;:();;}(,);();=();(!(,)){(((,),));}(,,,,);;}(){// could not delete from the old store
();(,,,,);(,,,,);;}}}