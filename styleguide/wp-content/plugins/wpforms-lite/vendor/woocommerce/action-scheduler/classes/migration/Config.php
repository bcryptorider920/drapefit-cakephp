;;;;/**
 * Class Config
 *
 * @package Action_Scheduler\Migration
 *
 * @since 3.0.0
 *
 * A config builder for the ActionScheduler\Migration\Runner class
 */{/** @var ActionScheduler_Store */;/** @var ActionScheduler_Logger */;/** @var ActionScheduler_Store */;/** @var ActionScheduler_Logger */;/** @var Progress bar */;/** @var bool */=;/**
	 * Config constructor.
	 */(){}/**
	 * Get the configured source store.
	 *
	 * @return ActionScheduler_Store
	 */(){(()){((,));};}/**
	 * Set the configured source store.
	 *
	 * @param ActionScheduler_Store $store Source store object.
	 */(){=;}/**
	 * Get the configured source loger.
	 *
	 * @return ActionScheduler_Logger
	 */(){(()){((,));};}/**
	 * Set the configured source logger.
	 *
	 * @param ActionScheduler_Logger $logger
	 */(){=;}/**
	 * Get the configured destination store.
	 *
	 * @return ActionScheduler_Store
	 */(){(()){((,));};}/**
	 * Set the configured destination store.
	 *
	 * @param ActionScheduler_Store $store
	 */(){=;}/**
	 * Get the configured destination logger.
	 *
	 * @return ActionScheduler_Logger
	 */(){(()){((,));};}/**
	 * Set the configured destination logger.
	 *
	 * @param ActionScheduler_Logger $logger
	 */(){=;}/**
	 * Get flag indicating whether it's a dry run.
	 *
	 * @return bool
	 */(){;}/**
	 * Set flag indicating whether it's a dry run.
	 *
	 * @param bool $dry_run
	 */(){=;}/**
	 * Get progress bar object.
	 *
	 * @return ActionScheduler\WPCLI\ProgressBar
	 */(){;}/**
	 * Set progress bar object.
	 *
	 * @param ActionScheduler\WPCLI\ProgressBar $progress_bar
	 */(){=;}}