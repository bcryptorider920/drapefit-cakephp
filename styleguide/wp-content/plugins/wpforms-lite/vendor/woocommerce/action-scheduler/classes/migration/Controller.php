;;;/**
 * Class Controller
 *
 * The main plugin/initialization class for migration to custom tables.
 *
 * @package Action_Scheduler\Migration
 *
 * @since 3.0.0
 *
 * @codeCoverageIgnore
 */{;/** @var Action_Scheduler\Migration\Scheduler */;/** @var string */;/** @var string */;/** @var bool */;/**
	 * Controller constructor.
	 *
	 * @param Scheduler $migration_scheduler Migration scheduler object.
	 */(){=;=;}/**
	 * Set the action store class name.
	 *
	 * @param string $class Classname of the store class.
	 *
	 * @return string
	 */(){(()){;}(){=;;}{;}}/**
	 * Set the action logger class name.
	 *
	 * @param string $class Classname of the logger class.
	 *
	 * @return string
	 */(){();(()){=;;}{;}}/**
	 * Get flag indicating whether a custom datastore is in use.
	 *
	 * @return bool
	 */(){;}/**
	 * Set up the background migration process
	 *
	 * @return void
	 */(){(()()){;}();}/**
	 * Get the default migration config object
	 *
	 * @return ActionScheduler\Migration\Config
	 */(){=;(!){=?():();=?():();=();();();(());(());(()){((,));}}(,);}/**
	 * Hook dashboard migration notice.
	 */(){(!()()){;}(,(,),,);}/**
	 * Show a dashboard notice that migration is in progress.
	 */(){(,(,));}/**
	 * Add store classes. Hook migration.
	 */(){(,(,),,);(,(,),,);(,(,));(,(,),,);// Action Scheduler may be displayed as a Tools screen or WooCommerce > Status administration screen
(,(,),,);(,(,),,);}/**
	 * Possibly hook the migration scheduler action.
	 *
	 * @author Jeremy Pry
	 */(){(!()()){;}();}/**
	 * Allow datastores to enable migration to AS tables.
	 */(){(!()){;}(){=(,);}(!());}/**
	 * Proceed with the migration if the dependencies have been met.
	 */(){(()){()();}}/**
	 * Singleton factory.
	 */(){(!()){=(());};}}