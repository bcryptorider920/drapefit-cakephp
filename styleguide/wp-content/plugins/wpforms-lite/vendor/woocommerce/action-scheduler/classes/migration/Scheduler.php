;/**
 * Class Scheduler
 *
 * @package Action_Scheduler\WP_CLI
 *
 * @since 3.0.0
 *
 * @codeCoverageIgnore
 */{/** Migration action hook. */=;/** Migration action group. */=;/**
	 * Set up the callback for the scheduled job.
	 */(){(,(,),,);}/**
	 * Remove the callback for the scheduled job.
	 */(){(,(,),);}/**
	 * The migration callback.
	 */(){=();=(());(){();}{(()+());}}/**
	 * Mark the migration complete.
	 */(){();();();}/**
	 * Get a flag indicating whether the migration is scheduled.
	 *
	 * @return bool Whether there is a pending action in the store to handle the migration
	 */(){=();!();}/**
	 * Schedule the migration.
	 *
	 * @param int $when Optional timestamp to run the next migration batch. Defaults to now.
	 *
	 * @return string The action ID
	 */(=){=();(!()){;}(()){=();}(,,(),);}/**
	 * Remove the scheduled migration action.
	 */(){(,,);}/**
	 * Get migration batch schedule interval.
	 *
	 * @return int Seconds between migration runs. Defaults to 0 seconds to allow chaining migration via Async Runners.
	 */(){(,);}/**
	 * Get migration batch size.
	 *
	 * @return int Number of actions to migrate in each batch. Defaults to 250.
	 */(){(,);}/**
	 * Get migration runner object.
	 *
	 * @return Runner
	 */(){=()();();}}