/**
 * Day of week field.  Allows: * / , - ? L #
 *
 * Days of the week can be represented as a number 0-7 (0|7 = Sunday)
 * or as a three letter string: SUN, MON, TUE, WED, THU, FRI, SAT.
 *
 * 'L' stands for "last". It allows you to specify constructs such as
 * "the last Friday" of a given month.
 *
 * '#' is allowed for the day-of-week field, and must be followed by a
 * number between one and five. It allows you to specify constructs such as
 * "the second Friday" of a given month.
 *
 * @author Michael Dowling <mtdowling@gmail.com>
 */{/**
     * {@inheritdoc}
     */(,){(){;}// Convert text day of the week values to integers
=((,,,,,,),(,),);=();=();=();// Find out if this is the last specific weekday of the month
((,)){=(,,(,,(,)));=;(,,);(()){(,,);}();}// Handle # hash tokens
((,)){(,)=(,);// Validate the hash fields
(<>){("}");}(>){();}// The current weekday must match the targeted weekday to proceed
(()){;}=;(,,);=;=;(<+){(()){(){;}}(,,);}();}// Handle day of the week values
((,)){=(,);([]){[]=;}([]){[]=;}=(,);}// Test to see which Sunday to use -- 0 == 7 == Sunday
=(,())?:;=();(,);}/**
     * {@inheritdoc}
     */(,=){(){();(,,);}{();(,,);};}/**
     * {@inheritdoc}
     */(){(,);}}