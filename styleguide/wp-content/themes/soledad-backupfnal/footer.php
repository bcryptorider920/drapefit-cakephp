<?php
/**
 * This is footer template of Soledad theme
 *
 * @package Wordpress
 * @since   1.0
 */

$penci_hide_footer = '';
$hide_fwidget = get_theme_mod( 'penci_footer_widget_area' );
$footer_layout = get_theme_mod( 'penci_footer_widget_area_layout' ) ? get_theme_mod( 'penci_footer_widget_area_layout' ) : 'style-1';
$penci_footer_width = get_theme_mod( 'penci_footer_width' );

if ( is_page() ) {
	$penci_hide_footer = get_post_meta( get_the_ID(), 'penci_page_hide_footer', true );

	$pmeta_page_footer = get_post_meta( get_the_ID(), 'penci_pmeta_page_footer', true );
	if ( isset( $pmeta_page_footer['penci_footer_style'] ) && $pmeta_page_footer['penci_footer_style'] ) {
		$footer_layout =  $pmeta_page_footer['penci_footer_style'];
	}

	if ( isset( $pmeta_page_footer['penci_hide_fwidget'] ) ) {
		if ( 'yes' == $pmeta_page_footer['penci_hide_fwidget'] ) {
			$hide_fwidget = true;
		} elseif ( 'no' == $pmeta_page_footer['penci_hide_fwidget'] ) {
			$hide_fwidget = false;
		}
	}

	if ( isset( $pmeta_page_footer['penci_footer_width'] ) && $pmeta_page_footer['penci_footer_width'] ) {
		$penci_footer_width =  $pmeta_page_footer['penci_footer_width'];
	}
}

$footer_logo_url = esc_url( home_url('/') );
if( get_theme_mod('penci_custom_url_logo_footer') ) {
	$footer_logo_url = get_theme_mod('penci_custom_url_logo_footer');
}
?>
<!-- END CONTAINER -->
</div>

<?php if( ! $penci_hide_footer ): ?>
<div class="clear-footer"></div>

<?php if ( get_theme_mod( 'penci_footer_adsense' ) ): echo '<div class="container penci-google-adsense penci-google-adsense-footer">'. do_shortcode( get_theme_mod( 'penci_footer_adsense' ) ) .'</div>'; endif; ?>

<?php if ( ! $hide_fwidget ) :
	if( ( in_array( $footer_layout, array( 'style-2', 'style-3', 'style-8', 'style-9', 'style-10' ) ) && ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) ) ) || ( in_array( $footer_layout, array( 'style-1', 'style-5', 'style-6', 'style-7' ) ) && ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) ) || ( $footer_layout == 'style-4' && ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' ) ) ) ):
		?>
		<div id="widget-area"<?php if( get_theme_mod( 'penci_footer_widget_bg_image' ) ): echo ' class="penci-lazy" data-src="'. get_theme_mod( 'penci_footer_widget_bg_image' ) .'"'; endif; ?>>
			<div class="container<?php echo esc_attr( $penci_footer_width ? ' container-' . $penci_footer_width : '' ) ?>">
				<?php if( in_array( $footer_layout, array( 'style-1', 'style-5', 'style-6', 'style-7' ) ) ){ ?>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-1' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-2' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?> last">
						<?php dynamic_sidebar( 'footer-3' ); ?>
					</div>
				<?php } elseif( in_array( $footer_layout, array( 'style-2', 'style-3', 'style-8', 'style-9', 'style-10' ) ) ) { ?>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-1' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?> last">
						<?php dynamic_sidebar( 'footer-2' ); ?>
					</div>
				<?php } elseif( $footer_layout == 'style-4' ) { ?>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-1' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-2' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?>">
						<?php dynamic_sidebar( 'footer-3' ); ?>
					</div>
					<div class="footer-widget-wrapper footer-widget-<?php echo $footer_layout; ?> last">
						<?php dynamic_sidebar( 'footer-4' ); ?>
					</div>
				<?php } ?>
			</div>
		</div>
<?php endif;
endif; /* End check if disable footer widget area */  ?>

<?php if ( is_active_sidebar( 'footer-instagram' ) ): ?>
	<div class="footer-instagram footer-instagram-html<?php if( get_theme_mod('penci_footer_insta_title_overlay') ): echo ' penci-insta-title-overlay'; endif; ?>">
		<?php dynamic_sidebar( 'footer-instagram' ); ?>
	</div>
<?php endif; ?>

<?php
/**
 * Display sign-up mailchimp form on the header
 * Check if 'footer-signup-form' has widget, if true display it
 *
 * @since 4.0
 */
if ( is_active_sidebar( 'footer-signup-form' ) ): ?>
	<div class="footer-subscribe">
		<?php dynamic_sidebar( 'footer-signup-form' ); ?>
	</div>
<?php endif; ?>

<footer id="footer-section" class="penci-footer-social-media penci-lazy<?php if( get_theme_mod( 'penci_footer_social_around' ) ): echo ' footer-social-remove-circle'; endif; if( get_theme_mod( 'penci_footer_social_drop_line' ) ): echo ' footer-social-drop-line'; endif; if( get_theme_mod( 'penci_footer_disable_radius_social' ) ): echo ' footer-social-remove-radius'; endif; if( get_theme_mod( 'penci_footer_social_remove_text' ) ): echo ' footer-social-remove-text'; endif; ?>"<?php if( get_theme_mod( 'penci_footer_copyright_bg_image' ) ): echo ' data-src="'. get_theme_mod( 'penci_footer_copyright_bg_image' ) .'"'; endif; ?> <?php if( ! get_theme_mod('penci_schema_wpfoot') ): ?>itemscope itemtype="https://schema.org/WPFooter"<?php endif; ?>>
    
    <?php include('footer-extra.php'); ?>
    

	
</footer>

<?php endif; //Hide footer  ?>
</div><!-- End .wrapper-boxed -->
<?php
if ( get_theme_mod( 'penci_menu_hbg_show' ) && ! get_theme_mod( 'penci_vertical_nav_show' ) ) {
	get_template_part( 'template-parts/menu-hamburger' );
}
?>
<?php
/* Get menu related posts popup */
if( is_singular( 'post' ) && get_theme_mod('penci_related_post_popup') ):
get_template_part( 'inc/templates/related_posts-popup' );
endif; ?>

<div id="fb-root"></div>
<?php
$gprd_desc       = penci_get_setting( 'penci_gprd_desc' );
$gprd_accept     = penci_get_setting( 'penci_gprd_btn_accept' );
$gprd_rmore      = penci_get_setting( 'penci_gprd_rmore' );
$gprd_rmore_link = penci_get_setting( 'penci_gprd_rmore_link' );
$penci_gprd_text = penci_get_setting( 'penci_gprd_policy_text' );
if ( get_theme_mod( 'penci_enable_cookie_law' ) && $gprd_desc && $gprd_accept ) :
	?>
	<div class="penci-wrap-gprd-law penci-wrap-gprd-law-close penci-close-all">
		<div class="penci-gprd-law">
			<p>
				<?php if ( $gprd_desc ): echo $gprd_desc; endif; ?>
				<?php if ( $gprd_accept ): echo '<a class="penci-gprd-accept" href="#">' . $gprd_accept . '</a>'; endif; ?>
				<?php if ( $gprd_rmore ): echo '<a class="penci-gprd-more" href="' . $gprd_rmore_link . '">' . $gprd_rmore . '</a>'; endif; ?>
			</p>
		</div>
		<?php if ( ! get_theme_mod( 'penci_show_cookie_law' ) ): ?>
			<a class="penci-gdrd-show" href="#"><?php echo $penci_gprd_text; ?></a>
		<?php endif; ?>
	</div>

<?php endif; ?>
<?php wp_footer(); ?>

<?php if( get_theme_mod('penci_footer_analytics') ):
echo get_theme_mod('penci_footer_analytics');
endif; ?>

</body>
</html>