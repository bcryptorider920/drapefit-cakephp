        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/extra/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/extra/style.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap-datetimepicker.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/design.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/responsive-accordion.css" type="text/css">
        <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/kidstyle.css" type="text/css">
        <script src='<?php bloginfo('template_directory'); ?>/js/jquery.min.js'></script>
        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/moment-with-locales.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/assets/js/bootstrap-datetimepicker.js"></script>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PH7WXHZ');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->