/**
 * The main class of the plugin which handles show, edit, save meta boxes for settings page
 * @package    Meta Box
 * @subpackage MB Settings Page
 * @author     Tran Ngoc Tuan Anh <rilwis@gmail.com>
 *//**
 * Class for handling meta boxes for settings page
 */{/**
	 * @var string Current settings page ID. It will be set when page loads.
	 */;/**
	 * Constructor
	 * Call parent constructor and add specific hooks
	 *
	 * @param array $meta_box
	 */(){();[]=[];(,(,));(,(,));(,(,));}/**
	 * Add hooks for current settings page
	 *
	 * @param array $page_args Settings page arguments
	 */(){=;// Add meta boxes
([]){([]){([],[],(,),,// Current page
[],[]);}}// Filter field meta (saved value)
(,(,),,);// Filter saved value
(,(,));}/**
	 * Get field meta value
	 *
	 * @param mixed $meta Meta value
	 * @param array $field Field parameters
	 *
	 * @return mixed
	 */(,){(!()){;}=([]);=();(()){=();}=;=([[]])?[[]]:[];// Escape attributes
=(,,);// Make sure meta value is an array for clonable and multiple fields
([][]){(()!()){/**
				 * Note: if field is clonable, $meta must be an array with values
				 * so that the foreach loop in self::show() runs properly
				 * @see self::show()
				 */=[]?():();}};}/**
	 * Save settings fields
	 *
	 * @param array $data Array of settings options
	 *
	 * @return array
	 */(){=(,"[]}");(!(,"[]}")){;}(){=[];=([])?[]:;=([])?[]:([]?():);/**
			 * WordPress automatically adds slashes to $_POST. This function remove slashes.
			 * Note: for post meta we don't need to do this because WordPress does automatically in update_post_meta().
			 */=();// Allow field class change the value
=(,,,,);=(,,,);[]=;};}/**
	 * Check if settings page is saved.
	 * @return bool
	 */(){=([]);=();(){(([[]])){;}};}/**
	 * Check if we're on the right edit screen.
	 *
	 * @param WP_Screen $screen Screen object. Optional. Use current screen object by default.
	 *
	 * @return bool
	 */(=){([],[]);}}