/**
 * The main class of the plugin which create settings page and setup meta boxes placeholder.
 *
 * @package    Meta Box
 * @subpackage MB Settings Page
 * @author     Tran Ngoc Tuan Anh <rilwis@gmail.com>
 *//**
 * Class for creating settings page and setup meta boxes placeholder
 */{/**
	 * Settings page arguments.
	 *
	 * @var array
	 */;/**
	 * Page hook. Will be set when add menu page.
	 *
	 * @var string
	 */;/**
	 * Constructor.
	 *
	 * @param array $args Page options like ID page_title, menu_title, capability...
	 */(=()){=();// Add hooks.
(,(,));(,(,));}/**
	 * Normalize settings page arguments.
	 *
	 * @param array $args Settings page arguments.
	 *
	 * @return array
	 */(){=(,(,// Page ID. Required. Will be used as slug in URL and option name (if missed).
,// Option name. Optional. Takes 'id' if missed.
,// Menu title. Optional. Takes 'page_title' if missed.
,// Page title. Optional. Takes 'menu_title' if missed.
,// Required capability to visit.
,// Icon URL. @see add_menu_page().
,// Menu position. @see add_menu_page().
,// ID of parent page. Optional.
,// Submenu title. Optional.
(),,,(),));// Setup optional parameters.
(![]){[]=[];}(![]){[]=[];}(![]){[]=[];};}/**
	 * Add top level menu or sub-menu. Depend on page options
	 */(){// Add top level menu.
(![]){=([],[],[],[],(,),[],[]);// If this menu has a default sub-menu.
([]){([],[],[],[],[],(,));}}// Add sub-menu.
{=([],[],[],[],[],(,));}// Enqueue scripts and styles.
("}",(,));// Load action.
("}",(,));}/**
	 * Output the main admin page
	 */(){=[]?:"[]}";([]);([]):([]):,();();;;?.().:;// Nonce for saving meta boxes status (collapsed/expanded) and order.
(,,);(,,);([]);([]>):(,,);;(,,);(,,);((,),,,);();}/**
	 * Enqueue scripts and styles for settings page.
	 */(){(,)=((()));(,.,,);// For meta boxes.
();();();// Enqueue settings page script and style.
(,.,(),,);(,,(,([]),));}/**
	 * Register the meta boxes via a custom hook.
	 */(){/**
		 * Custom hook runs when current page loads. Use this to add meta boxes and filters.
		 *
		 * @param array $page_args The page arguments
		 */(,);// Save settings when submit.
();// Show updated message.
(,(,));// Add help tabs.
();}/**
	 * Save settings when submit.
	 */(){// @codingStandardsIgnoreLine
(([])){;}=([],());=(,,[]);([],);([],,(,),);}/**
	 * Display notices.
	 * Use add_settings_error() to add notices.
	 */(){([]);}/**
	 * Add help tabs.
	 */(){(![]!([])){;}=();([]){// Auto generate help tab ID if missed.
(([])){[]="[]}";}();}}/**
	 * Show tab id of meta box.
	 *
	 * @param RW_Meta_Box $obj Meta Box instance.
	 */(){(!([])){,([]),;}}}