/**
 * The color field which uses WordPress color picker to select a color.
 *
 * @package Meta Box
 *//**
 * Color field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){=();=[];=();(,.,(),);([]){(,.,(),,);=();}(,.,,,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=(,(,(),));[]=([],(,,,));=();;}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field Field parameters.
	 * @param mixed $value Meta value.
	 * @return array
	 */(,=){=(,);=(,(([]),));[]=;([]){[]=;};}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value The value.
	 * @return string
	 */(,){(,);}}