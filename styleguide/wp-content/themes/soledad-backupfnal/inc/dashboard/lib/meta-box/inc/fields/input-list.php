/**
 * The input list field which displays choices in a list of inputs.
 *
 * @package Meta Box
 *//**
 * Input list field class.
 */{/**
	 * Enqueue scripts and styles
	 */(){(,.,(),);(,.,(),,);}/**
	 * Walk options.
	 *
	 * @param array $field     Field parameters.
	 * @param mixed $options   Select options.
	 * @param mixed $db_fields Database fields to use in the output.
	 * @param mixed $meta      Meta value.
	 *
	 * @return string
	 */(,,,){=(,,);=();(,[]?:,[]?:);(,[]?-:);;;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=[]?():;=();=();=(,(,,,));[]=[]?[]:;[]=![]!([])?:[];;}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field Field parameters.
	 * @param mixed $value Meta value.
	 *
	 * @return array
	 */(,=){=(,);[]=;[]=[]?:;[]=;;}/**
	 * Get html for select all|none for multiple checkbox.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){([][]){(,[],(,));};}}