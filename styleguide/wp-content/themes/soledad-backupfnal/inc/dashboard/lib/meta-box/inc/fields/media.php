/**
 * Media field class which users WordPress media popup to upload and select files.
 *
 * @package Meta Box
 *//**
 * The media field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){();(!()){(,(),(),,);}(,.,(),);(,.,(,,,),,);(,,((,(,,)),(,(,,)),(,(,,)),(,(,,)),(,(,,)),(,(,,)),(,,),(),(),(,(,,)),(,(,,)),(,(,,)),));}/**
	 * Add actions.
	 */(){=();=();(,((),));}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=;=(,);=(,);=(,(),(([])));;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();=(,((),,,,,(),));[]=([],([],[],[]?:,[],));[]=;;}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field Field parameters.
	 * @param mixed $value Meta value.
	 *
	 * @return array
	 */(,=){=(,);[]=;[]=[]?(,,[]):[];[]=;[]=;;}/**
	 * Get supported mime extensions.
	 *
	 * @return array
	 */(){=();=();(){=(,);[]=;=(,);(([[]])){[[]]=();}[[]]=([[]],);[[].]=[[]];};}/**
	 * Get meta values to save.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return array|mixed
	 */(,,,){=!()()?(,):;(,);(());}/**
	 * Save meta value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 */(,,,){=[];(,[]);(,(),,);}/**
	 * Template for media item.
	 */(){.;}}