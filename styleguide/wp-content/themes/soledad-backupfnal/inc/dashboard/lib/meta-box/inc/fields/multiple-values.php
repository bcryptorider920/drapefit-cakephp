/**
 * This class implements common methods used in fields which have multiple values
 * like checkbox list, autocomplete, etc.
 *
 * The difference when handling actions for these fields are the way they get/set
 * meta value. Briefly:
 * - If field is cloneable, value is saved as a single entry in the database
 * - Otherwise value is saved as multiple entries
 *
 * @package Meta Box
 *//**
 * Multiple values field class.
 */{/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();[]=;[]=[];(![]){[];};}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value The value.
	 * @return string
	 */(,){[][];}}