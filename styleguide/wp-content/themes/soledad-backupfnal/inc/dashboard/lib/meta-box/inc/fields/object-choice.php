/**
 * The object choice class which allows users to select specific objects in WordPress.
 *
 * @package Meta Box
 *//**
 * Abstract field to select an object: post, user, taxonomy, etc.
 */{/**
	 * Get field HTML
	 *
	 * @param array $field     Field parameters.
	 * @param mixed $options   Select options.
	 * @param mixed $db_fields Database fields to use in the output.
	 * @param mixed $meta      Meta value.
	 * @return string
	 */(,,,){(((),),,,,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();=(,(,(),,));([]){[]=;[]=;}([]){[]=;}([]){[]=;}(((),),);}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field Field parameters.
	 * @param mixed $value Meta value.
	 *
	 * @return array
	 */(,=){=(((),),,);([]){[];};}/**
	 * Get field names of object to be used by walker.
	 *
	 * @return array
	 */(){(,,,);}/**
	 * Enqueue scripts and styles.
	 */(){();();();();}/**
	 * Get correct rendering class for the field.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){(([],(,),)){;}(([],));}}