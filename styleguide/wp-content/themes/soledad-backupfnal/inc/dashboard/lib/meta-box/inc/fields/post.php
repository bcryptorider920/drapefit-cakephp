/**
 * The post field which allows users to select existing posts.
 *
 * @package Meta Box
 *//**
 * Post field class.
 */{/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){// Set default field args.
=();=(,(,,));(!([][])){[][]=[];}/**
		 * Set default placeholder.
		 * - If multiple post types: show 'Select a post'.
		 * - If single post type: show 'Select a %post_type_name%'.
		 */(([])){[]=(,);(([][])([][])){=([][]);// Translators: %s is the post type singular label.
[]=((,),);}}// Set parent option, which will change field name to `parent_id` to save as post parent.
([]){[]=;[]=;}// Set default query args.
[]=([],(,-,));;}/**
	 * Get field names of object to be used by walker.
	 *
	 * @return array
	 */(){(,,,);}/**
	 * Get meta value.
	 * If field is cloneable, value is saved as a single entry in DB.
	 * Otherwise value is saved as multiple entries (for backward compatibility).
	 *
	 * @see "save" method for better understanding
	 *
	 * @param int   $post_id Post ID.
	 * @param bool  $saved   Is the meta box saved.
	 * @param array $field   Field parameters.
	 *
	 * @return mixed
	 */(,,){(([])[]){=();;}(,,);}/**
	 * Get options for walker.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=([]);()?:();}/**
	 * Get option label.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value Option value.
	 *
	 * @return string
	 */(,){(,(()),((,,)),());}}