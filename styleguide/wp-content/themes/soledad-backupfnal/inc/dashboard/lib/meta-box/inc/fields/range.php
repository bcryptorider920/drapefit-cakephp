/**
 * The HTML5 range field.
 *
 * @package Meta Box
 *//**
 * HTML5 range field class.
 */{/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){=(,);(,);;}/**
	 * Enqueue styles.
	 */(){(,.,(),);(,.,(),,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=(,(,));=();;}/**
	 * Ensure number in range.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return int
	 */(,,,){=();=([]);=([]);(<){;}(>){;};}}