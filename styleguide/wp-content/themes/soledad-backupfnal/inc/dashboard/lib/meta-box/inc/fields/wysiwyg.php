/**
 * The WYSIWYG (editor) field.
 *
 * @package Meta Box
 *//**
 * WYSIWYG (editor) field class.
 */{/**
	 * Array of cloneable editors.
	 *
	 * @var array
	 */=();/**
	 * Enqueue scripts and styles.
	 */(){(,.,(),);(,.,(),,);}/**
	 * Change field value on save.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 * @return string
	 */(,,,){[]?:();}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){// Using output buffering because wp_editor() echos directly.
();[][]=[];=();// Use new wp_editor() since WP 3.3.
(,[],[]);();}/**
	 * Escape meta for field output.
	 *
	 * @param mixed $meta Meta value.
	 * @return mixed
	 */(){;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();=(,(,(),));[]=([],(,,// Use default WordPress full screen UI.
));// Keep the filter to be compatible with previous versions.
[]=(,[]);;}}