/**
 * Load plugin's files with check for installing it as a standalone plugin or
 * a module of a theme / plugin. If standalone plugin is already installed, it
 * will take higher priority.
 *
 * @package Meta Box
 *//**
 * Plugin loader class.
 *
 * @package Meta Box
 */{/**
	 * Define plugin constants.
	 */(){// Script version, used to add version for scripts and styles.
(,);(,)=((()));// Plugin URLs, for fast enqueuing scripts and styles.
(,);(,(.));(,(.));// Plugin paths, for including files.
(,);(,(.));}/**
	 * Get plugin base path and URL.
	 * The method is static and can be used in extensions.
	 *
	 * @link http://www.deluxeblogtips.com/2013/07/get-url-of-php-file-in-wordpress.html
	 * @param string $path Base folder path.
	 * @return array Path and URL.
	 */(=){// Plugin base path.
=(());=(((())));// Default URL.
=(,..().);// Included into themes.
((,())(,())(,)){=((()));=(,,);}=();=();(,);}/**
	 * Bootstrap the plugin.
	 */(){();// Register autoload for classes.
.;=();(,);(,);(.,,);(.,);(.,,);(.,,);();// Plugin core.
=();();// Validation module.
();=();();=();();// WPML Compatibility.
=();();// Public functions.
.;}}