/**
 * A registry for storing all meta boxes.
 *
 * @link    https://designpatternsphp.readthedocs.io/en/latest/Structural/Registry/README.html
 * @package Meta Box
 *//**
 * Meta box registry class.
 */{/**
	 * Internal data storage.
	 *
	 * @var array
	 */=();/**
	 * Add a meta box to the registry.
	 *
	 * @param RW_Meta_Box $meta_box Meta box instance.
	 */(){[]=;}/**
	 * Retrieve a meta box by id.
	 *
	 * @param string $id Meta box id.
	 *
	 * @return RW_Meta_Box|bool False or meta box object.
	 */(){([])?[]:;}/**
	 * Retrieve all meta boxes.
	 *
	 * @return array
	 */(){;}}