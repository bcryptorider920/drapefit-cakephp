<style>
.three-person {
        float: left;
        width: 100%;
        /* text-align: left; */
        /* border-bottom: 2px solid #232f3e; */
        margin-bottom: 5px;
        text-align: center;
    }
    .three-person ul{
        float: left;
        width:100%;
        border-bottom: 2px solid #232f3e;
    }

    .three-person ul li {
        display: inline-block;
        float: none;
    }
    .three-person ul li:first-child{
        border-left: none;
    }
    .three-person .nav.nav-tabs li a {
        display: inline-block;
        color: #232f3e !important;
        text-transform: uppercase;
        font-family: "Amazon Ember", Arial, sans-serif;
        font-weight: bold;
        font-size: 16px;
        padding: 14px 45px;
        position: relative;
        border: none !important;
    }
    .three-person .nav.nav-tabs li a:hover,.three-person .nav.nav-tabs li.active a{
        color: #ef7600 !important;
        background: none !important;
    }
    .three-person .nav.nav-tabs li a:hover:after,.three-person .nav.nav-tabs li.active a:after
    {
        height: 2px;
        background: #ef7600;
        content: "";
        position: absolute;
        left: 0;
        bottom: -1px;
        width:100%;
    }
	
	@media only screen and (max-width:1199px) {
      .three-person ul li {
        padding: 0 10px;
       }
    }
    @media only screen and (max-width:991px) {
        .three-person ul li {
            padding: 0 15px;
        }
        .stories{
            padding-left: 0;
        }
    }
    @media only screen and (max-width:767px) {
        .three-person .nav.nav-tabs li {
            margin-bottom: 0;
        }
        .three-person .nav.nav-tabs li a {
    width: 100%;
}


</style>
<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];


?>

<div class="three-person">
        
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <ul class="nav nav-tabs">
                                                    <!--<li class="active" ><a data-toggle="tab" href="#tab8"><i class="fa fa-male"></i> Men</a></li>

                                                        <li ><a data-toggle="tab" href="#tab9"><i class="fa fa-male"></i> women</a></li>

                                                        <li ><a data-toggle="tab" href="#tab10"><i class="fa fa-male"></i> Kids</a></li>

                                                        <li ><a data-toggle="tab" href="#tab11"><i class="fa fa-male"></i> General Information</a></li>-->
                                                        <li <?php if (strpos($url,'blog/men') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/drapefit/blog/men" aria-expanded="true"><i class="fa fa-male"></i> Men</a></li>

                                                        <li <?php if (strpos($url,'blog/women') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/drapefit/blog/women"><i class="fa fa-female"></i> women</a></li>

                                                        <li <?php if (strpos($url,'kids') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/drapefit/blog/kids"><i class="fa fa-child"></i> Kids</a></li>

                                                        <li <?php if (strpos($url,'general') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/drapefit/blog/general"><i class="fa fa-copy"></i> General</a></li>

                                                </ul>
                </div>
            
    </div>