<!DOCTYPE html>
<html>
    
<!-- Mirrored from www.drapefit.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Mar 2020 14:27:25 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta https-equiv="Content-Type" content="text/html;charset=UTF-8">
        <title>Get Men’s Cloth, Women’s Cloth, and Kids Cloth at Great Deals - Drape Fit</title>
        <meta name="keywords" content="Monthly Clothing Subscription Boxes For kids,Monthly Subscription Boxes for Kids,subscription boxes for children,subscription boxes for boys,monthly subscriptions for kids,Monthly Subscription Boxes for Men,men&#039;s monthly box subscription,men&#039;s monthly fashion box,men&#039;s clothing subscription box,Men&#039;s Clothing Subscription,clothing subscription box for men,best men&#039;s fashion subscription boxes,cool subscription boxes for men,Monthly Subscription Boxes for Women,Subscription Boxes for women,Fashion Subscription Boxes for Women,Fashion Subscription Boxes for Women,Women&#039;s Clothing Monthly Subscription Box,best subscription boxes for women,women&#039;s monthly clothing subscription,women&#039;s clothing subscription service,womens clothes delivered monthly,women&#039;s personal stylist clothing subscription"/>        <meta name="description" content="Now upgrade your Personal style with the best fashion site in the world! Get the latest fashion clothing for men, women and kids that fit with your age, size, and budget. Enjoy free shipping worldwide"/>        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.css" type="text/css">
        <link rel="stylesheet" href="assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="assets/css/design.css" type="text/css">
        <link rel="stylesheet" href="assets/css/responsive-accordion.css" type="text/css">
        <link rel="stylesheet" href="assets/css/kidstyle.css" type="text/css">
        <script src='js/jquery.min.js'></script>
        <script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/moment-with-locales.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap-datetimepicker.js"></script>
        <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PH7WXHZ');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
    </head>

 

  
        

        <section class="footer">
    <div class="container"> 
        <div class="row">
            <div class="col-md-3">
                <div class="footer-logo">
                                                <a href="index.html"> 
                                                        <img src="img/logo.png" alt="drapfit logo">
                        </a>
                        </br>
                        <p>WE DO BEST FIT</p>
                        <div class="footer-social">
                            <a class="us-country" href="#">United States</a>
                    <h3>Follow Us :</h3>
                    <ul>
        <li><a href="https://www.facebook.com/drapefitinc"><i class="fa fa-facebook-f"></i></a></li>
        <li><a href="https://twitter.com/drapefitinc"><i class="fa fa-twitter"></i> </a></li>
        <li><a href="https://www.instagram.com/drapefitinc/"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.pinterest.com/drapefitinc/"><i class="fa fa-pinterest"></i></a></li>
    </ul>                </div> 
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-links">
                    <h3>About Us</h3>
                    <ul>
                        <!-- <a href="footer.ctp"></a> -->
                        <li><a href="who-we-are.html">Who We Are</a></li>
                        <li><a href="our-mission.html">Our Mission</a></li>
                        <li><a href="our-stylist.html">Our Stylist</a></li>
                        <!-- <li><a href="https://www.drapefit.com/news">News</a></li>
                        <li><a href="https://www.drapefit.com/investors">Investors</a></li> -->
                        <li><a href="executive-team.html">Executive Team</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-links">
                    <h3>Service</h3>
                    <ul>
                                                    
                                                    
                                                                                <li><a href="men.html">Men</a></li>
                            <li><a href="men/big-tall.html">Big & Tall</a></li>
                            <li><a href="women.html">Women</a></li>
                            <li><a href="women/plus-size.html">Plus size Fits</a></li>
                            <li><a href="women/maternity.html">Maternity Fits</a></li>
                            <li><a href="women/petite.html">Petite</a></li>
                            <li><a href="women/women-jeans.html">Women Jeans</a></li>
                            <li><a href="women/women-business.html">Women Business Fits</a></li>
                            <li><a href="kids.html">Kids</a></li>
                                            </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer-links">
                    <h3>The Company</h3>
                    <ul>
                        <li><a href="style-blog.html">Style Blog</a></li>
                        <li><a href="news.html">News</a></li>
                        <li><a href="investors.html">Investors Relation</a></li>
                        <li><a href="careers.html">Careers</a></li>
<!--                         <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li> -->
                                            </ul>
                </div>
            </div>
            <div class="col-md-2">
               <div class="footer-links">
                    <h3>Recommended</h3>
                    <ul>
                        <li><a href="how-it-works.html">How Drape Fit Works</a></li>                
                        <li><a href="box-pricing.html">FIT Box Pricing</a></li>
                        <li><a href="personal-stylist.html">Personal Stylist</a></li>
                    </ul>
                </div>           
            </div>   
            <div class="col-md-2">
                <div class="footer-links">
                    <h3>Customer Care</h3>
                    <ul>
                        <li><a href="faq.html">FAQ</a></li>                
                        <li><a href="gifts.html">Gift Card</a></li>
                        <li><a href="return-exchange.html">Return & Exchange</a></li>
                        <li><a href="track-order.html">Track Order</a></li>
                        <li><a href="help-center.html">Help Center</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                        <!-- <li><a href="https://www.drapefit.com/privacy-policy">Track Order</a></li> -->
                    </ul>
                </div>
            </div>
                  
        </div>
    </div>
</section>
    <script type="text/javascript" id="hs-script-loader" async defer src="http://js.hs-scripts.com/6931110.js"></script>
        <section class="copy-right">
    <div class="container">
        <div class="row">
            <div class="col-md-6">         
                <span>@2020 DRAPE FIT INC. ALL RIGHTS RESERVED.</span>                        
            </div>
            <div class="col-md-6">
            <ul>
                <li><a href="terms-conditions.html">Terms & Conditions |</a><a href="privacy-policy.html"> Privacy Policy |</a><a href="map.html"> Sitemap</a></li>
            </ul> 
            </div>
        </div>
    </div>
    <a id="button"></a>
                    
                <!--<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6939712.js"></script>-->

                                
</section>
<script>
    $('.live-button').click(function () {
        var pageurl = 'index.html';
        $.ajax({
            type: 'POST',
            url: pageurl + 'users/chat_button_box_active',
            success: function (response) {
                if (response.status == 'success') {
                    $('#chat-button').hide();
                    $('#chat-div').show();



                }
            },

            dataType: 'json'
        });
    });

//    $(document).ready(function () {
//        var pageurl = 'https://www.drapefit.com/';
//        $.ajax({
//            type: 'POST',
//            url: pageurl + 'users/chat_check_auth',
//            success: function (response) {
//                if (response.status == 'success') {
//                    if (response.value == 1) {
//                        $('#btn1').hide();
//                    } else {
//                        $('#btn1').show();
//                    }
//
//                }
//            },
//
//            dataType: 'json'
//        });
//
//    });

    var btn = $('#button');

        $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
            btn.addClass('show');
          } else {
            btn.removeClass('show');
          }
        });
        
        btn.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({scrollTop:0}, '300');
    });
</script>
        <input type="hidden" id="pageurl" value="https://www.drapefit.com/"/>
    </body>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker4').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker5').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
  function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.src = "https://cdn.collectiveray.com/defer.js";
    document.body.appendChild(element);
  }
  if (window.addEventListener)
    window.addEventListener("load", downloadJSAtOnload, false);
  else if (window.attachEvent)
    window.attachEvent("onload", downloadJSAtOnload);
  else window.onload = downloadJSAtOnload;
</script>

<!-- Mirrored from www.drapefit.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Mar 2020 14:29:11 GMT -->
</html>