/**
 * Loader for settings page
 * @package    Meta Box
 * @subpackage MB Settings Page
 * @author     Tran Ngoc Tuan Anh <rilwis@gmail.com>
 *//**
 * Loader class
 */{/**
	 * Meta boxes for terms only.
	 * @var array
	 */=();/**
	 * Run hooks to get meta boxes for terms and initialize them.
	 */(){(,(,),);(,(,),);/**
		 * Initialize meta boxes for term.
		 * 'rwmb_meta_boxes' runs at priority 10, we use priority 20 to make sure $this->meta_boxes is set.
		 * @see mb_term_meta_filter()
		 */(,(,),);}/**
	 * Register settings pages.
	 */(){=(,());// Prevent errors showing if invalid value is returned from the filter above
(()!()){;}(){();}}/**
	 * Filter meta boxes to get only meta boxes for terms and remove them from posts.
	 *
	 * @param array $meta_boxes
	 *
	 * @return array
	 */(){(){(([])){[]=;([]);}};}/**
	 * Register meta boxes for term, each meta box is a section
	 */(){(){();}}}