/**
 * Autoload plugin classes.
 *
 * @package Meta Box
 *//**
 * Autoload class
 */{/**
	 * List of directories to load classes.
	 *
	 * @var array
	 */=();/**
	 * Adds a base directory for a class name prefix and/or suffix.
	 *
	 * @param string $base_dir A base directory for class files.
	 * @param string $prefix   The class name prefix.
	 * @param string $suffix   The class name suffix.
	 */(,,=){[]=((),,,);}/**
	 * Register autoloader for plugin classes.
	 * In PHP 5.3, SPL extension cannot be disabled and it's safe to use autoload.
	 * However, hosting providers can disable it in PHP 5.2. In that case, we provide a fallback for autoload.
	 *
	 * @link http://php.net/manual/en/spl.installation.php
	 * @link https://github.com/rilwis/meta-box/issues/810
	 */(){((,));(!()){();}}/**
	 * Autoload fields' classes.
	 *
	 * @param string $class Class name.
	 * @return mixed Boolean false if no mapped file can be loaded, or the name of the mapped file that was loaded.
	 */(){(){(([](,[]))([](,-([]))[])){;}=(,([]));([]()>([])){=(,,-([]));}=((,,)).;=[].;(()){;}};}/**
	 * Fallback for autoload in PHP 5.2.
	 */(){=(// Core.
,,,,,,,,,,,,// Walkers.
,,,,// Fields.
,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,);(){(."");}}/**
	 * If a file exists, require it from the file system.
	 *
	 * @param string $file The file to require.
	 * @return bool True if the file exists, false if not.
	 */(){(()){;;};}}