/**
 * The plugin core class which initialize plugin's code.
 *
 * @package Meta Box
 *//**
 * The Meta Box core class.
 *
 * @package Meta Box
 */{/**
	 * Initialization.
	 */(){(,,().);(,(,));// Uses priority 20 to support custom port types registered using the default priority.
(,(,),);(,(,));}/**
	 * Add links to Documentation and Extensions in plugin's list of action links.
	 *
	 * @since 4.3.11
	 * @param array $links Array of plugin links.
	 * @return array
	 */(){[]=.(,).;[]=.(,).;;}/**
	 * Register meta boxes.
	 * Advantages:
	 * - prevents incorrect hook.
	 * - no need to check for class existences.
	 */(){=(,());=();(){=();();();}}/**
	 * WordPress will prevent post data saving if a page template has been selected that does not exist.
	 * This is especially a problem when switching to our theme, and old page templates are in the post data.
	 * Unset the page template if the page does not exist to allow the post to save.
	 *
	 * @param WP_Post $post Post object.
	 * @since 4.3.10
	 */(){=(,,);=()();// If the template doesn't exists, remove the data to allow WordPress to save.
(!([])){(,);}}/**
	 * Get registered meta boxes via a filter.
	 *
	 * @deprecated No longer used. Keep for backward-compatibility with extensions.
	 *
	 * @return array
	 */(){=()();(,);}}