/**
 * The abstract choice field.
 *
 * @package Meta Box
 *//**
 * Abstract class for any kind of choice field.
 */{/**
	 * Walk options.
	 *
	 * @param array $field     Field parameters.
	 * @param mixed $options   Select options.
	 * @param mixed $db_fields Database fields to use in the output.
	 * @param mixed $meta      Meta value.
	 * @return string
	 */(,,,){;}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){=;=(,);=(,,);=(,);!()?(,,,,):;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();=(,(,(),));;}/**
	 * Get field names of object to be used by walker.
	 *
	 * @return array
	 */(){(,,,);}/**
	 * Get options for walker.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();([]){=()?:(,,);(([])([])){[[]]=;}};}/**
	 * Filter options for walker.
	 *
	 * @param array $field   Field parameters.
	 * @param array $options Array of choice options.
	 *
	 * @return array
	 */(,){=(,);=[];(&){=(,,);=(,,,);};}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value Meta value.
	 * @return string
	 */(,){(,,);}/**
	 * Get option label.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value Option value.
	 *
	 * @return string
	 */(,){=(,);([])?[]:;}}