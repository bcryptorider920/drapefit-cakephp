/**
 * The date and time picker field which allows users to select both date and time via jQueryUI datetime picker.
 *
 * @package Meta Box
 *//**
 * Datetime field class.
 */{/**
	 * Translate date format from jQuery UI date picker to PHP date().
	 * It's used to store timestamp value of the field.
	 * Missing:  '!' => '', 'oo' => '', '@' => '', "''" => "'".
	 *
	 * @var array
	 */=(,,,,,,,,,,,,);/**
	 * Translate time format from jQuery UI time picker to PHP date().
	 * It's used to store timestamp value of the field.
	 * Missing: 't' => '', T' => '', 'm' => '', 's' => ''.
	 *
	 * @var array
	 */=(,,,,,,,,,);/**
	 * Register scripts and styles.
	 */(){=.;(,"",(),);(,"",(),);(,"",(,),);(,.,(),);(,"",(,),);(,"",(,),);=.;(,"",(,),,);(,"",(),,);(,.,(,),,);(,.,(),,);(,.,(),,);=(,);=(,,());=(,,);=(,,);(){("",.(),);}}/**
	 * Enqueue scripts and styles.
	 */(){();();();}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  The field meta value.
	 * @param array $field The field parameters.
	 *
	 * @return string
	 */(,){=;([]){=[];=((.,),);(,(.),([])?([]):);=([])?[]:;}(,);([]){;};}/**
	 * Calculates the timestamp from the datetime string and returns it if $field['timestamp'] is set or the datetime string if not.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return string|int
	 */(,,,){[]?[]:;}/**
	 * Get meta value.
	 *
	 * @param int   $post_id The post ID.
	 * @param bool  $saved   Whether the meta box is saved at least once.
	 * @param array $field   The field parameters.
	 *
	 * @return mixed
	 */(,,){=(,,);([]){=(,);};}/**
	 * Format meta value if set 'timestamp'.
	 *
	 * @param array|string $meta  The meta value.
	 * @param array        $field Field parameters.
	 * @return array
	 */(,){(()){(){[]=(,);};}(?:,?((,),()):,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field The field parameters.
	 * @return array
	 */(){=(,(,,(),));// Deprecate 'format', but keep it for backward compatible.
// Use 'js_options' instead.
[]=([],(,,([])?:[],,));([]){[]=([],(,));}=();;}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field The field parameters.
	 * @param mixed $value The meta value.
	 *
	 * @return array
	 */(,=){=(,);=(,(([]),));[]=;;}/**
	 * Returns a date() compatible format string from the JavaScript format.
	 *
	 * @link http://www.php.net/manual/en/function.date.php
	 * @param array $field The field parameters.
	 *
	 * @return string
	 */(){([][],).[][].([][],);}}