/**
 * The file upload file which allows users to upload files via the default HTML <input type="file">.
 *
 * @package Meta Box
 *//**
 * File field class which uses HTML <input type="file"> to upload file.
 */{/**
	 * Enqueue scripts and styles.
	 */(){(,.,(),);(,.,(),,);(,,(// Translators: %d is the number of files in singular form.
(,),// Translators: %d is the number of files in plural form.
(,),));}/**
	 * Add custom actions.
	 */(){(,(,));(,(,));}/**
	 * Add data encoding type for file uploading
	 */(){;}/**
	 * Ajax callback for deleting files.
	 */(){=(,);=(,,);("}");(()){();}((,));}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=();=(,(,,),);=(,);// Show form upload.
(,[],);;}/**
	 * Get HTML for uploaded files.
	 *
	 * @param array $files List of uploaded files.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){=("[]}");=("[]}");=;(){// Ignore deleted files (if users accidentally deleted files or uses `force_delete` without saving post).
(()){(,,,);}}(,[],,,[]?:,[],[],);}/**
	 * Get HTML for uploaded file.
	 *
	 * @param int   $file  Attachment (file) ID.
	 * @param int   $index File index.
	 * @param array $field Field data.
	 * @return string
	 */(,,){=(,(,,));=(,(,,));=(,);=();=(,(,),);(,(),,(),(),(),(),,,,[],,);}/**
	 * Get meta values to save.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return array|mixed
	 */(,,,){=[];// @codingStandardsIgnoreLine
(([])){;}=();// Non-cloneable field.
(![]){=();(=;;){=("}}",);(!()){[]=;}};}// Cloneable field.
=();(){(([])){[]=();}(=;;){=("}}}",);(!()){[][]=;}}};}/**
	 * Transform $_FILES from $_FILES['field']['key']['index'] to $_FILES['field_index']['key'].
	 *
	 * @param string $input_name The field input name.
	 *
	 * @return int The number of uploaded files.
	 */(){// @codingStandardsIgnoreStart
([]){(){="}}";(!([])){[]=();}[][]=;}}([][]);// @codingStandardsIgnoreEnd
}/**
	 * Transform $_FILES from $_FILES['field']['key']['cloneIndex']['index'] to $_FILES['field_cloneIndex_index']['key'].
	 *
	 * @param string $input_name The field input name.
	 *
	 * @return array
	 */(){// @codingStandardsIgnoreStart
([]){(){(){="}}}";(!([])){[]=();}[][]=;}}}=();([][]){[]=();};// @codingStandardsIgnoreEnd
}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();=(,((),,,,));[]=;[]=.[];;}/**
	 * Get the field value. Return meaningful info of the files.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Not used for this field.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed Full info of uploaded files
	 */(,=(),=){=(,,);(![]){=(,,,);}{=();(){[]=(,,,);}=;}(([])){=(,,([]));};}/**
	 * Get uploaded files information.
	 *
	 * @param array $field Field parameters.
	 * @param array $files Files IDs.
	 * @param array $args  Additional arguments (for image size).
	 * @return array
	 */(,,){=();(){=(,,,);(){[]=;}};}/**
	 * Get uploaded file information.
	 *
	 * @param int   $file Attachment file ID (post ID). Required.
	 * @param array $args Array of arguments (for size).
	 *
	 * @return array|bool False if file not found. Array of (id, name, path, url) on success.
	 */(,=()){=();(!){;}((,(),,(),(),),());}/**
	 * Format value for the helper functions.
	 *
	 * @param array        $field Field parameters.
	 * @param string|array $value The field meta value.
	 * @return string
	 */(,){(![]){(,,);}=;(){.(,,).;};;}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array $field Field parameters.
	 * @param array $value The value.
	 * @return string
	 */(,){=;(){(,[],[]);};;}}