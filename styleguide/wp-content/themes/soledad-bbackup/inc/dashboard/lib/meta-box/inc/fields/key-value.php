/**
 * The key-value field which allows users to add pairs of keys and values.
 *
 * @package Meta Box
 *//**
 * Key-value field class.
 */{/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){// Key.
=([])?[]:;=(,);[]=[][];=(,());// Value.
=([])?[]:;=(,);[]=[][];(,());;}/**
	 * Show begin HTML markup for fields.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){=[]?"[]}[]}":;(([])){.;}(,[],[],);}/**
	 * Do not show field description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){;}/**
	 * Do not show field description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){;}/**
	 * Escape meta for field output.
	 *
	 * @param mixed $meta Meta value.
	 * @return mixed
	 */(){(){[]=(,);};}/**
	 * Sanitize field value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return string
	 */(,,,){(&){(([])([])){=;}}=();;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();[]=;[]=;[][]=;[]=([],((,),(,),));;}/**
	 * Format value for the helper functions.
	 *
	 * @param array        $field Field parameters.
	 * @param string|array $value The field meta value.
	 * @return string
	 */(,){=;(){(,[],[]);};;}}