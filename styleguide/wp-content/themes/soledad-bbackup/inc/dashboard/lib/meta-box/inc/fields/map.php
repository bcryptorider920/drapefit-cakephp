/**
 * The Google Maps field.
 *
 * @package Meta Box
 *//**
 * Map field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){/**
		 * Since June 2016, Google Maps requires a valid API key.
		 *
		 * @link http://googlegeodevelopers.blogspot.com/2016/06/building-for-scale-updates-to-google.html
		 * @link https://developers.google.com/maps/documentation/javascript/get-api-key
		 */=();=[];=(,[],);/**
		 * Allows developers load more libraries via a filter.
		 *
		 * @link https://developers.google.com/maps/documentation/javascript/libraries
		 */=(,);(,(),(),,);(,.,(),);(,.,(,),,);}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=([])?(,[]):[];=(,());(,([]),([]),([]),());([]){(,(,));};;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();=(,(,,,// Default API key, required by Google Maps since June 2016.
// Users should overwrite this key with their own key.
,));;}/**
	 * Get the field value.
	 * The difference between this function and 'meta' function is 'meta' function always returns the escaped value
	 * of the field saved in the database, while this function returns more meaningful value of the field.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Not used for this field.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed Array(latitude, longitude, zoom)
	 */(,=(),=){=(,,);(,,)=(,.);(,,);}/**
	 * Output the field value.
	 * Display Google maps.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Additional arguments. Not used for these fields.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed Field value
	 */(,=(),=){=(,,);(![]![]){;}(![]){[]=;}=(,([],[],,,,// Display marker?
,// Marker title, when hover.
,// Content of info window (when click on marker). HTML allowed.
(),// Default API key, required by Google Maps since June 2016.
// Users should overwrite this key with their own key.
,));/*
		 * Enqueue scripts.
		 * API key is get from $field (if found by RWMB_Helper::find_field()) or $args as a fallback.
		 * Note: We still can enqueue script which outputs in the footer.
		 */=([])?[]:[];=(,,);/*
		 * Allows developers load more libraries via a filter.
		 * @link https://developers.google.com/maps/documentation/javascript/libraries
		 */=(,);(,(),(),,);(,.,(),,);/*
		 * Google Maps options.
		 * Option name is the same as specified in Google Maps documentation.
		 * This array will be convert to Javascript Object and pass as map options.
		 * @link https://developers.google.com/maps/documentation/javascript/reference
		 */[]=([],(// Default to 'zoom' level set in admin, but can be overwritten.
[],// Map type, see https://developers.google.com/maps/documentation/javascript/reference#MapTypeId.
,));=(,(()),([]),([]));;}}