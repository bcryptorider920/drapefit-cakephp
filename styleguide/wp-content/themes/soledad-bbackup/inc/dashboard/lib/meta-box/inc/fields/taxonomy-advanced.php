/**
 * Taxonomy advanced field which saves terms' IDs in the post meta in CSV format.
 *
 * @package Meta Box
 *//**
 * The taxonomy advanced field class.
 */{/**
	 * Normalize the field parameters.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=(,(,));=[];=();[]=;;}/**
	 * Get meta values to save.
	 * Save terms in custom field in form of comma-separated IDs, no more by setting post terms.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return string
	 */(,,,){(,());}/**
	 * Save meta value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 */(,,,){=[];(){(,[],);}{(,[]);}}/**
	 * Get raw meta value.
	 *
	 * @param int   $object_id Object ID.
	 * @param array $field     Field parameters.
	 * @param array $args      Arguments of {@see rwmb_meta()} helper.
	 *
	 * @return mixed
	 */(,,=()){[]=;=(,,);(()){[]?():;}=()?(,):();=();[]?:();}/**
	 * Get the field value.
	 * Return list of post term objects.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Additional arguments.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return array List of post term objects.
	 */(,=(),=){(!){=();}// Get raw meta value in the database, no escape.
=(,,,);(()){;}// Allow to pass more arguments to "get_terms".
=((,,),);=([],);// Get single value if necessary.
(![]![]){=();};}}