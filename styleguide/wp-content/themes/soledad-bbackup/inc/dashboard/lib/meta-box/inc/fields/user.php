/**
 * The user select field.
 *
 * @package Meta Box
 *//**
 * User field class.
 */{/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){// Set default field args.
=();// Prevent select tree for user since it's not hierarchical.
[]=[]?:[];// Set to always flat.
[]=;// Set default placeholder.
[]=([])?(,):[];// Set default query args.
[]=([],(,,,,));;}/**
	 * Get users.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=([]);();}/**
	 * Get field names of object to be used by walker.
	 *
	 * @return array
	 */(){(,,,);}/**
	 * Get option label.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value Option value.
	 *
	 * @return string
	 */(,){=();.()...;}}