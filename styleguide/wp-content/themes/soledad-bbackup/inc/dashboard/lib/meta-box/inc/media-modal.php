/**
 * Add support for editing attachment custom fields in the media modal.
 *
 * @package Meta Box
 *//**
 * The media modal class.
 * Handling showing and saving custom fields in the media modal.
 */{/**
	 * List of custom fields.
	 *
	 * @var array
	 */=();/**
	 * Initialize.
	 */(){(,(,));(,(,),,);(,(,),,);(,(,),,);}/**
	 * Get list of custom fields and store in the current object for future use.
	 */(){=()();(){(()){=(,());}}}/**
	 * Add fields to the attachment edit popup.
	 *
	 * @param array   $form_fields An array of attachment form fields.
	 * @param WP_Post $post The WP_Post attachment object.
	 *
	 * @return mixed
	 */(,){(){=;[]=[];[]=;// Just ignore the field 'std' because there's no way to check it.
=(,,,);[]=;[]=...[].;[]=(,,);[[]]=;};}/**
	 * Save custom fields.
	 *
	 * @param array $post An array of post data.
	 * @param array $attachment An array of attachment metadata.
	 *
	 * @return array
	 */(,){(){=[];=(,,[]);=([])?[]:;// Allow field class change the value.
([]){=(,,[],);}{=(,,,,[]);=(,,);}=(,,,);// Call defined method to save meta value, if there's no methods, call common one.
(,,,,[]);};}/**
	 * Whether or not show the meta box when editing custom fields in the normal mode.
	 *
	 * @param bool  $show     Whether to show the meta box in normal editing mode.
	 * @param array $meta_box Meta Box parameters.
	 *
	 * @return bool
	 */(,){=!();;}/**
	 * Check if the meta box is for editing custom fields in the media modal.
	 *
	 * @param array $meta_box Meta Box parameters.
	 *
	 * @return bool
	 */(){(,[],)!([]);}}