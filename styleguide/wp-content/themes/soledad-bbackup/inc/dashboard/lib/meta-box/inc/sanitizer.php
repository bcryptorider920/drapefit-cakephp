/**
 * Sanitize field value before saving.
 *
 * @package Meta Box
 *//**
 * Sanitize class.
 */{/**
	 * Built-in callbacks for some specific types.
	 *
	 * @var array
	 */=(,,,,);/**
	 * Register hook to sanitize field value.
	 */(){// Built-in callback.
(){("}",);}// Custom callback.
=((),());(){("}",(,));}}/**
	 * Set the value of checkbox to 1 or 0 instead of 'checked' and empty string.
	 * This prevents using default value once the checkbox has been unchecked.
	 *
	 * @link https://github.com/rilwis/meta-box/issues/6
	 * @param string $value Checkbox value.
	 * @return int
	 */(){!();}}