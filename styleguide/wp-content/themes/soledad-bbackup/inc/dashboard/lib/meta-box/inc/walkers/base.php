/**
 * Base walker.
 * Walkers must inherit this class and overwrite methods with its own.
 *
 * @package Meta Box
 *//**
 * Base walker class.
 */{/**
	 * Field data.
	 *
	 * @access public
	 * @var array
	 */;/**
	 * Meta data.
	 *
	 * @access public
	 * @var array
	 */=();/**
	 * Constructor.
	 *
	 * @param array $db_fields Database fields.
	 * @param array $field     Field parameters.
	 * @param mixed $meta      Meta value.
	 */(,,){=(,(,,,));=;=;}}