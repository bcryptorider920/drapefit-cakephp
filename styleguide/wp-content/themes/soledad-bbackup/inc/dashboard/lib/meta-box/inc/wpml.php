/**
 * The WPML compatibility module, allowing all fields are translatable by WPML plugin.
 *
 * @package Meta Box
 *//**
 * WPML compatibility class
 */{/**
	 * List of fields that need to translate values (because they're saved as IDs).
	 *
	 * @var array
	 */=(,);/**
	 * Initialize.
	 */(){/**
		 * Run before meta boxes are registered so it can modify fields.
		 *
		 * @see modify_field()
		 */(,(,),);}/**
	 * Register hooks.
	 */(){(!()){;}(,(,),,);(,(,));}/**
	 * Translating IDs stored as field values upon WPML post/page duplication.
	 *
	 * @param mixed  $value           Meta value.
	 * @param string $target_language Target language.
	 * @param array  $meta_data       Meta arguments.
	 * @return mixed
	 */(,,){([]){;}=()([],([]));(!([],,)){;}// Object type needed for WPML filter differs between fields.
=[]?[]:[];// Translating values, whether are stored as comma separated strings or not.
((,)){=(,,,,);;}// Dealing with IDs stored as comma separated strings.
=();=(,);(){[]=(,,,,);}=(,);;}/**
	 * Modified field depends on its translation status.
	 * If the post is a translated version of another post and the field is set to:
	 * - Do not translate: hide the field.
	 * - Copy: make it disabled so users cannot edit.
	 * - Translate: do nothing.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return mixed
	 */(){;(([])){;}// Get post ID.
=(,,);(!){=(,,);}// If the post is the original one: do nothing.
(!()){;}// Get setting for the custom field translation.
=(,,,);(!([[]])){;}=([[]]);(){// Do not translate: hide it.
[];}(){// Copy: disable editing.
[]=;};}}