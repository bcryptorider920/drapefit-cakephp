
        <section class="footer">
    <div class="container"> 
        <div class="row">
            <div class="col-md-3" align="left">
                <div class="footer-logo">
                                                <a href="https://www.drapefit.com/"> 
                                                        <img src="https://www.drapefit.com/img/logo.png" alt="drapfit logo">
                        </a>
                        </br>
                        <p>WE DO BEST FIT</p>
                        <div class="footer-social">
                            <a class="us-country" href="#">United States</a>
                    <h3>Follow Us :</h3>
                    <ul>
        <li><a href="https://www.facebook.com/drapefitinc"><i class="fa fa-facebook-f"></i></a></li>
        <li><a href="https://twitter.com/drapefitinc"><i class="fa fa-twitter"></i> </a></li>
        <li><a href="https://www.instagram.com/drapefitinc/"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.pinterest.com/drapefitinc/"><i class="fa fa-pinterest"></i></a></li>
    </ul>                </div> 
                </div>
            </div>
            <div class="col-md-2" align="left">
                <div class="footer-links">
                    <h3>About Us</h3>
                    <ul align="left">
                        <!-- <a href="footer.ctp"></a> -->
                        <li><a href="https://www.drapefit.com/who-we-are">Who We Are</a></li>
                        <li><a href="https://www.drapefit.com/our-mission">Our Mission</a></li>
                        <li><a href="https://www.drapefit.com/our-stylist">Our Stylist</a></li>
                        <!-- <li><a href="https://www.drapefit.com/news">News</a></li>
                        <li><a href="https://www.drapefit.com/investors">Investors</a></li> -->
                        <li><a href="https://www.drapefit.com/executive-team">Executive Team</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2" align="left">
                <div class="footer-links">
                    <h3>Service</h3>
                    <ul align="left">
                                                    
                                                    
                                                                                <li><a href="men">Men</a></li>
                            <li><a href="https://www.drapefit.com/men/big-tall">Big & Tall</a></li>
                            <li><a href="https://www.drapefit.com/women">Women</a></li>
                            <li><a href="https://www.drapefit.com/women/plus-size">Plus size Fits</a></li>
                            <li><a href="https://www.drapefit.com/women/maternity">Maternity Fits</a></li>
                            <li><a href="https://www.drapefit.com/women/petite">Petite</a></li>
                            <li><a href="https://www.drapefit.com/women/women-jeans">Women Jeans</a></li>
                            <li><a href="https://www.drapefit.com/women/women-business">Women Business Fits</a></li>
                            <li><a href="https://www.drapefit.com/kids">Kids</a></li>
                                            </ul>
                </div>
            </div>
            <div class="col-md-2" align="left">
                <div class="footer-links">
                    <h3>The Company</h3>
                    <ul align="left">
                        <li><a href="https://www.drapefit.com/style-blog">Style Blog</a></li>
                        <li><a href="https://www.drapefit.com/news">News</a></li>
                        <li><a href="https://www.drapefit.com/investors">Investors Relation</a></li>
                        <li><a href="https://www.drapefit.com/careers">Careers</a></li>
<!--                         <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li>
                        <li><a href="https://www.drapefit.com/feedback-review">Feedback & Reviews</a></li> -->
                                            </ul>
                </div>
            </div>
            <div class="col-md-2" align="left">
               <div class="footer-links">
                    <h3>Recommended</h3>
                    <ul align="left">
                        <li><a href="https://www.drapefit.com/how-it-works">How Drape Fit Works</a></li>                
                        <li><a href="https://www.drapefit.com/box-pricing">FIT Box Pricing</a></li>
                        <li><a href="https://www.drapefit.com/personal-stylist">Personal Stylist</a></li>
                    </ul>
                </div>           
            </div>   
            <div class="col-md-2" align="left">
                <div class="footer-links">
                    <h3>Customer Care</h3>
                    <ul align="left">
                        <li><a href="https://www.drapefit.com/faq">FAQ</a></li>                
                        <li><a href="https://www.drapefit.com/gifts">Gift Card</a></li>
                        <li><a href="https://www.drapefit.com/return-exchange">Return & Exchange</a></li>
                        <li><a href="https://www.drapefit.com/track-order">Track Order</a></li>
                        <li><a href="https://www.drapefit.com/help-center">Help Center</a></li>
                        <li><a href="https://www.drapefit.com/contact-us">Contact Us</a></li>
                        <!-- <li><a href="https://www.drapefit.com/privacy-policy">Track Order</a></li> -->
                    </ul>
                </div>
            </div>
                  
        </div>
    </div>
</section>
<!--     <script type="text/javascript" id="hs-script-loader" async defer src="http://js.hs-scripts.com/6931110.js"></script> -->
        <section class="copy-right">
    <div class="container">
        <div class="row">
            <div class="col-md-6">         
                <span>@2020 DRAPE FIT INC. ALL RIGHTS RESERVED.</span>                        
            </div>
            <div class="col-md-6">
            <ul>
                <li ><a href="terms-conditions" style="color:#fff">Terms & Conditions |</a><a href="privacy-policy" style="color:#fff"> Privacy Policy |</a><a href="map" style="color:#fff"> Sitemap</a></li>
            </ul> 
            </div>
        </div>
    </div>
    <a id="button"></a>
                    
                <!--<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/6939712.js"></script>-->

                                
</section>
<script>
    $('.live-button').click(function () {
        var pageurl = 'index.html';
        $.ajax({
            type: 'POST',
            url: pageurl + 'users/chat_button_box_active',
            success: function (response) {
                if (response.status == 'success') {
                    $('#chat-button').hide();
                    $('#chat-div').show();



                }
            },

            dataType: 'json'
        });
    });

//    $(document).ready(function () {
//        var pageurl = 'https://www.drapefit.com/';
//        $.ajax({
//            type: 'POST',
//            url: pageurl + 'users/chat_check_auth',
//            success: function (response) {
//                if (response.status == 'success') {
//                    if (response.value == 1) {
//                        $('#btn1').hide();
//                    } else {
//                        $('#btn1').show();
//                    }
//
//                }
//            },
//
//            dataType: 'json'
//        });
//
//    });

    var btn = $('#button');

        $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
            btn.addClass('show');
          } else {
            btn.removeClass('show');
          }
        });
        
        btn.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({scrollTop:0}, '300');
    });
</script>
        <input type="hidden" id="pageurl" value="https://www.drapefit.com/"/>
    </body>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker4').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker5').datetimepicker({
                format: 'MM-DD-YYYY'
            });
        });
    </script>
    <script type="text/javascript">
  function downloadJSAtOnload() {
    var element = document.createElement("script");
    element.src = "https://cdn.collectiveray.com/defer.js";
    document.body.appendChild(element);
  }
  if (window.addEventListener)
    window.addEventListener("load", downloadJSAtOnload, false);
  else if (window.attachEvent)
    window.attachEvent("onload", downloadJSAtOnload);
  else window.onload = downloadJSAtOnload;
</script>


