<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145376852-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-145376852-1');
</script>
   <script type="application/ld+json">{"@context":"http://schema.org","@type":"Organization","url":"https://www.drapefit.com/","logo":"https://www.drapefit.com/img/mian-logo.png","sameAs":["https://www.facebook.com/drapefitinc","https://www.instagram.com/drapefitinc/","https://en.wikipedia.org/wiki/","https://twitter.com/drapefitinc","https://www.pinterest.com/drapefitinc/"]}</script>
    <body>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PH7WXHZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <style type="text/css">.submenu ul li.active a{ background: #1b2431;color: #f76c02;}.sign-up-form .alert-danger{ top: 0 !important; }</style>
<script type="text/javascript" src='../cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js'></script>
<script type="text/javascript" src='../cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js'></script> 
<script type="text/javascript"> $(document).ready(function () {
        $('#email-error').hide();
        $('#email-error1').hide();
    });</script>
<script type="text/javascript">function psw() {
        var x = document.getElementById("login_password");
        if (x.type === "password") {
            x.type = "text";
            $('#showpwd').html('hide');
        } else {
            x.type = "password";
            $('#showpwd').html('show');
        }
    }</script>
<script type="text/javascript">function signuppsw() {
        var y = document.getElementById("pwd");
        if (y.type === "password") {
            y.type = "text"; $('#showsignuppsw').html('hide');
        } else {
            y.type = "password";
            $('#showsignuppsw').html('show');
        }
    }</script>
<section class="header-top">
    <div class="container"> 
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                                                            <a href="https://www.drapefit.com/men"> 
                            <img src="img/mian-logo.png" alt="">
                        </a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="menu-bar menu-list">
                    <label for="menu-toggle"><p><img src="img/menu-toggle.png" alt=""></p></label>
                    <input type="checkbox" id="menu-toggle"/>
                                                            <ul id="menu">
                            <li>
                                <a  href="https://www.drapefit.com/men">
                                    <i class="fa fa-male"></i>
                                    <span>Men</span>
                                </a>                  
                            </li>
                            <li>
                                <a   href="https://www.drapefit.com/men">
                                    <i class="fa fa-female"></i>
                                    <span>Women</span>
                                </a>
                            </li>
                            <li>
                                <a   href="https://www.drapefit.com/kids">
                                    <i class="fa fa-child" aria-hidden="true"></i>
                                    <span>Kids</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="document.getElementById('id01').style.display = 'block';
                                        document.getElementById('id02').style.display = 'none';
                                        $('#userformlogin')[0].reset();">
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                    <span>Search</span>
                                </a>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="id01" class="modal">
    <form method="post" accept-charset="utf-8" data-toggle="validator" novalidate="novalidate" id="userformlogin" class="modal-content" action="https://www.drapefit.com/"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>    <span onclick="document.getElementById('id01').style.display = 'none'" class="close" id="close1" title="Close Modal">&times;</span>
    <div class="row">
        <div class="col-md-12">
            <div class="sign-up-page">
                <h1>Welcome Back</h1>                 
            </div>
            <p class="last-para">You don't have an account? <a href="javascript:void(0);" onclick="document.getElementById('id02').style.display = 'block'; document.getElementById('id01').style.display = 'none';
                    document.getElementById('id03').style.display = 'none';
                    $('#userform')[0].reset();">Sign Up </a> here.</p>
            <div class="sign-up-form">
                <div class="col-sm-12 alert alert-danger" id="login_errorlogin" style="display: none;"></div>
                <input type="text" autocomplete="off" placeholder="Enter Email" name="email" required id="login_email">


                <div class="show-password">
                    <input type="password" autocomplete="off" placeholder="Enter Password" name="password" required id="login_password">
                    <span id="showpwd" onclick="psw()">show</span>
                </div>
                <a href="javascript:void(0);" onclick="document.getElementById('forgotPassword').style.display = 'block';document.getElementById('id01').style.display = 'none';document.getElementById('id02').style.display = 'none';
                        document.getElementById('id03').style.display = 'none';">Forgot Password ?</a>              
            </div>
            <div class="clearfix"><button type="submit" class="signupbtn" id="signIn">Sign In</button></div>
            <div class="sign-up-page">
                <a class="facebook" href="javascript:void()" onclick="window.open('https://www.facebook.com/v2.2/dialog/oauth?client_id=444684589387412&amp;redirect_uri=https%3A%2F%2Fwww.drapefit.com%2Fusers%2Ffbreturn&amp;state=d8b2dc7a326e10a1f29ea7aac03877b0&amp;sdk=php-sdk-4.0.15&amp;scope=email', '_blank');"> Log in with Facebook</a>
                <a class="google" href="https://accounts.google.com/o/oauth2/auth?response_type=code&amp;redirect_uri=https%3A%2F%2Fwww.drapefit.com%2FgoogleLoginReturn&amp;client_id=815751471058-2g648h6ghbs0k3mdu15jm2d8ocb9pbhi.apps.googleusercontent.com&amp;scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.login+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fplus.me&amp;access_type=offline&amp;approval_prompt=auto"> Log in with Google</a>                  
            </div>
        </div>
    </div>
</form></div>

<div id="id02" class="modal">

<form method="post" accept-charset="utf-8" data-toggle="validator" novalidate="novalidate" id="userform" class="modal-content" action="https://www.drapefit.com/"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>    <span onclick="document.getElementById('id02').style.display = 'none'" class="close" title="Close Modal">&times;</span>
    <div class="row">
        <div class="col-md-12">
            <div class="sign-up-page">
                <h1>New to Drape Fit</h1>                  
                <p class="last-para">Already have an Account ? <a href="javascript:void(0);" onclick="document.getElementById('id01').style.display = 'block';document.getElementById('id02').style.display = 'none';
                        document.getElementById('id03').style.display = 'none'; $('#userformlogin')[0].reset();"> Sign In </a> here.</p>
            </div>
            <div class="sign-up-form">
                <div class="input_box">
                    <input type="text" placeholder="First Name" name="fname"  id="fname" required value="">
                </div>
                <div class="input_box2">
                    <input type="text" placeholder="Last Name" name="lname" id="lname" required value="">
                </div>
                <input type="text" placeholder="Enter Email" name="email" id="email" required value="">
                <label id="email-error" class="error" for="email"></label>
                <div class="show-password">
                    <input type="password" placeholder="Enter Password" name="pwd" id="pwd" required value="">
                    <span id="showsignuppsw" onclick="signuppsw()">show</span>
                </div>
                <p class="need">I need FIT for 
                    <label><input type="radio" name="gender" value="men"> Men</label>
                    <label><input type="radio" name="gender" value="women"> Women</label>
                    <label><input type="radio" name="gender" value="kids"> Kids</label>
                </p>
                <p>
                    <input type="checkbox" name="chk" id="chk" value="accepted" required>  
                    By continuing, you accept <a target="_blank" href="terms-conditions.html">Terms of Use</a> of DRAPE FIT.
                </p>
            </div>

            <div class="clearfix"><button type="submit" class="signupbtn"   id="signupbtn">Sign Up</button></div>
        </div>
    </div>
</form></div> 
<div id="id03" class="modal">
<form method="post" accept-charset="utf-8" data-toggle="validator" novalidate="novalidate" id="registeruserwise" class="modal-content" action="https://www.drapefit.com/"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>    <span onclick="document.getElementById('id03').style.display = 'none'" class="close" title="Close Modal">&times;</span>
    <div class="row">
        <div class="col-md-12">
            <div class="sign-up-page">
                <h1>New to Drape Fit</h1>                  
                <p class="last-para">Already have an Account ? <a href="javascript:void(0);" onclick="document.getElementById('id01').style.display = 'block';document.getElementById('id03').style.display = 'none';
                        document.getElementById('id02').style.display = 'none';
                        $('#userformlogin')[0].reset();"> Sign In </a> here.</p>
            </div>
            <div class="sign-up-form">
                <input type="text" placeholder="First Name" name="fname" required>
                <input type="text" placeholder="Last Name" name="lname" required>
                <input type="text" placeholder="Enter Email" name="email" class="eml" required>
                <label id="email-error1" class="error" for="email"></label>
                <input type="hidden" name="gender" value="index" required>
                <div class="show-password">
                    <input  id='cngPwd1' type="password" placeholder="Enter Password" name="pwd" required>
                    <span id="cngPwd" onclick="cngPwd()">show</span>
                </div>
            </div>
            <div class="clearfix"><button type="submit"  class="signupbtn">Sign Up</button></div>
        </div>
    </div>
</form>        
</div>
<div id="forgotPassword" class="modal">
<form method="post" accept-charset="utf-8" data-toggle="validator" novalidate="novalidate" id="forgotpassword_form" class="modal-content" action="https://www.drapefit.com/"><div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>    <span onclick="document.getElementById('forgotPassword').style.display = 'none'" class="close" id="closeforgotpass" title="Close Modal">&times;</span>
    <div class="row">
        <div class="col-md-12">
            <div class="sign-up-page" id='sign-up-page1'>
                <h1>Forget Password </h1>                 
            </div>
            <div class="sign-up-form">
                <div class="col-sm-12 alert alert-danger" id="login_error" style="display: none;"></div>
                <input type="text" placeholder="Enter Email" name="email" required id="forgot_email">  
                <span id="email-error"></span>
            </div>
            <div class="clearfix"><button type="submit" class="signupbtn" id="for_submit">Submit</button></div>
            <div class="clearfix"><button type="submit" style="display: none" class="signupbtn" id="for_submitOk">Ok</button></div>
        </div>
    </div>
</form></div>
<div id="loaderPyament" style="display: none; position: fixed; height: 100%; width: 100%; z-index: 11111111; padding-top: 20%; background: rgba(255, 255, 255, 0.7); top: 0; text-align: center;">
    <img src="img/widget_loader.gif"/>
</div>

