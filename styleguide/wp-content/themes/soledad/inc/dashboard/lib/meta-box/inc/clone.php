/**
 * The clone module, allowing users to clone (duplicate) fields.
 *
 * @package Meta Box
 *//**
 * The clone class.
 */{/**
	 * Get clone field HTML.
	 *
	 * @param mixed $meta  The meta value.
	 * @param array $field The field parameters.
	 *
	 * @return string
	 */(,){=;/**
		 * Note: $meta must contain value so that the foreach loop runs!
		 *
		 * @see meta()
		 */(){=;[]=[]."}";(>){(([])){[]=[]."}";}[]=[]."}";(!([][])){[][]=[][]."}";}}(([],(,),)){[]=[]."}";}([]){[];}// Wrap field HTML in a div with class="rwmb-clone" if needed.
="[]}";=;([]){;=;}="".;// Call separated methods for displaying each type of field.
(,,);=(,,,);// Remove clone button.
();;;}// End foreach().
;}/**
	 * Set value of meta before saving into database
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return mixed
	 */(,,,){(!()){=();}(([],(,),)){(,,,,);}(){=([])?[]:;=(,,,,);[]=(,,);};}/**
	 * Add clone button.
	 *
	 * @param array $field Field parameters.
	 * @return string $html
	 */(){(![]){;}=(,[],);.().;}/**
	 * Remove clone button.
	 *
	 * @param array $field Field parameters.
	 * @return string $html
	 */(){=(,,);..;}}