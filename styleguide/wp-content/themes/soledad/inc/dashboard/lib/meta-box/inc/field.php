/**
 * The field base class.
 * This is the parent class of all custom fields defined by the plugin, which defines all the common methods.
 * Fields must inherit this class and overwrite methods with its own.
 *
 * @package Meta Box
 *//**
 * The field base class.
 */{/**
	 * Add actions.
	 */(){}/**
	 * Enqueue scripts and styles.
	 */(){}/**
	 * Localize scripts with prevention of loading localized data twice.
	 *
	 * @link https://github.com/rilwis/meta-box/issues/850
	 *
	 * @param string $handle Script handle.
	 * @param string $name Object name.
	 * @param mixed  $data Localized data.
	 */(,,){/*
		 * Check with function_exists to make it work in WordPress 4.1.
		 * @link https://github.com/rilwis/meta-box/issues/1009
		 */(!()!()(,)){(,,);}}/**
	 * Show field HTML
	 * Filters are put inside this method, not inside methods such as "meta", "html", "begin_html", etc.
	 * That ensures the returned value are always been applied filters.
	 * This method is not meant to be overwritten in specific fields.
	 *
	 * @param array $field   Field parameters.
	 * @param bool  $saved   Whether the meta box is saved at least once.
	 * @param int   $post_id Post ID.
	 */(,,=){=(,,,);=(,,,);=(,,);=(,,,);// Separate code for cloneable and non-cloneable fields to make easy to maintain.
([]){=(,);}{// Call separated methods for displaying each type of field.
=(,,);=(,,,);}=(,,);=(,,,);=(,"",,);// Display label and input in DIV and allow user-defined classes to be appended.
="[]}".[];([]){;}(!([])){;}=([]..[],(),);=(,,,);;// WPCS: XSS OK.
}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){;}/**
	 * Show begin HTML markup for fields.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=;([]){=(,([]),[],());}=([])[]>?.[]:;=(,);.;}/**
	 * Show end HTML markup for fields.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){().(,).;}/**
	 * Display field label description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){=[]?.([]).:;[]?"}[]}":;}/**
	 * Display field description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){=[]?.([]).:;[]?"}[]}":;}/**
	 * Get raw meta value.
	 *
	 * @param int   $object_id Object ID.
	 * @param array $field     Field parameters.
	 * @param array $args      Arguments of {@see rwmb_meta()} helper.
	 *
	 * @return mixed
	 */(,,=()){(([])){;}(([])){=([]);}(([])){=[];}{=();}(!([])){[]=[]![];}(,[],);}/**
	 * Get meta value.
	 *
	 * @param int   $post_id Post ID.
	 * @param bool  $saved   Whether the meta box is saved at least once.
	 * @param array $field   Field parameters.
	 *
	 * @return mixed
	 */(,,){/**
		 * For special fields like 'divider', 'heading' which don't have ID, just return empty string
		 * to prevent notice error when displaying fields.
		 */(([])){;}// Get raw meta.
=(,,);// Use $field['std'] only when the meta box hasn't been saved (i.e. the first time we run).
=!?[]:;// Ensure multiple fields are arrays.
([]){([]){=;(){[]=;}}{=;}}// Escape attributes.
=(,,);// Make sure meta value is an array for clonable and multiple fields.
([][]){(()!()){/**
				 * If field is clonable, $meta must be an array with values so that the foreach loop in self::show() runs properly.
				 *
				 * @see self::show()
				 */=[]?():();}};}/**
	 * Escape meta for field output.
	 *
	 * @param mixed $meta Meta value.
	 *
	 * @return mixed
	 */(){()?(,):();}/**
	 * Set value of meta before saving into database.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return int
	 */(,,,){;}/**
	 * Save meta value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 */(,,,){=[];=[];// Remove post meta if it's empty.
(()){(,);;}// If field is cloneable, value is saved as a single entry in the database.
([]){// Remove empty values.
=;(){(()){([]);}}// Reset indexes.
=();(,,);;}// If field is multiple, value is saved as multiple entries in the database (WordPress behaviour).
([]){=;=;=(,);(){(,,,);}=(,);(){(,,);};}// Default: just update post meta.
(,,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=(,(,,,,,,,,,([])?[]:,,,,,(,),,,,,,(),));([]){[]=([],([],,));};}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field Field parameters.
	 * @param mixed $value Meta value.
	 *
	 * @return array
	 */(,=){=([],([],[],[],[],,[],));[]=(,(("[]}"),[]));;}/**
	 * Renders an attribute array into an html attributes string.
	 *
	 * @param array $attributes HTML attributes.
	 *
	 * @return string
	 */(){=;(){(){;}(()){=();}(?:,,());};}/**
	 * Get the field value.
	 * The difference between this function and 'meta' function is 'meta' function always returns the escaped value
	 * of the field saved in the database, while this function returns more meaningful value of the field, for ex.:
	 * for file/image: return array of file/image information instead of file/image IDs.
	 *
	 * Each field can extend this function and add more data to the returned value.
	 * See specific field classes for details.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Additional arguments. Rarely used. See specific fields for details.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed Field value
	 */(,=(),=){// Some fields does not have ID like heading, custom HTML, etc.
(([])){;}(!){=();}// Get raw meta value in the database, no escape.
=(,,,);// Make sure meta value is an array for cloneable and multiple fields.
([][]){=()?:();};}/**
	 * Output the field value.
	 * Depends on field value and field types, each field can extend this method to output its value in its own way
	 * See specific field classes for details.
	 *
	 * Note: we don't echo the field value directly. We return the output HTML of field, which will be used in
	 * rwmb_the_field function later.
	 *
	 * @use self::get_value()
	 * @see rwmb_the_value()
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Additional arguments. Rarely used. See specific fields for details.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return string HTML output of the field
	 */(,=(),=){=(,,,);(){;}(,,);}/**
	 * Format value for the helper functions.
	 *
	 * @param array        $field Field parameters.
	 * @param string|array $value The field meta value.
	 * @return string
	 */(,){(!()){(,,);}=;(){.(,,).;};;}/**
	 * Format a single value for the helper functions. Sub-fields should overwrite this method if necessary.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value The value.
	 * @return string
	 */(,){;}/**
	 * Call a method of a field.
	 * This should be replaced by static::$method( $args ) in PHP 5.3.
	 *
	 * @return mixed
	 */(){=();=();// Params: method name, field, other params.
(()){=();=();// Keep field as 1st param.
}{=();=();(){// Add field param after object id.
(,,,());}{[]=;// Add field as last param.
}}(((),),);}/**
	 * Map field types.
	 *
	 * @param array $field Field parameters.
	 * @return string Field mapped type.
	 */(){=([])?[]:;=(,(,,,));([])?[]:;}/**
	 * Get field class name.
	 *
	 * @param array $field Field parameters.
	 * @return string Field class name.
	 */(){=();=((,),,);=.().;=(,,);()?:;}/**
	 * Apply various filters based on field type, id.
	 * Filters:
	 * - rwmb_{$name}
	 * - rwmb_{$field['type']}_{$name}
	 * - rwmb_{$field['id']}_{$name}
	 *
	 * @return mixed
	 */(){=();// 3 first params must be: filter name, value, field. Other params will be used for filters.
=();=();=();// List of filters.
=(.,.[]..,);(([])){[]=.[]..;}// Filter params: value, field, other params. Note: value is changed after each run.
(,);(){=;(,);=(,);};}}