/**
 * The autocomplete field.
 *
 * @package Meta Box
 *//**
 * Autocomplete field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){(,.,,);(,.,(),,);(,,((,),));}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){(!()){=();}=(,,);=[];(([])){=();([]){[]=(,,);}=();}// Input field that triggers autocomplete.
// This field doesn't store field values, so it doesn't have "name" attribute.
// The value(s) of the field is store in hidden input(s). See below.
=(,([]),([]),());;// Each value is displayed with label and 'Delete' option.
// The hidden input has to have ".rwmb-*" class to make clone work.
=;(([])){([]){(!(,)){;}(,(),(,),([]),());}}{=();(){=(,,);(,(),(,),([]),());}};// .rwmb-autocomplete-results.
;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();=(,(,));;}}