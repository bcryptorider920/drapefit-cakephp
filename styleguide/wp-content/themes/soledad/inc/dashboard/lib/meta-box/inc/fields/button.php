/**
 * The button field. Simply displays a HTML button which might be used for JavaScript actions.
 *
 * @package Meta Box
 *//**
 * Button field class.
 */{/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field The field parameters.
	 * @return string
	 */(,){=();(,(),[]);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field The field parameters.
	 * @return array
	 */(){=(,((,),));=();;}/**
	 * Get the attributes for a field.
	 *
	 * @param array $field The field parameters.
	 * @param mixed $value The attribute value.
	 * @return array
	 */(,=){=(,);=(,([],));[];;}}