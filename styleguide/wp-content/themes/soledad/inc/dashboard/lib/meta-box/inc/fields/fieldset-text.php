/**
 * The text fieldset field, which allows users to enter content for a list of text fields.
 *
 * @package Meta Box
 *//**
 * Fieldset text class.
 */{/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=();=;([]){=([])?[]:;[][]=[]."}";[]=(,,(,));}=.[]..(,).;;}/**
	 * Do not show field description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){;}/**
	 * Do not show field description.
	 *
	 * @param array $field Field parameters.
	 * @return string
	 */(){;}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=();[]=;[][]=;[][]=;;}/**
	 * Format value for the helper functions.
	 *
	 * @param array        $field Field parameters.
	 * @param string|array $value Meta value.
	 * @return string
	 */(,){=;([]){"";};(![]){(,);}{(){(,);}};;}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array $field Field parameters.
	 * @param array $value The value.
	 * @return string
	 */(,){=;(){"";};;}}