/**
 * The image select field which behaves similar to the radio field but uses images as options.
 *
 * @package Meta Box
 *//**
 * The image select field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){(,.,(),);(,.,(),,);}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 * @return string
	 */(,){=();=;=;([]){[]=(,,[]?:,[],,((,),,));}(,);}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){=();[][]?:;;}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array  $field Field parameters.
	 * @param string $value The meta value.
	 * @return string
	 */(,){(,([][]));}}