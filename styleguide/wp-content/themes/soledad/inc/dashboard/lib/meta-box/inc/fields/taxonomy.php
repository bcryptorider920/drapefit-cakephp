/**
 * The taxonomy field which aims to replace the built-in WordPress taxonomy UI with more options.
 *
 * @package Meta Box
 *//**
 * Taxonomy field class which set post terms when saving.
 */{/**
	 * Add default value for 'taxonomy' field.
	 *
	 * @param array $field Field parameters.
	 * @return array
	 */(){// Backwards compatibility with field args.
(([][])){[]=[][];}(([][])){[]=[][];}(([][])){[]=[][];}// Set default field args.
=();=(,(,));// Set default query args.
[]=([],(,));/*
		 * Set default placeholder:
		 * - If multiple taxonomies: show 'Select a term'.
		 * - If single taxonomy: show 'Select a %taxonomy_name%'.
		 */(([])){[]=(,);(([])([])){=([]);// Translators: %s is the taxonomy singular label.
[]=((,),);}}// Prevent cloning for taxonomy field.
[]=;;}/**
	 * Get field names of object to be used by walker.
	 *
	 * @return array
	 */(){(,,,);}/**
	 * Get options for selects, checkbox list, etc via the terms.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){=([],[]);;}/**
	 * Save meta value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 */(,,,){=((,));=()?:;(,,[]);}/**
	 * Get raw meta value.
	 *
	 * @param int   $object_id Object ID.
	 * @param array $field     Field parameters.
	 * @param array $args      Arguments of {@see rwmb_meta()} helper.
	 *
	 * @return mixed
	 */(,,=()){(([])){;}=(,[]);(!()()){[]?():;}=(,);[]?:();}/**
	 * Get the field value.
	 * Return list of post term objects.
	 *
	 * @param  array    $field   Field parameters.
	 * @param  array    $args    Additional arguments.
	 * @param  int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return array List of post term objects.
	 */(,=(),=){=(,[]);// Get single value if necessary.
(![]![]()){=();};}/**
	 * Get option label.
	 *
	 * @param array  $field Field parameters.
	 * @param object $value The term object.
	 *
	 * @return string
	 */(,){(,// @codingStandardsIgnoreLine
(()),(),);}}