/**
 * The text list field which allows users to enter multiple texts.
 *
 * @package Meta Box
 *//**
 * Text list field class.
 */{/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=();=;=;([]){[]=(,[],([])?([]):,,);;}(,);}/**
	 * Format value for the helper functions.
	 *
	 * @param array        $field Field parameters.
	 * @param string|array $value The field meta value.
	 * @return string
	 */(,){=;([]){"";};(![]){(,);}{(){(,);}};;}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array $field Field parameters.
	 * @param array $value The value.
	 * @return string
	 */(,){=;(){"";};;}/**
	 * Save meta value.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 */(,,,){=[];(,[]);(,(),,);}}