/**
 * Image upload field which uses thickbox library to upload.
 *
 * @package Meta Box
 * @deprecated Use image_advanced instead
 *//**
 * The thickbox image field class.
 */{/**
	 * Add custom actions for the field.
	 */(){();(,(,));}/**
	 * Always enable insert to post button in the popup.
	 *
	 * @link https://github.com/rilwis/meta-box/issues/809
	 * @link http://wordpress.stackexchange.com/q/22175/2051
	 * @param array $vars Media item arguments in the popup.
	 * @return array
	 */(){[]=;// 'send' as in "Send to Editor".
;}/**
	 * Enqueue scripts and styles.
	 */(){();();();(,.,(),,);}/**
	 * Get field HTML.
	 *
	 * @param mixed $meta  Meta value.
	 * @param array $field Field parameters.
	 *
	 * @return string
	 */(,){=(,(,,),);// Uploaded images.
=(,);// Show form upload.
"[]}}";;}/**
	 * Get field value.
	 * It's the combination of new (uploaded) images and saved images.
	 *
	 * @param mixed $new     The submitted meta value.
	 * @param mixed $old     The existing meta value.
	 * @param int   $post_id The post ID.
	 * @param array $field   The field parameters.
	 *
	 * @return array
	 */(,,,){(((,)));}}