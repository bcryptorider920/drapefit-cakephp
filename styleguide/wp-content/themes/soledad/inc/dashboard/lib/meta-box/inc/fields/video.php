/**
 * Video field which uses WordPress media popup to upload and select video.
 *
 * @package Meta Box
 * @since 4.10
 *//**
 * The video field class.
 */{/**
	 * Enqueue scripts and styles.
	 */(){();(,.,(),);(,.,(),,);(,,((),));}/**
	 * Normalize parameters for field.
	 *
	 * @param array $field Field parameters.
	 *
	 * @return array
	 */(){[]=;=();;}/**
	 * Get uploaded file information.
	 *
	 * @param int   $file_id Attachment image ID (post ID). Required.
	 * @param array $args Array of arguments (for size).
	 *
	 * @return array|bool False if file not found. Array of image info on success.
	 */(,=()){(!()){;}=();=();=(,());=(,,[],,,,);[]=();=();(!()){(()){(!([])){[][]=[];}}(!([])!([])){[]=([],[],);}{[]=(,,);}}=();(!()){(,,)=(,);[]=(,,);(,,)=(,);[]=(,,);}{=();=;=;[]=(,,);[]=(,,);};}/**
	 * Format a single value for the helper functions.
	 *
	 * @param array $field Field parameters.
	 * @param array $value The value.
	 *
	 * @return string
	 */(,){=(,(,));((,,));}/**
	 * Template for media item.
	 */(){();(.);}}