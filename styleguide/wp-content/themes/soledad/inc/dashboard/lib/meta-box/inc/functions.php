/**
 * Plugin public functions.
 *
 * @package Meta Box
 */(!()){/**
	 * Get post meta.
	 *
	 * @param string   $key     Meta key. Required.
	 * @param array    $args    Array of arguments. Optional.
	 * @param int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed
	 */(,=(),=){=();=(,,);/*
		 * If field is not found, which can caused by registering meta boxes for the backend only or conditional registration.
		 * Then fallback to the old method to retrieve meta (which uses get_post_meta() as the latest fallback).
		 */(){(,(,,));}=([],(,),)?(,,,):(,,);(,,,,);}}(!()){/**
	 * Get field data.
	 *
	 * @param string   $key       Meta key. Required.
	 * @param array    $args      Array of arguments. Optional.
	 * @param int|null $object_id Object ID. null for current post. Optional.
	 *
	 * @return array
	 */(,=(),=){=(,(,));=();/**
		 * Filter meta type from object type and object id.
		 *
		 * @var string     Meta type, default is post type name.
		 * @var string     Object type.
		 * @var string|int Object id.
		 */=(,,[],);()(,,[]);}}(!()){/**
	 * Get post meta.
	 *
	 * @param string   $key     Meta key. Required.
	 * @param array    $args    Array of arguments. Optional.
	 * @param int|null $post_id Post ID. null for current post. Optional.
	 *
	 * @return mixed
	 */(,=(),=){=(,(,,,));=(,[],[],[],);=;([]){::[]=[];;::=;;}=(,);(,,,);}}// End if().
(!()){/**
	 * Get value of custom field.
	 * This is used to replace old version of rwmb_meta key.
	 *
	 * @param  string   $field_id Field ID. Required.
	 * @param  array    $args     Additional arguments. Rarely used. See specific fields for details.
	 * @param  int|null $post_id  Post ID. null for current post. Optional.
	 *
	 * @return mixed false if field doesn't exist. Field value otherwise.
	 */(,=(),=){=();=(,,);// Get field value.
=?(,,,):;/*
		 * Allow developers to change the returned value of field.
		 * For version < 4.8.2, the filter name was 'rwmb_get_field'.
		 *
		 * @param mixed    $value   Field value.
		 * @param array    $field   Field parameters.
		 * @param array    $args    Additional arguments. Rarely used. See specific fields for details.
		 * @param int|null $post_id Post ID. null for current post. Optional.
		 */=(,,,,);;}}(!()){/**
	 * Display the value of a field
	 *
	 * @param  string   $field_id Field ID. Required.
	 * @param  array    $args     Additional arguments. Rarely used. See specific fields for details.
	 * @param  int|null $post_id  Post ID. null for current post. Optional.
	 * @param  bool     $echo     Display field meta value? Default `true` which works in almost all cases. We use `false` for  the [rwmb_meta] shortcode.
	 *
	 * @return string
	 */(,=(),=,=){=();=(,,);(!){;}=(,,,);/*
		 * Allow developers to change the returned value of field.
		 * For version < 4.8.2, the filter name was 'rwmb_get_field'.
		 *
		 * @param mixed    $value   Field HTML output.
		 * @param array    $field   Field parameters.
		 * @param array    $args    Additional arguments. Rarely used. See specific fields for details.
		 * @param int|null $post_id Post ID. null for current post. Optional.
		 */=(,,,,);(){;// WPCS: XSS OK.
};}}// End if().
(!()){/**
	 * Shortcode to display meta value.
	 *
	 * @param array $atts Shortcode attributes, same as rwmb_meta() function, but has more "meta_key" parameter.
	 *
	 * @return string
	 */(){=(,((),));(([])){;}=[];=[];([],[]);(,,,);}(,);}(!()){/**
	 * Get the registry by type.
	 * Always return the same instance of the registry.
	 *
	 * @param string $type Registry type.
	 *
	 * @return object
	 */(){=();=((,),,);=.().;=(,,);(!([])){[]=();}[];}}(!()){/**
	 * Get storage class name.
	 *
	 * @param string $object_type Object type. Use post or term.
	 * @return string
	 */(){=((,),,);=();=(,,);=..;(!()){=;}(,,);}}(!()){/**
	 * Get storage instance.
	 *
	 * @param string      $object_type Object type. Use post or term.
	 * @param RW_Meta_Box $meta_box    Meta box object. Optional.
	 * @return RWMB_Storage_Interface
	 */(,=){=();=()();(,,,);}}(!()){/**
	 * Get meta box object from meta box data.
	 *
	 * @param  array $meta_box Array of meta box data.
	 * @return RW_Meta_Box
	 */(){/**
		 * Allow filter meta box class name.
		 *
		 * @var string Meta box class name.
		 * @var array  Meta box data.
		 */=(,,);();}}