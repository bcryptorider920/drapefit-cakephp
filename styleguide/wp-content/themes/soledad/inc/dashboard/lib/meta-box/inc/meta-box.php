/**
 * A class to rapid develop meta boxes for custom & built in content types
 * Piggybacks on WordPress
 *
 * @author  Tran Ngoc Tuan Anh <rilwis@gmail.com>
 * @license GNU GPL2+
 * @package Meta Box
 *//**
 * The main meta box class.
 *
 * @package Meta Box
 */{/**
	 * Meta box parameters.
	 *
	 * @var array
	 */;/**
	 * Detect whether the meta box is saved at least once.
	 * Used to prevent duplicated calls like revisions, manual hook to wp_insert_post, etc.
	 *
	 * @var bool
	 */=;/**
	 * The object ID.
	 *
	 * @var int
	 */=;/**
	 * The object type.
	 *
	 * @var string
	 */=;/**
	 * Create meta box based on given data.
	 *
	 * @param array $meta_box Meta box definition.
	 */(){=();=;=();(!){;}[]=([],);(()){();();}}/**
	 * Add fields to field registry.
	 */(){=();(){(){(,);}}}/**
	 * Conditional check for whether initializing meta box.
	 *
	 * - 1st filter applies to all meta boxes.
	 * - 2nd filter applies to only current meta box.
	 *
	 * @return bool
	 */(){=(,,);("}",,);}/**
	 * Add global hooks.
	 */(){// Enqueue common styles and scripts.
(,(,));// Add additional actions for fields.
(){(,);}}/**
	 * Specific hooks for meta box object. Default is 'post'.
	 * This should be extended in sub-classes to support meta fields for terms, user, settings pages, etc.
	 */(){// Add meta box.
(,(,));// Hide meta box if it's set 'default_hidden'.
(,(,),,);// Save post meta.
(){(){// Attachment uses other hooks.
// @see wp_update_post(), wp_insert_attachment().
(,(,));(,(,));}{("}",(,));}}}/**
	 * Enqueue common scripts and styles.
	 */(){(()!()){;}(,.,(),);(()){(,.,(),);}// Load clone script conditionally.
(){([]){(,.,(),,);;}}// Enqueue scripts and styles for fields.
(){(,);}// Auto save.
(){(,.,(),,);}/**
		 * Allow developers to enqueue more scripts and styles
		 *
		 * @param RW_Meta_Box $object Meta Box object
		 */(,);}/**
	 * Add meta box for multiple post types
	 */(){(){(,,(,),,,);}}/**
	 * Hide meta box if it's set 'default_hidden'
	 *
	 * @param array  $hidden Array of default hidden meta boxes.
	 * @param object $screen Current screen information.
	 *
	 * @return array
	 */(,){(()){[]=;};}/**
	 * Callback function to show fields in meta box
	 */(){(());=();// Container.
(,(?:),());("}","}");// Allow users to add custom code before meta box content.
// 1st action applies to all meta boxes.
// 2nd action applies to only current meta box.
(,);("}",);(){(,,,);}// Allow users to add custom code after meta box content.
// 1st action applies to all meta boxes.
// 2nd action applies to only current meta box.
(,);("}",);// End container.
;}/**
	 * Save data from meta box
	 *
	 * @param int $post_id Post ID.
	 */(){(!()){;}=;// Make sure meta is added to the post, not a revision.
(){=();(){=;}}// Before save action.
(,);("}",);(,);// After save action.
(,);("}",);}/**
	 * Save fields data.
	 *
	 * @param int   $post_id Post id.
	 * @param array $fields  Fields data.
	 */(,){(){=[]![];=(,,);// @codingStandardsIgnoreLine
=([[]])?[[]]:(?:());// Allow field class change the value.
([]){=(,,,);}{=(,,,,);=(,,);}=(,,,);// Call defined method to save meta value, if there's no methods, call common one.
(,,,,);}}/**
	 * Validate form when submit. Check:
	 * - If this function is called to prevent duplicated calls like revisions, manual hook to wp_insert_post, etc.
	 * - Autosave
	 * - If form is submitted properly
	 *
	 * @return bool
	 */(){=(,"}",);!(!())(