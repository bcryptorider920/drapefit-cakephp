/**
 * Select tree walker for cascading select fields.
 *
 * @package Meta Box
 *//**
 * The select tree walker class.
 */{/**
	 * Field data.
	 *
	 * @var string
	 */;/**
	 * Field meta value.
	 *
	 * @var array
	 */=();/**
	 * Constructor.
	 *
	 * @param array $db_fields Database fields.
	 * @param array $field     Field parameters.
	 * @param mixed $meta      Meta value.
	 */(,,){=(,(,,,));=;=;}/**
	 * Display array of elements hierarchically.
	 *
	 * @param array $options An array of options.
	 *
	 * @return string
	 */(){=[];=();(){=()?:;[][]=;}=([])?:[];(,,);}/**
	 * Display a hierarchy level.
	 *
	 * @param array $options   An array of options.
	 * @param int   $parent_id Parent item ID.
	 * @param bool  $active    Whether to show or hide.
	 *
	 * @return string
	 */(,=,=){=[];=;=(,,);=(,,);=[];=(,?:,,());([])?"[]}":;(,-);;(){(([])){(,,(,));}};;}}