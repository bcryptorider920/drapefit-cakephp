<style>
.three-person {
        float: left;
        width: 100%;
        /* text-align: left; */
        /* border-bottom: 2px solid #232f3e; */
        margin-bottom: 5px;
        text-align: center;
    }
    .three-person ul{
        float: left;
        width:100%;
        border-bottom: 2px solid #232f3e;
    }

    .three-person ul li {
        display: inline-block;
        float: none;
    }
    .three-person ul li:first-child{
        border-left: none;
    }
    .three-person .nav.nav-tabs li a {
        display: inline-block;
        color: #232f3e !important;
        text-transform: uppercase;
        font-family: "Amazon Ember", Arial, sans-serif;
        font-weight: bold;
        font-size: 16px;
        padding: 14px 45px;
        position: relative;
        border: none !important;
    }
    .three-person .nav.nav-tabs li a:hover,.three-person .nav.nav-tabs li.active a{
        color: #ef7600 !important;
        background: none !important;
    }
    .three-person .nav.nav-tabs li a:hover:after,.three-person .nav.nav-tabs li.active a:after
    {
        height: 2px;
        background: #ef7600;
        content: "";
        position: absolute;
        left: 0;
        bottom: -1px;
        width:100%;
    }
	
	@media only screen and (max-width:1199px) {
      .three-person ul li {
        padding: 0 10px;
       }
    }
    @media only screen and (max-width:991px) {
        .three-person ul li {
            padding: 0 15px;
        }
        .stories{
            padding-left: 0;
        }
    }
    @media only screen and (max-width:767px) {
        .three-person .nav.nav-tabs li {
            margin-bottom: 0;
        }
        .three-person .nav.nav-tabs li a {
    width: 100%;
}


</style>
<?php
$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];


?>

<div class="three-person">
        
                <div class="col-sm-12 col-lg-12 col-md-12">
                    <ul class="nav nav-tabs">
                                                    <!--<li class="active" ><a data-toggle="tab" href="#tab8"><i class="fa fa-male"></i> Men</a></li>

                                                        <li ><a data-toggle="tab" href="#tab9"><i class="fa fa-male"></i> women</a></li>

                                                        <li ><a data-toggle="tab" href="#tab10"><i class="fa fa-male"></i> Kids</a></li>

                                                        <li ><a data-toggle="tab" href="#tab11"><i class="fa fa-male"></i> General Information</a></li>-->
                                                        <li <?php if (strpos($url,'/men') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/styleguide/category/men" aria-expanded="true"><i class="fa fa-male"></i> Men</a></li>

<!--                                                         <li <?php if (strpos($url,'/women') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/styleguide/category/women"><i class="fa fa-female"></i> women</a></li> -->
						<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}
	.dropdown a.main {display: inline-block;
color: #232f3e !important;
text-transform: uppercase;
font-family: "Amazon Ember", Arial, sans-serif;
font-weight: bold;
font-size: 16px;
padding: 14px 45px;
position: relative;
border: none !important;text-decoration: none;}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;

}
	.dropdown .active a.main{color: #ef7600 !important;
background: none !important;}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
</style>
</head>
<body>



<div class="dropdown">
	<div <?php if (strpos($url,'/women') !== false){ ?>class="active " <?php } ?>><a class="main" href="<?php get_home_url(); ?>/styleguide/category/women"><i class="fa fa-female"></i>women</a>
  <div class="dropdown-content">
    <a href="<?php get_home_url(); ?>/styleguide/category/womenspring">WomenSpring</a>
    <a href="<?php get_home_url(); ?>/styleguide/category/womenwinter">WomenWinter</a>

  </div>
	</div>
</div>
						

                                                        <li <?php if (strpos($url,'kids') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/styleguide/category/kids"><i class="fa fa-child"></i> Kids</a></li>

                                                        <li <?php if (strpos($url,'general') !== false){ ?>class="active" <?php } ?>><a href="<?php get_home_url(); ?>/styleguide/category/general"><i class="fa fa-copy"></i> General</a></li>

                                                </ul>
                </div>
            
    </div>